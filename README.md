# simple X CMS Docs

## Libraries
[nano-ui Docs](https://github.com/vankizmann/nano-ui)  
[pico-js Docs](https://github.com/vankizmann/pico-js)

### Default Controller  
Will be extended by the most index and edit controllers.

- [SxIndexController](./app/webroot/simplex/src/js/app/controller/default/src/index)  
_Base index controller with usefull props and functions_

- [SxEditController](./app/webroot/simplex/src/js/app/controller/default/src/edit)  
_Base edit controller with usefull props and functions_

### Example Controller  
Default explanation for most index and edit controllers.

- [SxExampleIndex](./app/webroot/simplex/src/js/app/controller/example/src/index)  
_Example index controller for most controllers_

- [SxExampleEdit](./app/webroot/simplex/src/js/app/controller/example/src/edit)  
_Example edit controller for most controllers_

### Nano Components

- [NDataform](./app/webroot/simplex/src/js/nano/pro/dataform/src/dataform)  
_Form with data binding and ajax request_

- [NDatatable](./app/webroot/simplex/src/js/nano/pro/datatable/src/datatable)  
_Form with data binding and ajax request_

- [NDatatree](./app/webroot/simplex/src/js/nano/pro/datatree/src/datatree)  
_Tree with data binding and ajax request_

### Custom Components

- [SxCropper](./app/webroot/simplex/src/js/app/component/cropper/src/cropper) (comming soon)  
_Cropper for media files_

- [SxFocuspoint](./app/webroot/simplex/src/js/app/component/focuspoint/src/focuspoint) (comming soon)  
_Focuspoint for media files_

- [SxCustomfield](./app/webroot/simplex/src/js/app/component/customfield/src/customfield) (comming soon)  
_Custom field inside any component_

### Controller Components  

- [SxAcoTreeNode](./app/webroot/simplex/src/js/app/controller/aco/src/tree-node) (comming soon)  
_Tree node with custom actions like edit, delete etc._

- [SxArticleMarkup](./app/webroot/simplex/src/js/app/controller/article/src/markup) (comming soon)  
_Markup if article isn't linked, translated etc._

- [SxMenuTreeNode](./app/webroot/simplex/src/js/app/controller/menu/src/tree-node) (comming soon)  
_Tree node with custom actions like edit, delete etc._

- [SxAttachmentMarkup](./app/webroot/simplex/src/js/app/controller/attachment/src/markup) (comming soon)  
_Markup if attachment isn't linked, translated etc._

- [SxAttachmentTreeNode](./app/webroot/simplex/src/js/app/controller/attachment/src/tree-node) (comming soon)  
_Tree node with custom actions like edit, delete etc._

- [SxAttachmentDropzone](./app/webroot/simplex/src/js/app/controller/attachment/src/dropzone) (comming soon)  
_Dropzone which is collapseble used in media tab for articles_

- [SxAttachmentAttached](./app/webroot/simplex/src/js/app/controller/attachment/src/attached) (comming soon)  
_Attached item for dropzone_

- [SxAttachmentUpload](./app/webroot/simplex/src/js/app/controller/attachment/src/upload) (comming soon)  
_Drag and drop upload which is used in root instance once_

- [SxFormularField](./app/webroot/simplex/src/js/app/controller/formular/src/field) (comming soon)  
_Form fields as draggable list with option to add more_

- [SxFormularFieldItem](./app/webroot/simplex/src/js/app/controller/formular/src/field-item) (comming soon)  
_Node item of field inside sx-formular-field_

- [SxFormularFieldEdit](./app/webroot/simplex/src/js/app/controller/formular/src/field-edit) (comming soon)  
_Edit form for node item in sx-formular-field_

### Selects

- [SxSelectAco](./app/webroot/simplex/src/js/app/component/select/src/aco)  
_Select input for acos which will be autofilled by index controller_

- [SxSelectRole](./app/webroot/simplex/src/js/app/component/select/src/role)  
_Select input for roles_

- [SxSelectDomain](./app/webroot/simplex/src/js/app/component/select/src/domain)  
_Select input with domain filter option_

- [SxSelectFormular](./app/webroot/simplex/src/js/app/component/select/src/formular)  
_Select input for formulars_

- [SxSelectFormularconfig](./app/webroot/simplex/src/js/app/component/select/src/formularconfig)  
_Select input for formularconfigs_

### Layout

- [SxLayoutRoot](./app/webroot/simplex/src/js/app/layout/src/layout-root)  
_Vue root instance_

- [SxLayoutLogin](./app/webroot/simplex/src/js/app/layout/src/layout-login)  
_Vue root login instance_

- [SxLayoutMenu](./app/webroot/simplex/src/js/app/layout/src/layout-menu)  
_Backend main menu (1 depth)_

- [SxLayoutMain](./app/webroot/simplex/src/js/app/layout/src/layout-main)  
_Main frame with resizable tree an info bar_

- [SxLayoutLocation](./app/webroot/simplex/src/js/app/layout/src/layout-location)  
_Breadcrumbs on under menu_

- [SxLayoutAco](./app/webroot/simplex/src/js/app/layout/src/layout-aco)  
_Aco select dropdown_

- [SxLayoutLocale](./app/webroot/simplex/src/js/app/layout/src/layout-locale)  
_Locale select dropdown_

## Changelog

### 3.0.0
Comming soon
