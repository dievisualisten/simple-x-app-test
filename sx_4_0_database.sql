-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 29. Jun 2021 um 10:33
-- Server-Version: 10.1.30-MariaDB
-- PHP-Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `sx_4_0_clean`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `acos`
--

DROP TABLE IF EXISTS `acos`;
CREATE TABLE `acos` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pathname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachmentfolder` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `has_attachmentfolder` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=4096 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `acos`
--

INSERT INTO `acos` (`id`, `parent_id`, `lft`, `rght`, `level`, `name`, `pathname`, `attachmentfolder`, `has_attachmentfolder`, `created`, `modified`) VALUES
('3735626e-5ae4-11e6-8b77-86f30ca893d3', 'eeeeeeee-eeee-eeee-eeee-eeeeeeeeeeee', 4, 5, 1, 'Gemeinsam genutzter Ordner', 'Gemeinsam genutzter Ordner', 'shared', 1, '2021-01-01 00:00:00', '2021-06-26 00:55:57'),
('aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', 'eeeeeeee-eeee-eeee-eeee-eeeeeeeeeeee', 2, 3, 1, 'Systemadmin (Die Visualisten)', 'Systemadmin (Die Visualisten)', 'system', 1, '2021-01-01 00:00:00', '2021-06-26 00:54:33'),
('eeeeeeee-eeee-eeee-eeee-eeeeeeeeeeee', NULL, 1, 6, 0, 'System', 'System', '', 0, '2021-01-01 00:00:00', '2021-01-01 00:00:00');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `acos_users`
--

DROP TABLE IF EXISTS `acos_users`;
CREATE TABLE `acos_users` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aco_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=4096 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `acos_users`
--

INSERT INTO `acos_users` (`id`, `aco_id`, `user_id`, `created`, `modified`) VALUES
('21a83c2c-fce1-4fb4-b1f9-7b811685dd5c', 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', '11111111-1111-1111-1111-111111111111', '2021-06-29 00:29:21', '2021-06-29 00:29:21'),
('2248704e-fc6b-4627-ab98-99e9049c6f48', 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', 'cccccccc-cccc-cccc-cccc-cccccccccccc', '2021-06-29 00:30:33', '2021-06-29 00:30:33'),
('24690436-80b1-4a1b-b12b-4bf8590a030c', 'eeeeeeee-eeee-eeee-eeee-eeeeeeeeeeee', '11111111-1111-1111-1111-111111111111', '2021-06-29 00:29:21', '2021-06-29 00:29:21'),
('376acb02-672e-4703-83e7-962d014fb5f4', 'eeeeeeee-eeee-eeee-eeee-eeeeeeeeeeee', 'cccccccc-cccc-cccc-cccc-cccccccccccc', '2021-06-29 00:30:33', '2021-06-29 00:30:33'),
('ec613794-d861-11eb-94ee-00e04cb86ff4', '54be0e1d-3fc0-4c82-b250-1f041a25c659', 'cccccccc-cccc-cccc-cccc-cccccccccccc', '2020-09-11 01:02:53', '2020-09-11 01:02:53');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `actionviews`
--

DROP TABLE IF EXISTS `actionviews`;
CREATE TABLE `actionviews` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `menu_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `root_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `controller_action` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `addresses`
--

DROP TABLE IF EXISTS `addresses`;
CREATE TABLE `addresses` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `box` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lon` double DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=2730 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `addresses`
--

INSERT INTO `addresses` (`id`, `foreign_key`, `model`, `type`, `street`, `number`, `box`, `zip`, `city`, `country`, `lat`, `lon`, `created`, `modified`) VALUES
('16571228-ecd3-45c6-8e10-c927d60f3b78', '11111111-1111-1111-1111-111111111111', 'Users', 'Addresses', '', '', '', '', '', 'de', NULL, NULL, '2021-01-01 00:00:00', '2021-06-29 00:29:21'),
('bfedd551-2536-41c8-a7f6-b7ef7646e2cc', 'eeeeeeee-eeee-eeee-eeee-eeeeeeeeeeee', 'Acos', 'Addresses', '', '', '', '', '', 'de', NULL, NULL, '2021-01-01 00:00:00', '2021-01-01 00:00:00'),
('c503939a-966c-4bde-8b89-8055e4246535', '3735626e-5ae4-11e6-8b77-86f30ca893d3', 'Acos', 'Addresses', '', '', '', '', '', 'de', NULL, NULL, '2021-06-26 00:55:57', '2021-06-26 00:55:57'),
('cd797d0a-99f8-490d-9ccd-df96c89c0eb0', 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', 'Acos', 'Addresses', '', '', '', '', '', 'de', NULL, NULL, '2021-01-01 00:00:00', '2021-06-26 00:54:33'),
('e63f86b9-ddd3-4149-9367-f32614926a61', 'cccccccc-cccc-cccc-cccc-cccccccccccc', 'Users', 'Addresses', '', '', '', '', '', 'de', NULL, NULL, '2021-01-01 00:00:00', '2021-06-29 00:30:33');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `articles`
--

DROP TABLE IF EXISTS `articles`;
CREATE TABLE `articles` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `creator_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aco_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `formularconfig_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `layout` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `has_teasertext` tinyint(1) DEFAULT '0',
  `teasertext` longtext COLLATE utf8mb4_unicode_ci,
  `has_teasertext2` tinyint(1) DEFAULT '0',
  `teasertext2` longtext COLLATE utf8mb4_unicode_ci,
  `moretext` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `headline` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `topline` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subline` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `content2` longtext COLLATE utf8mb4_unicode_ci,
  `content3` longtext COLLATE utf8mb4_unicode_ci,
  `address` text COLLATE utf8mb4_unicode_ci,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extended_data` longtext COLLATE utf8mb4_unicode_ci,
  `extended_data_cfg` longtext COLLATE utf8mb4_unicode_ci,
  `pagetitle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `canonical` longtext COLLATE utf8mb4_unicode_ci,
  `noindex` tinyint(1) DEFAULT '0',
  `element` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) DEFAULT '1',
  `begin_publishing` datetime DEFAULT NULL,
  `end_publishing` datetime DEFAULT NULL,
  `component` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=780 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `articles`
--

INSERT INTO `articles` (`id`, `user_id`, `creator_id`, `aco_id`, `formularconfig_id`, `parent_id`, `identifier`, `type`, `layout`, `has_teasertext`, `teasertext`, `has_teasertext2`, `teasertext2`, `moretext`, `title`, `headline`, `topline`, `subline`, `content`, `content2`, `content3`, `address`, `slug`, `lat`, `lon`, `extended_data`, `extended_data_cfg`, `pagetitle`, `description`, `canonical`, `noindex`, `element`, `active`, `begin_publishing`, `end_publishing`, `component`, `created`, `modified`) VALUES
('1c0e98ab-50b4-47e0-bfee-461e6ee51007', NULL, NULL, 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', NULL, NULL, '', 'page', 'default', 0, '', 0, '', '', 'Multipage', '', '', '', '<h2>Headline 2</h2><p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. <strong>Lorem ipsum dolor&nbsp;</strong>sit amet, <em>consetetur sadipscing elitr,&nbsp;</em>sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd <s>gubergren</s>, no sea takimata sanctus est Lorem ipsum dolor sit <sup>amet</sup>.</p><ul><li>Lorem</li><li>ipsum</li><li>dolor&nbsp;</li><li>sit</li><li>amet</li></ul><hr><h3>Headline 3</h3><p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit <sub>amet</sub>.</p><table><thead><tr><th>Titel 1</th><th>Titel 2</th><th>Titel 3</th></tr></thead><tbody><tr><td>A1</td><td>A2</td><td>A3</td></tr><tr><td>B1</td><td>B2</td><td>B3</td></tr><tr><td>C1</td><td>C2</td><td>C3</td></tr></tbody></table><h4>Headline 4</h4><p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p><h5>Headline 5</h5><p>psum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p><h5>Headline 6</h5><p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p><p><br></p>', '', NULL, '', 'multipage', '0', '0', '{\"menu\":{\"url\":\"\"},\"title\":{\"foo\":null}}', '{\"article\":{\"nofollow\":null},\"menu\":{\"blank\":false},\"price\":{\"min\":null,\"max\":0,\"prefix\":null,\"suffix\":null},\"custom\":{\"openFirstFaqItem\":false,\"textleft\":false,\"element\":null},\"layout\":{\"openFirstFaqItem\":false,\"textleft\":false}}', '', '', '', 0, 0, 1, NULL, NULL, '', '2021-06-26 22:31:48', '2021-06-27 21:37:45'),
('2218f099-3715-4542-a87c-79c3a016350f', NULL, NULL, 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', NULL, NULL, '', 'page', 'default', 0, '', 0, '', '', 'Datenschutz', '', '', '', '', '', '', '', 'datenschutz', '0', '0', '{\"menu\":{\"url\":\"\"}}', '{\"article\":{\"nofollow\":null},\"menu\":{\"blank\":false},\"price\":{\"min\":null,\"max\":null,\"prefix\":null,\"suffix\":null},\"layout\":{\"openFirstFaqItem\":false,\"textleft\":false},\"custom\":{\"element\":null}}', '', '', '', 0, 0, 1, NULL, NULL, '', '2021-06-29 00:24:48', '2021-06-29 00:24:48'),
('52e11a50-5bbe-11eb-9735-8ba84fe9803a', NULL, NULL, 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', NULL, NULL, NULL, 'frontpage', 'default', 0, '', 0, '', '', 'Startseite', '', '', '', '', '', NULL, '', 'startseite', '0', '0', '{\"article\":{\"pagetitle\":null,\"nofollow\":null},\"title\":{\"foo\":null},\"menu\":{\"url\":\"\"}}', '{\"blog\":{\"category\":[]},\"price\":{\"min\":null,\"max\":null,\"prefix\":null,\"suffix\":null},\"custom\":{\"icon\":null,\"openFirstFaqItem\":false,\"textleft\":false,\"element\":null},\"domain_id\":\"4e69e939-11b4-45d8-a767-0ff41a25c659\",\"menu\":{\"blank\":false}}', '', '', '', 0, 0, 1, NULL, NULL, '', '2021-01-21 08:57:50', '2021-05-06 21:07:18'),
('56144d85-515b-4d38-af51-fe69ef38aba9', NULL, NULL, 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', NULL, NULL, '', 'faq', 'default', 0, '', 0, '', '', 'test', '', '', '', '', '', NULL, '', '', '', '', '[]', '[]', '', '', '', 0, 1, 1, NULL, NULL, '', '2021-06-26 22:56:58', '2021-06-26 22:56:58'),
('6298de48-5606-4fe8-9dba-ce0b7e9f3255', NULL, NULL, 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', NULL, NULL, '', 'page', 'default', 0, '', 0, '', '', 'Abschnitt mit Galerie & Downloads', '', '', '', '', '', NULL, '', 'abschnitt-mit-galerie-downloads', '0', '0', '{\"menu\":{\"url\":\"\"}}', '{\"article\":{\"nofollow\":null},\"menu\":{\"blank\":false},\"price\":{\"min\":null,\"max\":null,\"prefix\":null,\"suffix\":null},\"layout\":{\"openFirstFaqItem\":false,\"textleft\":false},\"custom\":{\"element\":null}}', '', '', '', 0, 0, 1, NULL, NULL, '', '2021-06-27 00:38:45', '2021-06-27 00:52:39'),
('79724c82-9e33-4bc5-913c-efe125d85664', NULL, NULL, 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', NULL, NULL, '', 'link', 'default', 0, '', 0, '', '', 'Externer Link', '', '', '', '', '', NULL, '', 'testlink', '0', '0', '{\"menu\":{\"url\":\"https:\\/\\/www.google.de\"}}', '{\"article\":{\"nofollow\":null},\"menu\":{\"blank\":true},\"price\":{\"min\":null,\"max\":null,\"prefix\":null,\"suffix\":null},\"layout\":{\"openFirstFaqItem\":false,\"textleft\":false},\"custom\":{\"element\":null}}', '', '', '', 0, 0, 1, NULL, NULL, '', '2021-06-26 23:41:02', '2021-06-29 00:25:35'),
('9fdb7903-e50d-4f81-8cdc-a69d1b97c9ac', NULL, NULL, 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', NULL, NULL, '', 'page', 'default', 0, '', 0, '', '', 'Impressum', '', '', '', '', '', '', '', 'impressum', '0', '0', '{\"menu\":{\"url\":\"\"}}', '{\"article\":{\"nofollow\":null},\"menu\":{\"blank\":false},\"price\":{\"min\":null,\"max\":null,\"prefix\":null,\"suffix\":null},\"layout\":{\"openFirstFaqItem\":false,\"textleft\":false},\"custom\":{\"element\":null}}', '', '', '', 0, 0, 1, NULL, NULL, '', '2021-06-29 00:25:03', '2021-06-29 00:25:03'),
('dfbaf39a-9550-40ab-a073-ef2ac921c294', NULL, NULL, 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', NULL, NULL, '', 'event', 'default', 0, '', 0, '', '', 'Serienevent', '', '', '', '', '', NULL, '', 'serienevent', '0', '0', '{\"menu\":{\"url\":\"\"}}', '{\"article\":{\"nofollow\":null},\"menu\":{\"blank\":false},\"price\":{\"min\":null,\"max\":null,\"prefix\":null,\"suffix\":null},\"layout\":{\"openFirstFaqItem\":false,\"textleft\":false},\"custom\":{\"element\":null},\"event\":{\"eventlabel\":null,\"start_date\":\"2021-06-01 00:13:18\",\"start_time\":null,\"end_date\":null,\"end_time\":null,\"multipleoccation\":true,\"intervallimit\":\"200\",\"weeklyoccation\":true,\"weeklydays\":[\"mo\"],\"weeklyinterval\":1,\"monthlyoccation\":false,\"monthlydate\":\"-1\",\"monthlyday\":[],\"monthlyinterval\":1},\"_events\":[\"2021-06-07\",\"2021-06-14\",\"2021-06-21\",\"2021-06-28\",\"2021-07-05\",\"2021-07-12\",\"2021-07-19\",\"2021-07-26\",\"2021-08-02\",\"2021-08-09\",\"2021-08-16\",\"2021-08-23\",\"2021-08-30\",\"2021-09-06\",\"2021-09-13\",\"2021-09-20\",\"2021-09-27\",\"2021-10-04\",\"2021-10-11\",\"2021-10-18\",\"2021-10-25\",\"2021-11-01\",\"2021-11-08\",\"2021-11-15\",\"2021-11-22\",\"2021-11-29\",\"2021-12-06\",\"2021-12-13\",\"2021-12-20\",\"2021-12-27\",\"2022-01-03\",\"2022-01-10\",\"2022-01-17\",\"2022-01-24\",\"2022-01-31\",\"2022-02-07\",\"2022-02-14\",\"2022-02-21\",\"2022-02-28\",\"2022-03-07\",\"2022-03-14\",\"2022-03-21\",\"2022-03-28\",\"2022-04-04\",\"2022-04-11\",\"2022-04-18\",\"2022-04-25\",\"2022-05-02\",\"2022-05-09\",\"2022-05-16\",\"2022-05-23\",\"2022-05-30\",\"2022-06-06\",\"2022-06-13\",\"2022-06-20\",\"2022-06-27\",\"2022-07-04\",\"2022-07-11\",\"2022-07-18\",\"2022-07-25\",\"2022-08-01\",\"2022-08-08\",\"2022-08-15\",\"2022-08-22\",\"2022-08-29\",\"2022-09-05\",\"2022-09-12\",\"2022-09-19\",\"2022-09-26\",\"2022-10-03\",\"2022-10-10\",\"2022-10-17\",\"2022-10-24\",\"2022-10-31\",\"2022-11-07\",\"2022-11-14\",\"2022-11-21\",\"2022-11-28\",\"2022-12-05\",\"2022-12-12\",\"2022-12-19\",\"2022-12-26\",\"2023-01-02\",\"2023-01-09\",\"2023-01-16\",\"2023-01-23\",\"2023-01-30\",\"2023-02-06\",\"2023-02-13\",\"2023-02-20\",\"2023-02-27\",\"2023-03-06\",\"2023-03-13\",\"2023-03-20\",\"2023-03-27\",\"2023-04-03\",\"2023-04-10\",\"2023-04-17\",\"2023-04-24\",\"2023-05-01\",\"2023-05-08\",\"2023-05-15\",\"2023-05-22\",\"2023-05-29\",\"2023-06-05\",\"2023-06-12\",\"2023-06-19\",\"2023-06-26\",\"2023-07-03\",\"2023-07-10\",\"2023-07-17\",\"2023-07-24\",\"2023-07-31\",\"2023-08-07\",\"2023-08-14\",\"2023-08-21\",\"2023-08-28\",\"2023-09-04\",\"2023-09-11\",\"2023-09-18\",\"2023-09-25\",\"2023-10-02\",\"2023-10-09\",\"2023-10-16\",\"2023-10-23\",\"2023-10-30\",\"2023-11-06\",\"2023-11-13\",\"2023-11-20\",\"2023-11-27\",\"2023-12-04\",\"2023-12-11\",\"2023-12-18\",\"2023-12-25\",\"2024-01-01\",\"2024-01-08\",\"2024-01-15\",\"2024-01-22\",\"2024-01-29\",\"2024-02-05\",\"2024-02-12\",\"2024-02-19\",\"2024-02-26\",\"2024-03-04\",\"2024-03-11\",\"2024-03-18\",\"2024-03-25\",\"2024-04-01\",\"2024-04-08\",\"2024-04-15\",\"2024-04-22\",\"2024-04-29\",\"2024-05-06\",\"2024-05-13\",\"2024-05-20\",\"2024-05-27\",\"2024-06-03\",\"2024-06-10\",\"2024-06-17\",\"2024-06-24\",\"2024-07-01\",\"2024-07-08\",\"2024-07-15\",\"2024-07-22\",\"2024-07-29\",\"2024-08-05\",\"2024-08-12\",\"2024-08-19\",\"2024-08-26\",\"2024-09-02\",\"2024-09-09\",\"2024-09-16\",\"2024-09-23\",\"2024-09-30\",\"2024-10-07\",\"2024-10-14\",\"2024-10-21\",\"2024-10-28\",\"2024-11-04\",\"2024-11-11\",\"2024-11-18\",\"2024-11-25\",\"2024-12-02\",\"2024-12-09\",\"2024-12-16\",\"2024-12-23\",\"2024-12-30\",\"2025-01-06\",\"2025-01-13\",\"2025-01-20\",\"2025-01-27\",\"2025-02-03\",\"2025-02-10\",\"2025-02-17\",\"2025-02-24\",\"2025-03-03\",\"2025-03-10\",\"2025-03-17\",\"2025-03-24\",\"2025-03-31\"]}', '', '', '', 0, 0, 1, NULL, NULL, '', '2021-06-27 00:14:27', '2021-06-27 00:27:42'),
('fd31e22d-3e99-444b-8d6d-7f3f56a3a727', NULL, NULL, 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', NULL, NULL, '', 'calendar', 'default', 0, '', 0, '', '', 'Kalender', '', '', '', '', '', NULL, '', 'kalender', '0', '0', '{\"menu\":{\"url\":\"\"}}', '{\"article\":{\"nofollow\":null},\"menu\":{\"blank\":false},\"price\":{\"min\":null,\"max\":null,\"prefix\":null,\"suffix\":null},\"layout\":{\"openFirstFaqItem\":false,\"textleft\":false},\"custom\":{\"element\":null}}', '', '', '', 0, 0, 1, NULL, NULL, '', '2021-06-26 23:57:43', '2021-06-27 21:35:49');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `articles_elements`
--

DROP TABLE IF EXISTS `articles_elements`;
CREATE TABLE `articles_elements` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `article_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `element_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sequence` int(11) NOT NULL,
  `draft` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=4096 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `articles_elements`
--

INSERT INTO `articles_elements` (`id`, `article_id`, `element_id`, `sequence`, `draft`, `created`, `modified`) VALUES
('3f1eeedd-4f0f-4baf-addd-60fbb2386edb', '1c0e98ab-50b4-47e0-bfee-461e6ee51007', '56144d85-515b-4d38-af51-fe69ef38aba9', 0, 0, '2021-06-26 22:56:58', '2021-06-26 22:56:58');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `articles_translations`
--

DROP TABLE IF EXISTS `articles_translations`;
CREATE TABLE `articles_translations` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `locale` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `teasertext` longtext COLLATE utf8mb4_unicode_ci,
  `teasertext2` longtext COLLATE utf8mb4_unicode_ci,
  `moretext` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `content2` longtext COLLATE utf8mb4_unicode_ci,
  `headline` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subline` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `topline` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `canonical` longtext COLLATE utf8mb4_unicode_ci,
  `extended_data` longtext COLLATE utf8mb4_unicode_ci,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=2048 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `attached`
--

DROP TABLE IF EXISTS `attached`;
CREATE TABLE `attached` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attachment_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sequence` int(11) NOT NULL DEFAULT '0',
  `model` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alternative` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=8192 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `attached`
--

INSERT INTO `attached` (`id`, `attachment_id`, `foreign_key`, `sequence`, `model`, `type`, `alternative`, `title`, `description`, `created`, `modified`) VALUES
('2945cc48-8fe6-4f9b-8b1e-1f6503963158', 'ac8af6d4-c21a-4a30-994d-90975a392ce4', '6298de48-5606-4fe8-9dba-ce0b7e9f3255', 0, 'Article', 'download', '', '', '', '2021-06-27 00:38:45', '2021-06-27 00:52:39'),
('2e190de8-981f-416c-a7a0-89293430d774', 'e0472812-7c84-4274-a57d-3c41867430ab', '6298de48-5606-4fe8-9dba-ce0b7e9f3255', 1, 'Article', 'download', '', '', '', '2021-06-27 00:38:45', '2021-06-27 00:52:39'),
('35676334-7175-4a27-9506-82a02ed889ff', 'ac8af6d4-c21a-4a30-994d-90975a392ce4', '6298de48-5606-4fe8-9dba-ce0b7e9f3255', 0, 'Article', 'titleimg', '', '', '', '2021-06-27 00:38:45', '2021-06-27 00:52:39'),
('3fe42724-a5d2-484d-b0bc-271d1b12b5d6', 'ac8af6d4-c21a-4a30-994d-90975a392ce4', '1c0e98ab-50b4-47e0-bfee-461e6ee51007', 0, 'Article', 'titleimg', '', '', '', '2021-06-27 00:18:26', '2021-06-27 21:37:46'),
('43127825-6ba6-49c0-9912-ec3d0d1ac3e3', 'e0472812-7c84-4274-a57d-3c41867430ab', '6298de48-5606-4fe8-9dba-ce0b7e9f3255', 1, 'Article', 'titleimg', '', '', '', '2021-06-27 00:38:45', '2021-06-27 00:52:39'),
('7301b003-3f7a-4820-9803-bb3b7ca35b59', 'ac8af6d4-c21a-4a30-994d-90975a392ce4', '79724c82-9e33-4bc5-913c-efe125d85664', 0, 'Article', 'teaser', '', '', '', '2021-06-27 00:34:18', '2021-06-29 00:25:35'),
('7beebbcb-61dd-4849-839f-5442654120e3', 'e0472812-7c84-4274-a57d-3c41867430ab', 'fd31e22d-3e99-444b-8d6d-7f3f56a3a727', 0, 'Article', 'titleimg', '', '', '', '2021-06-27 21:35:49', '2021-06-27 21:35:49'),
('7d6f56f8-e76a-478d-936b-2adc334cd243', 'e0472812-7c84-4274-a57d-3c41867430ab', '1c0e98ab-50b4-47e0-bfee-461e6ee51007', 1, 'Article', 'titleimg', '', '', '', '2021-06-27 09:10:49', '2021-06-27 21:37:46'),
('b354015a-c457-402f-bc88-89df037e7941', 'e0472812-7c84-4274-a57d-3c41867430ab', '6298de48-5606-4fe8-9dba-ce0b7e9f3255', 1, 'Article', 'teaser', '', '', '', '2021-06-27 00:38:45', '2021-06-27 00:52:39'),
('caecde14-aeac-45d3-ba8c-7dc1326e5a71', 'ac8af6d4-c21a-4a30-994d-90975a392ce4', '6298de48-5606-4fe8-9dba-ce0b7e9f3255', 1, 'Article', 'galerie', '', '', '', '2021-06-27 00:52:39', '2021-06-27 00:52:39'),
('ec985d9a-41e5-4fd8-9ec6-23f6080cc418', 'ac8af6d4-c21a-4a30-994d-90975a392ce4', '6298de48-5606-4fe8-9dba-ce0b7e9f3255', 0, 'Article', 'teaser', '', '', '', '2021-06-27 00:38:45', '2021-06-27 00:52:39'),
('f3b2f640-47eb-4a0a-b379-2270886af8b6', 'e0472812-7c84-4274-a57d-3c41867430ab', '6298de48-5606-4fe8-9dba-ce0b7e9f3255', 0, 'Article', 'galerie', '', '', '', '2021-06-27 00:52:39', '2021-06-27 00:52:39');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `attached_translations`
--

DROP TABLE IF EXISTS `attached_translations`;
CREATE TABLE `attached_translations` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `locale` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alternative` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `attachments`
--

DROP TABLE IF EXISTS `attachments`;
CREATE TABLE `attachments` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aco_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foreign_key` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumb_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rght` int(11) NOT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dirname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `basename` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_original` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumb` text COLLATE utf8mb4_unicode_ci,
  `alternative` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `copyright` text COLLATE utf8mb4_unicode_ci,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extended_data` longtext COLLATE utf8mb4_unicode_ci,
  `extended_data_cfg` longtext COLLATE utf8mb4_unicode_ci,
  `instructions` longtext COLLATE utf8mb4_unicode_ci,
  `identifier` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metadata` longtext COLLATE utf8mb4_unicode_ci,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_leaf` tinyint(1) DEFAULT '1',
  `is_croped` tinyint(1) DEFAULT '0',
  `deletable` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1365 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `attachments`
--

INSERT INTO `attachments` (`id`, `aco_id`, `foreign_key`, `parent_id`, `thumb_id`, `user_id`, `lft`, `rght`, `model`, `type`, `dirname`, `basename`, `name`, `name_original`, `thumb`, `alternative`, `copyright`, `description`, `title`, `extended_data`, `extended_data_cfg`, `instructions`, `identifier`, `metadata`, `provider`, `is_leaf`, `is_croped`, `deletable`, `created`, `modified`) VALUES
('11111111-1111-1111-1111-111111111111', '3735626e-5ae4-11e6-8b77-86f30ca893d3', NULL, NULL, '', NULL, 1, 2, NULL, 'folder', NULL, NULL, '-- Alle Dateien', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2021-01-01 00:00:00', '2021-01-01 00:00:00'),
('18c5dbb4-6fff-4b11-97f2-d4097c9fb35a', '3735626e-5ae4-11e6-8b77-86f30ca893d3', NULL, '2abb79e9-fd59-439d-acfb-066b1a8a3e04', '', '11111111-1111-1111-1111-111111111111', 4, 5, NULL, 'folder', 'shared', NULL, 'Bilder', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2021-01-01 00:00:00', '2021-01-01 00:00:00'),
('2abb79e9-fd59-439d-acfb-066b1a8a3e04', '3735626e-5ae4-11e6-8b77-86f30ca893d3', NULL, NULL, '', '11111111-1111-1111-1111-111111111111', 3, 10, NULL, 'folder', 'shared', NULL, 'Gemeinsam genutzter Ordner', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2021-01-01 00:00:00', '2021-01-01 00:00:00'),
('3fe2c244-59ff-45d3-87e6-e8c636b03772', 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', NULL, NULL, '', '11111111-1111-1111-1111-111111111111', 11, 22, NULL, 'folder', 'system', NULL, 'Systemadmin (Die Visualisten)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2021-01-01 00:00:00', '2021-01-01 00:00:00'),
('7100359e-a3c2-4c55-8d0f-ba683cf72ef4', 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', NULL, '3fe2c244-59ff-45d3-87e6-e8c636b03772', '', '11111111-1111-1111-1111-111111111111', 12, 17, NULL, 'folder', 'system', NULL, 'Bilder', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2021-01-01 00:00:00', '2021-06-26 00:54:33'),
('80c5525a-9403-4fd1-84b1-48458559dbf0', 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', NULL, '3fe2c244-59ff-45d3-87e6-e8c636b03772', '', '11111111-1111-1111-1111-111111111111', 20, 21, NULL, 'folder', 'system', NULL, 'Videos', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2021-01-01 00:00:00', '2021-06-26 00:54:33'),
('92213ee3-fa21-4bfb-83d0-c09ee5cff671', '3735626e-5ae4-11e6-8b77-86f30ca893d3', NULL, '2abb79e9-fd59-439d-acfb-066b1a8a3e04', '', '11111111-1111-1111-1111-111111111111', 6, 7, NULL, 'folder', 'shared', NULL, 'Dokumente', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2021-01-01 00:00:00', '2021-01-01 00:00:00'),
('9b672d57-85bb-4908-a51a-31769a71e447', '3735626e-5ae4-11e6-8b77-86f30ca893d3', NULL, '2abb79e9-fd59-439d-acfb-066b1a8a3e04', '', '11111111-1111-1111-1111-111111111111', 8, 9, NULL, 'folder', 'shared', NULL, 'Videos', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2021-01-01 00:00:00', '2021-01-01 00:00:00'),
('a544c520-888c-482f-9dd4-b1f0058227b8', 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', NULL, '3fe2c244-59ff-45d3-87e6-e8c636b03772', '', '11111111-1111-1111-1111-111111111111', 18, 19, NULL, 'folder', 'system', NULL, 'Dokumente', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2021-01-01 00:00:00', '2021-06-26 00:54:33'),
('ac8af6d4-c21a-4a30-994d-90975a392ce4', 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', NULL, '7100359e-a3c2-4c55-8d0f-ba683cf72ef4', NULL, NULL, 13, 14, NULL, 'image', 'system', 'lavendelfeld.jpg', 'Lavendelfeld', 'f5f4841c-a953-4b6b-9b99-a76c510ce957_6.jpg', '/media/system/crop/lavendelfeld.jpg', 'Lavendelfeld', 'Radius Images/Corbis', NULL, 'Lavendelfeld', NULL, NULL, NULL, NULL, '{\"FileSize\":\"1875879\",\"Extension\":\"jpg\",\"MimeType\":\"image\\/jpeg\",\"height\":\"1200\",\"width\":\"1920\",\"copyright\":\"Radius Images\\/Corbis\",\"Orientation\":\"1\",\"horizontalResolution\":\"72\",\"verticalResolution\":\"72\",\"software\":\"Adobe Photoshop CC 2014 (Windows)\",\"author\":\"Radius Images\\/Corbis\",\"title\":\"English Lavender field with tree at sunset, Valensole, Alpes-de-\",\"ColorSpace\":\"65535\",\"caption\":\"English Lavender field with tree at sunset, Valensole, Alpes-de-Haute-Provence, France\"}', NULL, 1, 0, 1, '2021-06-27 00:18:16', '2021-06-29 00:23:26'),
('e0472812-7c84-4274-a57d-3c41867430ab', 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', NULL, '7100359e-a3c2-4c55-8d0f-ba683cf72ef4', NULL, NULL, 15, 16, NULL, 'image', 'system', '13-fotolia.jpg', '13 - Fotolia', '13- Fotolia.jpg', '/media/system/crop/13-fotolia.jpg', 'Alt-Text 13 - Fotolia', '', 'Mehr Meer', '13 - Fotolia', NULL, NULL, NULL, NULL, '{\"FileSize\":\"2976640\",\"Extension\":\"jpg\",\"MimeType\":\"image\\/jpeg\",\"height\":\"1315\",\"width\":\"1980\",\"aperture\":\"f\\/6.3\",\"camera\":\"NIKON D90\",\"Orientation\":\"1\",\"horizontalResolution\":\"72\",\"verticalResolution\":\"72\",\"software\":\"Adobe Photoshop CC 2014 (Windows)\",\"exposure\":\"1\\/40\",\"iso\":\"200\",\"creationdate\":{\"date\":\"2015-05-28 20:47:26.000000\",\"timezone_type\":\"3\",\"timezone\":\"Europe\\/Berlin\"},\"focalLength\":\"18\",\"ColorSpace\":\"65535\"}', NULL, 1, 0, 1, '2021-06-27 00:38:22', '2021-06-29 00:23:03');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `attachments_translations`
--

DROP TABLE IF EXISTS `attachments_translations`;
CREATE TABLE `attachments_translations` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `locale` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alternative` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bankaccounts`
--

DROP TABLE IF EXISTS `bankaccounts`;
CREATE TABLE `bankaccounts` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `holder` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bankname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iban` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=2048 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `bankaccounts`
--

INSERT INTO `bankaccounts` (`id`, `foreign_key`, `model`, `type`, `holder`, `bankname`, `iban`, `bic`, `created`, `modified`) VALUES
('c62eb543-1cc0-4718-ba99-9f10f202175e', 'cccccccc-cccc-cccc-cccc-cccccccccccc', 'Users', 'Bankaccounts', '', '', '', '', '2021-01-01 00:00:00', '2021-06-29 00:30:33'),
('d602ad63-aea3-4ed9-9afc-b0303a2a0d52', '11111111-1111-1111-1111-111111111111', 'Users', 'Bankaccounts', '', '', '', '', '2021-01-01 00:00:00', '2021-06-29 00:29:21');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `categorized`
--

DROP TABLE IF EXISTS `categorized`;
CREATE TABLE `categorized` (
  `id` int(11) NOT NULL,
  `category_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `communications`
--

DROP TABLE IF EXISTS `communications`;
CREATE TABLE `communications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cell` varchar(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cell2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=2730 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `communications`
--

INSERT INTO `communications` (`id`, `foreign_key`, `type`, `model`, `email`, `email2`, `website`, `phone`, `phone2`, `cell`, `cell2`, `fax`, `created`, `modified`) VALUES
('59346f2b-13fa-45f9-8c07-0831e29898d9', 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', 'Communications', 'Acos', '', NULL, '', '', NULL, '', NULL, '', '2016-06-06 06:50:56', '2021-06-26 00:54:33'),
('5ac4c252-2601-47bd-a663-903b9c2ff87b', '11111111-1111-1111-1111-111111111111', 'Communications', 'Users', '', NULL, '', '', NULL, '', NULL, '', '2016-07-21 04:35:38', '2021-06-29 00:29:21'),
('7580ff22-e726-4f73-879d-b8c8383a21f2', 'cccccccc-cccc-cccc-cccc-cccccccccccc', 'Communications', 'Users', '', NULL, '', '', NULL, '', NULL, '', '2016-08-04 11:03:13', '2021-06-29 00:30:33'),
('af48fe8b-194d-4f17-bd78-10773e407060', 'eeeeeeee-eeee-eeee-eeee-eeeeeeeeeeee', 'Communications', 'Acos', '', NULL, '', '', NULL, '', NULL, '', '2016-07-21 07:14:43', '2016-07-21 07:14:43'),
('b0617f22-31a7-4ee8-907f-bdcee0228f31', '3735626e-5ae4-11e6-8b77-86f30ca893d3', 'Communications', 'Acos', '', '', '', '', '', '', '', '', '2021-06-26 00:55:57', '2021-06-26 00:55:57');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `companies`
--

DROP TABLE IF EXISTS `companies`;
CREATE TABLE `companies` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vatid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `taxid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `regnr` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `court` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` mediumtext COLLATE utf8mb4_unicode_ci,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=5461 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `companies`
--

INSERT INTO `companies` (`id`, `foreign_key`, `type`, `model`, `company`, `vatid`, `taxid`, `regnr`, `court`, `note`, `created`, `modified`) VALUES
('52f26cae-4fcc-4181-8bd7-24d01a25c659', 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', 'Companies', 'Acos', 'Die Visualisten GmbH', '', '', '', '', '', '2014-01-01 00:00:00', '2021-06-26 00:54:33'),
('8ba81e13-d8d9-4402-b793-49c06b760dbe', '3735626e-5ae4-11e6-8b77-86f30ca893d3', 'Companies', 'Acos', '', '', '', '', '', '', '2016-11-01 08:33:00', '2021-06-26 00:55:57'),
('f4cd9aca-41db-43cc-b0c9-e4dd2312d8e3', 'eeeeeeee-eeee-eeee-eeee-eeeeeeeeeeee', 'Companies', 'Acos', '', '', '', '', '', '', '2016-07-21 07:14:43', '2016-07-21 07:14:43');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `configurations`
--

DROP TABLE IF EXISTS `configurations`;
CREATE TABLE `configurations` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `default_value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `is_resource` tinyint(1) DEFAULT '0',
  `is_default` tinyint(1) DEFAULT '0',
  `is_pageconfiguration` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=236 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `configurations`
--

INSERT INTO `configurations` (`id`, `name`, `value`, `default_value`, `description`, `is_resource`, `is_default`, `is_pageconfiguration`, `created`, `modified`) VALUES
('0074388a-57c8-428f-b513-1b6a0f9130e3', 'resource.modules.article.pagetypes.event.default.panels.media.img3.show', '0', '0', '', 0, 1, 1, '2021-01-15 22:09:24', '2021-01-15 22:11:21'),
('010d0ca7-bd07-4ede-ba62-c96f4b8c30ea', 'resource.modules.article.pagetypes.faq.default.panels.map.show', '0', '0', '', 0, 1, 1, '2013-02-14 19:52:44', '2021-06-27 21:01:03'),
('015fb130-2e9a-4147-9a78-2d78503b6289', 'resource.modules.article.pagetypes.event.default.link.show', '0', '0', '', 0, 1, 1, '2016-08-30 06:15:16', '2016-08-30 06:18:06'),
('0289c806-9c72-4c6d-9c5c-acb09e140717', 'resource.modules.article.pagetypes.calendar.icon', 'fa fa-calendar', 'fa fa-calendar', '', 0, 1, 1, '2021-06-27 15:14:54', '2021-06-27 15:17:57'),
('03388567-f641-4677-8069-92edef8f1c2e', 'resource.modules.article.pagetypes.link.default.panels.media.galerie.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2021-01-15 22:38:40'),
('040dff74-fe0a-4b49-9095-f3b2a3a26c35', 'resource.modules.menu.teaser.teaser512.order', '1000', '1000', NULL, 0, 1, 0, '2018-10-10 00:37:27', '2018-10-10 00:37:27'),
('0517fa30-575b-11eb-9f7c-ef95f066447e', 'resource.modules.article.panels.media.tabs.img3.order', '500', '500', '', 0, 1, 0, '2021-01-15 18:56:51', '2021-01-15 18:56:51'),
('054397f8-8939-482b-ba41-a09aa59ad52f', 'resource.modules.article.pagetypes.faq.name', 'Faq', 'Faq', '', 0, 1, 1, '2016-08-26 03:59:28', '2021-06-26 22:53:56'),
('0636e36f-0588-496e-a7f5-ba3488347010', 'resource.modules.article.pagetypes.page.element.content3.show', '0', '0', '', 0, 1, 1, '2021-06-26 22:39:25', '2021-06-26 22:39:25'),
('0648e2a6-a68c-4bf9-aeee-4a9047ed858e', 'resource.modules.article.pagetypes.calendar.default.has_teasertext.show', '1', '1', '', 0, 1, 1, '2016-08-30 06:15:41', '2016-08-30 06:15:41'),
('064c261e-40a7-4328-95c7-c4ed9da39ef9', 'resource.modules.article.pagetypes.direction.default.panels.menu.teaser1024.show', '1', '1', '', 0, 1, 1, '2018-10-10 00:26:46', '2021-01-15 22:40:44'),
('07b23fba-2978-4b03-99aa-2e897f42ef1e', 'resource.modules.article.pagetypes.link.default.panels.map.show', '0', '0', '', 0, 1, 1, '2013-02-14 19:52:44', '2021-06-27 21:00:48'),
('07d6c62f-2c1c-4103-b7f4-b3e8604bed11', 'resource.modules.article.pagetypes.calendar.default.panels.media.download.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:12', '2014-02-20 09:43:48'),
('0927c469-a2fe-4a94-bef4-70b138618dbc', 'resource.modules.article.pagetypes.direction.default.panels.menu.teaser256.show', '0', '0', '', 0, 1, 1, '2013-08-01 09:49:50', '2013-08-01 09:49:50'),
('098fc744-0122-4fee-a277-44af2d8da7e7', 'resource.modules.article.pagetypes.page.default.panels.media.footerimg.show', '0', '0', '', 0, 1, 1, '2021-06-27 00:41:41', '2021-06-27 00:41:41'),
('0a2c69d0-e144-11ea-89a4-9f26e80d2c8b', 'identifier.logindefault.authconfig', 'auth.defaultfrontend', 'auth.defaultfrontend', '', 0, 1, 0, '2020-08-18 13:15:59', '2020-09-11 00:00:41'),
('0a362832-a220-4ceb-9317-e3cc11175a94', 'resource.modules.article.pagetypes.link.default.panels.event.show', '0', '0', '', 0, 1, 1, '2014-03-17 11:27:32', '2016-08-30 08:01:38'),
('0b13ca49-dbf3-49d8-b890-a7fceefa9db9', 'resource.modules.article.pagetypes.event.default.panels.media.img.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2014-03-31 07:01:37'),
('0c7c165d-1db2-44e6-9058-846542aa6b34', 'resource.modules.article.pagetypes.link.default.panels.menu.teaser4096.show', '0', '0', '', 0, 1, 1, '2018-10-10 00:27:20', '2021-06-27 00:34:42'),
('0d41a6d0-e149-11ea-89a4-9f26e80d2c8b', 'auth.backend.after_login_redirect_url', '/system', '/system', '', 0, 1, 0, '2020-08-18 13:51:11', '2020-08-18 13:51:11'),
('0dcd1e14-e961-45a0-b4ef-d23821abf6c1', 'resource.modules.article.pagetypes.link.default.subline.show', '0', '0', '', 0, 1, 1, '2016-08-30 06:16:59', '2021-06-26 22:37:34'),
('0f88fee5-fff5-4a5d-8153-cc1c929987a2', 'resource.modules.article.pagetypes.faq.default.panels.media.footerimg.show', '0', '0', '', 0, 1, 1, '2021-06-27 00:41:40', '2021-06-27 00:41:40'),
('10189e68-74ff-48a3-a936-b5fd233de524', 'resource.modules.article.pagetypes.calendar.default.panels.menu.teaser32.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:13', '2021-06-26 22:31:32'),
('10a2cce8-7278-4870-938b-54347bd12f25', 'resource.modules.article.pagetypes.faq.default.panels.menu.teaser1024.show', '1', '1', '', 0, 1, 1, '2018-10-10 00:26:46', '2021-01-15 22:40:44'),
('116cad06-c080-4c9d-b1d8-2d54f21c950c', 'resource.modules.article.pagetypes.calendar.default.content.show', '1', '1', '', 0, 1, 1, '2016-08-30 06:14:28', '2021-01-20 20:45:33'),
('11cec69e-2499-4e6b-b6ee-93ef8284e828', 'resource.modules.menu.teaser.teaser512.name', 'Frei ohne Abschnitt 4', 'Frei ohne Abschnitt 4', NULL, 0, 1, 0, '2018-10-10 00:36:17', '2018-10-10 01:25:25'),
('11e3bfc1-4e1c-4fbb-b34d-a146dc5b5b34', 'resource.modules.article.pagetypes.direction.default.panels.media.galerie.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2021-01-15 22:38:40'),
('124d29d6-8a16-4734-b1e7-3e6697346999', 'resource.modules.article.pagetypes.link.default.panels.media.video.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2014-03-24 07:51:46'),
('13f85564-50c6-4f95-9f8c-1d06a9749661', 'resource.modules.article.pagetypes.link.default.panels.menu.teaser8.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2021-06-27 00:34:49'),
('1444ece1-7b5c-4054-a2b5-ab4ffb0eca56', 'resource.modules.article.pagetypes.page.default.panels.menu.teaser1024.show', '1', '1', '', 0, 1, 1, '2018-10-10 00:26:46', '2021-01-15 22:40:44'),
('14b904ab-faa5-4de3-b9d2-c3f004755523', 'resource.modules.article.pagetypes.event.default.panels.formularconfig.show', '0', '0', '', 0, 1, 1, '2013-02-14 19:52:44', '2021-01-15 22:38:13'),
('152dbf13-b92a-4778-b6cc-a31ba7c6770e', 'resource.modules.article.pagetypes.event.element', '0', '0', '', 0, 1, 1, '2021-06-03 22:13:22', '2021-06-03 22:16:37'),
('16b95ad1-0f88-431c-af37-e78cb1dc7594', 'resource.modules.article.pagetypes.faq.default.panels.media.audio.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2012-11-19 18:34:12'),
('18e4ba72-6987-45f7-9670-5fae0be14ee7', 'resource.modules.article.pagetypes.event.default.panels.menu.teaser2048.show', '0', '0', '', 0, 1, 1, '2018-10-10 00:27:01', '2018-10-10 00:27:01'),
('1c0d111d-88c6-407f-9de9-53fb88edd5c7', 'resource.modules.article.pagetypes.event.default.panels.menu.teaser32.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:13', '2021-06-26 22:31:32'),
('1e0dcdac-acd4-4d94-b80b-798c8ec0ef8b', 'resource.modules.article.pagetypes.link.default.panels.media.footerimg.show', '0', '0', '', 0, 1, 1, '2021-06-27 00:41:40', '2021-06-27 00:41:40'),
('1e2eac62-2fac-4a9a-95ec-5478995e9dd1', 'resource.modules.article.pagetypes.faq.default.teaser.show', '1', '1', '', 0, 1, 1, '2016-08-30 06:15:28', '2016-08-30 06:15:28'),
('1e94ae48-04ea-4423-9552-b34259f7cb94', 'resource.modules.article.pagetypes.calendar.default.panels.rights.show', '1', '1', '', 0, 1, 1, '2013-02-14 19:52:45', '2021-01-19 15:27:39'),
('1f779916-c9ed-452d-8b70-92b273445aa6', 'resource.modules.article.pagetypes.calendar.default.panels.menu.teaser4096.show', '1', '1', '', 0, 1, 1, '2018-10-10 00:27:20', '2021-06-26 22:32:41'),
('206ee987-6450-4dd7-a3ce-41afee0d5a3a', 'resource.modules.article.pagetypes.calendar.default.subline.show', '0', '0', '', 0, 1, 1, '2016-08-30 06:16:59', '2021-06-26 22:37:34'),
('207b1cda-5b8e-4ecd-98b6-2ed20a0f6eec', 'resource.modules.article.pagetypes.faq.default.topline.show', '0', '0', '', 0, 1, 1, '2018-08-30 18:32:07', '2021-06-26 22:37:28'),
('21785560-56da-4b26-bbe0-5c2de111a4fe', 'resource.modules.article.pagetypes.frontpage.default.moretext.show', '0', '0', NULL, 0, 1, 1, '2016-08-30 06:45:27', '2021-01-20 20:46:58'),
('21b05f94-bd94-4c53-b6f4-1b5436ba4c23', 'resource.modules.menu.teaser.teaser1.tooltip', '1', '1', NULL, 0, 1, 0, '2018-08-30 16:21:52', '2021-06-26 22:18:00'),
('2206d981-5369-437d-b55c-0f4a3b378e82', 'resource.modules.menu.teaser.teaser256.tooltip', '256', '256', NULL, 0, 1, 0, '2018-08-30 16:24:18', '2021-06-26 22:18:33'),
('23bcbd6f-d0cf-4e24-b47f-8b7c02c0bebd', 'resource.modules.article.pagetypes.direction.default.panels.domain.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:13', '2012-11-19 18:34:13'),
('24564bfd-c2b4-4a6e-912b-f9ffacf0f003', 'resource.modules.article.pagetypes.calendar.default.panels.media.teaser.show', '1', '1', '', 0, 1, 1, '2014-01-21 06:40:03', '2014-01-21 06:40:03'),
('24736b04-6461-4ac3-9a3a-8b85ce5a9cf1', 'resource.modules.article.pagetypes.link.default.link.show', '1', '1', '', 0, 1, 1, '2016-08-30 06:15:16', '2021-06-27 00:30:37'),
('24bca238-9f9a-4e9f-9ea3-b724cb3884a7', 'resource.modules.article.pagetypes.event.default.panels.media.footerimg.show', '0', '0', '', 0, 1, 1, '2021-06-27 00:41:40', '2021-06-27 00:41:40'),
('260bed76-e0c1-4c1c-ae7c-1297c6fa0ad1', 'resource.modules.article.pagetypes.frontpage.default.has_teasertext2.show', '0', '0', NULL, 0, 1, 1, '2016-08-30 06:45:27', '2021-01-20 20:47:16'),
('26cfdb80-e149-11ea-89a4-9f26e80d2c8b', 'auth.backend.credentials.active', '1', '1', '', 0, 1, 0, '2020-08-18 13:51:41', '2020-08-18 13:51:41'),
('26e35334-9b4e-4938-927c-4be654f4c099', 'resource.modules.menu.teaser.teaser1024.name', 'Abschnitt', 'Abschnitt', NULL, 0, 1, 0, '2018-10-10 00:37:43', '2018-10-10 00:37:43'),
('279befc2-2ea7-47e0-a841-e79de5cd74dd', 'resource.modules.article.pagetypes.page.default.content.show', '1', '1', NULL, 0, 1, 1, '2016-08-30 06:14:28', '2021-01-20 20:45:33'),
('280467f5-b939-44b4-8f9a-6935ccd13054', 'resource.modules.article.pagetypes.link.default.topline.show', '0', '0', '', 0, 1, 1, '2018-08-30 18:32:07', '2021-06-26 22:37:28'),
('2943b04a-5a22-46b6-861c-a7c676f9f790', 'resource.modules.article.pagetypes.direction.default.panels.menu.teaser2048.show', '0', '0', '', 0, 1, 1, '2018-10-10 00:27:01', '2018-10-10 00:27:01'),
('29b70b27-04d4-412f-9649-524b194eb316', 'resource.modules.article.pagetypes.calendar.default.moretext.show', '1', '1', '', 0, 1, 1, '2016-08-30 06:16:13', '2016-08-30 06:16:13'),
('2a049d19-d6a8-4042-beaa-2c8068f19c0f', 'resource.modules.article.pagetypes.calendar.default.panels.menu.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:13', '2012-11-19 18:34:13'),
('2a0c7f1d-b7e9-4e72-b63e-988f508d5417', 'resource.modules.article.pagetypes.calendar.default.panels.menu.teaser16.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:12', '2021-01-20 21:23:15'),
('2a290779-7cc3-4e74-ac18-e6786d3be887', 'resource.modules.article.pagetypes.faq.default.panels.menu.teaser32.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:13', '2021-06-26 22:31:32'),
('2a69569c-20d1-4264-99b5-0fd6175d52a6', 'resource.modules.menu.teaser.teaser2048.show', '0', '0', NULL, 0, 1, 0, '2018-10-10 00:39:08', '2018-10-10 00:39:08'),
('2ac2ee0f-0364-4c05-b310-63e94dbc76d5', 'resource.modules.article.pagetypes.event.default.panels.media.audio.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2012-11-19 18:34:12'),
('2c1bca1d-18c0-4129-828e-dc29338be529', 'resource.modules.article.pagetypes.event.default.panels.media.download.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:12', '2014-02-20 09:43:48'),
('2cecda6b-4d91-4752-a6d0-d0b30b775cdb', 'resource.modules.article.pagetypes.link.default.panels.media.download.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2021-06-27 00:34:57'),
('2f329a98-0588-4e5d-9355-a1ff2541fe81', 'resource.modules.article.pagetypes.frontpage.default.panels.menu.teaser8192.show', '0', '0', '', 0, 1, 1, '2018-10-10 00:28:06', '2018-10-10 00:28:06'),
('2f3e6a86-73ec-43a7-a6e4-c60b191d6c94', 'resource.modules.article.pagetypes.frontpage.default.panels.menu.teaser2048.show', '0', '0', '', 0, 1, 1, '2018-10-10 00:27:02', '2018-10-10 00:27:02'),
('3048c62b-c184-4cb9-a911-eb8357e3ae01', 'resource.modules.article.pagetypes.frontpage.default.panels.media.footerimg.show', '0', '0', '', 0, 1, 1, '2021-06-27 00:41:41', '2021-06-27 00:41:41'),
('3089999f-fc9d-46a8-be15-c29f71e10121', 'resource.modules.article.pagetypes.direction.default.teaser.show', '1', '1', '', 0, 1, 1, '2016-08-30 06:15:28', '2016-08-30 06:15:28'),
('317a4e48-f312-4424-b5d3-52c9ba60fa39', 'resource.modules.article.pagetypes.direction.default.panels.options.show', '1', '1', '', 0, 1, 1, '2013-02-14 19:52:45', '2021-01-19 16:03:00'),
('31f178ce-531c-401c-aaec-6ca6358821ab', 'resource.modules.article.pagetypes.link.element.panels.media.footerimg.show', '0', '0', '', 0, 1, 1, '2021-06-27 00:41:40', '2021-06-27 00:41:40'),
('329fdb34-293f-452e-b992-b09819c81b54', 'resource.modules.article.pagetypes.event.default.panels.menu.teaser256.show', '0', '0', '', 0, 1, 1, '2013-08-01 09:49:50', '2013-08-01 09:49:50'),
('32c0bed5-dbaf-4ebe-8583-5105d524509e', 'resource.modules.article.panels.media.tabs.footerimg.order', '1100', '1100', '', 0, 1, 0, '2021-06-27 00:44:14', '2021-06-27 00:44:34'),
('335528e4-4ed4-4436-b1d9-696cf1f6f2dc', 'resource.modules.article.pagetypes.link.default.content3.show', '0', '0', '', 0, 1, 1, '2021-06-26 22:39:25', '2021-06-26 22:39:25'),
('3376ea7d-e687-4776-8def-46e906101a25', 'resource.modules.article.pagetypes.faq.default.has_teasertext2.show', '0', '0', '', 0, 1, 1, '2016-08-30 06:15:54', '2021-01-20 21:22:57'),
('348895de-05b7-4766-9520-ca03a541a696', 'resource.modules.article.pagetypes.faq.default.panels.media.download.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:12', '2014-02-20 09:43:48'),
('3557831f-4470-46c4-b0cd-008d90383e1a', 'resource.modules.article.pagetypes.direction.default.content2.show', '0', '0', '', 0, 1, 1, '2016-08-30 06:15:03', '2016-08-30 06:15:03'),
('3795fe4c-de64-4ac9-bec8-96061ba0f739', 'resource.modules.article.pagetypes.event.default.panels.media.galerie.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2021-01-15 22:38:40'),
('37c454e0-c652-4183-9f5e-151ad40ee17e', 'resource.modules.menu.teaser.teaser512.tooltip', '512', '512', NULL, 0, 1, 0, '2018-10-10 00:37:12', '2021-06-26 22:20:13'),
('39b69f2a-70c8-46f6-a627-19fe7059c9d2', 'resource.modules.article.pagetypes.faq.default.panels.media.galerie.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2021-01-15 22:38:40'),
('3ab4f602-54e1-40ad-b266-599939a5fb65', 'resource.modules.article.pagetypes.calendar.default.panels.menu.teaser1.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:12', '2018-10-10 00:46:19'),
('3c1f9e2e-5309-40da-bdbe-4bc61cf7f545', 'resource.modules.article.pagetypes.event.default.subline.show', '0', '0', '', 0, 1, 1, '2016-08-30 06:16:59', '2021-06-26 22:37:34'),
('3dbffc20-03f2-4d24-8df5-348802af8e02', 'resource.modules.article.pagetypes.event.icon', 'fa fa-clock', 'fa fa-clock', '', 0, 1, 1, '2021-06-27 15:14:54', '2021-06-27 15:17:13'),
('3f0b14f9-c992-4ddf-aea0-c75e5906a15e', 'resource.modules.article.pagetypes.direction.default.name', 'Standard', 'Standard', '', 0, 1, 1, '2012-08-09 10:42:35', '2016-08-26 04:20:26'),
('3fca4efc-2b1f-45eb-8dbd-834001e577e1', 'resource.modules.article.pagetypes.link.default.panels.menu.teaser512.show', '0', '0', '', 0, 1, 1, '2018-10-10 00:26:28', '2018-10-10 00:26:28'),
('408a5cd8-5412-4e0a-8f57-cb8ba49e6758', 'resource.modules.article.pagetypes.event.default.panels.menu.teaser128.show', '0', '0', '', 0, 1, 1, '2013-08-01 09:49:50', '2013-08-01 09:49:50'),
('419afeb0-f3ad-11ea-9b77-0b089fb1f902', 'auth.defaultfrontend.after_logout_redirect_url', '/', '/', '', 0, 1, 0, '2020-09-10 23:33:38', '2020-09-10 23:33:38'),
('42eaf973-9080-4c2b-ac28-9c629326544a', 'resource.modules.article.pagetypes.calendar.default.panels.menu.teaser1024.show', '1', '1', '', 0, 1, 1, '2018-10-10 00:26:46', '2021-01-15 22:40:44'),
('43742295-5186-4abb-be74-b33b3f77d35c', 'resource.modules.article.pagetypes.faq.default.panels.formularconfig.show', '0', '0', '', 0, 1, 1, '2013-02-14 19:52:44', '2021-01-15 22:38:13'),
('43f907d5-5672-4791-b7cd-d562beb3e8b3', 'resource.modules.article.pagetypes.direction.default.panels.media.doc.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2012-11-19 18:34:12'),
('440413e8-b40e-48db-b014-7ff7a9755dee', 'resource.modules.article.pagetypes.faq.default.content3.show', '0', '0', '', 0, 1, 1, '2021-06-26 22:39:25', '2021-06-26 22:39:25'),
('454b51f9-0da4-48b4-ae2b-56e2dc0d420a', 'resource.modules.article.pagetypes.event.default.panels.domain.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:13', '2012-11-19 18:34:13'),
('4631c994-3645-4db1-bbfe-42d11c28f051', 'resource.modules.article.pagetypes.event.element.panels.media.footerimg.show', '0', '0', '', 0, 1, 1, '2021-06-27 00:41:40', '2021-06-27 00:41:40'),
('481a431d-cb0b-4734-b2bd-c34d1401488d', 'resource.modules.article.pagetypes.link.default.panels.menu.teaser4.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2021-06-27 00:35:08'),
('48b825db-b52d-4f78-9a01-0e249bb9adf3', 'resource.modules.article.pagetypes.link.name', 'Link', 'Link', '', 0, 1, 1, '2016-08-26 03:59:28', '2021-06-27 00:30:11'),
('490f379d-a849-44e8-972b-0247690e6f7f', 'resource.modules.article.pagetypes.link.default.panels.media.teaser.show', '1', '1', '', 0, 1, 1, '2014-01-21 06:40:03', '2014-01-21 06:40:03'),
('491cd9fe-769f-45ed-9e36-1b064025a20b', 'resource.modules.menu.teaser.teaser256.order', '900', '900', NULL, 0, 1, 0, '2018-08-30 15:54:41', '2018-10-10 00:35:51'),
('4af90826-ad97-4712-ac7b-74eed5d68acb', 'resource.modules.article.pagetypes.direction.default.panels.map.show', '1', '1', '', 0, 1, 1, '2013-02-14 19:52:44', '2021-06-29 00:34:58'),
('4c1c8033-5c45-4c49-ac08-fe461c06ec50', 'resource.modules.article.pagetypes.direction.default.link.show', '0', '0', '', 0, 1, 1, '2016-08-30 06:15:16', '2016-08-30 06:18:06'),
('4d7e62e9-08cc-4384-aef6-03f41a25c659', 'resource.modules.formular.show', '1', '1', NULL, 1, 1, 0, '2011-03-14 19:48:09', '2011-04-27 18:09:49'),
('4da0f000-e144-11ea-89a4-9f26e80d2c8b', 'auth.defaultfrontend.after_login_redirect_url', '/', '/', '', 0, 1, 0, '2020-08-18 13:17:07', '2020-08-18 15:21:11'),
('4da3d016-6509-49f9-b835-972b977eedeb', 'resource.modules.article.pagetypes.event.default.panels.media.titleimg.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:12', '2012-11-19 18:34:12'),
('4df085e0-1dc9-4377-9250-e65ba5de2d03', 'resource.modules.article.pagetypes.calendar.default.panels.menu.teaser128.show', '0', '0', '', 0, 1, 1, '2013-08-01 09:49:50', '2013-08-01 09:49:50'),
('4eae5961-d718-44f6-83de-010c1a25c659', 'app.shop.checkouturl', 'checkout', 'checkout', NULL, 0, 1, 0, '2011-10-31 09:16:33', '2014-02-07 13:10:22'),
('4fb66d66-ea8e-49f3-a6b5-4f8b1bb4429a', 'resource.modules.article.pagetypes.faq.default.content2.show', '0', '0', '', 0, 1, 1, '2016-08-30 06:15:03', '2016-08-30 06:15:03'),
('501237b7-f630-41e1-85c6-15081a25c659', 'app.articles.defaulttype', 'page', 'page', NULL, 0, 1, 0, '2012-07-27 08:39:51', '2016-08-26 05:38:57'),
('50222bc0-f638-4a77-a853-12881a25c659', 'resource.modules.article.panels.media.tabs.titleimg.name', 'Titelbilder', 'Titelbilder', NULL, 0, 1, 0, '2012-08-08 11:05:04', '2012-08-08 11:05:04'),
('502233e5-0e98-4a41-9e63-12881a25c659', 'resource.modules.article.panels.media.tabs.doc.name', 'Dokumente', 'Dokumente', NULL, 0, 1, 0, '2012-08-08 11:39:49', '2012-08-08 11:39:49'),
('502233e5-3bcc-4df9-992b-12881a25c659', 'resource.modules.article.panels.media.tabs.backgroundimg.name', 'Hintergrundbilder', 'Hintergrundbilder', NULL, 0, 1, 0, '2012-08-08 11:39:49', '2012-08-08 11:39:49'),
('502233e5-7310-4815-9df4-12881a25c659', 'resource.modules.article.panels.media.tabs.download.name', 'Downloads', 'Downloads', NULL, 0, 1, 0, '2012-08-08 11:39:49', '2012-08-08 11:39:49'),
('502233e5-d3f0-4940-a612-12881a25c659', 'resource.modules.article.panels.media.tabs.audio.name', 'Audio', 'Audio', NULL, 0, 1, 0, '2012-08-08 11:39:49', '2012-08-08 11:39:49'),
('502233e5-dcec-487c-aeec-12881a25c659', 'resource.modules.article.panels.media.tabs.img.name', 'Bilder', 'Bilder', NULL, 0, 1, 0, '2012-08-08 11:39:49', '2021-01-15 18:53:59'),
('502233e5-dd98-4b58-bb96-12881a25c659', 'resource.modules.article.panels.media.tabs.video.name', 'Videos', 'Videos', NULL, 0, 1, 0, '2012-08-08 11:39:49', '2012-08-08 11:39:49'),
('502233e5-f778-4e95-8e2a-12881a25c659', 'resource.modules.article.panels.media.tabs.galerie.name', 'Galeriebilder', 'Galeriebilder', NULL, 0, 1, 0, '2012-08-08 11:39:49', '2021-01-15 18:58:33'),
('502347b8-bdc8-4ddf-9fec-1bc81a25c659', 'resource.modules.menu.teaser.teaser1.show', '1', '1', NULL, 0, 1, 0, '2012-08-09 07:16:40', '2012-08-09 07:16:40'),
('502347c0-8410-4f47-9859-1bc81a25c659', 'resource.modules.menu.teaser.teaser2.show', '1', '1', NULL, 0, 1, 0, '2012-08-09 07:16:48', '2012-08-09 07:16:48'),
('502347cc-97f0-412e-84eb-1bc81a25c659', 'resource.modules.menu.teaser.teaser4.show', '1', '1', NULL, 0, 1, 0, '2012-08-09 07:17:00', '2013-11-12 06:53:26'),
('502347d8-3888-4a4f-a73a-1bc81a25c659', 'resource.modules.menu.teaser.teaser8.show', '1', '1', NULL, 0, 1, 0, '2012-08-09 07:17:12', '2014-03-24 07:44:23'),
('502347e0-d710-4a4d-b683-1bc81a25c659', 'resource.modules.menu.teaser.teaser16.show', '1', '1', NULL, 0, 1, 0, '2012-08-09 07:17:20', '2021-06-26 22:17:41'),
('502347ee-89dc-49b5-8f52-1bc81a25c659', 'resource.modules.menu.teaser.teaser32.show', '1', '1', NULL, 0, 1, 0, '2012-08-09 07:17:34', '2014-08-29 18:11:08'),
('5023561a-1644-4692-999b-1b701a25c659', 'resource.modules.menu.teaser.teaser16.name', 'CTA', 'CTA', NULL, 0, 1, 0, '2012-08-09 08:18:02', '2021-06-26 22:17:16'),
('5023561a-9588-43e7-b4f9-1b701a25c659', 'resource.modules.menu.teaser.teaser2.name', 'Menü', 'Menü', NULL, 0, 1, 0, '2012-08-09 08:18:02', '2018-10-10 00:33:21'),
('5023561a-a138-4be6-9d19-1b701a25c659', 'resource.modules.menu.teaser.teaser8.name', 'Slider', 'Slider', NULL, 0, 1, 0, '2012-08-09 08:18:02', '2019-05-02 14:59:08'),
('5023561a-b0f8-4b0e-8504-1b701a25c659', 'resource.modules.menu.teaser.teaser1.name', 'Struktur', 'Struktur', NULL, 0, 1, 0, '2012-08-09 08:18:02', '2018-10-10 00:28:33'),
('5023561a-bf38-4226-b707-1b701a25c659', 'resource.modules.menu.teaser.teaser4.name', 'Teaser', 'Teaser', NULL, 0, 1, 0, '2012-08-09 08:18:02', '2013-11-12 06:54:00'),
('5023561a-c00c-414d-8225-1b701a25c659', 'resource.modules.menu.teaser.teaser32.name', 'Liste', 'Liste', NULL, 0, 1, 0, '2012-08-09 08:18:02', '2018-10-10 00:33:38'),
('502377fb-bebc-43f2-9ae7-04201a25c659', 'resource.modules.article.pagetypes.page.default.name', 'Standard', 'Standard', NULL, 0, 1, 1, '2012-08-09 10:42:35', '2016-08-26 04:20:26'),
('5029f346-31bc-47fd-ac62-04201a25c659', 'resource.modules.article.pagetypes.frontpage.default.name', 'Standard', 'Standard', NULL, 0, 1, 1, '2012-08-14 08:42:14', '2016-08-26 04:20:08'),
('5029f347-0b58-46fb-b9bd-04201a25c659', 'resource.modules.article.pagetypes.frontpage.default.panels.media.audio.show', '0', '0', NULL, 0, 1, 1, '2012-08-14 08:42:15', '2012-08-14 08:42:15'),
('5029f347-14e0-4da5-bb22-04201a25c659', 'resource.modules.article.pagetypes.frontpage.default.panels.media.video.show', '0', '0', NULL, 0, 1, 1, '2012-08-14 08:42:15', '2013-10-23 11:38:34'),
('5029f347-1e34-44f6-9f3e-04201a25c659', 'resource.modules.article.pagetypes.frontpage.default.panels.menu.teaser2.show', '0', '0', NULL, 0, 1, 1, '2012-08-14 08:42:15', '2012-08-14 08:42:15'),
('5029f347-3510-4d38-aa39-04201a25c659', 'resource.modules.article.pagetypes.frontpage.default.panels.media.doc.show', '0', '0', NULL, 0, 1, 1, '2012-08-14 08:42:15', '2012-08-14 08:42:15'),
('5029f347-38b4-4458-8671-04201a25c659', 'resource.modules.article.pagetypes.frontpage.default.panels.media.backgroundimg.show', '0', '0', NULL, 0, 1, 1, '2012-08-14 08:42:15', '2012-08-14 08:42:15'),
('5029f347-5234-49b6-bd05-04201a25c659', 'resource.modules.article.pagetypes.frontpage.default.panels.menu.show', '0', '0', NULL, 0, 1, 1, '2012-08-14 08:42:15', '2012-08-14 08:42:15'),
('5029f347-75e4-4c77-9b9d-04201a25c659', 'resource.modules.article.pagetypes.frontpage.default.panels.media.download.show', '1', '1', NULL, 0, 1, 1, '2012-08-14 08:42:15', '2014-03-24 07:56:12'),
('5029f347-8a44-4d65-9a7d-04201a25c659', 'resource.modules.article.pagetypes.frontpage.default.panels.media.titleimg.show', '1', '1', NULL, 0, 1, 1, '2012-08-14 08:42:15', '2012-08-14 08:42:15'),
('5029f347-9d08-4990-805f-04201a25c659', 'resource.modules.article.pagetypes.frontpage.default.panels.media.galerie.show', '0', '0', NULL, 0, 1, 1, '2012-08-14 08:42:15', '2021-01-15 19:13:53'),
('5029f347-c834-4073-bc92-04201a25c659', 'resource.modules.article.pagetypes.frontpage.default.panels.menu.teaser4.show', '1', '1', NULL, 0, 1, 1, '2012-08-14 08:42:15', '2013-10-23 05:46:55'),
('5029f347-d094-4293-a9d9-04201a25c659', 'resource.modules.article.pagetypes.frontpage.default.panels.media.img.show', '0', '0', NULL, 0, 1, 1, '2012-08-14 08:42:15', '2013-10-23 11:38:21'),
('5029f347-f980-4ffd-be2b-04201a25c659', 'resource.modules.article.pagetypes.frontpage.default.panels.menu.teaser1.show', '0', '0', NULL, 0, 1, 1, '2012-08-14 08:42:15', '2012-08-14 08:42:15'),
('5029f348-0524-407c-8013-04201a25c659', 'resource.modules.article.pagetypes.frontpage.default.panels.menu.teaser32.show', '0', '0', '', 0, 1, 1, '2012-08-14 08:42:16', '2018-10-10 00:48:23'),
('5029f348-11f0-4c2b-84c2-04201a25c659', 'resource.modules.article.pagetypes.frontpage.default.panels.menu.teaser16.show', '0', '0', NULL, 0, 1, 1, '2012-08-14 08:42:16', '2013-11-12 06:56:54'),
('5029f348-4a20-499f-9b3a-04201a25c659', 'resource.modules.article.pagetypes.frontpage.default.panels.domain.show', '1', '1', NULL, 0, 1, 1, '2012-08-14 08:42:16', '2012-08-14 08:42:16'),
('5029f348-e0b8-4efe-9d6c-04201a25c659', 'resource.modules.article.pagetypes.frontpage.default.panels.menu.teaser8.show', '0', '0', NULL, 0, 1, 1, '2012-08-14 08:42:16', '2013-11-12 06:57:06'),
('50aa6d94-0d6c-4ead-b6e0-19a81a25c659', 'resource.modules.article.pagetypes.page.default.panels.media.download.show', '1', '1', NULL, 0, 1, 1, '2012-11-19 18:34:12', '2014-02-20 09:43:48'),
('50aa6d94-1394-4800-8880-19a81a25c659', 'resource.modules.article.pagetypes.page.default.panels.menu.teaser2.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:12', '2018-10-10 00:47:10'),
('50aa6d94-3ccc-4aee-946a-19a81a25c659', 'resource.modules.article.pagetypes.page.default.panels.media.audio.show', '0', '0', NULL, 0, 1, 1, '2012-11-19 18:34:12', '2012-11-19 18:34:12'),
('50aa6d94-53bc-48fb-b27f-19a81a25c659', 'resource.modules.article.pagetypes.page.default.panels.media.video.show', '0', '0', NULL, 0, 1, 1, '2012-11-19 18:34:12', '2014-03-24 07:51:46'),
('50aa6d94-6628-4a43-8d64-19a81a25c659', 'resource.modules.article.pagetypes.page.default.panels.media.doc.show', '0', '0', NULL, 0, 1, 1, '2012-11-19 18:34:12', '2012-11-19 18:34:12'),
('50aa6d94-690c-4de9-8ad7-19a81a25c659', 'resource.modules.article.pagetypes.page.default.panels.media.galerie.show', '1', '1', NULL, 0, 1, 1, '2012-11-19 18:34:12', '2021-06-27 00:52:26'),
('50aa6d94-6efc-40a4-82cb-19a81a25c659', 'resource.modules.article.pagetypes.page.default.panels.media.backgroundimg.show', '0', '0', NULL, 0, 1, 1, '2012-11-19 18:34:12', '2012-11-19 18:34:12'),
('50aa6d94-9708-40fe-bd80-19a81a25c659', 'resource.modules.article.pagetypes.page.default.panels.menu.teaser16.show', '1', '1', NULL, 0, 1, 1, '2012-11-19 18:34:12', '2021-01-20 21:23:15'),
('50aa6d94-9860-4f4c-a45d-19a81a25c659', 'resource.modules.article.pagetypes.page.default.panels.menu.teaser4.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:12', '2018-10-10 00:47:59'),
('50aa6d94-9a14-4c91-a23b-19a81a25c659', 'resource.modules.article.pagetypes.page.default.panels.menu.teaser8.show', '1', '1', NULL, 0, 1, 1, '2012-11-19 18:34:12', '2021-01-20 21:23:05'),
('50aa6d94-a2cc-49f5-b27d-19a81a25c659', 'resource.modules.article.pagetypes.page.default.panels.media.titleimg.show', '1', '1', NULL, 0, 1, 1, '2012-11-19 18:34:12', '2012-11-19 18:34:12'),
('50aa6d94-caf4-4f58-af1d-19a81a25c659', 'resource.modules.article.pagetypes.page.default.panels.menu.teaser1.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:12', '2018-10-10 00:46:19'),
('50aa6d94-dcc8-42d2-b303-19a81a25c659', 'resource.modules.article.pagetypes.page.default.panels.media.img.show', '0', '0', NULL, 0, 1, 1, '2012-11-19 18:34:12', '2014-03-31 07:01:37'),
('50aa6d95-15a8-42a0-b58c-19a81a25c659', 'resource.modules.article.pagetypes.page.default.panels.domain.show', '0', '0', NULL, 0, 1, 1, '2012-11-19 18:34:13', '2012-11-19 18:34:13'),
('50aa6d95-9aec-4fc5-9a1b-19a81a25c659', 'resource.modules.article.pagetypes.page.default.panels.menu.teaser32.show', '1', '1', NULL, 0, 1, 1, '2012-11-19 18:34:13', '2021-06-26 22:31:32'),
('50aa6d95-bfa0-4d3b-a23a-19a81a25c659', 'resource.modules.article.pagetypes.page.default.panels.media.show', '1', '1', NULL, 0, 1, 1, '2012-11-19 18:34:13', '2012-11-19 18:34:13'),
('50aa6d95-c21c-49a7-a147-19a81a25c659', 'resource.modules.article.pagetypes.page.default.panels.menu.show', '1', '1', NULL, 0, 1, 1, '2012-11-19 18:34:13', '2012-11-19 18:34:13'),
('50ae2426-c67f-428d-980a-ecffadb5b43a', 'resource.modules.article.pagetypes.direction.default.panels.menu.teaser2.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:12', '2018-10-10 00:47:10'),
('50ae3cfa-f4d0-4d0e-9519-271c1a25c659', 'resource.modules.article.pagetypes.page.default.show', '1', '1', NULL, 1, 1, 1, '2012-11-22 15:55:54', '2016-08-26 03:35:14'),
('5110d27b-e018-4dc2-ae21-32b81a25c659', 'app.language.defaultredirectlanguage', 'de', 'de', NULL, 0, 1, 0, '2013-02-05 10:35:55', '2013-10-29 10:41:24'),
('511d327c-1fec-4a2b-9132-12e81a25c659', 'resource.modules.article.pagetypes.page.default.panels.formularconfig.show', '0', '0', NULL, 0, 1, 1, '2013-02-14 19:52:44', '2021-01-15 22:38:13'),
('511d327c-a08c-4df5-b51b-12e81a25c659', 'resource.modules.article.pagetypes.page.default.panels.map.show', '0', '0', NULL, 0, 1, 1, '2013-02-14 19:52:44', '2021-06-27 20:59:54'),
('511d327d-7bc8-4ef7-bd9c-12e81a25c659', 'resource.modules.article.pagetypes.page.default.panels.seo.show', '1', '1', NULL, 0, 1, 1, '2013-02-14 19:52:45', '2013-02-14 19:52:45'),
('511d327d-bd1c-4602-b80f-12e81a25c659', 'resource.modules.article.pagetypes.page.default.panels.rights.show', '1', '1', NULL, 0, 1, 1, '2013-02-14 19:52:45', '2021-01-19 15:27:39'),
('511d327d-f458-4996-8a69-12e81a25c659', 'resource.modules.article.pagetypes.page.default.panels.options.show', '1', '1', NULL, 0, 1, 1, '2013-02-14 19:52:45', '2021-01-19 16:03:00'),
('511d4228-152c-44c5-a871-12e81a25c659', 'resource.modules.article.pagetypes.frontpage.default.panels.map.show', '0', '0', NULL, 0, 1, 1, '2013-02-14 20:59:36', '2021-06-27 21:00:56'),
('511d4228-2f00-4d69-8309-12e81a25c659', 'resource.modules.article.pagetypes.frontpage.default.panels.formularconfig.show', '0', '0', NULL, 0, 1, 1, '2013-02-14 20:59:36', '2021-01-20 20:49:15'),
('511d4229-70ac-487b-b6b2-12e81a25c659', 'resource.modules.article.pagetypes.frontpage.default.panels.seo.show', '1', '1', NULL, 0, 1, 1, '2013-02-14 20:59:37', '2013-02-14 20:59:37'),
('511d4229-99d0-4e7b-885a-12e81a25c659', 'resource.modules.article.pagetypes.frontpage.default.panels.media.show', '1', '1', NULL, 0, 1, 1, '2013-02-14 20:59:37', '2013-02-14 20:59:37'),
('511d4229-99d4-45a7-8d5d-12e81a25c659', 'resource.modules.article.pagetypes.frontpage.default.panels.options.show', '1', '1', NULL, 0, 1, 1, '2013-02-14 20:59:37', '2021-01-20 20:41:10'),
('511d4229-9ef4-4e2f-935f-12e81a25c659', 'resource.modules.article.pagetypes.frontpage.default.panels.rights.show', '1', '1', NULL, 0, 1, 1, '2013-02-14 20:59:37', '2013-02-14 20:59:37'),
('51a5ee25-ee0c-4ee3-8b60-17fc1a25c659', 'resource.modules.email.show', '1', '1', NULL, 1, 1, 0, '2013-05-29 14:01:41', '2013-05-29 16:06:42'),
('51aee413-c4e8-40b3-8fd9-17fc1a25c659', 'resource.modules.actionview.show', '0', '0', NULL, 1, 1, 0, '2013-06-05 09:09:07', '2019-10-07 13:36:07'),
('51b0ed87-3b0c-4cd1-bc98-17fc1a25c659', 'resource.modules.formularconfig.show', '1', '1', NULL, 1, 1, 0, '2013-06-06 22:13:59', '2013-06-06 22:14:46'),
('51ee6c5a-9bd2-42de-931a-8d02db64da18', 'resource.modules.article.pagetypes.link.default.panels.menu.teaser256.show', '0', '0', '', 0, 1, 1, '2013-08-01 09:49:50', '2013-08-01 09:49:50'),
('51f92361-6da4-433d-8464-15841a25c659', 'resource.modules.menu.teaser.teaser64.show', '0', '0', NULL, 0, 1, 0, '2013-07-31 16:46:57', '2013-10-23 11:42:29'),
('51f92372-17b0-4841-b341-15841a25c659', 'resource.modules.menu.teaser.teaser128.show', '0', '0', NULL, 0, 1, 0, '2013-07-31 16:47:14', '2013-10-23 11:42:20'),
('51f92388-ef1c-4e49-b4cf-15841a25c659', 'resource.modules.menu.teaser.teaser256.show', '0', '0', NULL, 0, 1, 0, '2013-07-31 16:47:36', '2013-10-23 11:42:11'),
('51f92399-4abc-40d9-8113-15841a25c659', 'resource.modules.menu.teaser.teaser256.name', 'Frei ohne Abschnitt 3', 'Frei ohne Abschnitt 3', NULL, 0, 1, 0, '2013-07-31 16:47:53', '2019-05-02 14:24:08'),
('51f92473-7508-4685-ac6e-15841a25c659', 'resource.modules.menu.teaser.teaser128.name', 'Frei ohne Abschnitt 1', 'Frei ohne Abschnitt 1', NULL, 0, 1, 0, '2013-07-31 16:51:31', '2018-10-10 00:35:26'),
('51f92498-42a0-4596-a546-15841a25c659', 'resource.modules.menu.teaser.teaser64.name', 'Frei ohne Abschnitt 2', 'Frei ohne Abschnitt 2', NULL, 0, 1, 0, '2013-07-31 16:52:08', '2018-10-10 01:25:10'),
('51fa131e-0420-4a9b-b6a6-15841a25c659', 'resource.modules.article.pagetypes.page.default.panels.menu.teaser64.show', '0', '0', NULL, 0, 1, 1, '2013-08-01 09:49:50', '2013-08-01 09:49:50'),
('51fa131e-28bc-4eb4-9766-15841a25c659', 'resource.modules.article.pagetypes.frontpage.default.panels.menu.teaser128.show', '0', '0', NULL, 0, 1, 1, '2013-08-01 09:49:50', '2013-08-01 09:49:50'),
('51fa131e-3d0c-403d-a994-15841a25c659', 'resource.modules.article.pagetypes.frontpage.default.panels.menu.teaser64.show', '0', '0', NULL, 0, 1, 1, '2013-08-01 09:49:50', '2013-08-01 09:49:50'),
('51fa131e-75d8-4b1e-93b9-15841a25c659', 'resource.modules.article.pagetypes.page.default.panels.menu.teaser128.show', '0', '0', NULL, 0, 1, 1, '2013-08-01 09:49:50', '2013-08-01 09:49:50'),
('51fa131e-90e4-4341-8453-15841a25c659', 'resource.modules.article.pagetypes.frontpage.default.panels.menu.teaser256.show', '0', '0', NULL, 0, 1, 1, '2013-08-01 09:49:50', '2013-08-01 09:49:50'),
('51fa131e-ac50-41e5-8f80-15841a25c659', 'resource.modules.article.pagetypes.page.default.panels.menu.teaser256.show', '0', '0', NULL, 0, 1, 1, '2013-08-01 09:49:50', '2013-08-01 09:49:50'),
('52068316-2f8c-45ed-9f95-0aa159e11e94', 'resource.modules.article.pagetypes.event.default.panels.menu.teaser8.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:12', '2021-01-20 21:23:05'),
('5219d780-dc74-4cc4-b88e-19541a25c659', 'resource.modules.domain.show', '1', '1', NULL, 1, 1, 0, '2013-08-25 12:08:00', '2013-08-25 12:08:00'),
('527d10ed-0754-4990-a07b-22741a25c659', 'app.shop.sendinvoice', '1', '1', NULL, 0, 1, 0, '2013-11-08 17:27:25', '2013-11-08 17:27:25'),
('5281278d-a49c-46a4-9fbf-13f41a25c659', 'resource.modules.order.show', '1', '1', NULL, 1, 1, 0, '2013-11-11 19:53:01', '2013-11-11 19:53:01'),
('52d7afe5-0eec-4d03-8b8a-1bdc1a25c659', 'resource.modules.article.content2.name', '2. Inhalt', '2. Inhalt', NULL, 0, 1, 0, '2014-01-16 11:09:41', '2021-06-27 21:07:21'),
('52de075c-a7cc-48f4-8984-10a01a25c659', 'resource.modules.article.panels.media.tabs.teaser.name', 'Teaserbild', 'Teaserbild', NULL, 0, 1, 0, '2014-01-21 06:36:28', '2014-01-21 06:36:38'),
('52de0833-3cb4-4a72-b268-10a01a25c659', 'resource.modules.article.pagetypes.page.default.panels.media.teaser.show', '1', '1', NULL, 0, 1, 1, '2014-01-21 06:40:03', '2014-01-21 06:40:03'),
('52de0835-d5dc-46cf-b83b-10a01a25c659', 'resource.modules.article.pagetypes.frontpage.default.panels.media.teaser.show', '1', '1', NULL, 0, 1, 1, '2014-01-21 06:40:05', '2014-01-21 06:40:05'),
('52e5d9a6-6f20-46a9-bf60-26041a25c659', 'identifier.register.type', 'frontenduser', 'frontenduser', NULL, 0, 1, 0, '2014-01-27 04:59:34', '2014-01-29 08:24:02'),
('52e5dd8b-7600-4036-89ad-26041a25c659', 'login.frontenduser.role', 'ffffffff-ffff-ffff-ffff-ffffffffffff', 'ffffffff-ffff-ffff-ffff-ffffffffffff', NULL, 0, 1, 0, '2014-01-27 05:16:11', '2014-01-29 08:24:56'),
('52e8ab0d-ff7c-4304-a53c-26041a25c659', 'login.frontenduser.loginurl', '/anmelden', '/anmelden', NULL, 0, 1, 0, '2014-01-29 08:17:33', '2014-01-29 08:18:29'),
('52e8abc6-b49c-41f9-8181-26041a25c659', 'login.frontenduser.identifier', 'register', 'register', NULL, 0, 1, 0, '2014-01-29 08:20:38', '2014-01-29 08:20:38'),
('52e8abe9-83f0-4934-8aa5-26041a25c659', 'login.frontenduser.pwlosturl', '/pwlost', '/pwlost', NULL, 0, 1, 0, '2014-01-29 08:21:13', '2014-01-29 08:21:13'),
('52e8ac07-1cb4-4db3-9900-26041a25c659', 'login.frontenduser.logouturl', '/logout', '/logout', NULL, 0, 1, 0, '2014-01-29 08:21:43', '2014-01-29 08:21:43'),
('52f3e357-6b4c-471b-a623-24d01a25c659', 'app.shop.shippingcost.calcmethods', 'shipping', 'shipping', NULL, 0, 1, 0, '2014-02-06 20:32:39', '2014-02-19 18:35:33'),
('52f3e619-1258-41cd-a7ec-24d01a25c659', 'app.shop.shippingcost.costs.download', '0', '0', NULL, 0, 1, 0, '2014-02-06 20:44:25', '2014-02-06 20:44:31'),
('52f3e629-a1cc-4aae-8f79-24d01a25c659', 'app.shop.shippingcost.costs.perpost', '5', '5', NULL, 0, 1, 0, '2014-02-06 20:44:41', '2019-07-25 15:42:47'),
('52f3e790-af0c-45ef-af89-24d01a25c659', 'app.shop.shippingcost.freeover', '1000000', '1000000', NULL, 0, 1, 0, '2014-02-06 20:50:40', '2014-02-18 14:41:25'),
('52f406ea-7574-4656-8cde-24d01a25c659', 'app.shop.defaulttaxrate', '19', '19', NULL, 0, 1, 0, '2014-02-06 23:04:26', '2014-02-07 08:56:42'),
('53036677-782c-4686-8e98-181c1a25c659', 'app.shop.shippingcost.max', '1000000', '1000000', NULL, 0, 1, 0, '2014-02-18 14:56:07', '2014-02-18 15:11:29'),
('53037006-815c-4955-a904-181c1a25c659', 'app.shop.sendvoucher', '1', '1', NULL, 0, 1, 0, '2014-02-18 15:36:54', '2014-02-18 15:37:15'),
('53119958-1594-487e-9cd0-19e41a25c659', 'app.adminmode', '1', '1', NULL, 0, 1, 0, '2014-03-01 09:24:56', '2019-04-29 09:50:22'),
('53154773-947d-46e0-8471-a58676495365', 'resource.modules.menu.teaser.teaser2.tooltip', 'Menüpunkt / 2', 'Menüpunkt / 2', NULL, 0, 1, 0, '2018-08-30 16:22:24', '2021-06-26 22:21:53'),
('5326ce14-b9c4-42b0-a9f7-1aa01a25c659', 'resource.modules.article.pagetypes.page.default.panels.event.show', '0', '0', NULL, 0, 1, 1, '2014-03-17 11:27:32', '2016-08-30 08:01:38'),
('5326ce18-2738-4b52-9f8b-1aa01a25c659', 'resource.modules.article.pagetypes.frontpage.default.panels.event.show', '0', '0', NULL, 0, 1, 1, '2014-03-17 11:27:36', '2021-01-20 20:46:43'),
('533918f6-8b6c-402e-a8ea-2aa81a25c659', 'resource.modules.article.panels.media.tabs.audio.order', '900', '900', NULL, 0, 1, 0, '2014-03-31 09:27:50', '2021-01-15 18:51:57'),
('53391911-6220-4af7-aee1-2aa81a25c659', 'resource.modules.article.panels.media.tabs.backgroundimg.order', '100', '100', NULL, 0, 1, 0, '2014-03-31 09:28:17', '2021-01-15 18:51:40'),
('5339192b-6108-4e32-b238-2aa81a25c659', 'resource.modules.article.panels.media.tabs.doc.order', '700', '700', NULL, 0, 1, 0, '2014-03-31 09:28:43', '2021-01-15 18:52:16'),
('5339193d-9f90-41f7-a2fa-2aa81a25c659', 'resource.modules.article.panels.media.tabs.download.order', '600', '600', NULL, 0, 1, 0, '2014-03-31 09:29:01', '2021-01-15 18:52:23'),
('53391951-9970-4c08-80f8-2aa81a25c659', 'resource.modules.article.panels.media.tabs.img.order', '300', '300', NULL, 0, 1, 0, '2014-03-31 09:29:21', '2021-01-15 18:52:29'),
('53391965-fecc-4374-9549-2aa81a25c659', 'resource.modules.article.panels.media.tabs.teaser.order', '1200', '1200', NULL, 0, 1, 0, '2014-03-31 09:29:41', '2021-06-27 00:44:41'),
('5339197b-aa1c-4c6b-84ed-2aa81a25c659', 'resource.modules.article.panels.media.tabs.galerie.order', '1000', '1000', NULL, 0, 1, 0, '2014-03-31 09:30:03', '2021-01-15 18:58:41'),
('53391989-b188-41a2-9f69-2aa81a25c659', 'resource.modules.article.panels.media.tabs.titleimg.order', '200', '200', NULL, 0, 1, 0, '2014-03-31 09:30:17', '2021-01-15 18:51:47'),
('5339199c-5a58-4d1c-957a-2aa81a25c659', 'resource.modules.article.panels.media.tabs.video.order', '800', '800', NULL, 0, 1, 0, '2014-03-31 09:30:36', '2021-01-15 18:52:44'),
('53bd89bd-2750-4986-9a28-27941a25c659', 'resource.modules.article.subline.name', 'Subline', 'Subline', NULL, 0, 1, 0, '2014-07-09 20:28:13', '2016-08-30 05:53:50'),
('540491bf-7d08-4426-86fb-26241a25c659', 'app.shop.vouchershop', '1', '1', NULL, 0, 1, 0, '2014-09-01 17:33:19', '2019-10-01 11:19:03'),
('545c8af2-5620-4293-a44a-6a28ac10cd2f', 'resource.modules.voucher.show', '1', '1', NULL, 1, 1, 0, '2014-11-07 10:03:46', '2016-09-06 04:39:51'),
('546f30f3-fae0-4356-8138-119c1a25c659', 'app.fetchextras.canonicalurl', '1', '1', NULL, 0, 1, 0, '2014-11-21 13:32:51', '2014-11-21 13:35:31'),
('546f31a4-ca54-4368-a263-119c1a25c659', 'app.fetchextras.locallinks', '1', '1', NULL, 0, 1, 0, '2014-11-21 13:35:48', '2014-11-21 13:36:15'),
('546f31b7-810c-4fe6-817b-119c1a25c659', 'app.fetchextras.neighbors', '0', '0', NULL, 0, 1, 0, '2014-11-21 13:36:07', '2014-11-21 13:48:49'),
('546f32a8-0b0c-4a3a-8105-119c1a25c659', 'app.articles.article.extracontain', 'Attachments', 'Attachments', NULL, 0, 1, 0, '2014-11-21 13:40:08', '2019-04-29 09:01:52'),
('546f32b8-8e74-4f1e-9063-119c1a25c659', 'app.articles.children.extracontain', '', '', NULL, 0, 1, 0, '2014-11-21 13:40:24', '2014-11-21 13:44:20'),
('548fca04-e134-4d0c-ac5a-87581a25c659', 'app.shop.sendorderemailcopytoseller', '0', '0', NULL, 0, 1, 0, '2014-12-16 06:58:28', '2014-12-16 06:58:38'),
('548fca98-9f4c-47d8-9efa-87581a25c659', 'app.baseprotocol', 'http://', 'http://', NULL, 0, 1, 0, '2014-12-16 07:00:56', '2021-01-15 18:04:37'),
('54b6cc63-b4d7-4854-852f-9a1bb1fec5ba', 'resource.modules.article.pagetypes.calendar.default.name', 'Standard', 'Standard', '', 0, 1, 1, '2012-08-09 10:42:35', '2016-08-26 04:20:26'),
('54be7a58-26c0-49ad-852a-1f041a25c659', 'app.aco.levelnames', 'System,Kunde,Standort,Abteilung', 'System,Kunde,Standort,Abteilung', NULL, 0, 1, 0, '2015-01-20 16:55:04', '2015-01-26 09:45:03'),
('54c71e56-5374-4e10-96d1-1f141a25c659', 'resource.modules.aco.show', '1', '1', NULL, 1, 1, 0, '2015-01-27 06:12:54', '2019-07-25 19:57:35'),
('54c71ec3-d29c-430b-8f66-1f141a25c659', 'resource.modules.redirect.show', '1', '1', NULL, 1, 1, 0, '2015-01-27 06:14:43', '2016-08-26 03:43:09'),
('5559ec84-6909-4454-9198-22b57de0af35', 'resource.modules.article.pagetypes.faq.default.panels.event.show', '0', '0', '', 0, 1, 1, '2014-03-17 11:27:32', '2016-08-30 08:01:38'),
('55bc9352-8a69-4f38-a9e0-6615f561e6be', 'resource.modules.article.pagetypes.event.default.panels.map.show', '0', '0', '', 0, 1, 1, '2013-02-14 19:52:44', '2021-06-27 21:01:10'),
('55d1ac8c-5bb5-4d7d-913d-f7db5c6810d1', 'resource.modules.menu.teaser.teaser2048.order', '1200', '1200', NULL, 0, 1, 0, '2018-10-10 00:39:26', '2018-10-10 00:39:26'),
('56b6acdc-7a0c-4ccb-b4ad-f14fae904467', 'resource.modules.article.pagetypes.faq.element', '1', '1', '', 0, 1, 1, '2021-06-03 22:13:22', '2021-06-26 22:54:20'),
('57eb5d65-7b22-4360-b7bd-4a01bc2f0b38', 'resource.modules.article.pagetypes.faq.default.panels.menu.teaser8192.show', '0', '0', '', 0, 1, 1, '2018-10-10 00:28:06', '2018-10-10 00:28:06'),
('584df534-add6-411b-8dd8-0121edcc50f4', 'resource.modules.menu.teaser.teaser4.order', '300', '300', NULL, 0, 1, 0, '2018-08-30 15:52:58', '2018-10-10 00:29:56'),
('5aff26c1-c4ff-459f-8502-0a4825f7b40a', 'resource.modules.article.pagetypes.link.default.panels.media.img2.show', '0', '0', '', 0, 1, 1, '2021-01-15 22:09:04', '2021-01-15 22:09:04'),
('5bb6da10-adc9-4984-bc55-2ef2eb0102a3', 'resource.modules.article.pagetypes.calendar.default.panels.menu.teaser4.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:12', '2018-10-10 00:47:59'),
('5df3cd58-f7d9-4449-8074-a5ada432091f', 'resource.modules.article.pagetypes.calendar.default.show', '1', '1', '', 1, 1, 1, '2012-11-22 15:55:54', '2016-08-26 03:35:14'),
('5ea3acc2-1a75-4607-9023-c4cea9d208cb', 'resource.modules.article.pagetypes.frontpage.default.panels.menu.teaser512.show', '0', '0', '', 0, 1, 1, '2018-10-10 00:26:27', '2018-10-10 00:26:27'),
('5eb4161a-a158-467d-809b-9d7599bcc316', 'resource.modules.article.pagetypes.page.element', '0', '0', '', 0, 1, 1, '2021-06-03 22:13:22', '2021-06-03 22:16:37'),
('5f5e3155-ef34-4274-a75e-82c18cb046ae', 'resource.modules.article.pagetypes.direction.default.content.show', '1', '1', '', 0, 1, 1, '2016-08-30 06:14:28', '2021-01-20 20:45:33'),
('607662cc-ee4e-4dc4-893e-171ee1ab21ec', 'resource.modules.article.pagetypes.link.default.panels.media.doc.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2012-11-19 18:34:12'),
('61975d07-954a-4a20-9837-53af8e3a3bfb', 'resource.modules.article.pagetypes.faq.default.moretext.show', '1', '1', '', 0, 1, 1, '2016-08-30 06:16:13', '2016-08-30 06:16:13'),
('61b3777b-bd5d-484a-90fb-08ff648edabf', 'resource.modules.article.pagetypes.faq.default.content.show', '1', '1', '', 0, 1, 1, '2016-08-30 06:14:28', '2021-01-20 20:45:33'),
('6249d41e-c1df-4663-a097-51ffa5f9b9d4', 'resource.modules.article.pagetypes.page.default.panels.menu.teaser512.show', '0', '0', '', 0, 1, 1, '2018-10-10 00:26:28', '2018-10-10 00:26:28'),
('6260db8c-2fdb-4fd5-a471-ce756ffdd384', 'resource.modules.article.pagetypes.frontpage.default.link.show', '0', '0', NULL, 0, 1, 1, '2016-08-30 06:45:27', '2016-08-30 06:45:27'),
('63ba6d1d-b240-47f9-af64-01b9bc7b82a1', 'resource.modules.menu.teaser.teaser16.order', '500', '500', NULL, 0, 1, 0, '2018-08-30 15:53:34', '2018-10-10 00:32:06'),
('643b5d1b-4608-48c9-b5ac-45d27f295b8f', 'resource.modules.article.pagetypes.direction.default.panels.menu.teaser4.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:12', '2018-10-10 00:47:59'),
('643be19a-649c-4de2-b6c9-6b4974f262e1', 'resource.modules.article.pagetypes.event.default.panels.options.show', '1', '1', '', 0, 1, 1, '2013-02-14 19:52:45', '2021-01-19 16:03:00'),
('64705660-33f4-4906-ab6e-1508d3b08faa', 'resource.modules.article.pagetypes.calendar.default.panels.menu.teaser2048.show', '0', '0', '', 0, 1, 1, '2018-10-10 00:27:01', '2018-10-10 00:27:01'),
('6514ed5d-44ee-4198-9ded-aa4763e9b03d', 'resource.modules.article.pagetypes.link.default.panels.formularconfig.show', '0', '0', '', 0, 1, 1, '2013-02-14 19:52:44', '2021-01-15 22:38:13'),
('65adee39-cfde-4a95-9b6b-31be1b55cca7', 'resource.modules.article.pagetypes.link.default.panels.media.backgroundimg.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2012-11-19 18:34:12'),
('6797bd93-38d6-4ca6-8166-af5aef05c7ea', 'resource.modules.menu.teaser.teaser2.order', '200', '200', NULL, 0, 1, 0, '2018-08-30 15:52:45', '2018-10-10 00:29:33'),
('67ead99a-0bfa-4015-af90-1e9266ad3580', 'resource.modules.article.pagetypes.faq.default.panels.menu.teaser16.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:12', '2021-01-20 21:23:15'),
('6b64605c-68a9-4f33-b842-50a036aa2aa4', 'resource.modules.article.pagetypes.event.default.panels.menu.teaser16.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:12', '2021-01-20 21:23:15'),
('6b76c8ee-b99a-4c96-acea-e04577648581', 'resource.modules.article.pagetypes.event.default.panels.menu.teaser1.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:12', '2018-10-10 00:46:19'),
('6bd861cb-aba2-46d6-a24f-9c307f0442a8', 'resource.modules.article.pagetypes.direction.default.has_teasertext2.show', '0', '0', '', 0, 1, 1, '2016-08-30 06:15:54', '2021-01-20 21:22:57'),
('6c1562c2-e22b-4bfc-a0be-9fd318bb42b2', 'app.maintenance', '0', '0', NULL, 0, 1, 0, '2019-04-18 12:06:54', '2021-04-21 19:59:29'),
('6d1ce848-dddd-4cf9-b307-7db0e2403012', 'resource.modules.article.pagetypes.calendar.default.content2.show', '0', '0', '', 0, 1, 1, '2016-08-30 06:15:03', '2016-08-30 06:15:03'),
('6d75865e-e328-497f-bbe4-89094bd7e497', 'resource.modules.article.pagetypes.faq.default.show', '1', '1', '', 1, 1, 1, '2012-11-22 15:55:54', '2016-08-26 03:35:14'),
('6e922ce4-22b3-4e2d-81c5-24b033a95ee7', 'resource.modules.article.pagetypes.direction.default.panels.media.backgroundimg.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2012-11-19 18:34:12'),
('6f0497b8-4b67-4abf-ae5c-51468e668a3f', 'resource.modules.article.pagetypes.calendar.default.panels.map.show', '0', '0', '', 0, 1, 1, '2013-02-14 19:52:44', '2021-06-27 21:01:23'),
('6f3873af-6d40-453f-9420-ed9ff92bb7a9', 'resource.modules.article.pagetypes.page.default.subline.show', '0', '0', NULL, 0, 1, 1, '2016-08-30 06:16:59', '2021-06-26 22:37:34'),
('6f81c9ad-e794-4b4f-841b-3236f8921768', 'resource.modules.article.pagetypes.frontpage.default.panels.menu.teaser4096.show', '0', '0', '', 0, 1, 1, '2018-10-10 00:27:21', '2018-10-10 00:27:21'),
('70bf9958-9fc8-462f-ad61-cd661983257e', 'resource.modules.article.pagetypes.direction.default.has_teasertext.show', '1', '1', '', 0, 1, 1, '2016-08-30 06:15:41', '2016-08-30 06:15:41'),
('70fb76c5-989d-461c-8731-0e83fbe8b4b4', 'resource.modules.article.pagetypes.calendar.default.panels.menu.teaser2.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:12', '2018-10-10 00:47:10'),
('71026af4-dcc6-4a79-8bf1-a9764ce97e62', 'resource.modules.article.pagetypes.event.default.panels.menu.teaser512.show', '0', '0', '', 0, 1, 1, '2018-10-10 00:26:28', '2018-10-10 00:26:28'),
('726bf97a-a940-43f2-8289-0749dbe53d2e', 'resource.modules.article.pagetypes.direction.default.panels.menu.teaser4096.show', '1', '1', '', 0, 1, 1, '2018-10-10 00:27:20', '2021-06-26 22:32:41'),
('728be660-d7c4-4f10-b48d-7440b53aa6f9', 'resource.modules.article.pagetypes.link.default.has_teasertext.show', '1', '1', '', 0, 1, 1, '2016-08-30 06:15:41', '2016-08-30 06:15:41'),
('733ef868-ba17-4753-b34a-a9901cd2b896', 'resource.modules.article.pagetypes.frontpage.element', '0', '0', '', 0, 1, 1, '2021-06-03 22:13:22', '2021-06-03 22:16:50'),
('73e5f1b8-63f4-4cc4-b1ca-68ba35dabc96', 'resource.modules.article.pagetypes.calendar.default.panels.seo.show', '1', '1', '', 0, 1, 1, '2013-02-14 19:52:45', '2013-02-14 19:52:45'),
('7410659c-901e-48a1-903a-78f3d8c79c9b', 'resource.modules.article.pagetypes.faq.element.panels.media.footerimg.show', '0', '0', '', 0, 1, 1, '2021-06-27 00:41:40', '2021-06-27 00:41:40'),
('741e569c-cc7a-4ab8-9e5c-3f9ffbf28cfb', 'resource.modules.article.pagetypes.direction.default.panels.media.download.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:12', '2014-02-20 09:43:48'),
('742d9882-a6c6-4888-863c-6c189e81d200', 'resource.modules.article.pagetypes.event.default.panels.menu.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:13', '2012-11-19 18:34:13');
INSERT INTO `configurations` (`id`, `name`, `value`, `default_value`, `description`, `is_resource`, `is_default`, `is_pageconfiguration`, `created`, `modified`) VALUES
('74974ccf-c79d-4ce3-b316-ea13f3e43192', 'resource.modules.article.pagetypes.link.default.panels.media.audio.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2012-11-19 18:34:12'),
('74b1ba9c-4e75-47a7-a7c3-14dc95683f14', 'resource.modules.article.pagetypes.calendar.default.panels.media.img.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2014-03-31 07:01:37'),
('77bd37c0-64c0-4add-b1c5-152122d2a9ac', 'resource.modules.menu.teaser.teaser1.order', '1400', '1400', NULL, 0, 1, 0, '2018-08-30 15:51:02', '2021-06-26 22:15:47'),
('78a37423-a970-4c03-a50c-a37474254d1b', 'resource.modules.article.pagetypes.calendar.default.panels.menu.teaser64.show', '0', '0', '', 0, 1, 1, '2013-08-01 09:49:50', '2013-08-01 09:49:50'),
('7949bd85-0f68-48c3-a8a3-a96352b5e1e8', 'resource.modules.article.pagetypes.link.default.panels.domain.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:13', '2012-11-19 18:34:13'),
('79e77980-a449-4e87-86d1-db3cc4b8ece3', 'resource.modules.article.pagetypes.calendar.default.panels.media.galerie.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2021-01-15 22:38:40'),
('79f6d706-4292-42d7-9139-23b9b74cdffc', 'resource.modules.article.pagetypes.calendar.default.content3.show', '0', '0', '', 0, 1, 1, '2021-06-26 22:39:25', '2021-06-26 22:39:25'),
('7a6e4b70-e26e-11ea-858d-eb31ca473684', 'auth.backend.after_logout_redirect_url', '/login', '/login', '', 0, 1, 0, '2020-08-20 00:51:36', '2020-08-20 00:51:36'),
('7b86e3b8-e77e-4237-898c-33e1e84f1c2d', 'resource.modules.article.pagetypes.faq.icon', 'fa fa-question', 'fa fa-question', '', 0, 1, 1, '2021-06-27 15:14:54', '2021-06-27 15:17:37'),
('7c28f5de-6845-40e4-bca1-7751338e56b7', 'resource.modules.article.pagetypes.link.default.panels.seo.show', '0', '0', '', 0, 1, 1, '2013-02-14 19:52:45', '2021-06-27 00:30:56'),
('7c9be483-66a3-4261-a40d-a4fa3eb9a626', 'resource.modules.article.pagetypes.event.default.panels.menu.teaser8192.show', '0', '0', '', 0, 1, 1, '2018-10-10 00:28:06', '2018-10-10 00:28:06'),
('7d072b88-daaf-4144-9692-b98f76f60d99', 'resource.modules.article.pagetypes.direction.default.panels.media.video.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2014-03-24 07:51:46'),
('7ddfeb51-8806-4b8b-a1a8-ca09def2d757', 'resource.modules.article.pagetypes.faq.default.panels.menu.teaser128.show', '0', '0', '', 0, 1, 1, '2013-08-01 09:49:50', '2013-08-01 09:49:50'),
('7e502751-3528-4ae9-9d2e-ca35b254b2ed', 'resource.modules.article.pagetypes.faq.default.panels.options.show', '1', '1', '', 0, 1, 1, '2013-02-14 19:52:45', '2021-01-19 16:03:00'),
('7fbe826e-c173-4ded-a03f-6c5f6c2092c5', 'resource.modules.article.pagetypes.direction.default.panels.menu.teaser16.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:12', '2021-01-20 21:23:15'),
('80129d17-fa27-4d2f-8c2e-93ef6ca29e50', 'resource.modules.article.pagetypes.direction.default.panels.menu.teaser8.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:12', '2021-01-20 21:23:05'),
('80bcc555-c653-4def-ab6c-85b36e04e3a3', 'resource.modules.article.pagetypes.page.default.panels.menu.teaser2048.show', '0', '0', '', 0, 1, 1, '2018-10-10 00:27:01', '2018-10-10 00:27:01'),
('827945b8-ab63-4646-a2bb-a18629a7da5d', 'resource.modules.article.pagetypes.link.default.panels.menu.teaser64.show', '0', '0', '', 0, 1, 1, '2013-08-01 09:49:50', '2013-08-01 09:49:50'),
('82f5b99b-87dc-4314-88d0-241dbb797f7a', 'resource.modules.article.pagetypes.faq.default.panels.menu.teaser64.show', '0', '0', '', 0, 1, 1, '2013-08-01 09:49:50', '2013-08-01 09:49:50'),
('838c2515-513a-4f23-9964-bc516bcf0a20', 'resource.modules.article.pagetypes.page.default.panels.menu.teaser8192.show', '0', '0', '', 0, 1, 1, '2018-10-10 00:28:06', '2018-10-10 00:28:06'),
('84f87727-b3d3-4c24-8d50-77fb4072a4cf', 'resource.modules.article.pagetypes.link.default.panels.menu.teaser128.show', '0', '0', '', 0, 1, 1, '2013-08-01 09:49:50', '2013-08-01 09:49:50'),
('856e20be-b8be-4331-8bfc-eec33a55f033', 'resource.modules.article.pagetypes.calendar.default.panels.menu.teaser512.show', '0', '0', '', 0, 1, 1, '2018-10-10 00:26:28', '2018-10-10 00:26:28'),
('85cbc393-6a58-42cc-8c30-c9c645cc639f', 'resource.modules.article.pagetypes.event.default.panels.menu.teaser2.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:12', '2018-10-10 00:47:10'),
('865d15b2-a81b-4c13-8eeb-a98c86a7d5c0', 'resource.modules.menu.teaser.teaser128.tooltip', '128', '128', NULL, 0, 1, 0, '2018-08-30 16:24:07', '2021-06-26 22:18:15'),
('878fe4e4-61c4-429d-a421-077346119bfc', 'resource.modules.article.pagetypes.event.default.panels.media.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:13', '2012-11-19 18:34:13'),
('8898521b-3647-498e-a544-a365e4e7423b', 'resource.modules.article.pagetypes.event.default.panels.media.backgroundimg.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2012-11-19 18:34:12'),
('88fb086a-93dd-4101-a1cf-cd73fb6c1279', 'resource.modules.article.pagetypes.event.default.panels.media.video.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2014-03-24 07:51:46'),
('899e4b7d-373a-4412-bc68-790f7ea5a23b', 'resource.modules.article.pagetypes.direction.default.panels.media.img.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2014-03-31 07:01:37'),
('89b85a7c-12fb-46de-a92a-67214ae331a7', 'resource.modules.article.pagetypes.link.default.panels.media.titleimg.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2021-06-27 00:35:18'),
('8a7fd04e-6b67-4b45-9d57-5d95963b33b1', 'resource.modules.menu.teaser.teaser4096.tooltip', 'Eintrag im Akkordeon / 4096', 'Eintrag im Akkordeon / 4096', NULL, 0, 1, 0, '2018-10-10 00:40:33', '2021-06-26 22:21:32'),
('8ab82865-953f-4b44-b64c-cbd95c6e6e80', 'resource.modules.article.pagetypes.frontpage.default.has_teasertext.show', '0', '0', NULL, 0, 1, 1, '2016-08-30 06:45:27', '2021-01-20 20:47:11'),
('8b0e93c2-0582-4e6d-8e50-31eed44a7b4a', 'resource.modules.newsletterinterest.show', '1', '1', NULL, 1, 1, 0, '2019-04-26 14:57:57', '2019-04-26 14:57:57'),
('8c26be97-4de4-46bc-b635-a951cae54111', 'resource.modules.article.pagetypes.direction.default.panels.formularconfig.show', '0', '0', '', 0, 1, 1, '2013-02-14 19:52:44', '2021-01-15 22:38:13'),
('8c34f957-d46c-4194-bd8c-59680a3ce8ce', 'resource.modules.article.pagetypes.event.default.panels.media.img2.show', '0', '0', '', 0, 1, 1, '2021-01-15 22:09:04', '2021-01-15 22:09:04'),
('8f9670e3-b2ad-4737-8c09-32346cf3e8de', 'resource.modules.article.pagetypes.link.default.has_teasertext2.show', '0', '0', '', 0, 1, 1, '2016-08-30 06:15:54', '2021-01-20 21:22:57'),
('8fd677ca-3d76-4b37-a297-8445e61e587f', 'resource.modules.article.pagetypes.faq.default.panels.media.backgroundimg.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2012-11-19 18:34:12'),
('923a21b8-47fb-4ea0-8386-1b881351f56f', 'resource.modules.article.pagetypes.faq.default.name', 'Standard', 'Standard', '', 0, 1, 1, '2012-08-09 10:42:35', '2021-06-26 22:58:15'),
('92cbe395-1c33-460f-8e6b-aaf021be5a62', 'resource.modules.article.pagetypes.event.default.content2.show', '0', '0', '', 0, 1, 1, '2016-08-30 06:15:03', '2016-08-30 06:15:03'),
('932482d8-f1b0-428b-a6d9-8dfce93d234f', 'resource.modules.article.pagetypes.link.default.moretext.show', '1', '1', '', 0, 1, 1, '2016-08-30 06:16:13', '2016-08-30 06:16:13'),
('93932b1b-377f-41f9-9400-47dbd6dcfa3d', 'resource.modules.menu.teaser.teaser8.order', '400', '400', NULL, 0, 1, 0, '2018-08-30 15:53:08', '2018-10-10 00:31:50'),
('93bdc84f-c070-42ea-9eb4-7a9a600a24b0', 'resource.modules.article.pagetypes.direction.default.panels.media.titleimg.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:12', '2012-11-19 18:34:12'),
('94a8d5b3-2845-4e1d-942e-af69bd15e743', 'resource.modules.menu.teaser.teaser512.show', '0', '0', NULL, 0, 1, 0, '2018-10-10 00:37:01', '2018-10-10 00:37:01'),
('94aebd60-e1a3-11ea-829f-b76b8e1c211f', 'auth.backend.credentials.username', 'login', 'login', '', 0, 1, 0, '2020-08-19 00:39:04', '2020-08-19 00:39:04'),
('94cdcf7d-05f3-42c9-980d-e73172228293', 'resource.modules.menu.teaser.teaser4096.name', 'Akkordeon', 'Akkordeon', NULL, 0, 1, 0, '2018-10-10 00:39:52', '2021-06-26 22:20:01'),
('96277b67-c83f-42d8-b8cf-a613c947fdd7', 'resource.modules.article.pagetypes.direction.default.price.show', '1', '1', '', 0, 1, 1, '2016-08-30 06:16:24', '2021-01-19 16:38:04'),
('965994f9-6bdf-4ed2-a00f-e89b953ce472', 'resource.modules.article.pagetypes.link.default.panels.menu.teaser8192.show', '0', '0', '', 0, 1, 1, '2018-10-10 00:28:06', '2018-10-10 00:28:06'),
('96cbd735-f3f3-4be1-b77b-b7613ce89f2e', 'resource.modules.article.pagetypes.frontpage.default.content3.show', '0', '0', '', 0, 1, 1, '2021-06-26 22:39:25', '2021-06-26 22:39:25'),
('97021a67-e2f9-49fd-9000-ab15286ab4a2', 'resource.modules.pageconfiguration.show', '1', '1', NULL, 1, 1, 0, '2018-03-11 23:36:08', '2018-03-16 21:09:04'),
('974ab860-77cd-4552-8945-9a3e05f6b37a', 'resource.modules.article.pagetypes.faq.default.panels.menu.teaser2.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:12', '2018-10-10 00:47:10'),
('987a1dc2-71e5-4195-a8fc-601c4c94f741', 'resource.modules.article.pagetypes.calendar.default.panels.media.img2.show', '0', '0', '', 0, 1, 1, '2021-01-15 22:09:04', '2021-01-15 22:09:04'),
('9970695b-05fc-4c9c-ba58-7e552781ce1c', 'resource.modules.article.pagetypes.link.default.panels.options.show', '1', '1', '', 0, 1, 1, '2013-02-14 19:52:45', '2021-01-19 16:03:00'),
('99e331a3-ff57-462d-b484-dce7105d7824', 'resource.modules.article.pagetypes.event.default.content3.show', '0', '0', '', 0, 1, 1, '2021-06-26 22:39:25', '2021-06-26 22:39:25'),
('99f35df1-23fb-4ca7-ba72-31643de77b87', 'resource.modules.article.pagetypes.calendar.default.panels.formularconfig.show', '0', '0', '', 0, 1, 1, '2013-02-14 19:52:44', '2021-01-15 22:38:13'),
('9b32f0f0-3b61-45ca-9126-ce3101833462', 'resource.modules.article.pagetypes.calendar.default.panels.menu.teaser8.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:12', '2021-01-20 21:23:05'),
('9b521d14-5f93-4c77-b6b1-f793f1136ed8', 'resource.modules.article.pagetypes.page.icon', 'fa fa-file', 'fa fa-file', '', 0, 1, 1, '2021-06-27 15:14:54', '2021-06-27 15:14:54'),
('9bc1bf3e-0589-4265-aa98-acbe69e27743', 'resource.modules.article.pagetypes.event.default.name', 'Standard', 'Standard', '', 0, 1, 1, '2012-08-09 10:42:35', '2016-08-26 04:20:26'),
('a0827dac-755d-4b2b-9cbf-778a13e5c60e', 'resource.modules.article.pagetypes.event.default.panels.media.doc.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2012-11-19 18:34:12'),
('a1becbbc-39e7-4365-9185-13d339599cfa', 'resource.modules.article.pagetypes.direction.default.panels.rights.show', '1', '1', '', 0, 1, 1, '2013-02-14 19:52:45', '2021-01-19 15:27:39'),
('a23e2220-a69c-4dc6-8616-925c82199116', 'resource.modules.article.pagetypes.direction.name', 'Anfahrt', 'Anfahrt', '', 0, 1, 1, '2016-08-26 03:59:28', '2021-06-29 00:35:43'),
('a25fa8de-cbb0-4ae3-aff3-cbc576a1851a', 'resource.modules.article.pagetypes.link.icon', 'fa fa-link', 'fa fa-link', '', 0, 1, 1, '2021-06-27 15:14:54', '2021-06-27 15:18:04'),
('a3b15a4b-2555-4478-bf8e-fc7c84f1dfc8', 'resource.modules.article.pagetypes.direction.default.panels.seo.show', '1', '1', '', 0, 1, 1, '2013-02-14 19:52:45', '2013-02-14 19:52:45'),
('a44839cc-bb6a-4dff-a274-6f827edbbeec', 'resource.modules.article.pagetypes.faq.default.panels.rights.show', '1', '1', '', 0, 1, 1, '2013-02-14 19:52:45', '2021-01-19 15:27:39'),
('a450b683-2681-414d-b6f8-24833fd2ae4f', 'resource.modules.article.panels.media.tabs.footerimg.name', 'Abschlussbild', 'Abschlussbild', '', 0, 1, 0, '2021-06-27 00:42:55', '2021-06-27 00:42:55'),
('a4ed03a8-cc5f-4a60-8e30-7963767cdf1f', 'resource.modules.menu.teaser.teaser32.tooltip', 'Listeneintrag / 32', 'Listeneintrag / 32', NULL, 0, 1, 0, '2018-08-30 16:23:47', '2021-06-26 22:21:41'),
('a538d66d-dd79-4e72-99b7-a36ec3412069', 'resource.modules.article.pagetypes.link.element', '0', '0', '', 0, 1, 1, '2021-06-03 22:13:22', '2021-06-03 22:16:37'),
('a5b48fca-2154-43d9-b1c4-13a2e30915ec', 'resource.modules.menu.teaser.teaser8.tooltip', '8', '8', NULL, 0, 1, 0, '2018-08-30 16:23:09', '2021-06-26 22:20:28'),
('a64a635e-f9dc-464e-9026-85e2d8f5120d', 'resource.modules.article.pagetypes.link.default.panels.menu.teaser1.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2021-06-27 00:36:09'),
('a682a311-6fdb-402c-a314-09493ba7f1b8', 'resource.modules.article.pagetypes.faq.default.panels.media.doc.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2012-11-19 18:34:12'),
('a68ffee0-575a-11eb-9f7c-ef95f066447e', 'resource.modules.article.panels.media.tabs.img2.name', 'Bilder (2)', 'Bilder (2)', '', 0, 1, 0, '2021-01-15 18:54:43', '2021-01-15 18:55:21'),
('a6a01e9c-9836-4054-aa8f-94cd2abe3e09', 'resource.modules.article.pagetypes.frontpage.default.topline.show', '1', '1', '', 0, 1, 1, '2018-08-30 18:32:06', '2021-01-20 20:40:39'),
('a6bf1980-e14b-11ea-89a4-9f26e80d2c8b', 'auth.defaultfrontend.credentials.username', 'login,communications.email', 'login,communications.email', '', 0, 1, 0, '2020-08-18 14:10:02', '2020-08-18 15:20:28'),
('a6cfa325-1ea9-4403-b359-f7bd60b430ef', 'resource.modules.menu.teaser.teaser1024.tooltip', '1024', '1024', NULL, 0, 1, 0, '2018-10-10 00:38:31', '2021-06-26 22:18:09'),
('a743745c-95cc-486d-b460-49617430f5f7', 'resource.modules.article.pagetypes.faq.default.link.show', '0', '0', '', 0, 1, 1, '2016-08-30 06:15:16', '2016-08-30 06:18:06'),
('a7c0682b-5cda-49fc-9f3a-6b229fffce42', 'resource.modules.article.pagetypes.link.default.name', 'Standard', 'Standard', '', 0, 1, 1, '2012-08-09 10:42:35', '2016-08-26 04:20:26'),
('a7c0f4ea-3d0f-4e1d-a122-aedb4666a499', 'resource.modules.article.pagetypes.faq.default.panels.seo.show', '1', '1', '', 0, 1, 1, '2013-02-14 19:52:45', '2013-02-14 19:52:45'),
('a8307217-cb03-40fd-b046-77a80b48c0b2', 'resource.modules.article.pagetypes.frontpage.icon', 'fa fa-star', 'fa fa-star', '', 0, 1, 1, '2021-06-27 15:14:54', '2021-06-27 15:17:46'),
('a8fbd88b-2514-47bf-afeb-c73802cca188', 'resource.modules.article.pagetypes.page.element.panels.media.footerimg.show', '0', '0', '', 0, 1, 1, '2021-06-27 00:41:40', '2021-06-27 00:41:40'),
('a960ae99-ae43-41e5-87d3-e4980713e563', 'resource.modules.article.pagetypes.direction.default.panels.menu.teaser64.show', '0', '0', '', 0, 1, 1, '2013-08-01 09:49:50', '2013-08-01 09:49:50'),
('ab0129e0-4954-48ca-bcba-682c2a3427e1', 'resource.modules.article.pagetypes.calendar.default.panels.media.img3.show', '0', '0', '', 0, 1, 1, '2021-01-15 22:09:24', '2021-01-15 22:11:21'),
('ab72c659-05e9-489e-98db-b7b3ebd4b887', 'resource.modules.article.topline.name', 'Topline', 'Topline', NULL, 0, 1, 0, '2018-08-30 18:31:43', '2018-08-30 18:31:43'),
('abe0b88e-074d-4dfb-bce8-fdf390664fdd', 'resource.modules.menu.teaser.teaser2048.tooltip', '2048', '2048', NULL, 0, 1, 0, '2018-10-10 00:39:17', '2021-06-26 22:19:13'),
('abe55722-f979-11df-a4c3-002421a2bcd6', 'app.articles.limit', '10', '10', NULL, 0, 1, 0, '2010-01-01 00:00:00', '2020-09-10 13:53:52'),
('abe55df8-f979-11df-a4c3-002421a2bcd6', 'app.cmsname', 'simple X CMS ™ - 4.0 - Beta 1', 'simple X CMS ™ - 4.0 - Beta 1', NULL, 0, 1, 0, '2010-01-01 00:00:00', '2021-01-15 18:38:44'),
('abe56406-f979-11df-a4c3-002421a2bcd6', 'app.calendar.range.default', '180', '180', NULL, 0, 1, 0, '2010-01-01 00:00:00', '2010-01-01 00:00:00'),
('abe56712-f979-11df-a4c3-002421a2bcd6', 'app.language.accept', 'de', 'de', NULL, 0, 1, 0, '2010-01-01 00:00:00', '2021-01-15 18:38:58'),
('abe56a28-f979-11df-a4c3-002421a2bcd6', 'app.language.defaultlanguage', 'de', 'de', NULL, 0, 1, 0, '2010-01-01 00:00:00', '2010-01-01 00:00:00'),
('abe56d3e-f979-11df-a4c3-002421a2bcd6', 'app.language.multilingual', '0', '0', NULL, 0, 1, 0, '2010-01-01 00:00:00', '2021-01-15 18:39:35'),
('abe58864-f979-11df-a4c3-002421a2bcd6', 'app.hassearch', '0', '0', NULL, 0, 1, 0, '2010-01-01 00:00:00', '2011-04-05 16:44:38'),
('abe59174-f979-11df-a4c3-002421a2bcd6', 'email.bcc', '', '', NULL, 0, 1, 0, '2010-01-01 00:00:00', '2013-05-31 07:31:36'),
('abe5b3e8-f979-11df-a4c3-002421a2bcd6', 'app.email.systemadmin', 'systemadmin@simple-x.de', 'systemadmin@simple-x.de', NULL, 0, 1, 0, '2010-01-01 00:00:00', '2013-05-28 23:11:56'),
('abe5c310-f979-11df-a4c3-002421a2bcd6', 'client.name', 'Kundenname', 'Kundenname', NULL, 0, 1, 0, '2010-01-01 00:00:00', '2013-05-31 07:30:03'),
('abe6cf26-f979-11df-a4c3-002421a2bcd6', 'resource.modules.article.pagetypes.frontpage.default.show', '1', '1', NULL, 1, 1, 1, '2010-01-01 00:00:00', '2016-08-26 03:35:31'),
('abe893e2-f979-11df-a4c3-002421a2bcd6', 'resource.modules.article.show', '1', '1', NULL, 1, 1, 0, '2010-01-01 00:00:00', '2016-08-26 03:36:10'),
('abe902b4-f979-11df-a4c3-002421a2bcd6', 'resource.modules.attachment.show', '1', '1', NULL, 1, 1, 0, '2010-01-01 00:00:00', '2010-01-01 00:00:00'),
('abe90cf0-f979-11df-a4c3-002421a2bcd6', 'resource.modules.configuration.show', '1', '1', NULL, 1, 1, 0, '2010-01-01 00:00:00', '2010-01-01 00:00:00'),
('abe9104c-f979-11df-a4c3-002421a2bcd6', 'resource.modules.crontask.show', '1', '1', NULL, 1, 1, 0, '2010-01-01 00:00:00', '2010-01-01 00:00:00'),
('abe9170e-f979-11df-a4c3-002421a2bcd6', 'resource.modules.menu.alias.show', '1', '1', NULL, 1, 1, 0, '2010-01-01 00:00:00', '2016-08-26 03:38:22'),
('abe91a7e-f979-11df-a4c3-002421a2bcd6', 'resource.modules.menu.domain.show', '1', '1', NULL, 1, 1, 0, '2010-01-01 00:00:00', '2016-08-26 03:38:17'),
('abe91dda-f979-11df-a4c3-002421a2bcd6', 'resource.modules.menu.menu.show', '1', '1', NULL, 1, 1, 0, '2010-01-01 00:00:00', '2010-01-01 00:00:00'),
('abe92140-f979-11df-a4c3-002421a2bcd6', 'resource.modules.newsletter.show', '1', '1', NULL, 1, 1, 0, '2010-01-01 00:00:00', '2019-05-02 16:09:02'),
('abe92820-f979-11df-a4c3-002421a2bcd6', 'resource.modules.newsletterrecipient.show', '1', '1', NULL, 1, 1, 0, '2010-01-01 00:00:00', '2010-01-01 00:00:00'),
('abe9322a-f979-11df-a4c3-002421a2bcd6', 'resource.modules.resource.show', '1', '1', NULL, 1, 1, 0, '2010-01-01 00:00:00', '2010-01-01 00:00:00'),
('abe9357c-f979-11df-a4c3-002421a2bcd6', 'resource.modules.role.show', '1', '1', NULL, 1, 1, 0, '2010-01-01 00:00:00', '2010-01-01 00:00:00'),
('abe938ce-f979-11df-a4c3-002421a2bcd6', 'resource.modules.text.show', '1', '1', NULL, 1, 1, 0, '2010-01-01 00:00:00', '2010-11-26 17:26:34'),
('abe93c52-f979-11df-a4c3-002421a2bcd6', 'resource.modules.user.show', '1', '1', NULL, 1, 1, 0, '2010-01-01 00:00:00', '2010-01-01 00:00:00'),
('aec1305c-2828-42e0-89ea-8da2d38b6c17', 'resource.modules.article.pagetypes.calendar.element.panels.media.footerimg.show', '0', '0', '', 0, 1, 1, '2021-06-27 00:41:41', '2021-06-27 00:41:41'),
('aeeeaf74-c114-4415-a72f-68abfdc852fa', 'resource.modules.article.pagetypes.event.default.panels.media.teaser.show', '1', '1', '', 0, 1, 1, '2014-01-21 06:40:03', '2014-01-21 06:40:03'),
('af3c5dd1-4ded-4248-8987-dcd0df20fc4e', 'resource.modules.article.pagetypes.link.default.panels.rights.show', '1', '1', '', 0, 1, 1, '2013-02-14 19:52:45', '2021-01-19 15:27:39'),
('af82e1a0-b20d-11e9-85ee-594256b1033b', 'resource.modules.menu.teaser.teaser1.value', '1', '1', '', 0, 1, 0, '2019-07-29 16:32:44', '2019-07-29 16:32:44'),
('b09454db-48d0-4498-a9d5-91736379d98f', 'resource.modules.article.content3.name', '3. Inhalt', '3. Inhalt', '', 0, 1, 0, '2021-06-27 21:07:08', '2021-06-27 21:07:08'),
('b0b64acb-32eb-4b39-b13d-4179be77892b', 'resource.modules.article.pagetypes.link.default.price.show', '1', '1', '', 0, 1, 1, '2016-08-30 06:16:24', '2021-06-27 00:32:52'),
('b222c3c9-587d-4149-83df-679ba7060fc8', 'resource.modules.article.pagetypes.frontpage.default.content.show', '1', '1', NULL, 0, 1, 1, '2016-08-30 06:45:27', '2021-01-20 21:22:01'),
('b2262ed4-0566-4d02-be17-90cccbb61952', 'resource.modules.article.pagetypes.calendar.name', 'Kalender', 'Kalender', '', 0, 1, 1, '2016-08-26 03:59:28', '2021-06-26 23:50:16'),
('b27add80-c0df-4b30-865d-c9f8988f41cc', 'resource.modules.article.pagetypes.page.default.link.show', '0', '0', NULL, 0, 1, 1, '2016-08-30 06:15:16', '2016-08-30 06:18:06'),
('b39e92ec-4049-47ad-9e70-9cec13727780', 'resource.modules.article.pagetypes.calendar.default.panels.media.video.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2014-03-24 07:51:46'),
('b46fc0bf-dd74-4c3c-b951-99bbcf8b571d', 'resource.modules.article.pagetypes.frontpage.element.panels.media.footerimg.show', '0', '0', '', 0, 1, 1, '2021-06-27 00:41:41', '2021-06-27 00:41:41'),
('b5fcfc13-c65c-4c11-8978-47cfcc5c5c93', 'resource.modules.article.pagetypes.faq.default.panels.media.titleimg.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:12', '2012-11-19 18:34:12'),
('b68a7966-9a93-44f3-99cf-5fa0b5cdf77d', 'resource.modules.article.pagetypes.link.default.content.show', '1', '1', '', 0, 1, 1, '2016-08-30 06:14:28', '2021-06-27 15:22:09'),
('b7296860-f033-4e7a-8763-68c89366f868', 'resource.modules.article.pagetypes.direction.default.panels.menu.teaser512.show', '0', '0', '', 0, 1, 1, '2018-10-10 00:26:28', '2018-10-10 00:26:28'),
('b964ea40-9eb5-4425-90bf-bb45cb75d2a5', 'resource.modules.article.pagetypes.direction.default.panels.media.img2.show', '0', '0', '', 0, 1, 1, '2021-01-15 22:09:04', '2021-01-15 22:09:04'),
('b9c130c4-2247-4132-8e2d-df61d1b4992f', 'resource.modules.article.pagetypes.direction.default.panels.event.show', '0', '0', '', 0, 1, 1, '2014-03-17 11:27:32', '2016-08-30 08:01:38'),
('ba0117e3-bb3f-4d4c-afc7-e7e85d2f60ac', 'resource.modules.article.pagetypes.direction.default.panels.media.teaser.show', '1', '1', '', 0, 1, 1, '2014-01-21 06:40:03', '2014-01-21 06:40:03'),
('ba48fb31-2f55-45e5-82dd-07b3199f6774', 'resource.modules.article.pagetypes.calendar.default.panels.media.footerimg.show', '0', '0', '', 0, 1, 1, '2021-06-27 00:41:41', '2021-06-27 00:41:41'),
('bad37c89-c407-426c-8e8d-5224f6c9779d', 'resource.modules.article.pagetypes.event.default.has_teasertext2.show', '0', '0', '', 0, 1, 1, '2016-08-30 06:15:54', '2021-01-20 21:22:57'),
('bb56924e-2e33-40df-b809-70d760e2a414', 'resource.modules.article.pagetypes.link.default.panels.menu.teaser32.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:13', '2021-06-27 00:35:33'),
('bbd5a2d0-b20d-11e9-85ee-594256b1033b', 'resource.modules.menu.teaser.teaser2.value', '2', '2', '', 0, 1, 0, '2019-07-29 16:32:51', '2019-07-29 16:32:51'),
('bcaf8c65-20d4-49f7-88ce-357c008e4ee7', 'resource.modules.article.pagetypes.page.default.panels.menu.teaser4096.show', '1', '1', '', 0, 1, 1, '2018-10-10 00:27:20', '2021-06-26 22:32:41'),
('bd261003-6a40-4793-b6cc-8bb51eadd97a', 'resource.modules.article.pagetypes.faq.default.price.show', '1', '1', '', 0, 1, 1, '2016-08-30 06:16:24', '2021-01-19 16:38:04'),
('bda847aa-c584-4fe0-9bea-42727bc8facc', 'resource.modules.article.pagetypes.calendar.default.topline.show', '0', '0', '', 0, 1, 1, '2018-08-30 18:32:07', '2021-06-26 22:37:28'),
('be89bd79-99b0-40e6-95d8-3bda9c39691b', 'resource.modules.article.pagetypes.direction.default.show', '1', '1', '', 1, 1, 1, '2012-11-22 15:55:54', '2016-08-26 03:35:14'),
('bf4a435a-9154-4568-8328-6c306a86a750', 'resource.modules.article.pagetypes.calendar.default.panels.menu.teaser256.show', '0', '0', '', 0, 1, 1, '2013-08-01 09:49:50', '2013-08-01 09:49:50'),
('bf84da1d-8b19-436c-9d13-da42bcd7da19', 'resource.modules.article.pagetypes.calendar.default.panels.options.show', '1', '1', '', 0, 1, 1, '2013-02-14 19:52:45', '2021-01-19 16:03:00'),
('bf8a6d70-b20d-11e9-85ee-594256b1033b', 'resource.modules.menu.teaser.teaser4.value', '4', '4', '', 0, 1, 0, '2019-07-29 16:32:57', '2019-07-29 16:32:57'),
('c069424a-e064-48f9-949d-5c5e1bdbc553', 'resource.modules.article.pagetypes.faq.default.subline.show', '0', '0', '', 0, 1, 1, '2016-08-30 06:16:59', '2021-06-26 22:37:34'),
('c2d218f0-74aa-48c7-b315-cf962925b4fd', 'resource.modules.article.pagetypes.frontpage.default.panels.media.img2.show', '0', '0', '', 0, 1, 1, '2021-01-15 22:09:04', '2021-01-15 22:09:04'),
('c32e02c6-7140-4292-b841-654a4c4c4409', 'resource.modules.article.pagetypes.direction.default.subline.show', '0', '0', '', 0, 1, 1, '2016-08-30 06:16:59', '2021-06-26 22:37:34'),
('c3605117-fbd9-4d1a-8842-4b8721e34d1b', 'resource.modules.menu.teaser.teaser1024.order', '1100', '1100', NULL, 0, 1, 0, '2018-10-10 00:38:19', '2018-10-10 00:38:19'),
('c3977de9-f98a-4f23-9d8d-df76e0dcb33a', 'resource.modules.menu.teaser.teaser2048.name', 'Abschnitt 2', 'Abschnitt 2', NULL, 0, 1, 0, '2018-10-10 00:38:55', '2018-10-10 00:38:55'),
('c3a796d0-b20d-11e9-85ee-594256b1033b', 'resource.modules.menu.teaser.teaser8.value', '8', '8', '', 0, 1, 0, '2019-07-29 16:33:10', '2019-07-29 16:33:10'),
('c3fb1d8b-c415-4b81-8d29-8d37e0cfbdba', 'resource.modules.article.pagetypes.page.default.has_teasertext2.show', '0', '0', NULL, 0, 1, 1, '2016-08-30 06:15:54', '2021-01-20 21:22:57'),
('c56f26e0-2465-4426-9fde-c3a046dc30d6', 'resource.modules.article.pagetypes.page.default.moretext.show', '1', '1', NULL, 0, 1, 1, '2016-08-30 06:16:13', '2016-08-30 06:16:13'),
('c5ed52b0-575a-11eb-9f7c-ef95f066447e', 'resource.modules.article.panels.media.tabs.img2.order', '400', '400', '', 0, 1, 0, '2021-01-15 18:55:10', '2021-01-15 18:55:10'),
('c6559362-4fac-4b78-95dd-c7f6fd4d3352', 'resource.modules.article.pagetypes.page.default.teaser.show', '1', '1', NULL, 0, 1, 1, '2016-08-30 06:15:28', '2016-08-30 06:15:28'),
('c681b411-fd2d-4a61-9067-fd888e295dc2', 'resource.modules.article.pagetypes.calendar.default.has_teasertext2.show', '0', '0', '', 0, 1, 1, '2016-08-30 06:15:54', '2021-01-20 21:22:57'),
('c6c1f873-6005-42ca-bbef-01e05e1a1574', 'resource.modules.article.pagetypes.link.default.show', '1', '1', '', 1, 1, 1, '2012-11-22 15:55:54', '2016-08-26 03:35:14'),
('c739b820-1635-4b42-a4a8-231af58aff55', 'resource.modules.article.pagetypes.calendar.default.panels.media.audio.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2012-11-19 18:34:12'),
('c73d231f-c045-49f4-a14f-e639b746acc5', 'resource.modules.article.pagetypes.faq.default.panels.media.img3.show', '0', '0', '', 0, 1, 1, '2021-01-15 22:09:24', '2021-01-15 22:11:21'),
('c965af42-ef44-430e-9107-8c788fdcf11c', 'resource.modules.article.pagetypes.page.name', 'Seite', 'Seite', NULL, 0, 1, 1, '2016-08-26 03:59:28', '2016-08-30 06:47:56'),
('c9da20e6-0e20-4c56-bdd2-f35018f6e02a', 'resource.modules.article.pagetypes.faq.default.panels.menu.teaser512.show', '0', '0', '', 0, 1, 1, '2018-10-10 00:26:28', '2018-10-10 00:26:28'),
('ca254233-e6df-4a15-bfb6-7fd72172c989', 'resource.modules.menu.teaser.teaser4096.show', '1', '1', NULL, 0, 1, 0, '2018-10-10 00:40:04', '2018-10-10 00:40:04'),
('cacabad5-73ec-4083-bdce-00f699ead2ae', 'resource.modules.article.pagetypes.link.default.panels.media.img.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2014-03-31 07:01:37'),
('cade3530-b20d-11e9-85ee-594256b1033b', 'resource.modules.menu.teaser.teaser16.value', '16', '16', '', 0, 1, 0, '2019-07-29 16:33:17', '2019-07-29 16:33:17'),
('cbe59d18-511c-46a4-97bc-b5b5ad4d0b1c', 'resource.modules.article.pagetypes.faq.default.panels.menu.teaser1.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:12', '2018-10-10 00:46:19'),
('cc811e5c-6b26-4aa4-9ce0-e1bf39702b36', 'resource.modules.article.pagetypes.direction.default.topline.show', '0', '0', '', 0, 1, 1, '2018-08-30 18:32:07', '2021-06-26 22:37:28'),
('cef9f4fa-85fc-4210-a14f-0a55f4227d11', 'resource.modules.article.pagetypes.event.default.panels.menu.teaser64.show', '0', '0', '', 0, 1, 1, '2013-08-01 09:49:50', '2013-08-01 09:49:50'),
('cf2024a0-b20d-11e9-85ee-594256b1033b', 'resource.modules.menu.teaser.teaser32.value', '32', '32', '', 0, 1, 0, '2019-07-29 16:33:28', '2019-07-29 16:33:28'),
('cfeae3a7-9874-4e20-8b3b-6981495f99c9', 'resource.modules.article.pagetypes.frontpage.name', 'Startseite', 'Startseite', NULL, 0, 1, 1, '2016-08-26 03:59:28', '2016-08-30 06:46:23'),
('d0d839d6-80d9-43fc-973f-a8ed0ab24dda', 'resource.modules.article.pagetypes.calendar.default.panels.domain.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:13', '2012-11-19 18:34:13'),
('d0e45020-e148-11ea-89a4-9f26e80d2c8b', 'auth.defaultfrontend.credentials.active', '1', '1', '', 0, 1, 0, '2020-08-18 13:49:31', '2020-08-18 13:49:31'),
('d0f4de51-0822-4560-b8b7-df277d8655f1', 'resource.modules.menu.teaser.teaser128.order', '800', '800', NULL, 0, 1, 0, '2018-08-30 15:54:19', '2018-10-10 00:35:05'),
('d2b82145-1428-4aa0-a28c-7c68e11e9e82', 'resource.modules.article.pagetypes.calendar.default.panels.media.titleimg.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:12', '2012-11-19 18:34:12'),
('d2d5b44a-ba4c-4ac8-8352-9ee4a8ade369', 'resource.modules.article.pagetypes.event.default.show', '1', '1', '', 1, 1, 1, '2012-11-22 15:55:54', '2016-08-26 03:35:14'),
('d32422ae-c6b8-43c8-be16-d1fc19a5216e', 'resource.modules.article.pagetypes.frontpage.default.content2.show', '0', '0', NULL, 0, 1, 1, '2016-08-30 06:45:27', '2016-08-30 06:45:27'),
('d3b05fb8-034c-4a15-87c1-133dfc5a42ba', 'resource.modules.article.pagetypes.direction.default.panels.media.footerimg.show', '0', '0', '', 0, 1, 1, '2021-06-27 00:41:41', '2021-06-27 00:41:41'),
('d3b93059-7e8b-4003-a76d-a3405d1a267f', 'resource.modules.article.pagetypes.faq.default.panels.menu.teaser8.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:12', '2021-01-20 21:23:05'),
('d439e1aa-e2ee-4c3e-a242-3fb80cca2912', 'resource.modules.article.pagetypes.direction.default.panels.menu.teaser8192.show', '0', '0', '', 0, 1, 1, '2018-10-10 00:28:06', '2018-10-10 00:28:06'),
('d51830f1-70c6-464a-a447-e067420f98c9', 'resource.modules.article.pagetypes.faq.default.panels.menu.teaser2048.show', '0', '0', '', 0, 1, 1, '2018-10-10 00:27:01', '2018-10-10 00:27:01'),
('d54e93fa-1259-40c5-95c4-8c92e05aa8aa', 'resource.modules.article.pagetypes.faq.default.panels.media.img.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2014-03-31 07:01:37'),
('d5c72d30-b20d-11e9-85ee-594256b1033b', 'resource.modules.menu.teaser.teaser64.value', '64', '64', '', 0, 1, 0, '2019-07-29 16:33:36', '2019-07-29 16:33:36'),
('d63a7c78-962e-4f48-940a-8567aa90ede4', 'resource.modules.article.pagetypes.direction.default.panels.menu.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:13', '2012-11-19 18:34:13'),
('d7b727f0-575a-11eb-9f7c-ef95f066447e', 'resource.modules.article.panels.media.tabs.img3.name', 'Bilder (3)', 'Bilder (3)', '', 0, 1, 0, '2021-01-15 18:56:29', '2021-01-15 18:56:37'),
('d80a34f3-7b88-4a7c-a261-afa02086307c', 'resource.modules.article.pagetypes.frontpage.default.teaser.show', '0', '0', NULL, 0, 1, 1, '2016-08-30 06:45:27', '2021-01-20 20:46:21'),
('da0818dd-4b11-4e30-a308-d0470b7f46d6', 'resource.modules.article.pagetypes.direction.default.panels.media.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:13', '2012-11-19 18:34:13'),
('da31defd-1963-48b5-aa5e-fa9706be8e0b', 'resource.modules.article.pagetypes.faq.default.panels.menu.teaser256.show', '0', '0', '', 0, 1, 1, '2013-08-01 09:49:50', '2013-08-01 09:49:50'),
('da350ea0-b20d-11e9-85ee-594256b1033b', 'resource.modules.menu.teaser.teaser128.value', '128', '128', '', 0, 1, 0, '2019-07-29 16:33:44', '2019-07-29 16:33:44'),
('da8d0fa9-7560-40bd-a8f6-745d8770e937', 'resource.modules.article.pagetypes.event.default.moretext.show', '1', '1', '', 0, 1, 1, '2016-08-30 06:16:13', '2016-08-30 06:16:13'),
('dce6ec7a-60b3-4bb5-914a-bcf17aaa589c', 'resource.modules.article.pagetypes.calendar.default.panels.event.show', '0', '0', '', 0, 1, 1, '2014-03-17 11:27:32', '2016-08-30 08:01:38'),
('dd737643-a672-41ab-8b6f-7ba7d7e9c7de', 'resource.modules.article.pagetypes.event.name', 'Event', 'Event', '', 0, 1, 1, '2016-08-26 03:59:28', '2021-06-26 23:58:57'),
('dd91e5ab-17b3-4304-8943-a160c8687385', 'resource.modules.article.pagetypes.page.default.panels.media.img3.show', '0', '0', '', 0, 1, 1, '2021-01-15 22:09:24', '2021-01-15 22:11:21'),
('ddba08f6-d973-462f-9909-db6103df65b5', 'resource.modules.article.pagetypes.event.default.panels.menu.teaser4096.show', '1', '1', '', 0, 1, 1, '2018-10-10 00:27:20', '2021-06-26 22:32:41'),
('dee4db10-b20d-11e9-85ee-594256b1033b', 'resource.modules.menu.teaser.teaser256.value', '256', '256', '', 0, 1, 0, '2019-07-29 16:33:52', '2019-07-29 16:33:52'),
('df375009-20f2-4b0a-a79d-30848ce878c4', 'resource.modules.article.pagetypes.calendar.default.panels.media.doc.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2012-11-19 18:34:12'),
('df8b0c3d-b090-484e-ae1f-b2275fdee139', 'resource.modules.article.pagetypes.event.default.panels.menu.teaser4.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:12', '2018-10-10 00:47:59'),
('dfbe914b-006f-475f-9693-b61b6002231c', 'resource.modules.article.pagetypes.direction.icon', 'fa fa-map-marker', 'fa fa-map-marker', '', 0, 1, 1, '2021-06-27 15:14:54', '2021-06-27 15:21:23'),
('dfbf4d2f-aa88-4169-8977-82603d4e068e', 'resource.modules.article.pagetypes.link.default.teaser.show', '1', '1', '', 0, 1, 1, '2016-08-30 06:15:28', '2016-08-30 06:15:28'),
('e01e7a4c-38e1-4f33-ab18-df21a729d5e7', 'resource.modules.menu.teaser.teaser1024.show', '1', '1', NULL, 0, 1, 0, '2018-10-10 00:37:53', '2018-10-10 00:37:53'),
('e06f9811-839a-40c3-89f8-9d097db0322e', 'resource.modules.article.pagetypes.calendar.default.panels.media.backgroundimg.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2012-11-19 18:34:12'),
('e0a08e1d-d3c2-45f5-a565-bb04337d2804', 'resource.modules.article.pagetypes.page.default.content2.show', '0', '0', NULL, 0, 1, 1, '2016-08-30 06:15:03', '2016-08-30 06:15:03'),
('e1a4a689-4125-41e8-b51f-e7c42fcfad32', 'resource.modules.article.pagetypes.direction.default.panels.menu.teaser1.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:12', '2018-10-10 00:46:19'),
('e1d7b90e-3972-49cd-bff6-d72a45ae227d', 'resource.modules.article.pagetypes.page.default.topline.show', '0', '0', '', 0, 1, 1, '2018-08-30 18:32:07', '2021-06-26 22:37:28'),
('e22c8adc-ad40-45b3-b791-51ec434ce2a5', 'resource.modules.article.pagetypes.direction.element.panels.media.footerimg.show', '0', '0', '', 0, 1, 1, '2021-06-27 00:41:41', '2021-06-27 00:41:41'),
('e2d28c06-1ad4-48f4-a4af-bcdd4c4eea9d', 'resource.modules.article.pagetypes.link.default.panels.media.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:13', '2012-11-19 18:34:13'),
('e2e31810-8cdc-462f-adc3-69813da9af5a', 'resource.modules.article.pagetypes.event.default.content.show', '1', '1', '', 0, 1, 1, '2016-08-30 06:14:28', '2021-01-20 20:45:33'),
('e2f8358e-e0da-4f13-9332-b8bb5a04ca56', 'resource.modules.article.pagetypes.faq.default.panels.menu.teaser4096.show', '1', '1', '', 0, 1, 1, '2018-10-10 00:27:20', '2021-06-26 22:32:41'),
('e307029b-be49-41ea-bed3-e0db3372289c', 'resource.modules.article.pagetypes.frontpage.default.panels.media.img3.show', '0', '0', '', 0, 1, 1, '2021-01-15 22:09:24', '2021-01-15 22:10:51'),
('e32f0615-e30d-4dbe-a19e-45334eb84d9d', 'resource.modules.article.pagetypes.faq.default.panels.domain.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:13', '2012-11-19 18:34:13'),
('e335606b-2f0e-43da-a90a-9237875f27f3', 'resource.modules.article.pagetypes.calendar.element', '0', '0', '', 0, 1, 1, '2021-06-03 22:13:22', '2021-06-03 22:16:37'),
('e369b355-74c6-47a3-9614-51c1f7c27c8d', 'resource.modules.menu.teaser.teaser4096.order', '1300', '1300', NULL, 0, 1, 0, '2018-10-10 00:41:03', '2018-10-10 00:41:03'),
('e3ae22f0-b20d-11e9-85ee-594256b1033b', 'resource.modules.menu.teaser.teaser512.value', '512', '512', '', 0, 1, 0, '2019-07-29 16:34:00', '2019-07-29 16:34:00'),
('e3f056a9-2c8c-47da-ab75-45536c5196ac', 'resource.modules.article.pagetypes.direction.default.panels.media.img3.show', '0', '0', '', 0, 1, 1, '2021-01-15 22:09:24', '2021-01-15 22:11:21'),
('e407939f-d3b5-49df-8ee4-2b551c1cca03', 'resource.modules.article.pagetypes.direction.default.panels.menu.teaser32.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:13', '2021-06-26 22:31:32'),
('e5ac730e-cebc-4884-b350-995f2b655a5f', 'resource.modules.article.pagetypes.link.default.panels.menu.teaser2048.show', '0', '0', '', 0, 1, 1, '2018-10-10 00:27:01', '2018-10-10 00:27:01'),
('e5f2f294-e288-4e02-8069-ada1d5c9f567', 'resource.modules.article.pagetypes.event.default.teaser.show', '1', '1', '', 0, 1, 1, '2016-08-30 06:15:28', '2016-08-30 06:15:28'),
('e6259123-afda-4b1e-9284-5eb27e067441', 'resource.modules.article.pagetypes.event.default.panels.rights.show', '1', '1', '', 0, 1, 1, '2013-02-14 19:52:45', '2021-01-19 15:27:39'),
('e782097c-c771-41ec-b53a-e78a53652a69', 'resource.modules.article.pagetypes.event.default.topline.show', '0', '0', '', 0, 1, 1, '2018-08-30 18:32:07', '2021-06-26 22:37:28'),
('e78635da-1527-41cb-b1d5-3f2bdb29b29d', 'resource.modules.article.pagetypes.event.default.has_teasertext.show', '1', '1', '', 0, 1, 1, '2016-08-30 06:15:41', '2016-08-30 06:15:41'),
('e798a268-73c2-4108-b618-b53eee083fe1', 'resource.modules.menu.teaser.teaser64.tooltip', '64', '64', NULL, 0, 1, 0, '2018-08-30 16:24:00', '2021-06-26 22:20:21'),
('e7ff796d-faac-4f30-8b39-40b7a7ef2f3c', 'resource.modules.article.pagetypes.calendar.default.price.show', '1', '1', '', 0, 1, 1, '2016-08-30 06:16:24', '2021-01-19 16:38:04'),
('e8b473f3-72fb-4fc0-b447-c809b25ecff8', 'resource.modules.article.pagetypes.link.default.panels.media.img3.show', '0', '0', '', 0, 1, 1, '2021-01-15 22:09:24', '2021-01-15 22:11:21'),
('e8e1c560-b20d-11e9-85ee-594256b1033b', 'resource.modules.menu.teaser.teaser1024.value', '1024', '1024', '', 0, 1, 0, '2019-07-29 16:34:09', '2019-07-29 16:34:09'),
('e92f4bba-d3cd-4b06-882d-aa830e91fd50', 'resource.modules.article.pagetypes.page.default.panels.media.img2.show', '0', '0', '', 0, 1, 1, '2021-01-15 22:09:04', '2021-01-15 22:09:04'),
('e97cfe50-a845-4b23-b90d-7b504b22bae5', 'resource.modules.article.pagetypes.event.default.panels.seo.show', '1', '1', '', 0, 1, 1, '2013-02-14 19:52:45', '2013-02-14 19:52:45'),
('e9f94bcb-79af-4c1e-b161-203bfdfc5571', 'resource.modules.article.pagetypes.faq.default.panels.menu.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:13', '2012-11-19 18:34:13'),
('ec6164fb-92db-4a89-8b96-cd4dafb8d39f', 'resource.modules.menu.teaser.teaser64.order', '700', '700', NULL, 0, 1, 0, '2018-08-30 15:54:00', '2018-10-10 00:34:28'),
('ec7789a2-3fd7-40e9-ac1d-bb03dc3f1ccc', 'resource.modules.article.pagetypes.faq.default.panels.menu.teaser4.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:12', '2018-10-10 00:47:59'),
('ee2916e0-b20d-11e9-85ee-594256b1033b', 'resource.modules.menu.teaser.teaser2048.value', '2048', '2048', '', 0, 1, 0, '2019-07-29 16:34:22', '2019-07-29 16:34:22'),
('ee2ced0d-ee85-4d74-8cbe-0d3824cdcc31', 'resource.modules.article.pagetypes.frontpage.default.panels.menu.teaser1024.show', '0', '0', '', 0, 1, 1, '2018-10-10 00:26:45', '2018-10-10 00:26:45'),
('eefefebc-a254-4c0b-af36-bfff03f9d80e', 'resource.modules.menu.teaser.teaser32.order', '600', '600', NULL, 0, 1, 0, '2018-08-30 15:53:44', '2018-10-10 00:45:10'),
('ef46c455-4897-4e75-8040-f5dcb3f50706', 'resource.modules.menu.teaser.teaser4.tooltip', '4', '4', NULL, 0, 1, 0, '2018-08-30 16:22:41', '2021-06-26 22:19:36'),
('efd5b858-90ab-4894-94cb-f211f736288e', 'resource.modules.article.pagetypes.link.default.panels.menu.teaser2.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2021-06-27 00:35:41'),
('f0257191-141b-42a4-8e26-ef2629badc58', 'resource.modules.article.pagetypes.faq.default.panels.media.teaser.show', '1', '1', '', 0, 1, 1, '2014-01-21 06:40:03', '2014-01-21 06:40:03'),
('f0ecf277-494f-44d8-a5cd-801314c4c779', 'resource.modules.article.pagetypes.faq.default.panels.media.img2.show', '0', '0', '', 0, 1, 1, '2021-01-15 22:09:04', '2021-01-15 22:09:04'),
('f242d01d-014f-47e0-90fc-ec72337bfdd1', 'resource.modules.article.pagetypes.direction.element', '0', '0', '', 0, 1, 1, '2021-06-03 22:13:22', '2021-06-03 22:16:37'),
('f25a0984-6907-4742-9b1c-5f4350e82da9', 'resource.modules.article.pagetypes.calendar.default.teaser.show', '1', '1', '', 0, 1, 1, '2016-08-30 06:15:28', '2016-08-30 06:15:28'),
('f2ad7235-8692-41fe-8c70-b331a45d063c', 'resource.modules.article.pagetypes.calendar.default.link.show', '0', '0', '', 0, 1, 1, '2016-08-30 06:15:16', '2016-08-30 06:18:06'),
('f2eda70e-60ba-4738-a962-6a3180158713', 'resource.modules.article.pagetypes.calendar.default.panels.media.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:13', '2012-11-19 18:34:13'),
('f2fbafb8-13d5-4624-91c3-cdee890e546e', 'resource.modules.article.pagetypes.page.default.price.show', '1', '1', NULL, 0, 1, 1, '2016-08-30 06:16:24', '2021-01-19 16:38:04'),
('f308ea05-fc3d-4a99-a513-a54482931a2f', 'resource.modules.article.pagetypes.link.default.panels.menu.teaser16.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2021-06-27 00:35:47'),
('f3423500-2cd3-4237-ae03-60dac3537ef5', 'resource.modules.article.pagetypes.frontpage.default.price.show', '0', '0', NULL, 0, 1, 1, '2016-08-30 06:45:27', '2021-01-20 20:40:58'),
('f3ec178e-8db2-43da-abce-2433ef1b4315', 'resource.modules.article.pagetypes.event.default.panels.event.show', '1', '1', '', 0, 1, 1, '2014-03-17 11:27:32', '2021-06-27 00:12:58'),
('f46e0f90-e148-11ea-89a4-9f26e80d2c8b', 'auth.defaultfrontend.credentials.approved', '1', '1', '', 0, 1, 0, '2020-08-18 13:50:17', '2020-08-18 13:50:17'),
('f47ed4e3-94ff-4df5-bd54-768e6b718dda', 'resource.modules.article.pagetypes.frontpage.default.subline.show', '1', '1', NULL, 0, 1, 1, '2016-08-30 06:45:27', '2021-01-19 01:32:06'),
('f5b429c4-1e64-4246-88fa-d3b3669eca7c', 'resource.modules.article.pagetypes.page.default.has_teasertext.show', '1', '1', NULL, 0, 1, 1, '2016-08-30 06:15:41', '2016-08-30 06:15:41'),
('f5dc4ab0-370a-4c2b-b05c-c010b40c28a8', 'resource.modules.article.pagetypes.link.default.content2.show', '0', '0', '', 0, 1, 1, '2016-08-30 06:15:03', '2016-08-30 06:15:03'),
('f610b5c0-b20d-11e9-85ee-594256b1033b', 'resource.modules.menu.teaser.teaser4096.value', '4096', '4096', '', 0, 1, 0, '2019-07-29 16:34:32', '2019-08-02 17:47:32'),
('f64f4a73-310b-41cc-9930-54408efbdb71', 'resource.modules.article.pagetypes.faq.default.panels.media.video.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2014-03-24 07:51:46'),
('f6669138-1f1d-44a1-a745-51eb46964a69', 'resource.modules.menu.teaser.teaser16.tooltip', '16', '16', NULL, 0, 1, 0, '2018-08-30 16:23:34', '2021-06-26 22:17:51'),
('f7466537-c7ff-4936-9b19-e26f67936d0a', 'resource.modules.article.pagetypes.event.default.price.show', '1', '1', '', 0, 1, 1, '2016-08-30 06:16:24', '2021-01-19 16:38:04'),
('f74dbf16-5d0c-4aa4-b9b5-503fc71d2ad4', 'resource.modules.article.pagetypes.direction.default.content3.show', '0', '0', '', 0, 1, 1, '2021-06-26 22:39:25', '2021-06-26 22:39:25'),
('f7c41edc-87cf-40cf-bac9-efb589ca960c', 'resource.modules.article.pagetypes.link.default.panels.menu.teaser1024.show', '0', '0', '', 0, 1, 1, '2018-10-10 00:26:46', '2021-06-27 00:35:54'),
('f87281dd-2a9f-4926-9830-128def029648', 'resource.modules.article.pagetypes.faq.default.panels.media.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:13', '2012-11-19 18:34:13'),
('f8cf4eee-c134-4a1b-a42d-3b82ad14aa2e', 'resource.modules.article.pagetypes.event.default.panels.menu.teaser1024.show', '1', '1', '', 0, 1, 1, '2018-10-10 00:26:46', '2021-01-15 22:40:44'),
('f8e6e32b-bb20-4df0-ad43-d994e7e28088', 'resource.modules.article.pagetypes.link.default.panels.menu.show', '1', '1', '', 0, 1, 1, '2012-11-19 18:34:13', '2012-11-19 18:34:13'),
('f9980112-367b-4d48-a6f1-040fba0f259e', 'resource.modules.article.pagetypes.calendar.default.panels.menu.teaser8192.show', '0', '0', '', 0, 1, 1, '2018-10-10 00:28:06', '2018-10-10 00:28:06'),
('fa1c877f-3385-47fd-9188-52b89d8d53e2', 'resource.modules.article.pagetypes.direction.default.panels.media.audio.show', '0', '0', '', 0, 1, 1, '2012-11-19 18:34:12', '2012-11-19 18:34:12'),
('fa3d0c80-f3b0-11ea-9b77-0b089fb1f902', 'identifier.registerdefault.authconfig', 'auth.defaultfrontend', 'auth.defaultfrontend', '', 0, 1, 0, '2020-09-11 00:00:22', '2020-09-11 00:00:54'),
('fa5905c8-47d5-4b2c-b4f0-3b69b99ca8b9', 'resource.modules.article.pagetypes.direction.default.moretext.show', '1', '1', '', 0, 1, 1, '2016-08-30 06:16:13', '2016-08-30 06:16:13'),
('fea786bf-e073-45f8-836b-376582b8e18e', 'resource.modules.article.pagetypes.direction.default.panels.menu.teaser128.show', '0', '0', '', 0, 1, 1, '2013-08-01 09:49:50', '2013-08-01 09:49:50'),
('ff1f8800-2efd-4295-995d-2e2c8205d52f', 'resource.modules.article.pagetypes.faq.default.has_teasertext.show', '1', '1', '', 0, 1, 1, '2016-08-30 06:15:41', '2016-08-30 06:15:41');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `crontasks`
--

DROP TABLE IF EXISTS `crontasks`;
CREATE TABLE `crontasks` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `method` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `execute` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0',
  `restartable` tinyint(1) DEFAULT '0',
  `status` longtext COLLATE utf8mb4_unicode_ci,
  `log` longtext COLLATE utf8mb4_unicode_ci,
  `last_start` datetime DEFAULT NULL,
  `last_executed` datetime DEFAULT NULL,
  `life_sign` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=963 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `crontasks`
--

INSERT INTO `crontasks` (`id`, `name`, `method`, `parameter`, `execute`, `active`, `restartable`, `status`, `log`, `last_start`, `last_executed`, `life_sign`, `created`, `modified`) VALUES
('0bb1beb0-9981-11df-82ce-002421a2bcd6', 'Suche - elasticsearchCheckSearchd', 'elasticsearchCheckSearchd', '', 'always', 0, 0, '-', NULL, NULL, '2011-10-11 16:58:24', NULL, '2010-07-27 15:20:05', '2020-07-31 16:42:00'),
('4c515cc1-069c-4918-b045-0e881a25c658', 'Newsletter - Testempfänger generieren', 'newsletterGenerateRecipients', '20', 'now', 0, 0, 'Ok', NULL, '2014-02-24 09:37:35', '2014-02-24 09:37:35', NULL, '2010-07-29 12:49:37', '2020-07-31 16:40:21'),
('4c515cc1-069c-4918-b045-0e881a25c659', 'Newsletter  - versenden', 'newsletterSendToSend', '', 'always', 0, 0, '2017-02-08 13:49:28 | INFO | Job: Newsletter versenden | Newsletter Test wurde an 1 von 1 Empfänger gesendet. Es wird weiter gesendet.\n2017-02-08 13:49:28 | INFO | Job: Newsletter versenden | OK', '2017-02-01 12:34:37 | Starte den Versand aller Kampagnen\n2017-02-01 12:34:37 | Keine Kampagne im Versand\n2017-02-01 12:35:53 | Starte den Versand aller Kampagnen\n2017-02-01 12:35:53 | Erstellung der Newsletterkampagne Test\n2017-02-01 12:35:53 | INFO | Newsletter versenden | Die Newsletterkampagne Test wurde erstellt. Es wird an 1 Empfänger gesendet.', '2019-04-15 11:52:57', '2017-02-08 13:49:28', '2019-04-15 11:52:57', '2010-07-29 12:49:37', '2020-07-31 16:47:55'),
('4d542027-0bd4-4d1b-a662-03111a25c659', 'Crontask - Heartbeat', 'checkPulse', '', 'always', 0, 0, '2017-02-01 12:04:06 | INFO | Crontask Heartbeat | OK', '', '2017-02-01 12:04:06', '2017-02-01 12:04:06', NULL, '2011-02-10 18:28:07', '2020-07-31 16:41:37'),
('4f352c1f-25f8-4b59-9554-08c51a25c659', 'Import - importExternalDatasources', 'importExternalDatasources', '', '2.0', 0, 0, 'Ok', NULL, '2014-03-13 11:59:32', '2014-03-13 11:59:39', NULL, '2012-02-10 15:39:27', '2020-07-31 16:42:28'),
('4f352c2f-b20c-4a79-a0ea-08191a25c559', 'Crontask - Abgestürzte Tasks melden und ggf. neu starten', 'checkLongRunningTasks', '', 'always', 0, 1, '2017-02-01 12:04:06 | INFO | Abgestürzte Tasks melden und ggf. neu starten | OK', '', '2017-02-01 12:04:06', '2017-02-01 12:04:06', NULL, '2012-02-10 15:39:43', '2020-07-31 16:41:50'),
('5123b1cf-8020-4480-96d3-17501a25c659', 'Datenbank  - Komplette Datenbank sichern', 'dbDump', '', 'now', 0, 0, 'Ok', NULL, '2014-02-21 08:50:54', '2014-02-21 08:51:11', NULL, '2013-02-19 18:09:35', '2020-07-31 16:40:55'),
('5123b236-44fc-46b9-8d89-17501a25c659', 'Datenbank - Sicherung einspielen', 'dbDumpInsert', '', 'now', 0, 0, 'Ok', NULL, '2014-02-21 08:54:54', '2014-02-21 08:57:16', NULL, '2013-02-19 18:11:18', '2020-07-31 16:41:13'),
('5306edbf-4dec-4a8c-bc02-181c1a25c659', 'Medien - alle fehlenden Versionen aller Medien generieren', 'updateAttachments', '', 'now', 0, 0, '-', NULL, '2014-02-21 08:30:23', '2014-02-21 08:32:48', NULL, '2014-02-21 07:10:07', '2020-07-31 16:28:59'),
('530702a9-f990-45ae-b0bd-181c1a25c659', 'Medien  - alle Versionen aller Medien neu generieren', 'regenerateAttachments', '', 'now', 0, 0, '-', NULL, '2014-02-21 08:39:33', '2014-02-21 08:41:57', NULL, '2014-02-21 08:39:21', '2020-07-31 16:28:49'),
('530746d1-4c4c-4fb7-b91d-181c1a25c659', 'Artikel - generateArticlesFromExtsources', 'generateArticlesFromExtsources', '', '02:00', 0, 0, '-', NULL, '2014-03-13 08:57:22', '2014-03-13 08:57:22', NULL, '2014-02-21 13:30:09', '2020-07-31 16:43:10'),
('530c8dae-ad24-48ae-a90b-24601a25c659', 'Weiterleitungen - updateInternalLinks', 'updateInternalLinks', '', '2.0', 0, 0, '', NULL, '2017-01-26 19:37:18', '2014-03-13 11:59:32', NULL, '2014-02-25 13:33:50', '2020-07-31 16:42:52'),
('531ece0b-0290-4356-b4f3-0f581a25c659', 'Suche - elasticsearchRebuildAllIndizes', 'elasticsearchRebuildAllIndizes', 'Menu,Article', '2.0', 0, 0, 're-indexed Menu, 221 records (0 failed) in 0.075559282302856 Minuten.\nre-indexed Menu, 36 records (0 failed) in 0.030267949899038 Minuten.\nre-indexed Article, 266 records (0 failed) in 0.098302014668783 Minuten.\nre-indexed Article, 5 records (0 failed) in 0.016284283002218 Minuten.', NULL, '2014-03-13 12:29:25', '2014-03-13 12:29:45', NULL, '2014-03-11 09:49:15', '2021-05-14 01:01:40'),
('542cfe3a-da7c-458d-960a-41932efc16a2', 'Weiterleitungen - Automatische Weiterleitungen erstellen', 'make301Redirects', '', '02:30', 0, 0, '', NULL, '2019-01-14 13:34:29', '2014-11-20 09:08:12', '2019-01-14 13:34:29', '2014-10-02 09:26:50', '2020-07-31 16:47:50'),
('5448e85b-4064-4a08-a41f-419f2efc16a2', 'Shop - Bestellungen versenden', 'shopProcessOrder', '', 'always', 0, 0, 'Ok', NULL, '2014-11-20 16:10:01', '2014-11-20 16:10:01', NULL, '2014-10-23 13:36:59', '2020-07-31 16:29:36'),
('5f352c2f-b21c-3a79-a0ea-08191a25c559', 'Newsletter - Bounces überprüfen', 'newsletterHandlebounces', '', '2.0', 0, 0, '', NULL, '2014-03-13 11:59:31', '2014-03-13 11:59:31', NULL, '2012-02-10 15:39:43', '2020-07-31 16:40:37'),
('5f352c2f-b21c-4a79-a0ea-08191a25c559', 'Newsletter - Statistik aktualisieren', 'newsletterStates', '', '1.0', 0, 0, 'Ok', NULL, '2014-03-13 11:59:31', '2014-03-13 11:59:31', NULL, '2012-02-10 15:39:43', '2020-07-31 16:40:43');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `domains`
--

DROP TABLE IF EXISTS `domains`;
CREATE TABLE `domains` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sequence` int(11) DEFAULT '0',
  `key` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0',
  `is_default` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=156 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `domains`
--

INSERT INTO `domains` (`id`, `sequence`, `key`, `value`, `info`, `parameter`, `type`, `active`, `is_default`, `created`, `modified`) VALUES
('00a7b0b0-dae0-11e9-918e-2187a3caaa93', 4, '4', '4/12', NULL, '', 'column', 1, 0, '2019-09-19 15:18:49', '2019-09-19 15:18:49'),
('00b63c40-c40e-11e9-bf17-6b4be1779473', 4, '2.0', 'alle 2 Stunden', NULL, '', 'interval', 1, 0, '2019-08-21 14:20:14', '2019-08-21 14:20:14'),
('00e5d050-dace-11e9-ae8c-5b2804313010', 38, 'containeropen', 'Container Start', NULL, '', 'formfield', 1, 0, '2019-09-19 13:10:06', '2019-09-19 13:10:48'),
('016fe3a0-0302-11eb-a343-0deb73075151', 7, 'url', 'Url', '', '', 'validationrule', 1, 0, '2020-09-30 11:48:06', '2020-09-30 11:48:06'),
('0571bbe0-dae0-11e9-918e-2187a3caaa93', 5, '5', '5/12', NULL, '', 'column', 1, 0, '2019-09-19 15:19:01', '2019-09-19 15:19:01'),
('084310f0-c40e-11e9-bf17-6b4be1779473', 5, '3.0', 'alle 3 Stunden', NULL, '', 'interval', 1, 0, '2019-08-21 14:20:28', '2019-08-21 14:20:28'),
('0a955df0-dace-11e9-ae8c-5b2804313010', 39, 'containerclose', 'Container Ende', NULL, '', 'formfield', 1, 0, '2019-09-19 13:10:41', '2019-09-19 13:10:41'),
('0b50ba70-0302-11eb-a343-0deb73075151', 8, 'file', 'Datei', '', '', 'validationrule', 1, 0, '2020-09-30 11:48:24', '2020-09-30 11:48:24'),
('0d13ed50-dae0-11e9-918e-2187a3caaa93', 6, '6', '6/12', NULL, '', 'column', 1, 0, '2019-09-19 15:19:12', '2019-09-19 15:19:12'),
('0da96c00-f3a2-11ea-9aae-df53ae2f5385', NULL, 'model', 'Model', '', '', 'resourcetype', 1, 0, '2020-09-10 22:14:03', '2020-09-10 22:14:03'),
('111250b0-c40e-11e9-bf17-6b4be1779473', 6, '4.0', 'alle 4 Stunden', NULL, '', 'interval', 1, 0, '2019-08-21 14:20:41', '2019-08-21 14:20:41'),
('119ff016-1494-4df7-9055-685b6493d072', 0, '2', '2 Jahre', '', '', 'berufserfahrung', 1, 1, '2014-01-01 00:00:00', '2020-10-15 14:15:46'),
('136c60b0-dae0-11e9-918e-2187a3caaa93', 7, '7', '7/12', NULL, '', 'column', 1, 0, '2019-09-19 15:19:20', '2019-09-19 15:19:20'),
('1612c980-0302-11eb-a343-0deb73075151', 9, 'fileSizeTypes', 'Datei mit max. Größe und Typen', '', '', 'validationrule', 1, 0, '2020-09-30 11:48:46', '2020-09-30 11:48:46'),
('185e1820-dae0-11e9-918e-2187a3caaa93', 8, '8', '8/12', NULL, '', 'column', 1, 0, '2019-09-19 15:19:29', '2019-09-19 15:19:29'),
('196808b0-dacd-11e9-960d-d17b590185c1', 31, 'h2', 'Headline (h2)', NULL, '', 'formfield', 1, 0, '2019-09-19 13:03:34', '2019-09-19 13:03:34'),
('19cbe590-c40e-11e9-bf17-6b4be1779473', 7, '6.0', 'alle 6 Stunden', NULL, '', 'interval', 1, 0, '2019-08-21 14:21:00', '2019-08-21 14:21:10'),
('1b3def40-dacf-11e9-ae8c-5b2804313010', 11, 'date', 'Datum', NULL, '', 'formfield', 1, 0, '2019-09-19 13:18:01', '2019-09-19 13:18:01'),
('1cf2e280-dae0-11e9-918e-2187a3caaa93', 9, '9', '9/12', NULL, '', 'column', 1, 0, '2019-09-19 15:19:37', '2019-09-19 15:19:37'),
('204849b0-dacd-11e9-960d-d17b590185c1', 32, 'h3', 'Headline (h3)', NULL, '', 'formfield', 1, 0, '2019-09-19 13:03:51', '2019-09-19 13:03:51'),
('21feffc0-dae0-11e9-918e-2187a3caaa93', 10, '10', '10/12', NULL, '', 'column', 1, 0, '2019-09-19 15:19:46', '2019-09-19 15:19:46'),
('23701ce0-0302-11eb-a343-0deb73075151', 10, 'dateinfuture', 'Datum in der Zukunft', '', '', 'validationrule', 1, 0, '2020-09-30 11:49:14', '2020-09-30 11:49:14'),
('23b0ac90-dace-11e9-ae8c-5b2804313010', 0, 'element', 'Element (.ctp)', NULL, '', 'formfield', 1, 0, '2019-09-19 13:11:32', '2019-09-19 13:11:32'),
('24ee8e50-dacf-11e9-ae8c-5b2804313010', 12, 'time', 'Zeit', NULL, '', 'formfield', 1, 0, '2019-09-19 13:18:10', '2019-09-19 13:18:10'),
('286f67f0-dae0-11e9-918e-2187a3caaa93', 11, '11', '11/12', NULL, '', 'column', 1, 0, '2019-09-19 15:19:57', '2019-09-19 15:19:57'),
('29b19180-c40e-11e9-bf17-6b4be1779473', 8, '8.0', 'alle 8 Stunden', NULL, '', 'interval', 1, 0, '2019-08-21 14:21:26', '2019-08-21 14:21:26'),
('2a743340-dacd-11e9-960d-d17b590185c1', 33, 'h4', 'Headline (h4)', NULL, '', 'formfield', 1, 0, '2019-09-19 13:04:07', '2019-09-19 13:04:07'),
('2a9564f0-dacf-11e9-ae8c-5b2804313010', 13, 'datetime', 'Datum und Zeit', NULL, '', 'formfield', 1, 0, '2019-09-19 13:18:38', '2019-09-19 13:18:38'),
('2d27b8ce-4764-11e3-959c-e8039af04ab0', 0, '1', '1 Jahr', NULL, '', 'berufserfahrung', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d27c2a1-4764-11e3-959c-e8039af04ab0', 0, '2', '2 Jahre', NULL, '', 'berufserfahrung', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d27c4eb-4764-11e3-959c-e8039af04ab0', 0, '3', '3 Jahre', NULL, '', 'berufserfahrung', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d27c6b0-4764-11e3-959c-e8039af04ab0', 0, '4', '4 Jahre', NULL, '', 'berufserfahrung', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d27c86d-4764-11e3-959c-e8039af04ab0', 0, '5', '5 Jahre', NULL, '', 'berufserfahrung', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d27ca1d-4764-11e3-959c-e8039af04ab0', 0, '6', '6 Jahre', NULL, '', 'berufserfahrung', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d27cbd1-4764-11e3-959c-e8039af04ab0', 0, '7', '7%', NULL, '', 'mwst', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d27cd85-4764-11e3-959c-e8039af04ab0', 0, '8', '8 Jahre', NULL, '', 'berufserfahrung', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d27cf39-4764-11e3-959c-e8039af04ab0', 0, '9', '9 oder mehr Jahre', NULL, '', 'berufserfahrung', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d27d0e9-4764-11e3-959c-e8039af04ab0', 0, '0', 'keine', NULL, '', 'berufserfahrung', 1, 1, '2014-01-01 00:00:00', '2020-10-15 14:15:53'),
('2d27d2b3-4764-11e3-959c-e8039af04ab0', 0, 'amex', 'American Express', NULL, '', 'cc', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d27d481-4764-11e3-959c-e8039af04ab0', 0, 'mc', 'Mastercard', NULL, '', 'cc', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d27d646-4764-11e3-959c-e8039af04ab0', 0, 'visa', 'Visa', NULL, '', 'cc', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d27d7fe-4764-11e3-959c-e8039af04ab0', 0, 'ac', 'Ascension', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d27dbf8-4764-11e3-959c-e8039af04ab0', 0, 'ad', 'Andorra', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d27ddd7-4764-11e3-959c-e8039af04ab0', 0, 'ae', 'Vereinigte Arabische Emirate', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d27df98-4764-11e3-959c-e8039af04ab0', 0, 'af', 'Afghanistan', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d27e14c-4764-11e3-959c-e8039af04ab0', 0, 'ag', 'Antigua und Barbuda', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d27e309-4764-11e3-959c-e8039af04ab0', 0, 'ai', 'Anguilla', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d27e524-4764-11e3-959c-e8039af04ab0', 0, 'al', 'Albanien', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d27e6e5-4764-11e3-959c-e8039af04ab0', 0, 'am', 'Armenien', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d27e89d-4764-11e3-959c-e8039af04ab0', 0, 'an', 'Niederländische Antillen', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d27ea63-4764-11e3-959c-e8039af04ab0', 0, 'ao', 'Angola', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d27ec1b-4764-11e3-959c-e8039af04ab0', 0, 'aq', 'Antarktis', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d27edcf-4764-11e3-959c-e8039af04ab0', 0, 'ar', 'Argentinien', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d27ef88-4764-11e3-959c-e8039af04ab0', 0, 'as', 'Amerikanisch-Samoa', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d27f145-4764-11e3-959c-e8039af04ab0', 0, 'at', 'Österreich', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d27f2fd-4764-11e3-959c-e8039af04ab0', 0, 'au', 'Australien', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d27f4b5-4764-11e3-959c-e8039af04ab0', 0, 'aw', 'Aruba', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d27f665-4764-11e3-959c-e8039af04ab0', 0, 'ax', 'Aland', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d27f81e-4764-11e3-959c-e8039af04ab0', 0, 'az', 'Aserbaidschan', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d27f9d2-4764-11e3-959c-e8039af04ab0', 0, 'ba', 'Bosnien und Herzegowina', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d27fb8a-4764-11e3-959c-e8039af04ab0', 0, 'bb', 'Barbados', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d27fd3a-4764-11e3-959c-e8039af04ab0', 0, 'bd', 'Bangladesch', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d27feef-4764-11e3-959c-e8039af04ab0', 0, 'be', 'Belgien', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2802b9-4764-11e3-959c-e8039af04ab0', 0, 'bf', 'Burkina Faso', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2804a5-4764-11e3-959c-e8039af04ab0', 0, 'bg', 'Bulgarien', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d280662-4764-11e3-959c-e8039af04ab0', 0, 'bh', 'Bahrain', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d280816-4764-11e3-959c-e8039af04ab0', 0, 'bi', 'Burundi', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2809ca-4764-11e3-959c-e8039af04ab0', 0, 'bj', 'Benin', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d280caa-4764-11e3-959c-e8039af04ab0', 0, 'bm', 'Bermuda', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d280ed1-4764-11e3-959c-e8039af04ab0', 0, 'bn', 'Brunei', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28108e-4764-11e3-959c-e8039af04ab0', 0, 'bo', 'Bolivien', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28124f-4764-11e3-959c-e8039af04ab0', 0, 'br', 'Brasilien', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d281403-4764-11e3-959c-e8039af04ab0', 0, 'bs', 'Bahamas', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2815bc-4764-11e3-959c-e8039af04ab0', 0, 'bt', 'Bhutan', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d281770-4764-11e3-959c-e8039af04ab0', 0, 'bv', 'Bouvetinsel', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28192d-4764-11e3-959c-e8039af04ab0', 0, 'bw', 'Botswana', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d281ae5-4764-11e3-959c-e8039af04ab0', 0, 'by', 'Weißrussland', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d281ca2-4764-11e3-959c-e8039af04ab0', 0, 'bz', 'Belize', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d281e56-4764-11e3-959c-e8039af04ab0', 0, 'ca', 'Kanada', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28200a-4764-11e3-959c-e8039af04ab0', 0, 'cc', 'Kokosinseln', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2821bf-4764-11e3-959c-e8039af04ab0', 0, 'cd', 'Kongo, Demokratische Republik', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d282377-4764-11e3-959c-e8039af04ab0', 0, 'cf', 'Zentralafrikanische Republik', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d282534-4764-11e3-959c-e8039af04ab0', 0, 'cg', 'Kongo, Republik', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2826e8-4764-11e3-959c-e8039af04ab0', 0, 'ch', 'Schweiz', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d282ad9-4764-11e3-959c-e8039af04ab0', 0, 'ci', 'Cote d\'Ivoire', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d282cb4-4764-11e3-959c-e8039af04ab0', 0, 'ck', 'Cookinseln', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d282e71-4764-11e3-959c-e8039af04ab0', 0, 'cl', 'Chile', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d283032-4764-11e3-959c-e8039af04ab0', 0, 'cm', 'Kamerun', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2831e6-4764-11e3-959c-e8039af04ab0', 0, 'cn', 'China, Volksrepublik', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2833f8-4764-11e3-959c-e8039af04ab0', 0, 'co', 'Kolumbien', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2835b9-4764-11e3-959c-e8039af04ab0', 0, 'cr', 'Costa Rica', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d283776-4764-11e3-959c-e8039af04ab0', 0, 'cs', 'Serbien und Montenegro', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d283933-4764-11e3-959c-e8039af04ab0', 0, 'cu', 'Kuba', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d283ae7-4764-11e3-959c-e8039af04ab0', 0, 'cv', 'Kap Verde, Republik', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d283ca3-4764-11e3-959c-e8039af04ab0', 0, 'cx', 'Weihnachtsinsel', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d283e5c-4764-11e3-959c-e8039af04ab0', 0, 'cy', 'Zypern, Republik', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d284019-4764-11e3-959c-e8039af04ab0', 0, 'cz', 'Tschechische Republik', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2841da-4764-11e3-959c-e8039af04ab0', 0, 'de', 'Deutschland', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d284392-4764-11e3-959c-e8039af04ab0', 0, 'dg', 'Diego Garcia', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28454b-4764-11e3-959c-e8039af04ab0', 0, 'dj', 'Dschibuti', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2846ff-4764-11e3-959c-e8039af04ab0', 0, 'dk', 'Dänemark', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2848bc-4764-11e3-959c-e8039af04ab0', 0, 'dm', 'Dominica', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d284a74-4764-11e3-959c-e8039af04ab0', 0, 'do', 'Dominikanische Republik', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d284c31-4764-11e3-959c-e8039af04ab0', 0, 'dz', 'Algerien', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d284de1-4764-11e3-959c-e8039af04ab0', 0, 'ec', 'Ecuador', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2851ec-4764-11e3-959c-e8039af04ab0', 0, 'ee', 'Estland', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28551c-4764-11e3-959c-e8039af04ab0', 0, 'eg', 'Ägypten', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28585a-4764-11e3-959c-e8039af04ab0', 0, 'eh', 'Westsahara', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d285bad-4764-11e3-959c-e8039af04ab0', 0, 'er', 'Eritrea', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d285d8c-4764-11e3-959c-e8039af04ab0', 0, 'es', 'Spanien', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d285f40-4764-11e3-959c-e8039af04ab0', 0, 'et', 'Äthiopien', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2860f4-4764-11e3-959c-e8039af04ab0', 0, 'fi', 'Finnland', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2865cc-4764-11e3-959c-e8039af04ab0', 0, 'fj', 'Fidschi', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2867df-4764-11e3-959c-e8039af04ab0', 0, 'fk', 'Falklandinseln', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28699b-4764-11e3-959c-e8039af04ab0', 0, 'fm', 'Mikronesien, Föderierte Staaten von', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d286b61-4764-11e3-959c-e8039af04ab0', 0, 'fo', 'Färöer', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d286d15-4764-11e3-959c-e8039af04ab0', 0, 'fr', 'Frankreich', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d286ecd-4764-11e3-959c-e8039af04ab0', 0, 'ga', 'Gabun', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d287082-4764-11e3-959c-e8039af04ab0', 0, 'gb', 'Vereinigtes Königreich von Großbritannien und Nordirland', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28724b-4764-11e3-959c-e8039af04ab0', 0, 'gd', 'Grenada', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2873ff-4764-11e3-959c-e8039af04ab0', 0, 'ge', 'Georgien', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2878a4-4764-11e3-959c-e8039af04ab0', 0, 'gf', 'Französisch-Guayana', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d287ba6-4764-11e3-959c-e8039af04ab0', 0, 'gg', 'Guernsey, Vogtei', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d287ece-4764-11e3-959c-e8039af04ab0', 0, 'gh', 'Ghana, Republik', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2880c3-4764-11e3-959c-e8039af04ab0', 0, 'gi', 'Gibraltar', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2882e6-4764-11e3-959c-e8039af04ab0', 0, 'gl', 'Grönland', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28849e-4764-11e3-959c-e8039af04ab0', 0, 'gm', 'Gambia', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d288657-4764-11e3-959c-e8039af04ab0', 0, 'gn', 'Guinea, Republik', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d288814-4764-11e3-959c-e8039af04ab0', 0, 'gp', 'Guadeloupe', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2889cc-4764-11e3-959c-e8039af04ab0', 0, 'gq', 'Äquatorialguinea, Republik', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d288b89-4764-11e3-959c-e8039af04ab0', 0, 'gr', 'Griechenland', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d288d41-4764-11e3-959c-e8039af04ab0', 0, 'gs', 'Südgeorgien und die Südlichen Sandwichinseln', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d288f07-4764-11e3-959c-e8039af04ab0', 0, 'gt', 'Guatemala', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2890bf-4764-11e3-959c-e8039af04ab0', 0, 'gu', 'Guam', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d289277-4764-11e3-959c-e8039af04ab0', 0, 'gw', 'Guinea-Bissau, Republik', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d289430-4764-11e3-959c-e8039af04ab0', 0, 'gy', 'Guyana', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2895e4-4764-11e3-959c-e8039af04ab0', 0, 'hk', 'Hongkong', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2897a1-4764-11e3-959c-e8039af04ab0', 0, 'hm', 'Heard und McDonaldinseln', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d289995-4764-11e3-959c-e8039af04ab0', 0, 'hn', 'Honduras', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d289b49-4764-11e3-959c-e8039af04ab0', 0, 'hr', 'Kroatien', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d289e94-4764-11e3-959c-e8039af04ab0', 0, 'ht', 'Haiti', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28a077-4764-11e3-959c-e8039af04ab0', 0, 'hu', 'Ungarn', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28a238-4764-11e3-959c-e8039af04ab0', 0, 'ic', 'Kanarische Inseln', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28a3f5-4764-11e3-959c-e8039af04ab0', 0, 'id', 'Indonesien', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28a5ad-4764-11e3-959c-e8039af04ab0', 0, 'ie', 'Irland, Republik', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28a766-4764-11e3-959c-e8039af04ab0', 0, 'il', 'Israel', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28a985-4764-11e3-959c-e8039af04ab0', 0, 'im', 'Insel Man', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28ab3d-4764-11e3-959c-e8039af04ab0', 0, 'in', 'Indien', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28acfa-4764-11e3-959c-e8039af04ab0', 0, 'io', 'Britisches Territorium im Indischen Ozean', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28aebb-4764-11e3-959c-e8039af04ab0', 0, 'iq', 'Irak', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28b06b-4764-11e3-959c-e8039af04ab0', 0, 'ir', 'Iran', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28b21f-4764-11e3-959c-e8039af04ab0', 0, 'is', 'Island', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28b3d3-4764-11e3-959c-e8039af04ab0', 0, 'it', 'Italien', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28b588-4764-11e3-959c-e8039af04ab0', 0, 'je', 'Jersey', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28b73c-4764-11e3-959c-e8039af04ab0', 0, 'jm', 'Jamaika', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28b8f0-4764-11e3-959c-e8039af04ab0', 0, 'jo', 'Jordanien', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28baa8-4764-11e3-959c-e8039af04ab0', 0, 'jp', 'Japan', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28bc58-4764-11e3-959c-e8039af04ab0', 0, 'ke', 'Kenia', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28be08-4764-11e3-959c-e8039af04ab0', 0, 'kg', 'Kirgisistan', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28bfbc-4764-11e3-959c-e8039af04ab0', 0, 'kh', 'Kambodscha', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28c171-4764-11e3-959c-e8039af04ab0', 0, 'ki', 'Kiribati', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28c329-4764-11e3-959c-e8039af04ab0', 0, 'km', 'Komoren', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28c6da-4764-11e3-959c-e8039af04ab0', 0, 'kn', 'St. Kitts und Nevis', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28c8b5-4764-11e3-959c-e8039af04ab0', 0, 'kp', 'Korea, Demokratische Volkrepublik', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28ca7a-4764-11e3-959c-e8039af04ab0', 0, 'kr', 'Korea, Republik', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28cc33-4764-11e3-959c-e8039af04ab0', 0, 'kw', 'Kuwait', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28cde7-4764-11e3-959c-e8039af04ab0', 0, 'ky', 'Kaimaninseln', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28cff5-4764-11e3-959c-e8039af04ab0', 0, 'kz', 'Kasachstan', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28d1b1-4764-11e3-959c-e8039af04ab0', 0, 'la', 'Laos', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28d366-4764-11e3-959c-e8039af04ab0', 0, 'lb', 'Libanon', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28d516-4764-11e3-959c-e8039af04ab0', 0, 'lc', 'St. Lucia', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28d6ce-4764-11e3-959c-e8039af04ab0', 0, 'li', 'Liechtenstein, Fürstentum', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28d88f-4764-11e3-959c-e8039af04ab0', 0, 'lk', 'Sri Lanka', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28da43-4764-11e3-959c-e8039af04ab0', 0, 'lr', 'Liberia, Republik', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28dc00-4764-11e3-959c-e8039af04ab0', 0, 'ls', 'Lesotho', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28ddb4-4764-11e3-959c-e8039af04ab0', 0, 'lt', 'Litauen', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28df64-4764-11e3-959c-e8039af04ab0', 0, 'lu', 'Luxemburg', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28e118-4764-11e3-959c-e8039af04ab0', 0, 'lv', 'Lettland', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28e2c4-4764-11e3-959c-e8039af04ab0', 0, 'ly', 'Libyen', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28e47c-4764-11e3-959c-e8039af04ab0', 0, 'ma', 'Marokko', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28e62c-4764-11e3-959c-e8039af04ab0', 0, 'mc', 'Monaco', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28e7dc-4764-11e3-959c-e8039af04ab0', 0, 'md', 'Moldawien', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28e990-4764-11e3-959c-e8039af04ab0', 0, 'me', 'Montenegro', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28ec9f-4764-11e3-959c-e8039af04ab0', 0, 'mg', 'Madagaskar, Republik', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28ee75-4764-11e3-959c-e8039af04ab0', 0, 'mh', 'Marshallinseln', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28f032-4764-11e3-959c-e8039af04ab0', 0, 'mk', 'Mazedonien', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28f1e6-4764-11e3-959c-e8039af04ab0', 0, 'ml', 'Mali, Republik', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28f39f-4764-11e3-959c-e8039af04ab0', 0, 'mm', 'Myanmar', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28f553-4764-11e3-959c-e8039af04ab0', 0, 'mn', 'Mongolei', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28f76a-4764-11e3-959c-e8039af04ab0', 0, 'mo', 'Macao', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28f922-4764-11e3-959c-e8039af04ab0', 0, 'mp', 'Nördliche Marianen', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28fada-4764-11e3-959c-e8039af04ab0', 0, 'mq', 'Martinique', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28fc93-4764-11e3-959c-e8039af04ab0', 0, 'mr', 'Mauretanien', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d28fe47-4764-11e3-959c-e8039af04ab0', 0, 'ms', 'Montserrat', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d290000-4764-11e3-959c-e8039af04ab0', 0, 'mt', 'Malta', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2901b4-4764-11e3-959c-e8039af04ab0', 0, 'mu', 'Mauritius, Republik', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2903b1-4764-11e3-959c-e8039af04ab0', 0, 'mv', 'Malediven', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29058b-4764-11e3-959c-e8039af04ab0', 0, 'mw', 'Malawi, Republik', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d290744-4764-11e3-959c-e8039af04ab0', 0, 'mx', 'Mexiko', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2908fc-4764-11e3-959c-e8039af04ab0', 0, 'my', 'Malaysia', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d290ab0-4764-11e3-959c-e8039af04ab0', 0, 'mz', 'Mosambik', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d290c65-4764-11e3-959c-e8039af04ab0', 0, 'na', 'Namibia, Republik', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d290e1d-4764-11e3-959c-e8039af04ab0', 0, 'nc', 'Neukaledonien', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d290fcd-4764-11e3-959c-e8039af04ab0', 0, 'ne', 'Niger', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d291181-4764-11e3-959c-e8039af04ab0', 0, 'nf', 'Norfolkinsel', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d291733-4764-11e3-959c-e8039af04ab0', 0, 'ng', 'Nigeria', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d291a8b-4764-11e3-959c-e8039af04ab0', 0, 'ni', 'Nicaragua', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d291d80-4764-11e3-959c-e8039af04ab0', 0, 'nl', 'Niederlande', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2920ca-4764-11e3-959c-e8039af04ab0', 0, 'no', 'Norwegen', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2922a0-4764-11e3-959c-e8039af04ab0', 0, 'np', 'Nepal', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d292459-4764-11e3-959c-e8039af04ab0', 0, 'nr', 'Nauru', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29260d-4764-11e3-959c-e8039af04ab0', 0, 'nt', 'Neutrale Zone', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2927ca-4764-11e3-959c-e8039af04ab0', 0, 'nu', 'Niue', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d292982-4764-11e3-959c-e8039af04ab0', 0, 'nz', 'Neuseeland', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d292b37-4764-11e3-959c-e8039af04ab0', 0, 'om', 'Oman', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d292ceb-4764-11e3-959c-e8039af04ab0', 0, 'pa', 'Panama', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d292ea3-4764-11e3-959c-e8039af04ab0', 0, 'pe', 'Peru', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d293053-4764-11e3-959c-e8039af04ab0', 0, 'pf', 'Französisch-Polynesien', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d293214-4764-11e3-959c-e8039af04ab0', 0, 'pg', 'Papua-Neuguinea', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2933cd-4764-11e3-959c-e8039af04ab0', 0, 'ph', 'Philippinen', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d293581-4764-11e3-959c-e8039af04ab0', 0, 'pk', 'Pakistan', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d293735-4764-11e3-959c-e8039af04ab0', 0, 'pl', 'Polen', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d293b33-4764-11e3-959c-e8039af04ab0', 0, 'pm', 'St. Pierre und Miquelon', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d293e42-4764-11e3-959c-e8039af04ab0', 0, 'pn', 'Pitcairninseln', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29428d-4764-11e3-959c-e8039af04ab0', 0, 'pr', 'Puerto Rico', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d294552-4764-11e3-959c-e8039af04ab0', 0, 'ps', 'Palästinensische Autonomiegebiete', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29472d-4764-11e3-959c-e8039af04ab0', 0, 'pt', 'Portugal', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2948ea-4764-11e3-959c-e8039af04ab0', 0, 'pw', 'Palau', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d294a9a-4764-11e3-959c-e8039af04ab0', 0, 'py', 'Paraguay', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d294c4a-4764-11e3-959c-e8039af04ab0', 0, 'qa', 'Katar', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d294dfe-4764-11e3-959c-e8039af04ab0', 0, 're', 'Réunion', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d294fb2-4764-11e3-959c-e8039af04ab0', 0, 'ro', 'Rumänien', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d295166-4764-11e3-959c-e8039af04ab0', 0, 'ru', 'Russische Föderation', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d295327-4764-11e3-959c-e8039af04ab0', 0, 'rw', 'Ruanda, Republik', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2954e0-4764-11e3-959c-e8039af04ab0', 0, 'sa', 'Saudi-Arabien, Königreich', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29569d-4764-11e3-959c-e8039af04ab0', 0, 'sb', 'Salomonen', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d295855-4764-11e3-959c-e8039af04ab0', 0, 'sc', 'Seychellen, Republik der', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d295a0d-4764-11e3-959c-e8039af04ab0', 0, 'sd', 'Sudan', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d295bc2-4764-11e3-959c-e8039af04ab0', 0, 'se', 'Schweden', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d295d72-4764-11e3-959c-e8039af04ab0', 0, 'sg', 'Singapur', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d295f2a-4764-11e3-959c-e8039af04ab0', 0, 'sh', 'Die Kronkolonie St. Helena und Nebengebiete', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d296364-4764-11e3-959c-e8039af04ab0', 0, 'si', 'Slowenien', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d296543-4764-11e3-959c-e8039af04ab0', 0, 'sj', 'Svalbard und Jan Mayen', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d296704-4764-11e3-959c-e8039af04ab0', 0, 'sk', 'Slowakei', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2968c1-4764-11e3-959c-e8039af04ab0', 0, 'sl', 'Sierra Leone, Republik', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d296a7d-4764-11e3-959c-e8039af04ab0', 0, 'sm', 'San Marino', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d296c8b-4764-11e3-959c-e8039af04ab0', 0, 'sn', 'Senegal', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d296e44-4764-11e3-959c-e8039af04ab0', 0, 'so', 'Somalia, Demokratische Republik', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d297001-4764-11e3-959c-e8039af04ab0', 0, 'sr', 'Suriname', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2971b5-4764-11e3-959c-e8039af04ab0', 0, 'st', 'São Tomé und Príncipe', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d297372-4764-11e3-959c-e8039af04ab0', 0, 'su', 'Union der Sozialistischen Sowjetrepubliken', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d297548-4764-11e3-959c-e8039af04ab0', 0, 'sv', 'El Salvador', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29772b-4764-11e3-959c-e8039af04ab0', 0, 'sy', 'Syrien', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2978e4-4764-11e3-959c-e8039af04ab0', 0, 'sz', 'Swasiland', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d297a9c-4764-11e3-959c-e8039af04ab0', 0, 'ta', 'Tristan da Cunha', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d297c55-4764-11e3-959c-e8039af04ab0', 0, 'tc', 'Turks- und Caicosinseln', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d297e11-4764-11e3-959c-e8039af04ab0', 0, 'td', 'Tschad, Republik', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d297fca-4764-11e3-959c-e8039af04ab0', 0, 'tf', 'Französische Süd- und Antarktisgebiete', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29818f-4764-11e3-959c-e8039af04ab0', 0, 'tg', 'Togo, Republik', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d298343-4764-11e3-959c-e8039af04ab0', 0, 'th', 'Thailand', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2984fc-4764-11e3-959c-e8039af04ab0', 0, 'tj', 'Tadschikistan', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2986b4-4764-11e3-959c-e8039af04ab0', 0, 'tk', 'Tokelau', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d298d38-4764-11e3-959c-e8039af04ab0', 0, 'tl', 'Timor-Leste, Demokratische Republik', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29907e-4764-11e3-959c-e8039af04ab0', 0, 'tm', 'Turkmenistan', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2993f8-4764-11e3-959c-e8039af04ab0', 0, 'tn', 'Tunesien', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2995db-4764-11e3-959c-e8039af04ab0', 0, 'to', 'Tonga', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d299798-4764-11e3-959c-e8039af04ab0', 0, 'tr', 'Türkei', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29994c-4764-11e3-959c-e8039af04ab0', 0, 'tt', 'Trinidad und Tobago', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d299b04-4764-11e3-959c-e8039af04ab0', 0, 'tv', 'Tuvalu', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d299cb8-4764-11e3-959c-e8039af04ab0', 0, 'tw', 'Taiwan', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d299e68-4764-11e3-959c-e8039af04ab0', 0, 'tz', 'Tansania, Vereinigte Republik', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29a025-4764-11e3-959c-e8039af04ab0', 0, 'ua', 'Ukraine', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29a1d1-4764-11e3-959c-e8039af04ab0', 0, 'ug', 'Uganda, Republik', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29a389-4764-11e3-959c-e8039af04ab0', 0, 'us', 'Vereinigte Staaten von Amerika', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29a546-4764-11e3-959c-e8039af04ab0', 0, 'uy', 'Uruguay', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29a6f6-4764-11e3-959c-e8039af04ab0', 0, 'uz', 'Usbekistan', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29a8a6-4764-11e3-959c-e8039af04ab0', 0, 'va', 'Vatikanstadt', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29aa56-4764-11e3-959c-e8039af04ab0', 0, 'vc', 'St. Vincent und die Grenadinen (GB)', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29ac0e-4764-11e3-959c-e8039af04ab0', 0, 've', 'Venezuela', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29adc7-4764-11e3-959c-e8039af04ab0', 0, 'vg', 'Britische Jungferninseln', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29b428-4764-11e3-959c-e8039af04ab0', 0, 'vi', 'Amerikanische Jungferninseln', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29b64c-4764-11e3-959c-e8039af04ab0', 0, 'vn', 'Vietnam', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29b804-4764-11e3-959c-e8039af04ab0', 0, 'vu', 'Vanuatu', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29ba23-4764-11e3-959c-e8039af04ab0', 0, 'wf', 'Wallis und Futuna', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29bbe4-4764-11e3-959c-e8039af04ab0', 0, 'ws', 'Samoa', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29bd98-4764-11e3-959c-e8039af04ab0', 0, 'ye', 'Jemen', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29bf4c-4764-11e3-959c-e8039af04ab0', 0, 'yt', 'Mayotte', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29c101-4764-11e3-959c-e8039af04ab0', 0, 'za', 'Südafrika, Republik', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29c2b9-4764-11e3-959c-e8039af04ab0', 0, 'zm', 'Sambia, Republik', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29c472-4764-11e3-959c-e8039af04ab0', 0, 'zw', 'Simbabwe, Republik', NULL, '', 'country', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29c62e-4764-11e3-959c-e8039af04ab0', 0, 'aed', 'AED', NULL, 'AE', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29c7e7-4764-11e3-959c-e8039af04ab0', 0, 'afn', 'AFN', NULL, 'AF', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29c99b-4764-11e3-959c-e8039af04ab0', 0, 'all', 'ALL', NULL, 'AL', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29cb47-4764-11e3-959c-e8039af04ab0', 0, 'amd', 'AMD', NULL, 'AM', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29ccf2-4764-11e3-959c-e8039af04ab0', 0, 'ang', 'ANG', NULL, 'AW', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29ce9e-4764-11e3-959c-e8039af04ab0', 0, 'ang', 'ANG', NULL, 'AN', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29d04a-4764-11e3-959c-e8039af04ab0', 0, 'aoa', 'AOA', NULL, 'AO', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29d1f5-4764-11e3-959c-e8039af04ab0', 0, 'ars', 'ARS', NULL, 'AR', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29d3a1-4764-11e3-959c-e8039af04ab0', 0, 'aud', 'AUD', NULL, 'AU', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29d716-4764-11e3-959c-e8039af04ab0', 0, 'aud', 'AUD', NULL, 'HM', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29da07-4764-11e3-959c-e8039af04ab0', 0, 'aud', 'AUD', NULL, 'KI', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29dd3c-4764-11e3-959c-e8039af04ab0', 0, 'aud', 'AUD', NULL, 'CC', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29df1f-4764-11e3-959c-e8039af04ab0', 0, 'aud', 'AUD', NULL, 'NR', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29e131-4764-11e3-959c-e8039af04ab0', 0, 'aud', 'AUD', NULL, 'NF', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29e2e5-4764-11e3-959c-e8039af04ab0', 0, 'aud', 'AUD', NULL, 'CX', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29e49e-4764-11e3-959c-e8039af04ab0', 0, 'azn', 'AZN', NULL, 'AZ', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29e64a-4764-11e3-959c-e8039af04ab0', 0, 'bam', 'BAM', NULL, 'BA', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29e7f9-4764-11e3-959c-e8039af04ab0', 0, 'bbd', 'BBD', NULL, 'BB', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29e9a1-4764-11e3-959c-e8039af04ab0', 0, 'bdt', 'BDT', NULL, 'BD', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29eb51-4764-11e3-959c-e8039af04ab0', 0, 'bgn', 'BGN', NULL, 'BG', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29ecf8-4764-11e3-959c-e8039af04ab0', 0, 'bhd', 'BHD', NULL, 'BH', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29eea8-4764-11e3-959c-e8039af04ab0', 0, 'bif', 'BIF', NULL, 'BI', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29f054-4764-11e3-959c-e8039af04ab0', 0, 'bmd', 'BMD', NULL, 'BM', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29f1ff-4764-11e3-959c-e8039af04ab0', 0, 'bnd', 'BND', NULL, 'BN', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29f3a7-4764-11e3-959c-e8039af04ab0', 0, 'bob', 'BOB', NULL, 'BO', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29f557-4764-11e3-959c-e8039af04ab0', 0, 'brl', 'BRL', NULL, 'BR', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29f706-4764-11e3-959c-e8039af04ab0', 0, 'bsd', 'BSD', NULL, 'BS', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29f8b2-4764-11e3-959c-e8039af04ab0', 0, 'btn', 'BTN', NULL, 'BT', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29fa5e-4764-11e3-959c-e8039af04ab0', 0, 'bwp', 'BWP', NULL, 'BW', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d29fc0e-4764-11e3-959c-e8039af04ab0', 0, 'byr', 'BYR', NULL, 'BY', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a005d-4764-11e3-959c-e8039af04ab0', 0, 'bzd', 'BZD', NULL, 'BZ', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a0389-4764-11e3-959c-e8039af04ab0', 0, 'cad', 'CAD', NULL, 'CA', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a0687-4764-11e3-959c-e8039af04ab0', 0, 'cdf', 'CDF', NULL, 'CD', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a09b8-4764-11e3-959c-e8039af04ab0', 0, 'chf', 'CHF', NULL, 'LI', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a0b86-4764-11e3-959c-e8039af04ab0', 0, 'chf', 'CHF', NULL, 'CH', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a0d3a-4764-11e3-959c-e8039af04ab0', 0, 'clp', 'CLP', NULL, 'CL', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a0eea-4764-11e3-959c-e8039af04ab0', 0, 'cny', 'CNY', NULL, 'CN', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a1095-4764-11e3-959c-e8039af04ab0', 0, 'cop', 'COP', NULL, 'CO', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a1241-4764-11e3-959c-e8039af04ab0', 0, 'crc', 'CRC', NULL, 'CR', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a13ed-4764-11e3-959c-e8039af04ab0', 0, 'cup', 'CUP', NULL, 'CU', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a1598-4764-11e3-959c-e8039af04ab0', 0, 'cve', 'CVE', NULL, 'CV', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a1748-4764-11e3-959c-e8039af04ab0', 0, 'cyp', 'CYP', NULL, 'CY', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a18ef-4764-11e3-959c-e8039af04ab0', 0, 'czk', 'CZK', NULL, 'CZ', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a1a9b-4764-11e3-959c-e8039af04ab0', 0, 'djf', 'DJF', NULL, 'DJ', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a1c42-4764-11e3-959c-e8039af04ab0', 0, 'dkk', 'DKK', NULL, 'DK', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a1df2-4764-11e3-959c-e8039af04ab0', 0, 'dkk', 'DKK', NULL, 'FO', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a1f9e-4764-11e3-959c-e8039af04ab0', 0, 'dkk', 'DKK', NULL, 'GL', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a214a-4764-11e3-959c-e8039af04ab0', 0, 'dop', 'DOP', NULL, 'DO', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a22f1-4764-11e3-959c-e8039af04ab0', 0, 'dzd', 'DZD', NULL, 'DZ', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a27f0-4764-11e3-959c-e8039af04ab0', 0, 'eek', 'EEK', NULL, 'EE', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a2b1c-4764-11e3-959c-e8039af04ab0', 0, 'egp', 'EGP', NULL, 'EG', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a2cf3-4764-11e3-959c-e8039af04ab0', 0, 'ern', 'ERN', NULL, 'ER', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a2ea2-4764-11e3-959c-e8039af04ab0', 0, 'etb', 'ETB', NULL, 'ET', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a30b5-4764-11e3-959c-e8039af04ab0', 0, 'eur', 'EUR', NULL, 'AX', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a3265-4764-11e3-959c-e8039af04ab0', 0, 'eur', 'EUR', NULL, 'AD', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a3410-4764-11e3-959c-e8039af04ab0', 0, 'eur', 'EUR', NULL, 'BE', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a35b8-4764-11e3-959c-e8039af04ab0', 0, 'eur', 'EUR', NULL, 'DE', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a375f-4764-11e3-959c-e8039af04ab0', 0, 'eur', 'EUR', NULL, 'FI', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a390b-4764-11e3-959c-e8039af04ab0', 0, 'eur', 'EUR', NULL, 'FR', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a3ab6-4764-11e3-959c-e8039af04ab0', 0, 'eur', 'EUR', NULL, 'GF', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a3c62-4764-11e3-959c-e8039af04ab0', 0, 'eur', 'EUR', NULL, 'TF', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a3e0e-4764-11e3-959c-e8039af04ab0', 0, 'eur', 'EUR', NULL, 'GR', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a3fb5-4764-11e3-959c-e8039af04ab0', 0, 'eur', 'EUR', NULL, 'GP', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a4165-4764-11e3-959c-e8039af04ab0', 0, 'eur', 'EUR', NULL, 'IE', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a4310-4764-11e3-959c-e8039af04ab0', 0, 'eur', 'EUR', NULL, 'IT', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a44bc-4764-11e3-959c-e8039af04ab0', 0, 'eur', 'EUR', NULL, 'LU', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a4668-4764-11e3-959c-e8039af04ab0', 0, 'eur', 'EUR', NULL, 'MT', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a4818-4764-11e3-959c-e8039af04ab0', 0, 'eur', 'EUR', NULL, 'MQ', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a49c3-4764-11e3-959c-e8039af04ab0', 0, 'eur', 'EUR', NULL, 'YT', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a4d0e-4764-11e3-959c-e8039af04ab0', 0, 'eur', 'EUR', NULL, 'MC', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a4edc-4764-11e3-959c-e8039af04ab0', 0, 'eur', 'EUR', NULL, 'NL', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a5090-4764-11e3-959c-e8039af04ab0', 0, 'eur', 'EUR', NULL, 'AT', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00');
INSERT INTO `domains` (`id`, `sequence`, `key`, `value`, `info`, `parameter`, `type`, `active`, `is_default`, `created`, `modified`) VALUES
('2d2a5240-4764-11e3-959c-e8039af04ab0', 0, 'eur', 'EUR', NULL, 'PT', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a53f4-4764-11e3-959c-e8039af04ab0', 0, 'eur', 'EUR', NULL, 'RE', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a55f5-4764-11e3-959c-e8039af04ab0', 0, 'eur', 'EUR', NULL, 'SM', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a57f2-4764-11e3-959c-e8039af04ab0', 0, 'eur', 'EUR', NULL, 'ES', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a59c4-4764-11e3-959c-e8039af04ab0', 0, 'eur', 'EUR', NULL, 'PM', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a5b74-4764-11e3-959c-e8039af04ab0', 0, 'eur', 'EUR', NULL, 'VA', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a5d20-4764-11e3-959c-e8039af04ab0', 0, 'fjd', 'FJD', NULL, 'FJ', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a5ecf-4764-11e3-959c-e8039af04ab0', 0, 'flp', 'FLP', NULL, 'FK', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a6088-4764-11e3-959c-e8039af04ab0', 0, 'gbp', 'GBP', NULL, 'GS', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a6263-4764-11e3-959c-e8039af04ab0', 0, 'gbp', 'GBP', NULL, 'GB', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a6417-4764-11e3-959c-e8039af04ab0', 0, 'gel', 'GEL', NULL, 'GE', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a65c7-4764-11e3-959c-e8039af04ab0', 0, 'ggp', 'GGP', NULL, 'GG', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a677b-4764-11e3-959c-e8039af04ab0', 0, 'ghc', 'GHC', NULL, 'GH', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a692b-4764-11e3-959c-e8039af04ab0', 0, 'gip', 'GIP', NULL, 'GI', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a6adb-4764-11e3-959c-e8039af04ab0', 0, 'gmd', 'GMD', NULL, 'GM', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a6c86-4764-11e3-959c-e8039af04ab0', 0, 'gnf', 'GNF', NULL, 'GN', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a6e36-4764-11e3-959c-e8039af04ab0', 0, 'gtq', 'GTQ', NULL, 'GT', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a6fe2-4764-11e3-959c-e8039af04ab0', 0, 'gyd', 'GYD', NULL, 'GY', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a734f-4764-11e3-959c-e8039af04ab0', 0, 'hnl', 'HNL', NULL, 'HN', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a7558-4764-11e3-959c-e8039af04ab0', 0, 'hnl', 'HNL', NULL, 'HK', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a770d-4764-11e3-959c-e8039af04ab0', 0, 'hrk', 'HRK', NULL, 'HR', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a78bc-4764-11e3-959c-e8039af04ab0', 0, 'huf', 'HUF', NULL, 'IS', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a7a6c-4764-11e3-959c-e8039af04ab0', 0, 'huf', 'HUF', NULL, 'HU', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a7c21-4764-11e3-959c-e8039af04ab0', 0, 'idr', 'IDR', NULL, 'IQ', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a7e26-4764-11e3-959c-e8039af04ab0', 0, 'idr', 'IDR', NULL, 'TL', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a7fde-4764-11e3-959c-e8039af04ab0', 0, 'ils', 'ILS', NULL, 'IL', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a8193-4764-11e3-959c-e8039af04ab0', 0, 'imp', 'IMP', NULL, 'IM', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a833e-4764-11e3-959c-e8039af04ab0', 0, 'inr', 'INR', NULL, 'ID', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a84ee-4764-11e3-959c-e8039af04ab0', 0, 'irr', 'IRR', NULL, 'IR', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a869e-4764-11e3-959c-e8039af04ab0', 0, 'isk', 'ISK', NULL, 'IN', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a884a-4764-11e3-959c-e8039af04ab0', 0, 'jep', 'JEP', NULL, 'JE', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a89fa-4764-11e3-959c-e8039af04ab0', 0, 'jmd', 'JMD', NULL, 'JM', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a8baa-4764-11e3-959c-e8039af04ab0', 0, 'jod', 'JOD', NULL, 'JO', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a8d59-4764-11e3-959c-e8039af04ab0', 0, 'jpy', 'JPY', NULL, 'JP', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a8f05-4764-11e3-959c-e8039af04ab0', 0, 'kes', 'KES', NULL, 'KE', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a90b1-4764-11e3-959c-e8039af04ab0', 0, 'kgs', 'KGS', NULL, 'KG', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a9265-4764-11e3-959c-e8039af04ab0', 0, 'khr', 'KHR', NULL, 'KH', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a9437-4764-11e3-959c-e8039af04ab0', 0, 'kmf', 'KMF', NULL, 'KM', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a972c-4764-11e3-959c-e8039af04ab0', 0, 'kpw', 'KPW', NULL, 'KP', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a9a76-4764-11e3-959c-e8039af04ab0', 0, 'krw', 'KRW', NULL, 'KR', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a9c5a-4764-11e3-959c-e8039af04ab0', 0, 'kwd', 'KWD', NULL, 'KW', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a9e0e-4764-11e3-959c-e8039af04ab0', 0, 'kyd', 'KYD', NULL, 'KY', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2a9fbe-4764-11e3-959c-e8039af04ab0', 0, 'kzt', 'KZT', NULL, 'KZ', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2aa172-4764-11e3-959c-e8039af04ab0', 0, 'lak', 'LAK', NULL, 'LA', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2aa322-4764-11e3-959c-e8039af04ab0', 0, 'lbp', 'LBP', NULL, 'LB', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2aa52c-4764-11e3-959c-e8039af04ab0', 0, 'lkr', 'LKR', NULL, 'LK', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2aa6e4-4764-11e3-959c-e8039af04ab0', 0, 'lrd', 'LRD', NULL, 'LR', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2aa894-4764-11e3-959c-e8039af04ab0', 0, 'lsl', 'LSL', NULL, 'LS', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2aaa40-4764-11e3-959c-e8039af04ab0', 0, 'ltl', 'LTL', NULL, 'LT', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2aabef-4764-11e3-959c-e8039af04ab0', 0, 'lvl', 'LVL', NULL, 'LV', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2aad9b-4764-11e3-959c-e8039af04ab0', 0, 'lyd', 'LYD', NULL, 'LY', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2aaf4b-4764-11e3-959c-e8039af04ab0', 0, 'mad', 'MAD', NULL, 'MA', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ab0fb-4764-11e3-959c-e8039af04ab0', 0, 'mad', 'MAD', NULL, 'EH', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ab2ab-4764-11e3-959c-e8039af04ab0', 0, 'mdl', 'MDL', NULL, 'MD', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ab456-4764-11e3-959c-e8039af04ab0', 0, 'mga', 'MGA', NULL, 'MG', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ab602-4764-11e3-959c-e8039af04ab0', 0, 'mkd', 'MKD', NULL, 'MK', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ab7b2-4764-11e3-959c-e8039af04ab0', 0, 'mmk', 'MMK', NULL, 'MM', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ab95e-4764-11e3-959c-e8039af04ab0', 0, 'mnt', 'MNT', NULL, 'MN', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2abb0e-4764-11e3-959c-e8039af04ab0', 0, 'mop', 'MOP', NULL, 'MO', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2abcbd-4764-11e3-959c-e8039af04ab0', 0, 'mro', 'MRO', NULL, 'MR', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2abe69-4764-11e3-959c-e8039af04ab0', 0, 'mur', 'MUR', NULL, 'MU', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ac52d-4764-11e3-959c-e8039af04ab0', 0, 'mvr', 'MVR', NULL, 'MV', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ac9a3-4764-11e3-959c-e8039af04ab0', 0, 'mwk', 'MWK', NULL, 'MW', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2acba8-4764-11e3-959c-e8039af04ab0', 0, 'mxn', 'MXN', NULL, 'MX', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2acd65-4764-11e3-959c-e8039af04ab0', 0, 'myr', 'MYR', NULL, 'MY', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2acf1d-4764-11e3-959c-e8039af04ab0', 0, 'mzm', 'MZM', NULL, 'MZ', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ad0d2-4764-11e3-959c-e8039af04ab0', 0, 'ngn', 'NGN', NULL, 'NG', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ad286-4764-11e3-959c-e8039af04ab0', 0, 'nio', 'NIO', NULL, 'NI', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ad436-4764-11e3-959c-e8039af04ab0', 0, 'nok', 'NOK', NULL, 'BV', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ad5ea-4764-11e3-959c-e8039af04ab0', 0, 'nok', 'NOK', NULL, 'NO', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ad79a-4764-11e3-959c-e8039af04ab0', 0, 'nok', 'NOK', NULL, 'SJ', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ad94a-4764-11e3-959c-e8039af04ab0', 0, 'npr', 'NPR', NULL, 'NP', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2adaf5-4764-11e3-959c-e8039af04ab0', 0, 'nzd', 'NZD', NULL, 'CK', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2adca5-4764-11e3-959c-e8039af04ab0', 0, 'nzd', 'NZD', NULL, 'NZ', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ade55-4764-11e3-959c-e8039af04ab0', 0, 'nzd', 'NZD', NULL, 'NU', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ae001-4764-11e3-959c-e8039af04ab0', 0, 'nzd', 'NZD', NULL, 'PN', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ae1b1-4764-11e3-959c-e8039af04ab0', 0, 'nzd', 'NZD', NULL, 'TK', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ae361-4764-11e3-959c-e8039af04ab0', 0, 'omr', 'OMR', NULL, 'OM', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ae50c-4764-11e3-959c-e8039af04ab0', 0, 'pen', 'PEN', NULL, 'PE', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ae6c1-4764-11e3-959c-e8039af04ab0', 0, 'pgk', 'PGK', NULL, 'PG', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ae897-4764-11e3-959c-e8039af04ab0', 0, 'php', 'PHP', NULL, 'PH', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2aeb1d-4764-11e3-959c-e8039af04ab0', 0, 'pkr', 'PKR', NULL, 'PK', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2aecde-4764-11e3-959c-e8039af04ab0', 0, 'pln', 'PLN', NULL, 'PL', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2aee8e-4764-11e3-959c-e8039af04ab0', 0, 'pyg', 'PYG', NULL, 'PY', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2af03e-4764-11e3-959c-e8039af04ab0', 0, 'qar', 'QAR', NULL, 'QA', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2af1ed-4764-11e3-959c-e8039af04ab0', 0, 'ron', 'RON', NULL, 'RO', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2af399-4764-11e3-959c-e8039af04ab0', 0, 'rub', 'RUB', NULL, 'RU', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2af549-4764-11e3-959c-e8039af04ab0', 0, 'rub', 'RUB', NULL, 'TJ', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2af768-4764-11e3-959c-e8039af04ab0', 0, 'rwf', 'RWF', NULL, 'RW', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2af95c-4764-11e3-959c-e8039af04ab0', 0, 'sar', 'SAR', NULL, 'SA', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2afb15-4764-11e3-959c-e8039af04ab0', 0, 'sbd', 'SBD', NULL, 'SB', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2afcc9-4764-11e3-959c-e8039af04ab0', 0, 'scr', 'SCR', NULL, 'SC', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2afe79-4764-11e3-959c-e8039af04ab0', 0, 'sdd', 'SDD', NULL, 'SD', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b0029-4764-11e3-959c-e8039af04ab0', 0, 'sek', 'SEK', NULL, 'SE', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b01d9-4764-11e3-959c-e8039af04ab0', 0, 'sgd', 'SGD', NULL, 'SG', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b0384-4764-11e3-959c-e8039af04ab0', 0, 'shp', 'SHP', NULL, 'SH', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b0530-4764-11e3-959c-e8039af04ab0', 0, 'sit', 'SIT', NULL, 'SI', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b06dc-4764-11e3-959c-e8039af04ab0', 0, 'skk', 'SKK', NULL, 'SK', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b088c-4764-11e3-959c-e8039af04ab0', 0, 'sll', 'SLL', NULL, 'SL', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b0a37-4764-11e3-959c-e8039af04ab0', 0, 'sos', 'SOS', NULL, 'SO', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b0bdf-4764-11e3-959c-e8039af04ab0', 0, 'srd', 'SRD', NULL, 'SR', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b0d8f-4764-11e3-959c-e8039af04ab0', 0, 'std', 'STD', NULL, 'ST', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b0f54-4764-11e3-959c-e8039af04ab0', 0, 'svc', 'SVC', NULL, 'SV', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b1104-4764-11e3-959c-e8039af04ab0', 0, 'syp', 'SYP', NULL, 'SY', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b12b4-4764-11e3-959c-e8039af04ab0', 0, 'szl', 'SZL', NULL, 'SZ', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b145f-4764-11e3-959c-e8039af04ab0', 0, 'thb', 'THB', NULL, 'TH', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b160f-4764-11e3-959c-e8039af04ab0', 0, 'tmm', 'TMM', NULL, 'TM', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b17bb-4764-11e3-959c-e8039af04ab0', 0, 'tnd', 'TND', NULL, 'TN', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b1966-4764-11e3-959c-e8039af04ab0', 0, 'top', 'TOP', NULL, 'TO', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b1b12-4764-11e3-959c-e8039af04ab0', 0, 'try', 'TRY', NULL, 'TR', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b1cbe-4764-11e3-959c-e8039af04ab0', 0, 'ttd', 'TTD', NULL, 'TT', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b1e6e-4764-11e3-959c-e8039af04ab0', 0, 'tvd', 'TVD', NULL, 'TV', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b2019-4764-11e3-959c-e8039af04ab0', 0, 'twd', 'TWD', NULL, 'TW', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b21c5-4764-11e3-959c-e8039af04ab0', 0, 'tzs', 'TZS', NULL, 'TZ', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b2371-4764-11e3-959c-e8039af04ab0', 0, 'uah', 'UAH', NULL, 'UA', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b251c-4764-11e3-959c-e8039af04ab0', 0, 'ugx', 'UGX', NULL, 'UG', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b26c8-4764-11e3-959c-e8039af04ab0', 0, 'usd', 'USD', NULL, 'AS', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b2878-4764-11e3-959c-e8039af04ab0', 0, 'usd', 'USD', NULL, 'VI', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b2a23-4764-11e3-959c-e8039af04ab0', 0, 'usd', 'USD', NULL, 'VG', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b2bcf-4764-11e3-959c-e8039af04ab0', 0, 'usd', 'USD', NULL, 'IO', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b2d7f-4764-11e3-959c-e8039af04ab0', 0, 'usd', 'USD', NULL, 'EC', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b2f2b-4764-11e3-959c-e8039af04ab0', 0, 'usd', 'USD', NULL, 'GU', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b30db-4764-11e3-959c-e8039af04ab0', 0, 'usd', 'USD', NULL, 'HT', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b3286-4764-11e3-959c-e8039af04ab0', 0, 'usd', 'USD', NULL, 'MH', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b3436-4764-11e3-959c-e8039af04ab0', 0, 'usd', 'USD', NULL, 'FM', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b362f-4764-11e3-959c-e8039af04ab0', 0, 'usd', 'USD', NULL, 'MP', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b385f-4764-11e3-959c-e8039af04ab0', 0, 'usd', 'USD', NULL, 'PW', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b3a17-4764-11e3-959c-e8039af04ab0', 0, 'usd', 'USD', NULL, 'PA', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b3bc7-4764-11e3-959c-e8039af04ab0', 0, 'usd', 'USD', NULL, 'PR', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b3d77-4764-11e3-959c-e8039af04ab0', 0, 'usd', 'USD', NULL, 'TC', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b3f23-4764-11e3-959c-e8039af04ab0', 0, 'usd', 'USD', NULL, 'US', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b40d3-4764-11e3-959c-e8039af04ab0', 0, 'uyu', 'UYU', NULL, 'UY', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b427e-4764-11e3-959c-e8039af04ab0', 0, 'uzs', 'UZS', NULL, 'UZ', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b4433-4764-11e3-959c-e8039af04ab0', 0, 'veb', 'VEB', NULL, 'VE', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b45de-4764-11e3-959c-e8039af04ab0', 0, 'vnd', 'VND', NULL, 'VN', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b478e-4764-11e3-959c-e8039af04ab0', 0, 'vuv', 'VUV', NULL, 'VU', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b4942-4764-11e3-959c-e8039af04ab0', 0, 'wst', 'WST', NULL, 'WS', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b4b32-4764-11e3-959c-e8039af04ab0', 0, 'xaf', 'XAF', NULL, 'GQ', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b4ce7-4764-11e3-959c-e8039af04ab0', 0, 'xaf', 'XAF', NULL, 'GA', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b4e96-4764-11e3-959c-e8039af04ab0', 0, 'xaf', 'XAF', NULL, 'CM', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b5046-4764-11e3-959c-e8039af04ab0', 0, 'xaf', 'XAF', NULL, 'CG', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b51f6-4764-11e3-959c-e8039af04ab0', 0, 'xaf', 'XAF', NULL, 'TD', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b53a6-4764-11e3-959c-e8039af04ab0', 0, 'xaf', 'XAF', NULL, 'CF', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b5556-4764-11e3-959c-e8039af04ab0', 0, 'xcd', 'XCD', NULL, 'AI', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b5706-4764-11e3-959c-e8039af04ab0', 0, 'xcd', 'XCD', NULL, 'AG', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b58b2-4764-11e3-959c-e8039af04ab0', 0, 'xcd', 'XCD', NULL, 'DM', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b5a62-4764-11e3-959c-e8039af04ab0', 0, 'xcd', 'XCD', NULL, 'GD', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b5c0d-4764-11e3-959c-e8039af04ab0', 0, 'xcd', 'XCD', NULL, 'MS', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b5dd2-4764-11e3-959c-e8039af04ab0', 0, 'xcd', 'XCD', NULL, 'KN', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b5f82-4764-11e3-959c-e8039af04ab0', 0, 'xcd', 'XCD', NULL, 'LC', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b612e-4764-11e3-959c-e8039af04ab0', 0, 'xcd', 'XCD', NULL, 'VC', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b62de-4764-11e3-959c-e8039af04ab0', 0, 'xof', 'XOF', NULL, 'BJ', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b648a-4764-11e3-959c-e8039af04ab0', 0, 'xof', 'XOF', NULL, 'BF', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b6639-4764-11e3-959c-e8039af04ab0', 0, 'xof', 'XOF', NULL, 'CI', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b67e9-4764-11e3-959c-e8039af04ab0', 0, 'xof', 'XOF', NULL, 'GW', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b6995-4764-11e3-959c-e8039af04ab0', 0, 'xof', 'XOF', NULL, 'ML', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b6b45-4764-11e3-959c-e8039af04ab0', 0, 'xof', 'XOF', NULL, 'NE', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b6cf1-4764-11e3-959c-e8039af04ab0', 0, 'xof', 'XOF', NULL, 'SN', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b6ea0-4764-11e3-959c-e8039af04ab0', 0, 'xof', 'XOF', NULL, 'TG', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b7050-4764-11e3-959c-e8039af04ab0', 0, 'xpf', 'XPF', NULL, 'PF', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b7200-4764-11e3-959c-e8039af04ab0', 0, 'xpf', 'XPF', NULL, 'NC', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b73ac-4764-11e3-959c-e8039af04ab0', 0, 'xpf', 'XPF', NULL, 'WF', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b7558-4764-11e3-959c-e8039af04ab0', 0, 'yer', 'YER', NULL, 'YE', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b770c-4764-11e3-959c-e8039af04ab0', 0, 'zar', 'ZAR', NULL, 'NA', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b78b7-4764-11e3-959c-e8039af04ab0', 0, 'zar', 'ZAR', NULL, 'ZA', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b7a67-4764-11e3-959c-e8039af04ab0', 0, 'zmk', 'ZMK', NULL, 'ZM', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b7c17-4764-11e3-959c-e8039af04ab0', 0, 'zwd', 'ZWD', NULL, 'ZW', 'currency', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b7dcb-4764-11e3-959c-e8039af04ab0', 0, 'ledig', 'ledig', NULL, '', 'familystatus', 1, 1, '2014-01-01 00:00:00', '2014-09-01 15:34:57'),
('2d2b7f91-4764-11e3-959c-e8039af04ab0', 0, 'verheiratet', 'verheiratet', NULL, '', 'familystatus', 1, 1, '2014-01-01 00:00:00', '2014-09-01 15:34:53'),
('2d2b814d-4764-11e3-959c-e8039af04ab0', 0, 'ja', 'Ja', NULL, '', 'janein', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b830e-4764-11e3-959c-e8039af04ab0', 0, 'nein', 'Nein', NULL, '', 'janein', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b84e5-4764-11e3-959c-e8039af04ab0', 0, 'aa', 'Afar', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b869d-4764-11e3-959c-e8039af04ab0', 0, 'ab', 'Abchasisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b889a-4764-11e3-959c-e8039af04ab0', 0, 'af', 'Afrikaans', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b8a71-4764-11e3-959c-e8039af04ab0', 0, 'am', 'Amharisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b8c2d-4764-11e3-959c-e8039af04ab0', 0, 'ar', 'Arabisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b8de2-4764-11e3-959c-e8039af04ab0', 0, 'as', 'Assamesisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b8f9a-4764-11e3-959c-e8039af04ab0', 0, 'ay', 'Aymara', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b914e-4764-11e3-959c-e8039af04ab0', 0, 'az', 'Aserbaidschanisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b9307-4764-11e3-959c-e8039af04ab0', 0, 'ba', 'Baschkirisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b94bb-4764-11e3-959c-e8039af04ab0', 0, 'be', 'Belorussisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b966f-4764-11e3-959c-e8039af04ab0', 0, 'bg', 'Bulgarisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b981f-4764-11e3-959c-e8039af04ab0', 0, 'bh', 'Biharisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b99cf-4764-11e3-959c-e8039af04ab0', 0, 'bi', 'Bislamisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b9b7f-4764-11e3-959c-e8039af04ab0', 0, 'bn', 'Bengalisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b9d2f-4764-11e3-959c-e8039af04ab0', 0, 'bo', 'Tibetanisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2b9edf-4764-11e3-959c-e8039af04ab0', 0, 'br', 'Bretonisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ba08f-4764-11e3-959c-e8039af04ab0', 0, 'ca', 'Katalanisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ba247-4764-11e3-959c-e8039af04ab0', 0, 'co', 'Korsisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ba3ff-4764-11e3-959c-e8039af04ab0', 0, 'cs', 'Tschechisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ba5af-4764-11e3-959c-e8039af04ab0', 0, 'cy', 'Walisisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ba764-4764-11e3-959c-e8039af04ab0', 0, 'da', 'Dänisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ba918-4764-11e3-959c-e8039af04ab0', 0, 'de', 'Deutsch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2baae6-4764-11e3-959c-e8039af04ab0', 0, 'dz', 'Dzongkha, Bhutani', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2baca7-4764-11e3-959c-e8039af04ab0', 0, 'el', 'Griechisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bae5b-4764-11e3-959c-e8039af04ab0', 0, 'en', 'Englisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bb00f-4764-11e3-959c-e8039af04ab0', 0, 'eo', 'Esperanto', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bb1cc-4764-11e3-959c-e8039af04ab0', 0, 'es', 'Spanisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bb37c-4764-11e3-959c-e8039af04ab0', 0, 'et', 'Estnisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bb534-4764-11e3-959c-e8039af04ab0', 0, 'eu', 'Baskisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bb6e8-4764-11e3-959c-e8039af04ab0', 0, 'fa', 'Persisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bb89d-4764-11e3-959c-e8039af04ab0', 0, 'fi', 'Finnisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bba55-4764-11e3-959c-e8039af04ab0', 0, 'fj', 'Fiji', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bbc09-4764-11e3-959c-e8039af04ab0', 0, 'fo', 'Faröisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bbdbd-4764-11e3-959c-e8039af04ab0', 0, 'fr', 'Französisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bbf72-4764-11e3-959c-e8039af04ab0', 0, 'fy', 'Friesisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bc126-4764-11e3-959c-e8039af04ab0', 0, 'ga', 'Irisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bc2d6-4764-11e3-959c-e8039af04ab0', 0, 'gd', 'Schottisches Gälisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bc48e-4764-11e3-959c-e8039af04ab0', 0, 'gl', 'Galizisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bc642-4764-11e3-959c-e8039af04ab0', 0, 'gn', 'Guarani', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bc7f2-4764-11e3-959c-e8039af04ab0', 0, 'gu', 'Gujaratisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bc9a2-4764-11e3-959c-e8039af04ab0', 0, 'ha', 'Haussa', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bcb4e-4764-11e3-959c-e8039af04ab0', 0, 'he', 'Hebräisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bcd02-4764-11e3-959c-e8039af04ab0', 0, 'hi', 'Hindi', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bceb2-4764-11e3-959c-e8039af04ab0', 0, 'hr', 'Kroatisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bd066-4764-11e3-959c-e8039af04ab0', 0, 'hu', 'Ungarisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bd230-4764-11e3-959c-e8039af04ab0', 0, 'hy', 'Armenisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bd3e4-4764-11e3-959c-e8039af04ab0', 0, 'ia', 'Interlingua', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bd598-4764-11e3-959c-e8039af04ab0', 0, 'id', 'Indonesisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bd748-4764-11e3-959c-e8039af04ab0', 0, 'ie', 'Interlingue', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bd900-4764-11e3-959c-e8039af04ab0', 0, 'ik', 'Inupiak', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bdab0-4764-11e3-959c-e8039af04ab0', 0, 'is', 'Isländisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bdc60-4764-11e3-959c-e8039af04ab0', 0, 'it', 'Italienisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bde14-4764-11e3-959c-e8039af04ab0', 0, 'iu', 'Inuktitut', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bdfcd-4764-11e3-959c-e8039af04ab0', 0, 'ja', 'Japanisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2be17d-4764-11e3-959c-e8039af04ab0', 0, 'jv', 'Javanisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2be331-4764-11e3-959c-e8039af04ab0', 0, 'ka', 'Georgisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2be4e5-4764-11e3-959c-e8039af04ab0', 0, 'kk', 'Kasachisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2be699-4764-11e3-959c-e8039af04ab0', 0, 'kl', 'Kalaallisut (Grönländisch)', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2be856-4764-11e3-959c-e8039af04ab0', 0, 'km', 'Kambodschanisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bea0a-4764-11e3-959c-e8039af04ab0', 0, 'kn', 'Kannada', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bebba-4764-11e3-959c-e8039af04ab0', 0, 'ko', 'Koreanisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bed6e-4764-11e3-959c-e8039af04ab0', 0, 'ks', 'Kaschmirisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bef1e-4764-11e3-959c-e8039af04ab0', 0, 'ku', 'Kurdisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bf0ce-4764-11e3-959c-e8039af04ab0', 0, 'ky', 'Kirgisisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bf27e-4764-11e3-959c-e8039af04ab0', 0, 'la', 'Lateinisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bf432-4764-11e3-959c-e8039af04ab0', 0, 'ln', 'Lingala', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bf5e2-4764-11e3-959c-e8039af04ab0', 0, 'lo', 'Laotisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bf796-4764-11e3-959c-e8039af04ab0', 0, 'lt', 'Litauisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bfa5c-4764-11e3-959c-e8039af04ab0', 0, 'lv', 'Lettisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bfc3b-4764-11e3-959c-e8039af04ab0', 0, 'mg', 'Malagasisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bfdf4-4764-11e3-959c-e8039af04ab0', 0, 'mi', 'Maorisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2bffa8-4764-11e3-959c-e8039af04ab0', 0, 'mk', 'Mazedonisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c015c-4764-11e3-959c-e8039af04ab0', 0, 'ml', 'Malajalam', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c0310-4764-11e3-959c-e8039af04ab0', 0, 'mn', 'Mongolisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c04c4-4764-11e3-959c-e8039af04ab0', 0, 'mo', 'Moldavisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c0674-4764-11e3-959c-e8039af04ab0', 0, 'mr', 'Marathi', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c0829-4764-11e3-959c-e8039af04ab0', 0, 'ms', 'Malaysisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c09dd-4764-11e3-959c-e8039af04ab0', 0, 'mt', 'Maltesisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c0b8d-4764-11e3-959c-e8039af04ab0', 0, 'my', 'Burmesisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c0d3d-4764-11e3-959c-e8039af04ab0', 0, 'na', 'Nauruisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c0eec-4764-11e3-959c-e8039af04ab0', 0, 'ne', 'Nepalisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c10a1-4764-11e3-959c-e8039af04ab0', 0, 'nl', 'Holländisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c1255-4764-11e3-959c-e8039af04ab0', 0, 'no', 'Norwegisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c1405-4764-11e3-959c-e8039af04ab0', 0, 'oc', 'Okzitanisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c15c2-4764-11e3-959c-e8039af04ab0', 0, 'om', 'Oromo', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c179c-4764-11e3-959c-e8039af04ab0', 0, 'or', 'Orija', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c1dca-4764-11e3-959c-e8039af04ab0', 0, 'pa', 'Pundjabisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c1ff6-4764-11e3-959c-e8039af04ab0', 0, 'pl', 'Polnisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c21b7-4764-11e3-959c-e8039af04ab0', 0, 'ps', 'Paschtu', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c2370-4764-11e3-959c-e8039af04ab0', 0, 'pt', 'Portugiesisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c2528-4764-11e3-959c-e8039af04ab0', 0, 'qu', 'Quechua', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c26e1-4764-11e3-959c-e8039af04ab0', 0, 'rm', 'Rätoromanisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c2899-4764-11e3-959c-e8039af04ab0', 0, 'rn', 'Kirundisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c2a4d-4764-11e3-959c-e8039af04ab0', 0, 'ro', 'Rumänisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c2bfd-4764-11e3-959c-e8039af04ab0', 0, 'ru', 'Russisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c2db6-4764-11e3-959c-e8039af04ab0', 0, 'rw', 'Kijarwanda', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c2f6a-4764-11e3-959c-e8039af04ab0', 0, 'sa', 'Sanskrit', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c3122-4764-11e3-959c-e8039af04ab0', 0, 'sd', 'Zinti', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c32db-4764-11e3-959c-e8039af04ab0', 0, 'sg', 'Sango', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c348b-4764-11e3-959c-e8039af04ab0', 0, 'si', 'Singhalesisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c3643-4764-11e3-959c-e8039af04ab0', 0, 'sk', 'Slowakisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c37f8-4764-11e3-959c-e8039af04ab0', 0, 'sl', 'Slowenisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c39a7-4764-11e3-959c-e8039af04ab0', 0, 'sm', 'Samoanisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c3b60-4764-11e3-959c-e8039af04ab0', 0, 'sn', 'Schonisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c3d14-4764-11e3-959c-e8039af04ab0', 0, 'so', 'Somalisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c3ec8-4764-11e3-959c-e8039af04ab0', 0, 'sq', 'Albanisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c407c-4764-11e3-959c-e8039af04ab0', 0, 'sr', 'Serbisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c4231-4764-11e3-959c-e8039af04ab0', 0, 'ss', 'Swasiländisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c43e9-4764-11e3-959c-e8039af04ab0', 0, 'st', 'Sesothisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c4599-4764-11e3-959c-e8039af04ab0', 0, 'su', 'Sudanesisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c475e-4764-11e3-959c-e8039af04ab0', 0, 'sv', 'Schwedisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c4917-4764-11e3-959c-e8039af04ab0', 0, 'sw', 'Suaheli', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c4acb-4764-11e3-959c-e8039af04ab0', 0, 'ta', 'Tamilisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c4c83-4764-11e3-959c-e8039af04ab0', 0, 'te', 'Tegulu', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c4e38-4764-11e3-959c-e8039af04ab0', 0, 'tg', 'Tadschikisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c4ff0-4764-11e3-959c-e8039af04ab0', 0, 'th', 'Thai', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c51a4-4764-11e3-959c-e8039af04ab0', 0, 'ti', 'Tigrinja', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c5358-4764-11e3-959c-e8039af04ab0', 0, 'tk', 'Turkmenisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c550d-4764-11e3-959c-e8039af04ab0', 0, 'tl', 'Tagalog', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c56c1-4764-11e3-959c-e8039af04ab0', 0, 'tn', 'Sezuan', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c5871-4764-11e3-959c-e8039af04ab0', 0, 'to', 'Tongaisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c5a29-4764-11e3-959c-e8039af04ab0', 0, 'tr', 'Türkisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c5bdd-4764-11e3-959c-e8039af04ab0', 0, 'ts', 'Tsongaisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c5d92-4764-11e3-959c-e8039af04ab0', 0, 'tt', 'Tatarisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c5f4a-4764-11e3-959c-e8039af04ab0', 0, 'tw', 'Twi', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c60fe-4764-11e3-959c-e8039af04ab0', 0, 'ug', 'Uigur', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c62ae-4764-11e3-959c-e8039af04ab0', 0, 'uk', 'Ukrainisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c6462-4764-11e3-959c-e8039af04ab0', 0, 'ur', 'Urdu', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c6617-4764-11e3-959c-e8039af04ab0', 0, 'uz', 'Usbekisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c67cb-4764-11e3-959c-e8039af04ab0', 0, 'vi', 'Vietnamesisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c6983-4764-11e3-959c-e8039af04ab0', 0, 'vo', 'Volapük', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c6b3c-4764-11e3-959c-e8039af04ab0', 0, 'wo', 'Wolof', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c6cec-4764-11e3-959c-e8039af04ab0', 0, 'xh', 'Xhosa', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c6efa-4764-11e3-959c-e8039af04ab0', 0, 'yi', 'Jiddish', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c754e-4764-11e3-959c-e8039af04ab0', 0, 'yo', 'Joruba', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c78d9-4764-11e3-959c-e8039af04ab0', 0, 'za', 'Zhuang', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c7bd6-4764-11e3-959c-e8039af04ab0', 0, 'zh', 'Chinesisch', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c7eb2-4764-11e3-959c-e8039af04ab0', 0, 'zu', 'Zulu', NULL, '', 'language', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c819a-4764-11e3-959c-e8039af04ab0', 0, '+1', '+1', NULL, 'US', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c8475-4764-11e3-959c-e8039af04ab0', 0, '+1242', '+1242', NULL, 'BS', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c876a-4764-11e3-959c-e8039af04ab0', 0, '+1246', '+1246', NULL, 'BB', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c8a38-4764-11e3-959c-e8039af04ab0', 0, '+1264', '+1264', NULL, 'AI', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c8d2d-4764-11e3-959c-e8039af04ab0', 0, '+1268', '+1268', NULL, 'AG', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c9015-4764-11e3-959c-e8039af04ab0', 0, '+1284', '+1284', NULL, 'VG', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c9320-4764-11e3-959c-e8039af04ab0', 0, '+1340', '+1340', NULL, 'VI', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c9a6c-4764-11e3-959c-e8039af04ab0', 0, '+1345', '+1345', NULL, 'KY', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2c9e84-4764-11e3-959c-e8039af04ab0', 0, '+1441', '+1441', NULL, 'BM', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ca092-4764-11e3-959c-e8039af04ab0', 0, '+1473', '+1473', NULL, 'GD', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ca24b-4764-11e3-959c-e8039af04ab0', 0, '+1649', '+1649', NULL, 'TC', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ca3ff-4764-11e3-959c-e8039af04ab0', 0, '+1664', '+1664', NULL, 'MS', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ca5b3-4764-11e3-959c-e8039af04ab0', 0, '+1670', '+1670', NULL, 'MP', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ca767-4764-11e3-959c-e8039af04ab0', 0, '+1671', '+1671', NULL, 'GU', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ca917-4764-11e3-959c-e8039af04ab0', 0, '+1684', '+1684', NULL, 'AS', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2caac3-4764-11e3-959c-e8039af04ab0', 0, '+1758', '+1758', NULL, 'LC', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2cac73-4764-11e3-959c-e8039af04ab0', 0, '+1767', '+1767', NULL, 'DM', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2cae23-4764-11e3-959c-e8039af04ab0', 0, '+1784', '+1784', NULL, 'VC', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2cafd2-4764-11e3-959c-e8039af04ab0', 0, '+1809', '+1809', NULL, 'DO', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2cb187-4764-11e3-959c-e8039af04ab0', 0, '+1868', '+1868', NULL, 'TT', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2cb33b-4764-11e3-959c-e8039af04ab0', 0, '+1869', '+1869', NULL, 'KN', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2cb4ef-4764-11e3-959c-e8039af04ab0', 0, '+1876', '+1876', NULL, 'JM', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2cb69f-4764-11e3-959c-e8039af04ab0', 0, '+1939', '+1939', NULL, 'PR', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2cb84f-4764-11e3-959c-e8039af04ab0', 0, '+1nxx', '+1NXX', NULL, 'CA', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2cb9fa-4764-11e3-959c-e8039af04ab0', 0, '+20', '+20', NULL, 'EG', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2cbcbc-4764-11e3-959c-e8039af04ab0', 0, '+211', '+211', NULL, 'MA', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2cbfba-4764-11e3-959c-e8039af04ab0', 0, '+213', '+213', NULL, 'DZ', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2cc17f-4764-11e3-959c-e8039af04ab0', 0, '+216', '+216', NULL, 'TN', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2cc333-4764-11e3-959c-e8039af04ab0', 0, '+218', '+218', NULL, 'LY', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2cc4eb-4764-11e3-959c-e8039af04ab0', 0, '+220', '+220', NULL, 'GM', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2cc6a0-4764-11e3-959c-e8039af04ab0', 0, '+221', '+221', NULL, 'SN', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2cc854-4764-11e3-959c-e8039af04ab0', 0, '+222', '+222', NULL, 'MR', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2cca80-4764-11e3-959c-e8039af04ab0', 0, '+223', '+223', NULL, 'ML', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ccc74-4764-11e3-959c-e8039af04ab0', 0, '+224', '+224', NULL, 'GN', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2cce2d-4764-11e3-959c-e8039af04ab0', 0, '+225', '+225', NULL, 'CI', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ccfdc-4764-11e3-959c-e8039af04ab0', 0, '+226', '+226', NULL, 'BF', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2cd195-4764-11e3-959c-e8039af04ab0', 0, '+227', '+227', NULL, 'NE', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2cd34d-4764-11e3-959c-e8039af04ab0', 0, '+228', '+228', NULL, 'TG', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2cd5f1-4764-11e3-959c-e8039af04ab0', 0, '+229', '+229', NULL, 'BJ', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2cd7d0-4764-11e3-959c-e8039af04ab0', 0, '+230', '+230', NULL, 'MU', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2cd984-4764-11e3-959c-e8039af04ab0', 0, '+231', '+231', NULL, 'LR', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2cdb38-4764-11e3-959c-e8039af04ab0', 0, '+232', '+232', NULL, 'SL', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2cdcf1-4764-11e3-959c-e8039af04ab0', 0, '+233', '+233', NULL, 'GH', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2cdea5-4764-11e3-959c-e8039af04ab0', 0, '+234', '+234', NULL, 'NG', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ce055-4764-11e3-959c-e8039af04ab0', 0, '+235', '+235', NULL, 'TD', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ce2b9-4764-11e3-959c-e8039af04ab0', 0, '+236', '+236', NULL, 'CF', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ce4a0-4764-11e3-959c-e8039af04ab0', 0, '+237', '+237', NULL, 'CM', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ce72a-4764-11e3-959c-e8039af04ab0', 0, '+238', '+238', NULL, 'CV', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2cea8a-4764-11e3-959c-e8039af04ab0', 0, '+239', '+239', NULL, 'ST', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ceef3-4764-11e3-959c-e8039af04ab0', 0, '+240', '+240', NULL, 'GQ', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2cf2b9-4764-11e3-959c-e8039af04ab0', 0, '+241', '+241', NULL, 'GA', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2cf633-4764-11e3-959c-e8039af04ab0', 0, '+242', '+242', NULL, 'CG', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2cfa31-4764-11e3-959c-e8039af04ab0', 0, '+243', '+243', NULL, 'CD', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2cfdcc-4764-11e3-959c-e8039af04ab0', 0, '+244', '+244', NULL, 'AO', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d013d-4764-11e3-959c-e8039af04ab0', 0, '+245', '+245', NULL, 'GW', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d0584-4764-11e3-959c-e8039af04ab0', 0, '+246', '+246', NULL, 'DG', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d07f9-4764-11e3-959c-e8039af04ab0', 0, '+247', '+247', NULL, 'AC', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d09cf-4764-11e3-959c-e8039af04ab0', 0, '+248', '+248', NULL, 'SC', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d0c95-4764-11e3-959c-e8039af04ab0', 0, '+249', '+249', NULL, 'SD', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d0fce-4764-11e3-959c-e8039af04ab0', 0, '+250', '+250', NULL, 'RW', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d12c8-4764-11e3-959c-e8039af04ab0', 0, '+251', '+251', NULL, 'ET', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d1668-4764-11e3-959c-e8039af04ab0', 0, '+252', '+252', NULL, 'SO', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00');
INSERT INTO `domains` (`id`, `sequence`, `key`, `value`, `info`, `parameter`, `type`, `active`, `is_default`, `created`, `modified`) VALUES
('2d2d19a9-4764-11e3-959c-e8039af04ab0', 0, '+253', '+253', NULL, 'DJ', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d1ccd-4764-11e3-959c-e8039af04ab0', 0, '+254', '+254', NULL, 'KE', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d1ea0-4764-11e3-959c-e8039af04ab0', 0, '+255', '+255', NULL, 'TZ', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d2076-4764-11e3-959c-e8039af04ab0', 0, '+256', '+256', NULL, 'UG', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d2251-4764-11e3-959c-e8039af04ab0', 0, '+257', '+257', NULL, 'BI', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d2423-4764-11e3-959c-e8039af04ab0', 0, '+258', '+258', NULL, 'MZ', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d25f5-4764-11e3-959c-e8039af04ab0', 0, '+260', '+260', NULL, 'ZM', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d27d0-4764-11e3-959c-e8039af04ab0', 0, '+261', '+261', NULL, 'MG', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d29aa-4764-11e3-959c-e8039af04ab0', 0, '+262', '+262', NULL, 'RE', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d2b96-4764-11e3-959c-e8039af04ab0', 0, '+263', '+263', NULL, 'ZW', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d2db9-4764-11e3-959c-e8039af04ab0', 0, '+264', '+264', NULL, 'NA', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d2f98-4764-11e3-959c-e8039af04ab0', 0, '+265', '+265', NULL, 'MW', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d31af-4764-11e3-959c-e8039af04ab0', 0, '+266', '+266', NULL, 'LS', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d33e3-4764-11e3-959c-e8039af04ab0', 0, '+267', '+267', NULL, 'BW', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d36d0-4764-11e3-959c-e8039af04ab0', 0, '+268', '+268', NULL, 'SZ', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d3a4d-4764-11e3-959c-e8039af04ab0', 0, '+269', '+269', NULL, 'KM', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d3ccf-4764-11e3-959c-e8039af04ab0', 0, '+269', '+269', NULL, 'YT', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d3ea5-4764-11e3-959c-e8039af04ab0', 0, '+27', '+27', NULL, 'ZA', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d4073-4764-11e3-959c-e8039af04ab0', 0, '+290', '+290', NULL, 'SH', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d4230-4764-11e3-959c-e8039af04ab0', 0, '+290', '+290', NULL, 'TA', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d43fe-4764-11e3-959c-e8039af04ab0', 0, '+291', '+291', NULL, 'ER', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d45d0-4764-11e3-959c-e8039af04ab0', 0, '+297', '+297', NULL, 'AW', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d478d-4764-11e3-959c-e8039af04ab0', 0, '+298', '+298', NULL, 'FO', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d4963-4764-11e3-959c-e8039af04ab0', 0, '+299', '+299', NULL, 'GL', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d4b24-4764-11e3-959c-e8039af04ab0', 0, '+30', '+30', NULL, 'GR', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d4cee-4764-11e3-959c-e8039af04ab0', 0, '+31', '+31', NULL, 'NL', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d4ea6-4764-11e3-959c-e8039af04ab0', 0, '+32', '+32', NULL, 'BE', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d5063-4764-11e3-959c-e8039af04ab0', 0, '+33', '+33', NULL, 'FR', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d5220-4764-11e3-959c-e8039af04ab0', 0, '+34', '+34', NULL, 'ES', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d53d8-4764-11e3-959c-e8039af04ab0', 0, '+350', '+350', NULL, 'GI', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d55aa-4764-11e3-959c-e8039af04ab0', 0, '+351', '+351', NULL, 'PT', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d577c-4764-11e3-959c-e8039af04ab0', 0, '+352', '+352', NULL, 'LU', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d597d-4764-11e3-959c-e8039af04ab0', 0, '+353', '+353', NULL, 'IE', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d5b5c-4764-11e3-959c-e8039af04ab0', 0, '+354', '+354', NULL, 'IS', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d5d2f-4764-11e3-959c-e8039af04ab0', 0, '+355', '+355', NULL, 'AL', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d5efc-4764-11e3-959c-e8039af04ab0', 0, '+356', '+356', NULL, 'MT', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d60c6-4764-11e3-959c-e8039af04ab0', 0, '+357', '+357', NULL, 'CY', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d627e-4764-11e3-959c-e8039af04ab0', 0, '+358', '+358', NULL, 'FI', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d6437-4764-11e3-959c-e8039af04ab0', 0, '+35818', '+35818', NULL, 'AX', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d65fc-4764-11e3-959c-e8039af04ab0', 0, '+359', '+359', NULL, 'BG', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d67c1-4764-11e3-959c-e8039af04ab0', 0, '+36', '+36', NULL, 'HU', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d698b-4764-11e3-959c-e8039af04ab0', 0, '+370', '+370', NULL, 'LT', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d6b5d-4764-11e3-959c-e8039af04ab0', 0, '+371', '+371', NULL, 'LV', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d6d16-4764-11e3-959c-e8039af04ab0', 0, '+372', '+372', NULL, 'EE', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d6ece-4764-11e3-959c-e8039af04ab0', 0, '+373', '+373', NULL, 'MD', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d708f-4764-11e3-959c-e8039af04ab0', 0, '+374', '+374', NULL, 'AM', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d724c-4764-11e3-959c-e8039af04ab0', 0, '+375', '+375', NULL, 'BY', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d741a-4764-11e3-959c-e8039af04ab0', 0, '+376', '+376', NULL, 'AD', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d75d6-4764-11e3-959c-e8039af04ab0', 0, '+377', '+377', NULL, 'MC', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d7797-4764-11e3-959c-e8039af04ab0', 0, '+378', '+378', NULL, 'SM', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d7950-4764-11e3-959c-e8039af04ab0', 0, '+380', '+380', NULL, 'UA', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d7b0d-4764-11e3-959c-e8039af04ab0', 0, '+381', '+381', NULL, 'CS', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d7cd2-4764-11e3-959c-e8039af04ab0', 0, '+382', '+382', NULL, 'ME', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d7e93-4764-11e3-959c-e8039af04ab0', 0, '+385', '+385', NULL, 'HR', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d80b6-4764-11e3-959c-e8039af04ab0', 0, '+386', '+386', NULL, 'SI', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d837c-4764-11e3-959c-e8039af04ab0', 0, '+387', '+387', NULL, 'BA', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d8602-4764-11e3-959c-e8039af04ab0', 0, '+3883', '+3883', NULL, 'EU', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d8832-4764-11e3-959c-e8039af04ab0', 0, '+389', '+389', NULL, 'MK', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d89ef-4764-11e3-959c-e8039af04ab0', 0, '+39', '+39', NULL, 'IT', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d8ba7-4764-11e3-959c-e8039af04ab0', 0, '+3906', '+3906', NULL, 'VA', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d8d68-4764-11e3-959c-e8039af04ab0', 0, '+40', '+40', NULL, 'RO', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d8f3b-4764-11e3-959c-e8039af04ab0', 0, '+41', '+41', NULL, 'CH', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d9100-4764-11e3-959c-e8039af04ab0', 0, '+420', '+420', NULL, 'CZ', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d92b8-4764-11e3-959c-e8039af04ab0', 0, '+421', '+421', NULL, 'SK', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d9475-4764-11e3-959c-e8039af04ab0', 0, '+423', '+423', NULL, 'LI', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d963a-4764-11e3-959c-e8039af04ab0', 0, '+43', '+43', NULL, 'AT', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d992b-4764-11e3-959c-e8039af04ab0', 0, '+44', '+44', NULL, 'GG', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d9b46-4764-11e3-959c-e8039af04ab0', 0, '+44', '+44', NULL, 'IM', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d9d07-4764-11e3-959c-e8039af04ab0', 0, '+44', '+44', NULL, 'JE', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2d9ebb-4764-11e3-959c-e8039af04ab0', 0, '+44', '+44', NULL, 'GB', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2da06b-4764-11e3-959c-e8039af04ab0', 0, '+45', '+45', NULL, 'DK', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2da21b-4764-11e3-959c-e8039af04ab0', 0, '+46', '+46', NULL, 'SE', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2da3cb-4764-11e3-959c-e8039af04ab0', 0, '+47', '+47', NULL, 'NO', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2da576-4764-11e3-959c-e8039af04ab0', 0, '+48', '+48', NULL, 'PL', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2da740-4764-11e3-959c-e8039af04ab0', 0, '+49', '+49', NULL, 'DE', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2da8f0-4764-11e3-959c-e8039af04ab0', 0, '+500', '+500', NULL, 'FK', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2daaa8-4764-11e3-959c-e8039af04ab0', 0, '+508', '+508', NULL, 'PM', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2dac65-4764-11e3-959c-e8039af04ab0', 0, '+51', '+51', NULL, 'BZ', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2dae15-4764-11e3-959c-e8039af04ab0', 0, '+51', '+51', NULL, 'PE', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2dafc9-4764-11e3-959c-e8039af04ab0', 0, '+52', '+52', NULL, 'GT', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2db179-4764-11e3-959c-e8039af04ab0', 0, '+52', '+52', NULL, 'MX', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2db325-4764-11e3-959c-e8039af04ab0', 0, '+53', '+53', NULL, 'SV', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2db4d5-4764-11e3-959c-e8039af04ab0', 0, '+53', '+53', NULL, 'CU', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2db680-4764-11e3-959c-e8039af04ab0', 0, '+54', '+54', NULL, 'AR', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2db82c-4764-11e3-959c-e8039af04ab0', 0, '+54', '+54', NULL, 'HN', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2db9dc-4764-11e3-959c-e8039af04ab0', 0, '+55', '+55', NULL, 'BR', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2dbb87-4764-11e3-959c-e8039af04ab0', 0, '+55', '+55', NULL, 'NI', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2dbd33-4764-11e3-959c-e8039af04ab0', 0, '+56', '+56', NULL, 'CL', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2dbedf-4764-11e3-959c-e8039af04ab0', 0, '+56', '+56', NULL, 'CR', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2dc08f-4764-11e3-959c-e8039af04ab0', 0, '+57', '+57', NULL, 'CO', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2dc23a-4764-11e3-959c-e8039af04ab0', 0, '+57', '+57', NULL, 'PA', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2dc3e6-4764-11e3-959c-e8039af04ab0', 0, '+58', '+58', NULL, 'VE', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2dc592-4764-11e3-959c-e8039af04ab0', 0, '+59', '+59', NULL, 'HT', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2dc739-4764-11e3-959c-e8039af04ab0', 0, '+590', '+590', NULL, 'GP', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2dc8e9-4764-11e3-959c-e8039af04ab0', 0, '+591', '+591', NULL, 'BO', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2dca99-4764-11e3-959c-e8039af04ab0', 0, '+592', '+592', NULL, 'GY', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2dcc4d-4764-11e3-959c-e8039af04ab0', 0, '+593', '+593', NULL, 'EC', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2dce16-4764-11e3-959c-e8039af04ab0', 0, '+594', '+594', NULL, 'GF', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2dcfcb-4764-11e3-959c-e8039af04ab0', 0, '+595', '+595', NULL, 'PY', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2dd17b-4764-11e3-959c-e8039af04ab0', 0, '+596', '+596', NULL, 'MQ', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2dd32a-4764-11e3-959c-e8039af04ab0', 0, '+597', '+597', NULL, 'SR', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2dd4da-4764-11e3-959c-e8039af04ab0', 0, '+598', '+598', NULL, 'UY', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2dd68a-4764-11e3-959c-e8039af04ab0', 0, '+599', '+599', NULL, 'AN', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2dd83a-4764-11e3-959c-e8039af04ab0', 0, '+60', '+60', NULL, 'MY', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2dd9ee-4764-11e3-959c-e8039af04ab0', 0, '+61', '+61', NULL, 'AU', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ddb9a-4764-11e3-959c-e8039af04ab0', 0, '+62', '+62', NULL, 'ID', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ddd46-4764-11e3-959c-e8039af04ab0', 0, '+63', '+63', NULL, 'PH', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ddef6-4764-11e3-959c-e8039af04ab0', 0, '+64', '+64', NULL, 'NZ', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2de0a6-4764-11e3-959c-e8039af04ab0', 0, '+649', '+649', NULL, 'PN', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2de25e-4764-11e3-959c-e8039af04ab0', 0, '+65', '+65', NULL, 'SG', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2de40e-4764-11e3-959c-e8039af04ab0', 0, '+66', '+66', NULL, 'TH', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2de5be-4764-11e3-959c-e8039af04ab0', 0, '+670', '+670', NULL, 'TL', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2de769-4764-11e3-959c-e8039af04ab0', 0, '+672', '+672', NULL, 'AQ', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2de919-4764-11e3-959c-e8039af04ab0', 0, '+6723', '+6723', NULL, 'NF', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2deac9-4764-11e3-959c-e8039af04ab0', 0, '+673', '+673', NULL, 'BN', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2dec7d-4764-11e3-959c-e8039af04ab0', 0, '+674', '+674', NULL, 'NR', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2dee2d-4764-11e3-959c-e8039af04ab0', 0, '+675', '+675', NULL, 'PG', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2defd9-4764-11e3-959c-e8039af04ab0', 0, '+676', '+676', NULL, 'TO', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2df189-4764-11e3-959c-e8039af04ab0', 0, '+677', '+677', NULL, 'SB', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2df335-4764-11e3-959c-e8039af04ab0', 0, '+678', '+678', NULL, 'VU', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2df502-4764-11e3-959c-e8039af04ab0', 0, '+679', '+679', NULL, 'FJ', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2df7ef-4764-11e3-959c-e8039af04ab0', 0, '+680', '+680', NULL, 'PW', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2df9a3-4764-11e3-959c-e8039af04ab0', 0, '+681', '+681', NULL, 'WF', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2dfb57-4764-11e3-959c-e8039af04ab0', 0, '+682', '+682', NULL, 'CK', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2dfd0b-4764-11e3-959c-e8039af04ab0', 0, '+683', '+683', NULL, 'NU', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2dfec0-4764-11e3-959c-e8039af04ab0', 0, '+686', '+686', NULL, 'KI', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e010e-4764-11e3-959c-e8039af04ab0', 0, '+687', '+687', NULL, 'NC', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e04b2-4764-11e3-959c-e8039af04ab0', 0, '+688', '+688', NULL, 'TV', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e078d-4764-11e3-959c-e8039af04ab0', 0, '+689', '+689', NULL, 'PF', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e0ae9-4764-11e3-959c-e8039af04ab0', 0, '+690', '+690', NULL, 'TK', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e0e5a-4764-11e3-959c-e8039af04ab0', 0, '+691', '+691', NULL, 'FM', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e1131-4764-11e3-959c-e8039af04ab0', 0, '+692', '+692', NULL, 'MH', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e13a5-4764-11e3-959c-e8039af04ab0', 0, '+7', '+7', NULL, 'KZ', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e1591-4764-11e3-959c-e8039af04ab0', 0, '+7', '+7', NULL, 'RU', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e17fd-4764-11e3-959c-e8039af04ab0', 0, '+81', '+81', NULL, 'JP', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e19c7-4764-11e3-959c-e8039af04ab0', 0, '+82', '+82', NULL, 'KR', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e1b77-4764-11e3-959c-e8039af04ab0', 0, '+84', '+84', NULL, 'VN', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e1dd2-4764-11e3-959c-e8039af04ab0', 0, '+850', '+850', NULL, 'KP', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e2093-4764-11e3-959c-e8039af04ab0', 0, '+852', '+852', NULL, 'HK', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e2348-4764-11e3-959c-e8039af04ab0', 0, '+853', '+853', NULL, 'MO', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e250d-4764-11e3-959c-e8039af04ab0', 0, '+855', '+855', NULL, 'KH', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e2706-4764-11e3-959c-e8039af04ab0', 0, '+856', '+856', NULL, 'LA', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e28dc-4764-11e3-959c-e8039af04ab0', 0, '+86', '+86', NULL, 'CN', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e2aa2-4764-11e3-959c-e8039af04ab0', 0, '+880', '+880', NULL, 'BD', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e2c89-4764-11e3-959c-e8039af04ab0', 0, '+886', '+886', NULL, 'TW', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e2e3d-4764-11e3-959c-e8039af04ab0', 0, '+90', '+90', NULL, 'TR', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e2ff1-4764-11e3-959c-e8039af04ab0', 0, '+91', '+91', NULL, 'IN', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e31a1-4764-11e3-959c-e8039af04ab0', 0, '+92', '+92', NULL, 'PK', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e334d-4764-11e3-959c-e8039af04ab0', 0, '+93', '+93', NULL, 'AF', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e3501-4764-11e3-959c-e8039af04ab0', 0, '+94', '+94', NULL, 'LK', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e36ad-4764-11e3-959c-e8039af04ab0', 0, '+95', '+95', NULL, 'MM', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e3861-4764-11e3-959c-e8039af04ab0', 0, '+960', '+960', NULL, 'MV', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e3a15-4764-11e3-959c-e8039af04ab0', 0, '+961', '+961', NULL, 'LB', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e3bc9-4764-11e3-959c-e8039af04ab0', 0, '+962', '+962', NULL, 'JO', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e3d7e-4764-11e3-959c-e8039af04ab0', 0, '+963', '+963', NULL, 'SY', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e3f29-4764-11e3-959c-e8039af04ab0', 0, '+964', '+964', NULL, 'IQ', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e40d9-4764-11e3-959c-e8039af04ab0', 0, '+965', '+965', NULL, 'KW', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e4289-4764-11e3-959c-e8039af04ab0', 0, '+966', '+966', NULL, 'SA', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e4502-4764-11e3-959c-e8039af04ab0', 0, '+967', '+967', NULL, 'YE', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e4707-4764-11e3-959c-e8039af04ab0', 0, '+968', '+968', NULL, 'OM', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e48bc-4764-11e3-959c-e8039af04ab0', 0, '+970', '+970', NULL, 'PS', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e4a6b-4764-11e3-959c-e8039af04ab0', 0, '+971', '+971', NULL, 'AE', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e4c20-4764-11e3-959c-e8039af04ab0', 0, '+972', '+972', NULL, 'IL', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e4dd4-4764-11e3-959c-e8039af04ab0', 0, '+973', '+973', NULL, 'BH', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e4f84-4764-11e3-959c-e8039af04ab0', 0, '+974', '+974', NULL, 'QA', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e5134-4764-11e3-959c-e8039af04ab0', 0, '+975', '+975', NULL, 'BT', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e52df-4764-11e3-959c-e8039af04ab0', 0, '+976', '+976', NULL, 'MN', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e5493-4764-11e3-959c-e8039af04ab0', 0, '+977', '+977', NULL, 'NP', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e563f-4764-11e3-959c-e8039af04ab0', 0, '+98', '+98', NULL, 'IR', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e57f3-4764-11e3-959c-e8039af04ab0', 0, '+992', '+992', NULL, 'TJ', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e59a3-4764-11e3-959c-e8039af04ab0', 0, '+993', '+993', NULL, 'TM', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e5b57-4764-11e3-959c-e8039af04ab0', 0, '+994', '+994', NULL, 'AZ', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e5d10-4764-11e3-959c-e8039af04ab0', 0, '+995', '+995', NULL, 'GE', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e5ec0-4764-11e3-959c-e8039af04ab0', 0, '+996', '+996', NULL, 'KG', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e6070-4764-11e3-959c-e8039af04ab0', 0, '+998', '+998', NULL, 'UZ', 'phone', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e6224-4764-11e3-959c-e8039af04ab0', 0, 'frau', 'Frau', NULL, '', 'salutation', 1, 1, '2014-01-01 00:00:00', '2014-02-05 19:36:57'),
('2d2e63e1-4764-11e3-959c-e8039af04ab0', 0, 'herr', 'Herr', NULL, '', 'salutation', 1, 1, '2014-01-01 00:00:00', '2014-02-05 19:36:50'),
('2d2e6595-4764-11e3-959c-e8039af04ab0', 0, 'dr', 'Dr.', NULL, '', 'title', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e6752-4764-11e3-959c-e8039af04ab0', 0, 'prof', 'Prof.', NULL, '', 'title', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e690e-4764-11e3-959c-e8039af04ab0', 0, '.ac', '.ac', NULL, 'AC', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e6adc-4764-11e3-959c-e8039af04ab0', 0, '.ad', '.ad', NULL, 'AD', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e6c8c-4764-11e3-959c-e8039af04ab0', 0, '.ae', '.ae', NULL, 'AE', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e6e3c-4764-11e3-959c-e8039af04ab0', 0, '.af', '.af', NULL, 'AF', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e6ff0-4764-11e3-959c-e8039af04ab0', 0, '.ag', '.ag', NULL, 'AG', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e71a0-4764-11e3-959c-e8039af04ab0', 0, '.ai', '.ai', NULL, 'AI', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e7350-4764-11e3-959c-e8039af04ab0', 0, '.al', '.al', NULL, 'AL', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e7500-4764-11e3-959c-e8039af04ab0', 0, '.am', '.am', NULL, 'AM', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e76ac-4764-11e3-959c-e8039af04ab0', 0, '.an', '.an', NULL, 'AN', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e785b-4764-11e3-959c-e8039af04ab0', 0, '.ao', '.ao', NULL, 'AO', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e7a0b-4764-11e3-959c-e8039af04ab0', 0, '.aq', '.aq', NULL, 'AQ', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e7bbb-4764-11e3-959c-e8039af04ab0', 0, '.ar', '.ar', NULL, 'AR', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e7d6b-4764-11e3-959c-e8039af04ab0', 0, '.as', '.as', NULL, 'AS', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e7f1f-4764-11e3-959c-e8039af04ab0', 0, '.at', '.at', NULL, 'AT', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e80d4-4764-11e3-959c-e8039af04ab0', 0, '.au', '.au', NULL, 'AU', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e827f-4764-11e3-959c-e8039af04ab0', 0, '.aw', '.aw', NULL, 'AW', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e8433-4764-11e3-959c-e8039af04ab0', 0, '.ax', '.ax', NULL, 'AX', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e85e3-4764-11e3-959c-e8039af04ab0', 0, '.az', '.az', NULL, 'AZ', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e8793-4764-11e3-959c-e8039af04ab0', 0, '.ba', '.ba', NULL, 'BA', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e893f-4764-11e3-959c-e8039af04ab0', 0, '.bb', '.bb', NULL, 'BB', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e8aef-4764-11e3-959c-e8039af04ab0', 0, '.bd', '.bd', NULL, 'BD', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e8c9f-4764-11e3-959c-e8039af04ab0', 0, '.be', '.be', NULL, 'BE', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e8e4f-4764-11e3-959c-e8039af04ab0', 0, '.bf', '.bf', NULL, 'BF', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e8ffa-4764-11e3-959c-e8039af04ab0', 0, '.bg', '.bg', NULL, 'BG', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e91fb-4764-11e3-959c-e8039af04ab0', 0, '.bh', '.bh', NULL, 'BH', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e946c-4764-11e3-959c-e8039af04ab0', 0, '.bi', '.bi', NULL, 'BI', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e962d-4764-11e3-959c-e8039af04ab0', 0, '.bj', '.bj', NULL, 'BJ', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e97e1-4764-11e3-959c-e8039af04ab0', 0, '.bm', '.bm', NULL, 'BM', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e9995-4764-11e3-959c-e8039af04ab0', 0, '.bn', '.bn', NULL, 'BN', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e9b45-4764-11e3-959c-e8039af04ab0', 0, '.bo', '.bo', NULL, 'BO', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2e9dd8-4764-11e3-959c-e8039af04ab0', 0, '.br', '.br', NULL, 'BR', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ea010-4764-11e3-959c-e8039af04ab0', 0, '.bs', '.bs', NULL, 'BS', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ea1c9-4764-11e3-959c-e8039af04ab0', 0, '.bt', '.bt', NULL, 'BT', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ea379-4764-11e3-959c-e8039af04ab0', 0, '.bv', '.bv', NULL, 'BV', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ea529-4764-11e3-959c-e8039af04ab0', 0, '.bw', '.bw', NULL, 'BW', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ea6d9-4764-11e3-959c-e8039af04ab0', 0, '.by', '.by', NULL, 'BY', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ea891-4764-11e3-959c-e8039af04ab0', 0, '.bz', '.bz', NULL, 'BZ', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2eaa41-4764-11e3-959c-e8039af04ab0', 0, '.ca', '.ca', NULL, 'CA', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2eabf1-4764-11e3-959c-e8039af04ab0', 0, '.cc', '.cc', NULL, 'CC', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2eada5-4764-11e3-959c-e8039af04ab0', 0, '.cd', '.cd', NULL, 'CD', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2eaf55-4764-11e3-959c-e8039af04ab0', 0, '.cf', '.cf', NULL, 'CF', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2eb105-4764-11e3-959c-e8039af04ab0', 0, '.cg', '.cg', NULL, 'CG', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2eb2b1-4764-11e3-959c-e8039af04ab0', 0, '.ch', '.ch', NULL, 'CH', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2eb45c-4764-11e3-959c-e8039af04ab0', 0, '.ci', '.ci', NULL, 'CI', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2eb60c-4764-11e3-959c-e8039af04ab0', 0, '.ck', '.ck', NULL, 'CK', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2eb7b3-4764-11e3-959c-e8039af04ab0', 0, '.cl', '.cl', NULL, 'CL', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2eba53-4764-11e3-959c-e8039af04ab0', 0, '.cm', '.cm', NULL, 'CM', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ebc65-4764-11e3-959c-e8039af04ab0', 0, '.cn', '.cn', NULL, 'CN', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ebe3b-4764-11e3-959c-e8039af04ab0', 0, '.co', '.co', NULL, 'CO', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ebff0-4764-11e3-959c-e8039af04ab0', 0, '.cr', '.cr', NULL, 'CR', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ec1a4-4764-11e3-959c-e8039af04ab0', 0, '.cs', '.cs', NULL, 'CS', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ec358-4764-11e3-959c-e8039af04ab0', 0, '.cu', '.cu', NULL, 'CU', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ec508-4764-11e3-959c-e8039af04ab0', 0, '.cv', '.cv', NULL, 'CV', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ec6b4-4764-11e3-959c-e8039af04ab0', 0, '.cx', '.cx', NULL, 'CX', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ec864-4764-11e3-959c-e8039af04ab0', 0, '.cy', '.cy', NULL, 'CY', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2eca13-4764-11e3-959c-e8039af04ab0', 0, '.cz', '.cz', NULL, 'CZ', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ecbc8-4764-11e3-959c-e8039af04ab0', 0, '.de', '.de', NULL, 'DE', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ecd78-4764-11e3-959c-e8039af04ab0', 0, '.dk', '.dk', NULL, 'DK', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ecf27-4764-11e3-959c-e8039af04ab0', 0, '.dm', '.dm', NULL, 'DM', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ed0dc-4764-11e3-959c-e8039af04ab0', 0, '.do', '.do', NULL, 'DO', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ed326-4764-11e3-959c-e8039af04ab0', 0, '.dz', '.dz', NULL, 'DZ', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ed5e7-4764-11e3-959c-e8039af04ab0', 0, '.ec', '.ec', NULL, 'EC', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ed8d3-4764-11e3-959c-e8039af04ab0', 0, '.ee', '.ee', NULL, 'EE', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2edbaf-4764-11e3-959c-e8039af04ab0', 0, '.eg', '.eg', NULL, 'EG', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2edddf-4764-11e3-959c-e8039af04ab0', 0, '.eh', '.eh', NULL, 'EH', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2edfdc-4764-11e3-959c-e8039af04ab0', 0, '.er', '.er', NULL, 'ER', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ee266-4764-11e3-959c-e8039af04ab0', 0, '.es', '.es', NULL, 'ES', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ee42f-4764-11e3-959c-e8039af04ab0', 0, '.et', '.et', NULL, 'ET', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ee5e4-4764-11e3-959c-e8039af04ab0', 0, '.eu', '.eu', NULL, 'EU', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ee79c-4764-11e3-959c-e8039af04ab0', 0, '.fi', '.fi', NULL, 'FI', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2eea3b-4764-11e3-959c-e8039af04ab0', 0, '.fj', '.fj', NULL, 'FJ', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2eebfc-4764-11e3-959c-e8039af04ab0', 0, '.fk', '.fk', NULL, 'FK', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2eedac-4764-11e3-959c-e8039af04ab0', 0, '.fm', '.fm', NULL, 'FM', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2eef5c-4764-11e3-959c-e8039af04ab0', 0, '.fo', '.fo', NULL, 'FO', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ef110-4764-11e3-959c-e8039af04ab0', 0, '.fr', '.fr', NULL, 'FR', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ef2c0-4764-11e3-959c-e8039af04ab0', 0, '.ga', '.ga', NULL, 'GA', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ef475-4764-11e3-959c-e8039af04ab0', 0, '.gb', '.gb', NULL, 'GB', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ef629-4764-11e3-959c-e8039af04ab0', 0, '.gd', '.gd', NULL, 'GD', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ef7d9-4764-11e3-959c-e8039af04ab0', 0, '.ge', '.ge', NULL, 'GE', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ef984-4764-11e3-959c-e8039af04ab0', 0, '.gf', '.gf', NULL, 'GF', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2efb34-4764-11e3-959c-e8039af04ab0', 0, '.gg', '.gg', NULL, 'GG', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2efce0-4764-11e3-959c-e8039af04ab0', 0, '.gh', '.gh', NULL, 'GH', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2efe98-4764-11e3-959c-e8039af04ab0', 0, '.gi', '.gi', NULL, 'GI', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f0044-4764-11e3-959c-e8039af04ab0', 0, '.gl', '.gl', NULL, 'GL', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f01fc-4764-11e3-959c-e8039af04ab0', 0, '.gm', '.gm', NULL, 'GM', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f03b1-4764-11e3-959c-e8039af04ab0', 0, '.gn', '.gn', NULL, 'GN', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f055c-4764-11e3-959c-e8039af04ab0', 0, '.gp', '.gp', NULL, 'GP', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f0759-4764-11e3-959c-e8039af04ab0', 0, '.gq', '.gq', NULL, 'GQ', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f09e7-4764-11e3-959c-e8039af04ab0', 0, '.gr', '.gr', NULL, 'GR', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f0bad-4764-11e3-959c-e8039af04ab0', 0, '.gt', '.gt', NULL, 'GT', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f0d5d-4764-11e3-959c-e8039af04ab0', 0, '.gu', '.gu', NULL, 'GU', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f0f11-4764-11e3-959c-e8039af04ab0', 0, '.gw', '.gw', NULL, 'GW', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f10c5-4764-11e3-959c-e8039af04ab0', 0, '.gy', '.gy', NULL, 'GY', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f1275-4764-11e3-959c-e8039af04ab0', 0, '.hk', '.hk', NULL, 'HK', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f14b2-4764-11e3-959c-e8039af04ab0', 0, '.hm', '.hm', NULL, 'HM', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f178d-4764-11e3-959c-e8039af04ab0', 0, '.hn', '.hn', NULL, 'HN', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f19b5-4764-11e3-959c-e8039af04ab0', 0, '.hr', '.hr', NULL, 'HR', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f1b72-4764-11e3-959c-e8039af04ab0', 0, '.ht', '.ht', NULL, 'HT', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f1d19-4764-11e3-959c-e8039af04ab0', 0, '.hu', '.hu', NULL, 'HU', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f1ecd-4764-11e3-959c-e8039af04ab0', 0, '.id', '.id', NULL, 'ID', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f207d-4764-11e3-959c-e8039af04ab0', 0, '.ie', '.ie', NULL, 'IE', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f2229-4764-11e3-959c-e8039af04ab0', 0, '.il', '.il', NULL, 'IL', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f23d4-4764-11e3-959c-e8039af04ab0', 0, '.im', '.im', NULL, 'IM', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f2584-4764-11e3-959c-e8039af04ab0', 0, '.in', '.in', NULL, 'IN', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f272c-4764-11e3-959c-e8039af04ab0', 0, '.io', '.io', NULL, 'IO', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f28d7-4764-11e3-959c-e8039af04ab0', 0, '.iq', '.iq', NULL, 'IQ', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f2a83-4764-11e3-959c-e8039af04ab0', 0, '.ir', '.ir', NULL, 'IR', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f2c2f-4764-11e3-959c-e8039af04ab0', 0, '.is', '.is', NULL, 'IS', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f2df4-4764-11e3-959c-e8039af04ab0', 0, '.it', '.it', NULL, 'IT', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f2fa4-4764-11e3-959c-e8039af04ab0', 0, '.je', '.je', NULL, 'JE', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f3154-4764-11e3-959c-e8039af04ab0', 0, '.jm', '.jm', NULL, 'JM', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f32ff-4764-11e3-959c-e8039af04ab0', 0, '.jo', '.jo', NULL, 'JO', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f34a7-4764-11e3-959c-e8039af04ab0', 0, '.jp', '.jp', NULL, 'JP', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f3657-4764-11e3-959c-e8039af04ab0', 0, '.ke', '.ke', NULL, 'KE', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f3802-4764-11e3-959c-e8039af04ab0', 0, '.kg', '.kg', NULL, 'KG', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f39ae-4764-11e3-959c-e8039af04ab0', 0, '.kh', '.kh', NULL, 'KH', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f3b5e-4764-11e3-959c-e8039af04ab0', 0, '.ki', '.ki', NULL, 'KI', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f3d05-4764-11e3-959c-e8039af04ab0', 0, '.km', '.km', NULL, 'KM', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f3eb1-4764-11e3-959c-e8039af04ab0', 0, '.kn', '.kn', NULL, 'KN', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f4058-4764-11e3-959c-e8039af04ab0', 0, '.kp', '.kp', NULL, 'KP', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f4204-4764-11e3-959c-e8039af04ab0', 0, '.kr', '.kr', NULL, 'KR', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f43af-4764-11e3-959c-e8039af04ab0', 0, '.kw', '.kw', NULL, 'KW', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f455f-4764-11e3-959c-e8039af04ab0', 0, '.ky', '.ky', NULL, 'KY', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f4707-4764-11e3-959c-e8039af04ab0', 0, '.kz', '.kz', NULL, 'KZ', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f48ae-4764-11e3-959c-e8039af04ab0', 0, '.la', '.la', NULL, 'LA', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f4a5e-4764-11e3-959c-e8039af04ab0', 0, '.lb', '.lb', NULL, 'LB', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f4c0a-4764-11e3-959c-e8039af04ab0', 0, '.lc', '.lc', NULL, 'LC', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f4db5-4764-11e3-959c-e8039af04ab0', 0, '.li', '.li', NULL, 'LI', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f4f65-4764-11e3-959c-e8039af04ab0', 0, '.lk', '.lk', NULL, 'LK', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f5111-4764-11e3-959c-e8039af04ab0', 0, '.lr', '.lr', NULL, 'LR', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f52b8-4764-11e3-959c-e8039af04ab0', 0, '.ls', '.ls', NULL, 'LS', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f5468-4764-11e3-959c-e8039af04ab0', 0, '.lt', '.lt', NULL, 'LT', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f5669-4764-11e3-959c-e8039af04ab0', 0, '.lu', '.lu', NULL, 'LU', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f583b-4764-11e3-959c-e8039af04ab0', 0, '.lv', '.lv', NULL, 'LV', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f59f4-4764-11e3-959c-e8039af04ab0', 0, '.ly', '.ly', NULL, 'LY', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f5ba4-4764-11e3-959c-e8039af04ab0', 0, '.ma', '.ma', NULL, 'MA', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f5d4f-4764-11e3-959c-e8039af04ab0', 0, '.mc', '.mc', NULL, 'MC', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f5eff-4764-11e3-959c-e8039af04ab0', 0, '.md', '.md', NULL, 'MD', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f60ab-4764-11e3-959c-e8039af04ab0', 0, '.me', '.me', NULL, 'ME', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f6257-4764-11e3-959c-e8039af04ab0', 0, '.mg', '.mg', NULL, 'MG', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f6402-4764-11e3-959c-e8039af04ab0', 0, '.mh', '.mh', NULL, 'MH', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f65ae-4764-11e3-959c-e8039af04ab0', 0, '.mk', '.mk', NULL, 'MK', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f675e-4764-11e3-959c-e8039af04ab0', 0, '.ml', '.ml', NULL, 'ML', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f690e-4764-11e3-959c-e8039af04ab0', 0, '.mm', '.mm', NULL, 'MM', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f6ab9-4764-11e3-959c-e8039af04ab0', 0, '.mn', '.mn', NULL, 'MN', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f6c6d-4764-11e3-959c-e8039af04ab0', 0, '.mo', '.mo', NULL, 'MO', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f6e1d-4764-11e3-959c-e8039af04ab0', 0, '.mp', '.mp', NULL, 'MP', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f6fcd-4764-11e3-959c-e8039af04ab0', 0, '.mq', '.mq', NULL, 'MQ', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f7179-4764-11e3-959c-e8039af04ab0', 0, '.mr', '.mr', NULL, 'MR', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f7325-4764-11e3-959c-e8039af04ab0', 0, '.ms', '.ms', NULL, 'MS', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f74d0-4764-11e3-959c-e8039af04ab0', 0, '.mt', '.mt', NULL, 'MT', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f767c-4764-11e3-959c-e8039af04ab0', 0, '.mu', '.mu', NULL, 'MU', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f782c-4764-11e3-959c-e8039af04ab0', 0, '.mv', '.mv', NULL, 'MV', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f79d7-4764-11e3-959c-e8039af04ab0', 0, '.mw', '.mw', NULL, 'MW', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f7b87-4764-11e3-959c-e8039af04ab0', 0, '.mx', '.mx', NULL, 'MX', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f7d4d-4764-11e3-959c-e8039af04ab0', 0, '.my', '.my', NULL, 'MY', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f7efc-4764-11e3-959c-e8039af04ab0', 0, '.mz', '.mz', NULL, 'MZ', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f80ac-4764-11e3-959c-e8039af04ab0', 0, '.na', '.na', NULL, 'NA', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f8258-4764-11e3-959c-e8039af04ab0', 0, '.nc', '.nc', NULL, 'NC', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f8404-4764-11e3-959c-e8039af04ab0', 0, '.ne', '.ne', NULL, 'NE', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f85af-4764-11e3-959c-e8039af04ab0', 0, '.nf', '.nf', NULL, 'NF', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f875b-4764-11e3-959c-e8039af04ab0', 0, '.ng', '.ng', NULL, 'NG', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f8902-4764-11e3-959c-e8039af04ab0', 0, '.ni', '.ni', NULL, 'NI', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f8aaa-4764-11e3-959c-e8039af04ab0', 0, '.nl', '.nl', NULL, 'NL', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f8c55-4764-11e3-959c-e8039af04ab0', 0, '.no', '.no', NULL, 'NO', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f8e05-4764-11e3-959c-e8039af04ab0', 0, '.np', '.np', NULL, 'NP', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f8fb1-4764-11e3-959c-e8039af04ab0', 0, '.nr', '.nr', NULL, 'NR', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f9158-4764-11e3-959c-e8039af04ab0', 0, '.nt', '.nt', NULL, 'NT', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f9304-4764-11e3-959c-e8039af04ab0', 0, '.nu', '.nu', NULL, 'NU', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f94af-4764-11e3-959c-e8039af04ab0', 0, '.nz', '.nz', NULL, 'NZ', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f965f-4764-11e3-959c-e8039af04ab0', 0, '.om', '.om', NULL, 'OM', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f9807-4764-11e3-959c-e8039af04ab0', 0, '.pa', '.pa', NULL, 'PA', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f99b7-4764-11e3-959c-e8039af04ab0', 0, '.pe', '.pe', NULL, 'PE', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f9b62-4764-11e3-959c-e8039af04ab0', 0, '.pf', '.pf', NULL, 'PF', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2f9d39-4764-11e3-959c-e8039af04ab0', 0, '.pg', '.pg', NULL, 'PG', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fa014-4764-11e3-959c-e8039af04ab0', 0, '.ph', '.ph', NULL, 'PH', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fa29a-4764-11e3-959c-e8039af04ab0', 0, '.pk', '.pk', NULL, 'PK', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fa579-4764-11e3-959c-e8039af04ab0', 0, '.pl', '.pl', NULL, 'PL', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fa798-4764-11e3-959c-e8039af04ab0', 0, '.pm', '.pm', NULL, 'PM', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2faa3c-4764-11e3-959c-e8039af04ab0', 0, '.pn', '.pn', NULL, 'PN', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fac01-4764-11e3-959c-e8039af04ab0', 0, '.pr', '.pr', NULL, 'PR', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fadb1-4764-11e3-959c-e8039af04ab0', 0, '.ps', '.ps', NULL, 'PS', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2faf61-4764-11e3-959c-e8039af04ab0', 0, '.pt', '.pt', NULL, 'PT', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fb10d-4764-11e3-959c-e8039af04ab0', 0, '.pw', '.pw', NULL, 'PW', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fb2b4-4764-11e3-959c-e8039af04ab0', 0, '.py', '.py', NULL, 'PY', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fb460-4764-11e3-959c-e8039af04ab0', 0, '.qa', '.qa', NULL, 'QA', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fb610-4764-11e3-959c-e8039af04ab0', 0, '.re', '.re', NULL, 'RE', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fb7bb-4764-11e3-959c-e8039af04ab0', 0, '.ro', '.ro', NULL, 'RO', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fb967-4764-11e3-959c-e8039af04ab0', 0, '.ru', '.ru', NULL, 'RU', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fbb17-4764-11e3-959c-e8039af04ab0', 0, '.rw', '.rw', NULL, 'RW', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fbcc7-4764-11e3-959c-e8039af04ab0', 0, '.sa', '.sa', NULL, 'SA', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fbe77-4764-11e3-959c-e8039af04ab0', 0, '.sb', '.sb', NULL, 'SB', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fc022-4764-11e3-959c-e8039af04ab0', 0, '.sc', '.sc', NULL, 'SC', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fc1ce-4764-11e3-959c-e8039af04ab0', 0, '.sd', '.sd', NULL, 'SD', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fc37e-4764-11e3-959c-e8039af04ab0', 0, '.se', '.se', NULL, 'SE', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fc529-4764-11e3-959c-e8039af04ab0', 0, '.sg', '.sg', NULL, 'SG', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fc6d5-4764-11e3-959c-e8039af04ab0', 0, '.sh', '.sh', NULL, 'SH', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fc881-4764-11e3-959c-e8039af04ab0', 0, '.si', '.si', NULL, 'SI', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fca4f-4764-11e3-959c-e8039af04ab0', 0, '.sj', '.sj', NULL, 'SJ', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fcd4c-4764-11e3-959c-e8039af04ab0', 0, '.sk', '.sk', NULL, 'SK', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fcf2b-4764-11e3-959c-e8039af04ab0', 0, '.sl', '.sl', NULL, 'SL', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fd13d-4764-11e3-959c-e8039af04ab0', 0, '.sm', '.sm', NULL, 'SM', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fd3ea-4764-11e3-959c-e8039af04ab0', 0, '.sn', '.sn', NULL, 'SN', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fd6f8-4764-11e3-959c-e8039af04ab0', 0, '.so', '.so', NULL, 'SO', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fd93e-4764-11e3-959c-e8039af04ab0', 0, '.sr', '.sr', NULL, 'SR', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00');
INSERT INTO `domains` (`id`, `sequence`, `key`, `value`, `info`, `parameter`, `type`, `active`, `is_default`, `created`, `modified`) VALUES
('2d2fdaee-4764-11e3-959c-e8039af04ab0', 0, '.st', '.st', NULL, 'ST', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fdc99-4764-11e3-959c-e8039af04ab0', 0, '.su', '.su', NULL, 'SU', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fde45-4764-11e3-959c-e8039af04ab0', 0, '.sv', '.sv', NULL, 'SV', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fdff5-4764-11e3-959c-e8039af04ab0', 0, '.sy', '.sy', NULL, 'SY', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fe1a0-4764-11e3-959c-e8039af04ab0', 0, '.sz', '.sz', NULL, 'SZ', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fe34c-4764-11e3-959c-e8039af04ab0', 0, '.tc', '.tc', NULL, 'TC', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fe4fc-4764-11e3-959c-e8039af04ab0', 0, '.td', '.td', NULL, 'TD', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fe6ac-4764-11e3-959c-e8039af04ab0', 0, '.tf', '.tf', NULL, 'TF', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fe85c-4764-11e3-959c-e8039af04ab0', 0, '.tg', '.tg', NULL, 'TG', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fea0c-4764-11e3-959c-e8039af04ab0', 0, '.th', '.th', NULL, 'TH', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2febbc-4764-11e3-959c-e8039af04ab0', 0, '.tj', '.tj', NULL, 'TJ', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fed6c-4764-11e3-959c-e8039af04ab0', 0, '.tk', '.tk', NULL, 'TK', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fef17-4764-11e3-959c-e8039af04ab0', 0, '.tl', '.tl', NULL, 'TL', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ff11d-4764-11e3-959c-e8039af04ab0', 0, '.tm', '.tm', NULL, 'TM', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ff3af-4764-11e3-959c-e8039af04ab0', 0, '.tn', '.tn', NULL, 'TN', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ff574-4764-11e3-959c-e8039af04ab0', 0, '.to', '.to', NULL, 'TO', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ff724-4764-11e3-959c-e8039af04ab0', 0, '.tr', '.tr', NULL, 'TR', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ff8d0-4764-11e3-959c-e8039af04ab0', 0, '.tt', '.tt', NULL, 'TT', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ffa84-4764-11e3-959c-e8039af04ab0', 0, '.tv', '.tv', NULL, 'TV', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ffc34-4764-11e3-959c-e8039af04ab0', 0, '.tw', '.tw', NULL, 'TW', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2ffde0-4764-11e3-959c-e8039af04ab0', 0, '.tz', '.tz', NULL, 'TZ', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d2fff8b-4764-11e3-959c-e8039af04ab0', 0, '.ua', '.ua', NULL, 'UA', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d300137-4764-11e3-959c-e8039af04ab0', 0, '.ug', '.ug', NULL, 'UG', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d3002e3-4764-11e3-959c-e8039af04ab0', 0, '.us', '.us', NULL, 'US', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d300493-4764-11e3-959c-e8039af04ab0', 0, '.uy', '.uy', NULL, 'UY', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d30063e-4764-11e3-959c-e8039af04ab0', 0, '.uz', '.uz', NULL, 'UZ', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d3007ea-4764-11e3-959c-e8039af04ab0', 0, '.va', '.va', NULL, 'VA', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d30099e-4764-11e3-959c-e8039af04ab0', 0, '.vc', '.vc', NULL, 'VC', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d300b4a-4764-11e3-959c-e8039af04ab0', 0, '.ve', '.ve', NULL, 'VE', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d300cfa-4764-11e3-959c-e8039af04ab0', 0, '.vg', '.vg', NULL, 'VG', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d300ea5-4764-11e3-959c-e8039af04ab0', 0, '.vi', '.vi', NULL, 'VI', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d301051-4764-11e3-959c-e8039af04ab0', 0, '.vn', '.vn', NULL, 'VN', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d3011fc-4764-11e3-959c-e8039af04ab0', 0, '.vu', '.vu', NULL, 'VU', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d3013ac-4764-11e3-959c-e8039af04ab0', 0, '.wf', '.wf', NULL, 'WF', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d301558-4764-11e3-959c-e8039af04ab0', 0, '.ws', '.ws', NULL, 'WS', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d3016ff-4764-11e3-959c-e8039af04ab0', 0, '.ye', '.ye', NULL, 'YE', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d3018c0-4764-11e3-959c-e8039af04ab0', 0, '.yt', '.yt', NULL, 'YT', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d301a68-4764-11e3-959c-e8039af04ab0', 0, '.za', '.za', NULL, 'ZA', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d301c13-4764-11e3-959c-e8039af04ab0', 0, '.zm', '.zm', NULL, 'ZM', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d301dbb-4764-11e3-959c-e8039af04ab0', 0, '.zw', '.zw', NULL, 'ZW', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d301f6b-4764-11e3-959c-e8039af04ab0', 0, '.dj', '.dj', NULL, 'DJ', 'tld', 1, 1, '2014-01-01 00:00:00', '2014-02-03 15:35:38'),
('2d302123-4764-11e3-959c-e8039af04ab0', 0, 'wellness', 'Wellness', NULL, '', 'newsletterlist', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2d30266f-4764-11e3-959c-e8039af04ab0', 0, '19', '19%', NULL, '', 'mwst', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('2e3ba0e0-dae0-11e9-918e-2187a3caaa93', 12, '12', '12/12', NULL, '', 'column', 1, 0, '2019-09-19 15:20:06', '2019-09-24 11:20:30'),
('33f3b680-0302-11eb-a343-0deb73075151', 11, 'numeric', 'Zahlen', '', '', 'validationrule', 1, 0, '2020-09-30 11:49:28', '2020-09-30 11:49:28'),
('33f4e8b0-dacd-11e9-960d-d17b590185c1', 34, 'h5', 'Headline (h5)', NULL, '', 'formfield', 1, 0, '2019-09-19 13:04:19', '2019-09-19 13:04:19'),
('34c56a10-c40e-11e9-bf17-6b4be1779473', 9, '12.0', 'alle 12 Stunden', NULL, '', 'interval', 1, 0, '2019-08-21 14:21:40', '2019-08-21 14:21:40'),
('3ac65ca0-dacd-11e9-960d-d17b590185c1', 35, 'h6', 'Headline (h6)', NULL, '', 'formfield', 1, 0, '2019-09-19 13:04:32', '2019-09-19 13:04:32'),
('3b844380-dacf-11e9-ae8c-5b2804313010', 10, 'number', 'Zahl', NULL, '', 'formfield', 1, 0, '2019-09-19 13:18:52', '2019-09-19 13:18:52'),
('3c8ec6e0-0302-11eb-a343-0deb73075151', 12, 'range', 'Zwischen (Numerisch)', '', '', 'validationrule', 1, 0, '2020-09-30 11:49:50', '2020-09-30 11:49:50'),
('3e6f9910-dace-11e9-ae8c-5b2804313010', 40, 'textblock', 'Textblock', NULL, '', 'formfield', 1, 0, '2019-09-19 13:12:05', '2019-09-19 13:12:05'),
('439c6840-dacf-11e9-ae8c-5b2804313010', 16, 'confirm', 'Confirm', NULL, '', 'formfield', 1, 0, '2019-09-19 13:19:02', '2019-09-19 13:19:02'),
('4991c540-0302-11eb-a343-0deb73075151', 13, 'validuntil', 'Gültig bis', '', '', 'validationrule', 1, 0, '2020-09-30 11:50:07', '2020-09-30 11:50:07'),
('4a678670-db86-11e9-94b7-2d0e0cdf35b2', 2, '==', 'Ist gleich (==)', NULL, '', 'operator', 1, 0, '2019-09-20 11:10:25', '2019-09-20 11:10:25'),
('4e4fa720-c40e-11e9-bf17-6b4be1779473', 11, '00:00', 'täglich um 00:00', NULL, '', 'interval', 1, 0, '2019-08-21 14:22:27', '2019-08-21 14:22:27'),
('5108a530-dace-11e9-ae8c-5b2804313010', 41, 'textreplacement', 'Text aus Texttabelle (Schlüssel)', NULL, '', 'formfield', 1, 0, '2019-09-19 13:12:43', '2019-09-19 13:12:43'),
('527b1af8-5b0c-4725-b363-15981a25c659', 0, 'concardis', 'Kreditkarte', NULL, '', 'paymentmethod', 0, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('527b1b15-d628-438b-ba42-15981a25c659', 0, 'creditcard', 'Kreditkarte', NULL, '', 'paymentmethod', 0, 1, '2014-01-01 00:00:00', '2014-09-03 14:59:47'),
('527b1b3b-a43c-461f-a09e-15981a25c659', 0, 'prepayment', 'Vorkasse', NULL, '', 'paymentmethod', 0, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('527b1c1e-a328-4fae-a5c6-15981a25c659', 0, 'paypal', 'PayPal', NULL, '', 'paymentmethod', 0, 1, '2014-01-01 00:00:00', '2014-02-07 12:31:43'),
('527b1c96-e364-4536-a076-15981a25c659', 0, 'rechnung', 'Rechnung', NULL, '', 'paymentmethod', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('527b1ced-4a14-4c48-a1fb-15981a25c659', 0, 'sofortueberweisung', 'Sofortüberweisung', NULL, '', 'paymentmethod', 0, 1, '2014-01-01 00:00:00', '2014-02-07 12:31:33'),
('527bc5ba-1bfc-4c38-bb8b-1e301a25c659', 0, 'perpost', 'Per Post', NULL, '', 'shippingoption', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('527bc5c7-bd4c-455b-9d79-1e301a25c659', 0, 'download', 'Als Download', NULL, '', 'shippingoption', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('52e203c0-ba34-43d2-933e-23881a25c659', 0, 'ab', 'ab', NULL, '', 'priceprefix', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('52e203ed-7e18-45b4-af43-23881a25c659', 0, 'pp', 'pro Person', NULL, '', 'pricesuffix', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('52e2040d-e930-47d9-9c2a-23881a25c659', 0, 'pn', 'pro Nacht', NULL, '', 'pricesuffix', 1, 1, '2014-01-01 00:00:00', '2014-01-01 00:00:00'),
('52f1d0a8-4398-4126-9aae-24d01a25c659', 0, '7', '7 Tage', NULL, '', 'deliverytime', 1, 1, '2014-02-05 06:48:24', '2014-02-05 06:48:24'),
('52f4b9ae-5f88-4d81-a094-24d01a25c659', 0, '0', '0%', NULL, '', 'mwst', 1, 1, '2014-02-07 11:47:10', '2014-02-07 11:47:29'),
('52f8a22c-eb08-4919-8fd3-24d01a25c659', 0, 'herr', 'männlich', NULL, '', 'gender', 1, 1, '2014-02-10 10:55:56', '2014-02-10 10:55:56'),
('52f8a23a-e1a8-4f8d-82ef-24d01a25c659', 0, 'frau', 'weiblich', NULL, '', 'gender', 1, 1, '2014-02-10 10:56:10', '2014-02-10 10:56:10'),
('53036710-f2d4-41c3-b794-181c1a25c659', 0, '4.90', '4.90', NULL, '', 'shippingcost', 1, 1, '2014-02-18 14:58:40', '2014-02-18 14:58:40'),
('537d4a70-0302-11eb-a343-0deb73075151', 14, 'checked', 'Checkbox markiert', '', '', 'validationrule', 1, 0, '2020-09-30 11:50:25', '2020-09-30 11:50:25'),
('53e99230-1410-48ff-85ff-2a341a25c659', 0, 'payonecreditcard', 'Kreditkarte', NULL, '', 'paymentmethod', 1, 1, '2014-08-12 06:04:00', '2014-08-12 06:04:00'),
('540710b5-9988-4ced-b1f5-0f881a25c659', 0, 'payonepaypal', 'PayPal', NULL, '', 'paymentmethod', 1, 1, '2014-09-03 14:59:33', '2014-09-03 14:59:33'),
('585a4d10-c40e-11e9-bf17-6b4be1779473', 12, '00:30', 'täglich um 00:30', NULL, '', 'interval', 1, 0, '2019-08-21 14:22:56', '2019-08-21 14:22:56'),
('5d9887a0-0301-11eb-aa5e-970ae140487c', 0, 'alphaNumeric', 'Alphanumerisch', '', '', 'validationrule', 1, 0, '2020-09-30 11:43:57', '2020-09-30 11:44:09'),
('679532a0-dace-11e9-ae8c-5b2804313010', 1, 'hidden', 'Verstecktes Feld', NULL, '', 'formfield', 1, 0, '2019-09-19 13:13:28', '2019-09-19 13:13:28'),
('6dc84b20-c40e-11e9-bf17-6b4be1779473', 13, '01:00', 'täglich um 01:00', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('7caa7fc0-db86-11e9-94b7-2d0e0cdf35b2', 1, '<=', 'Ist kleiner oder gleich (<=)', NULL, '', 'operator', 1, 0, '2019-09-20 11:10:50', '2019-09-20 11:10:50'),
('8279cf90-dace-11e9-ae8c-5b2804313010', 2, 'text', 'Input', NULL, '', 'formfield', 1, 0, '2019-09-19 13:13:40', '2019-09-19 14:38:08'),
('87df4140-c412-11e9-95d7-cefb7f872937', 14, '01:30', 'täglich um 01:30', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('89b41770-dace-11e9-ae8c-5b2804313010', 4, 'textarea', 'Textarea', NULL, '', 'formfield', 1, 0, '2019-09-19 13:13:53', '2019-09-19 13:13:53'),
('8ad3d080-0301-11eb-a343-0deb73075151', 1, 'cc', 'Kreditkartennummer', '', '', 'validationrule', 1, 0, '2020-09-30 11:44:58', '2020-09-30 11:45:47'),
('8b5b2e20-db86-11e9-94b7-2d0e0cdf35b2', 3, '>=', 'Ist größer oder gleich (>=)', NULL, '', 'operator', 1, 0, '2019-09-20 11:11:16', '2019-09-20 11:11:16'),
('9877b190-dace-11e9-ae8c-5b2804313010', 8, 'checkbox', 'Checkbox', NULL, '', 'formfield', 1, 0, '2019-09-19 13:14:23', '2019-09-19 13:14:23'),
('9aa4c120-db86-11e9-94b7-2d0e0cdf35b2', 0, '<', 'Ist kleiner (<)', NULL, '', 'operator', 1, 0, '2019-09-20 11:11:38', '2019-09-20 11:11:38'),
('9d996ae0-0301-11eb-a343-0deb73075151', 2, 'email', 'E-Mail', '', '', 'validationrule', 1, 0, '2020-09-30 11:45:21', '2020-09-30 11:46:25'),
('a343abb0-dace-11e9-ae8c-5b2804313010', 9, 'radio', 'Radio', NULL, '', 'formfield', 1, 0, '2019-09-19 13:14:32', '2019-09-19 13:14:32'),
('a7bfdb60-db86-11e9-94b7-2d0e0cdf35b2', 4, '>', 'Ist größer (>)', NULL, '', 'operator', 1, 0, '2019-09-20 11:11:53', '2019-09-20 11:11:53'),
('a8cb3a80-dace-11e9-ae8c-5b2804313010', 3, 'password', 'Password', NULL, '', 'formfield', 1, 0, '2019-09-19 13:14:52', '2019-09-19 13:14:52'),
('a960dc00-0301-11eb-a343-0deb73075151', 3, 'time', 'Zeitangabe', '', '', 'validationrule', 1, 0, '2020-09-30 11:45:38', '2020-09-30 11:46:33'),
('b030d970-4c1f-11ea-9ffa-3d6651e69b4a', NULL, 'allgemein', 'Allgemein', NULL, '', 'mapgroup', 1, 0, '2020-02-10 17:10:01', '2020-07-31 15:12:37'),
('b4a3e9b0-dace-11e9-ae8c-5b2804313010', 14, 'file', 'Fileinput (Single)', NULL, '', 'formfield', 1, 0, '2019-09-19 13:15:29', '2019-09-19 13:15:29'),
('b73fc270-f39e-11ea-9404-8c85907e67e5', 0, 'can', 'Recht', NULL, NULL, 'resourcetype', 1, 1, '2020-09-03 00:00:00', '2020-09-03 00:00:00'),
('c40a9e80-c40d-11e9-bf17-6b4be1779473', 0, 'now', 'Jetzt einmal', NULL, '', 'interval', 1, 0, '2019-08-21 14:19:05', '2019-08-21 14:19:05'),
('ca3a7e60-dace-11e9-ae8c-5b2804313010', 15, 'multifile', 'Fileinput (Multiple)', NULL, '', 'formfield', 1, 0, '2019-09-19 13:15:58', '2019-09-19 13:17:43'),
('cb0712a0-dacd-11e9-ae8c-5b2804313010', 36, 'hr', 'Trennlinie', NULL, '', 'formfield', 1, 0, '2019-09-19 13:08:30', '2019-09-19 13:08:30'),
('cc3597d0-f3a1-11ea-9aae-df53ae2f5385', NULL, 'url', 'Url', '', '', 'resourcetype', 1, 0, '2020-09-10 22:12:19', '2020-09-10 22:16:10'),
('d19aba40-dacd-11e9-ae8c-5b2804313010', 37, 'br', 'Umbruch', NULL, '', 'formfield', 1, 0, '2019-09-19 13:09:51', '2019-09-19 13:09:51'),
('d41c57d0-0301-11eb-a343-0deb73075151', 4, 'date', 'Datum', '', '', 'validationrule', 1, 0, '2020-09-30 11:46:53', '2020-09-30 11:46:53'),
('d9c75d40-4c1f-11ea-8bac-1123e26ada3f', NULL, 'developer', 'Entwickler', NULL, '', 'mapgroup', 1, 0, '2020-02-10 17:10:59', '2020-02-10 17:10:59'),
('dc9c9c00-dace-11e9-ae8c-5b2804313010', 5, 'select', 'Select', NULL, '', 'formfield', 1, 0, '2019-09-19 13:16:15', '2019-09-19 13:16:15'),
('dd0bea80-dacc-11e9-960d-d17b590185c1', 30, 'h1', 'Headline (h1)', NULL, '', 'formfield', 1, 0, '2019-09-19 13:03:15', '2019-09-19 13:06:19'),
('df4f3cb0-4107-11ea-9fcf-5177ab1eb932', NULL, 'default', 'Standard', NULL, '', 'eventlabel', 1, 0, '2020-01-27 14:21:23', '2020-01-27 14:21:23'),
('dff5d600-c40d-11e9-bf17-6b4be1779473', 1, 'always', 'Immer', NULL, '', 'interval', 1, 0, '2019-08-21 14:19:20', '2019-08-21 14:19:36'),
('e0459800-0301-11eb-a343-0deb73075151', 5, 'isUnique', 'Eindeutig', '', '', 'validationrule', 1, 0, '2020-09-30 11:47:30', '2020-09-30 11:47:30'),
('e6543ff0-dace-11e9-ae8c-5b2804313010', 6, 'selectsuggest', 'Select (Suggest)', NULL, '', 'formfield', 1, 0, '2019-09-19 13:16:54', '2019-09-19 13:17:35'),
('e84166d0-c40d-11e9-bf17-6b4be1779473', 2, '0.5', 'alle halbe Stunde', NULL, '', 'interval', 1, 0, '2019-08-21 14:19:32', '2019-08-21 14:19:40'),
('e8727c50-dadf-11e9-918e-2187a3caaa93', 1, '1', '1/12', NULL, '', 'column', 1, 0, '2019-09-19 15:18:21', '2019-09-19 15:18:21'),
('ede1d940-4c1f-11ea-8bac-1123e26ada3f', NULL, 'designer', 'Gestaltung', NULL, '', 'mapgroup', 1, 0, '2020-02-10 17:11:15', '2020-02-10 17:11:15'),
('f07c8cc0-f3a1-11ea-9aae-df53ae2f5385', NULL, 'action', 'Controller / Action', '', '', 'resourcetype', 1, 0, '2020-09-10 22:13:08', '2020-09-10 22:13:08'),
('f4be4540-c40d-11e9-bf17-6b4be1779473', 3, '1.0', 'jede Stunde', NULL, '', 'interval', 1, 0, '2019-08-21 14:19:58', '2019-08-21 14:19:58'),
('f4d17280-dadf-11e9-918e-2187a3caaa93', 2, '2', '2/12', NULL, '', 'column', 1, 0, '2019-09-19 15:18:32', '2019-09-19 15:18:32'),
('f6814650-0301-11eb-a343-0deb73075151', 6, 'minage', 'Mindestalter', '', '', 'validationrule', 1, 0, '2020-09-30 11:47:49', '2020-09-30 11:47:49'),
('fb631e50-dadf-11e9-918e-2187a3caaa93', 3, '3', '3/12', NULL, '', 'column', 1, 0, '2019-09-19 15:18:41', '2019-09-19 15:18:41'),
('fb7a8844-c412-11e9-95d7-cefb7f872937', 15, '02:00', 'täglich um 02:00', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7bfcec-c412-11e9-95d7-cefb7f872937', 16, '02:30', 'täglich um 02:30', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7c192a-c412-11e9-95d7-cefb7f872937', 17, '03:00', 'täglich um 03:00', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7c320c-c412-11e9-95d7-cefb7f872937', 18, '03:30', 'täglich um 03:30', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7c4c2e-c412-11e9-95d7-cefb7f872937', 19, '04:00', 'täglich um 04:00', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7c636c-c412-11e9-95d7-cefb7f872937', 20, '04:30', 'täglich um 04:30', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7c7f82-c412-11e9-95d7-cefb7f872937', 21, '05:00', 'täglich um 05:00', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7c9512-c412-11e9-95d7-cefb7f872937', 22, '05:30', 'täglich um 05:30', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7cab74-c412-11e9-95d7-cefb7f872937', 23, '06:00', 'täglich um 06:00', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7cc096-c412-11e9-95d7-cefb7f872937', 24, '06:30', 'täglich um 06:30', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7cd6a8-c412-11e9-95d7-cefb7f872937', 25, '07:00', 'täglich um 07:00', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7ceca6-c412-11e9-95d7-cefb7f872937', 26, '07:30', 'täglich um 07:30', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7d034e-c412-11e9-95d7-cefb7f872937', 27, '08:00', 'täglich um 08:00', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7d1884-c412-11e9-95d7-cefb7f872937', 28, '08:30', 'täglich um 08:30', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7d2d60-c412-11e9-95d7-cefb7f872937', 29, '09:00', 'täglich um 09:00', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7d46a6-c412-11e9-95d7-cefb7f872937', 30, '09:30', 'täglich um 09:30', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7d5c22-c412-11e9-95d7-cefb7f872937', 31, '10:00', 'täglich um 10:00', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7d718a-c412-11e9-95d7-cefb7f872937', 32, '10:30', 'täglich um 10:30', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7d8990-c412-11e9-95d7-cefb7f872937', 33, '11:00', 'täglich um 11:00', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7d9df4-c412-11e9-95d7-cefb7f872937', 34, '11:30', 'täglich um 11:30', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7db3de-c412-11e9-95d7-cefb7f872937', 35, '12:00', 'täglich um 12:00', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7dc7fc-c412-11e9-95d7-cefb7f872937', 36, '12:30', 'täglich um 12:30', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7dddc8-c412-11e9-95d7-cefb7f872937', 37, '13:00', 'täglich um 13:00', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7e43b2-c412-11e9-95d7-cefb7f872937', 38, '13:30', 'täglich um 13:30', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7e5f96-c412-11e9-95d7-cefb7f872937', 39, '14:00', 'täglich um 14:00', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7e78aa-c412-11e9-95d7-cefb7f872937', 40, '14:30', 'täglich um 14:30', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7e90f6-c412-11e9-95d7-cefb7f872937', 41, '15:00', 'täglich um 15:00', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7ea8fc-c412-11e9-95d7-cefb7f872937', 42, '15:30', 'täglich um 15:30', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7ec1a2-c412-11e9-95d7-cefb7f872937', 43, '16:00', 'täglich um 16:00', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7ed836-c412-11e9-95d7-cefb7f872937', 44, '16:30', 'täglich um 16:30', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7eef38-c412-11e9-95d7-cefb7f872937', 45, '17:00', 'täglich um 17:00', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7f0400-c412-11e9-95d7-cefb7f872937', 46, '17:30', 'täglich um 17:30', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7f1972-c412-11e9-95d7-cefb7f872937', 47, '18:00', 'täglich um 18:00', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7f3092-c412-11e9-95d7-cefb7f872937', 48, '18:30', 'täglich um 18:30', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7f44f6-c412-11e9-95d7-cefb7f872937', 49, '19:00', 'täglich um 19:00', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7f5950-c412-11e9-95d7-cefb7f872937', 50, '19:30', 'täglich um 19:30', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7f7854-c412-11e9-95d7-cefb7f872937', 51, '20:00', 'täglich um 20:00', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7f8f38-c412-11e9-95d7-cefb7f872937', 52, '20:30', 'täglich um 20:30', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7fa572-c412-11e9-95d7-cefb7f872937', 53, '21:00', 'täglich um 21:00', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7fbb98-c412-11e9-95d7-cefb7f872937', 54, '21:30', 'täglich um 21:30', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7fd3da-c412-11e9-95d7-cefb7f872937', 55, '22:00', 'täglich um 22:00', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7fe96a-c412-11e9-95d7-cefb7f872937', 56, '22:30', 'täglich um 22:30', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb7fffc2-c412-11e9-95d7-cefb7f872937', 57, '23:00', 'täglich um 23:00', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fb801520-c412-11e9-95d7-cefb7f872937', 58, '23:30', 'täglich um 23:30', NULL, '', 'interval', 1, 0, '2019-08-21 14:23:20', '2019-08-21 14:23:20'),
('fe2d71a0-dace-11e9-ae8c-5b2804313010', 7, 'multiselect', 'Select (Multiple)', NULL, '', 'formfield', 1, 0, '2019-09-19 13:17:21', '2019-09-19 13:17:21');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `elements`
--

DROP TABLE IF EXISTS `elements`;
CREATE TABLE `elements` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aco_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creator_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `extended_data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `extended_data_cfg` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `emails`
--

DROP TABLE IF EXISTS `emails`;
CREATE TABLE `emails` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aco_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_default` tinyint(1) DEFAULT '0',
  `email_to` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_bcc` mediumtext COLLATE utf8mb4_unicode_ci,
  `return_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reply_to` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `smtp_host` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `smtp_port` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `smtp_username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `smtp_password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imap_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imap_host` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imap_port` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imap_username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imap_password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=8192 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `emails`
--

INSERT INTO `emails` (`id`, `aco_id`, `name`, `is_default`, `email_to`, `from_name`, `from_email`, `email_bcc`, `return_path`, `reply_to`, `smtp_host`, `smtp_port`, `smtp_username`, `smtp_password`, `imap_email`, `imap_host`, `imap_port`, `imap_username`, `imap_password`, `created`, `modified`) VALUES
('51a62852-43a4-4bd7-bf77-17fc1a25c659', '54be0e1d-3fc0-4c82-b250-1f041a25c659', 'Standard', 1, 'info@simple-x.de', 'simple X CMS', 'sender@simple-x.de', '', '', '', 'smtp.simple-x.de', '25', 'systemadmin@simple-x.de', '6Tdt9E47tP', 'info@dvwebapps.de', 'imap.simple-x.de', '143', 'systemadmin@simple-x.de', '6Tdt9E47tP', '0000-00-00 00:00:00', '2019-03-29 00:14:25');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `events`
--

DROP TABLE IF EXISTS `events`;
CREATE TABLE `events` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `article_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `menu_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `menu_parent_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=606 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `events`
--

INSERT INTO `events` (`id`, `article_id`, `date`, `menu_id`, `menu_parent_id`) VALUES
('0000448b-d3ec-4826-9796-9c458948cfde', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-08-14 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('00454305-c1de-48b3-a3f6-ff2a9bf05748', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-08-26 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('00c70f37-031a-47c7-9cf2-8dffc9a837ea', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-03-25 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('00c79b0c-07ec-44cd-89ba-e4c544a2eacc', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-01-29 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('01487934-ead6-4cca-8d71-3ed67f231ae0', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-11-04 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('020f9707-4a79-4d4f-b8ea-028935dc21fb', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-05-08 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('0313021c-599a-404a-a52b-bf432f507a2a', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-01-16 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('04440c9a-0c70-487d-aedb-79aac49f723b', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2021-06-07 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('0567b4cf-8991-43e8-b09b-99f5c347eebb', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-02-20 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('06b27567-eb78-4d75-bc15-78b62a1866cc', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2021-07-19 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('070edf98-c891-47e9-aca9-cb7b4ee47774', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-07-15 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('078e313f-5097-4a41-b7a6-52eeef539e63', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2021-10-04 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('07b5d059-6948-4796-b231-f466741f00e8', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-07-01 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('07e4006e-9334-4c38-b595-e45c683a9bd4', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-01-24 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('0c105f13-962b-497d-8cc9-83cc3c0d6052', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2025-03-17 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('0d3291a6-4b8f-4a1a-ad9f-02082482746a', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-07-03 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('0dfb9b01-46c3-491f-a7d8-4decd216e698', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-10-03 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('0ee96381-9672-45bb-987c-ff65285ca911', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2021-08-30 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('0f5737c0-58d1-4589-a214-2e11d76fa1b6', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-10-31 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('0f923697-1094-4dde-bd14-b5ed9714c36f', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-08-01 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('1203e40c-6160-4315-8d34-0e0e0ad3a256', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-08-12 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('158a9a81-5f74-4099-a1ea-08271bcb6974', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-09-04 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('16a84a31-ce5f-42c5-bb3e-50cf8c38322e', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2021-11-22 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('17865bf9-a547-4952-a48e-0384b9c3ecec', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-10-16 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('18e82f1a-13b2-4334-8fd4-62fa29b41005', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-09-11 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('1bb64318-d276-4335-b454-defe7d7a3772', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-09-19 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('1c317981-3b3c-49eb-a7bc-cf5a2ae917e2', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-10-07 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('1ce6ade6-7e2d-451c-8a95-dc9766fb4deb', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2025-03-03 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('207cb31e-3f7c-4ad4-aee9-0a33e284a6fa', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-03-06 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('219896c6-339b-4720-b101-2d3e4173f384', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-04-24 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('21ecb9d5-336b-4f1b-8061-b7b3ad645e81', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-06-13 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('2251acd5-03d0-4367-8d7c-fd3ba289cdb4', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-06-27 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('24f7ada2-7e4a-427b-a8c8-1d4a41b685a0', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-05-13 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('25b31bf5-183c-4707-b51f-eb43513ee069', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-02-12 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('28bcdb1e-4b0b-4d6e-a3b2-b9032579992c', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2025-02-10 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('2b4d3068-b7fa-455e-89c6-760bef99d546', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-08-29 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('2c91dce2-a5cf-455c-b0ad-1af6454785e4', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2025-02-03 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('2cf72a40-0de1-4b93-8564-4c1b30168208', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-05-01 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('2e35c204-9f8d-4133-9527-9f13c7927a80', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-03-20 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('2ecc898a-8ff4-45e2-a2ce-4104c1c603dd', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-10-23 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('30db416a-1c80-4d20-ad73-95f991191c9d', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2021-07-05 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('31bc0a9f-e7f8-4d5f-aeb8-3e70e162e779', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-12-19 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('32451d8a-59f7-452f-b359-c5d77e81b716', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-03-11 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('364b4e89-807c-49d6-9897-dc896a961916', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-11-27 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('36572be6-9153-4d42-b8e2-a4c0ab294827', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-11-21 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('37143fac-9998-4159-9d57-c515c632b2b3', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-06-24 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('3791547f-1723-48f3-bc01-ea9936a6b298', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-11-11 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('381c3ce8-9ea5-4c22-8955-67e10a848d61', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-05-27 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('38af7445-422a-41a1-b6fa-5f070f3d3ed8', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-05-23 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('3e90b07a-25e5-4b2a-ab68-f2e18bebb22e', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-09-12 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('42912855-24ae-491a-96dc-cf73b50e3e9b', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-09-05 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('456bbe72-5369-441f-b443-b4dd0a273a40', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-09-09 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('49d69e1e-6f99-419b-a2c1-6735a1be927e', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2021-08-09 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('4aa0d1a6-3856-42a7-84a9-b0b3f96b45c1', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-07-22 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('4b430290-5bad-4f98-982d-ff42b37bfb65', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2025-01-06 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('4bf8a668-cd48-48b3-a6c0-f6edab139ea6', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-06-26 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('503e2730-4fdd-4572-90fe-f338f3241061', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-11-06 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('51f7759f-39d0-44c1-b33b-32115db08fbc', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-09-25 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('52169e89-198b-483a-b1d4-cd2443aea242', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-01-01 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('523c3a16-debe-496d-8eaf-f69bb4b1c78c', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-04-25 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('525de9c5-2ce0-4cfa-8f5d-e6c6d118bcb7', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-02-19 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('53e102cc-c8b0-4ccc-869b-c3f25c740456', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-05-30 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('56c831fe-2e3e-4882-af37-23487f6d0e34', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2021-10-11 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('57c53940-2a35-480e-b369-a1cb88c37e2c', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-05-16 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('5844962b-df5a-4664-8361-76a4010d4293', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-06-20 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('58b74172-5b2f-47ea-8712-07b9a4d8c97c', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-11-18 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('58cacd50-d7cc-4112-9f56-8e0ac92ae7b6', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-01-10 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('5a6776ef-6cbb-4465-a1cb-157877a70a08', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2025-01-13 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('5a8b817a-f801-498a-b024-3e0b683f1c92', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2021-08-16 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('5b47f4d3-a14a-4788-826a-89076e7c07eb', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2025-02-17 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('5da81783-ecb1-4a44-9386-1abd3c24079a', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-02-27 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('5df0ee12-0ac1-44a0-b673-76eb8d82ab74', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2025-03-24 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('5f0a640e-0357-4b67-b01a-9251ac4cc50e', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2021-12-20 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('5f1431a0-a809-40e7-8001-b2c8ed9b5960', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-05-09 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('5f7ae9cd-3d6b-49c8-b4f2-15919a7eb638', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2021-07-26 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('60aafc45-fef8-4be0-9884-8dc7afb5902a', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-09-30 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('61c38127-9c46-41e2-b688-68643836510b', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2021-08-23 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('6270f34d-b30a-4069-8637-e80e60a7f670', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-02-14 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('6894f49b-3d43-4da5-8708-ce67febc9f54', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-01-23 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('6b1f9c62-f777-4138-8d47-b64771e164d3', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-01-30 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('6c223b7f-afd8-415f-9fc7-e6088def49b1', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-10-24 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('6c982366-8fdb-4ac1-8c8d-de420776967d', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-01-09 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('6dc93044-e999-44bb-b79c-58b244a07a16', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-07-31 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('6f982b7c-5d92-4cd2-935f-56f77c09b21b', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-12-12 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('7057bb39-d658-46e0-a3c6-af0902576147', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-01-02 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('70a6185d-3a87-4704-b7f8-64306007a9e2', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-06-19 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('71d4e6f8-a3db-41e7-8f19-9ff952511906', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2021-09-20 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('746473cf-d4d8-4f09-a7b8-478287a8adeb', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-03-27 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('748740e5-2a2a-4df3-8fac-bafdbdeb744e', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-11-14 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('74db5562-586a-4796-b067-d2cd5daf4d80', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-06-06 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('76ed8e8f-2654-4911-9aa7-b190f60179cd', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-05-15 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('77d771f6-496f-4bbd-8b00-6ebbf1bcb01b', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-07-08 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('7a0d0856-604e-46ba-945a-a621a9c0b5b9', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2021-11-29 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('7d1dada7-bacf-48ba-ae6b-80feb485a48e', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2021-06-14 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('81789ed7-2e1d-41dc-92a8-50432abdf511', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-12-09 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('82db12dd-4d3b-477b-8d74-6ce3775e62a8', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-10-09 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('854830c4-ab92-469e-b44f-be0b71e993f6', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-09-16 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('860a53de-ab26-4cda-a4e6-ce8f3e554890', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-08-08 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('8712699a-119e-4b37-88c3-b1d130062f8c', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-04-01 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('878bfe64-7b43-4ef8-9589-1e42d28a89f1', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-04-22 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('88a990ac-9d25-43fe-8041-09a22cf7024c', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-02-21 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('8955f275-050b-440e-ac24-1b0458517f01', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-02-07 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('8c16ee2e-14b1-4abf-b337-1b9fd38ef3d8', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2021-11-01 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('8c7b8e2f-504d-4899-b4a0-667653839624', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-07-10 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('8dd814e7-9d27-4ac8-8326-f53d2f8a1f22', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-07-11 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('8de0f04d-e538-4c88-8eb1-f4ba126bcf70', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-05-29 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('8f55afff-580d-4d92-aef1-d0fa7036bc61', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-07-17 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('8f9c9a2c-3b32-4467-9453-c92373387382', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-03-13 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('9087b051-2c80-4d98-b3af-d203d104bdb7', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2021-08-02 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('91bed811-9d1c-4996-9985-b518bfba662f', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-06-10 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('92e7b9aa-eff2-49f1-9252-8dd9883ff129', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-11-20 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('92e8d64e-310e-4673-9377-47e065ba2128', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2021-06-28 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('93b55907-ccfa-49d7-aff5-17c3b942d721', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-01-31 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('94de168f-e99f-4613-8f13-9752c48a8186', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-03-21 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('9692ecda-bc69-49a5-93a6-024a52ec241c', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-12-18 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('96c1f68f-3a14-4e5b-94ea-725600d3661e', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-04-04 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('9b178c1f-4f26-422c-82a6-8f2d392336c5', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-04-03 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('9b546aba-771f-4828-be26-ec663ac416ff', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2025-01-27 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('9bc6e17c-3700-4208-8afe-9aa09888159b', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-01-22 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('9be3a5ba-a118-497b-8f0d-deee99c1cd25', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-10-30 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('9c3a0fa6-06f6-46c2-9713-f090588d6c70', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-12-04 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('9caabcf3-e9fb-4cff-944b-39b90e8b3322', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-07-18 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('9dddcc9b-9e3c-4623-ad84-7f431af0248f', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-02-06 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('9e58832a-d429-4e18-8817-cb352f92a420', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-03-18 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('9ea9242f-e7be-4f0c-babf-3a78331ad017', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-09-02 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('9f4e3391-d473-4ea6-abe7-7ed5743d5e30', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2025-03-31 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('9f687d06-c698-472f-8212-c4e541c6acd6', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-05-06 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('9f77f351-2a64-46c4-8a78-bf244c7e600b', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-04-29 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('9f9c5fd5-1bb1-4d24-b189-3da8863a1561', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-07-24 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('a0396302-4b74-4563-a4d2-dcf29360726f', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-12-05 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('a20bdd30-3670-45f5-b9aa-be21c605ec48', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-05-02 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('a225a724-084c-4e71-9dda-f3e8abe14d08', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2021-12-27 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('a444dcc5-3363-44c9-9184-fe59e5c72d2a', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-05-22 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('a61b265f-a1cb-450f-a08d-786b663c17ca', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-11-13 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('a6462108-ed42-4087-bf97-30a691373ad0', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2025-01-20 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('a6f730b9-0bd1-4b4d-b37a-06e55ec815f8', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-04-08 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('a7e46906-49e8-4ecc-bbd6-4cf6fd71ce62', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-10-28 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('a814392d-98bb-4787-8b11-f8ca7c4e2ce1', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-06-17 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('a8b2d84d-09a6-4173-b326-c4802ffbdfda', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2021-12-13 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('abbed49c-23f2-4bae-a4b4-74156fa65a39', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-03-28 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('aeb2f57f-304d-428d-9363-ae222b4d0001', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-01-08 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('af9bf5c4-253a-4905-b108-b90c0f9c9a58', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-08-22 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('afd932d9-d495-40ce-9957-3b691a6d8a44', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2021-09-06 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('b0fef640-580c-4423-aa94-9b8eaa106f5d', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2025-02-24 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('b23b0a87-af7a-44d8-8883-d5cb422ad4ed', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-01-17 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('b37281b6-a684-4f87-901a-2ceb5b991289', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-07-25 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('b462303a-dbbe-4858-aa85-0a3c591347fd', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2021-09-13 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('b46dae2e-cfcf-4169-821e-5af7ec2470f3', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2025-03-10 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('b4d2a7c6-fcc6-4c3e-83f5-0ab1803854a2', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2021-07-12 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('b73ccfc9-928a-4d85-9942-21f338c3d703', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-10-17 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('b9650131-8b67-4c6c-9270-63aee52b5b27', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-10-21 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('b97c402e-8a6b-4955-a431-60c19328aaea', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-12-25 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('bb2cc992-8b26-4561-a631-d4676af30b4f', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-09-18 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('bb60f118-b83a-4dae-aef8-49e31fc5ee95', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-12-02 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('bc5e5990-87b0-4d78-8e97-319f7bf792a7', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2021-11-15 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('be23164b-569b-49a6-8c64-da58b9db09f4', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-11-25 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('c04d831f-34cf-459a-9998-6667c8ef8ab2', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-01-15 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('c0d5e6d1-501c-44aa-bb77-e33512c57fcf', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-08-19 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('c107eb76-533b-4f88-a461-db2e1b3684f5', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-02-26 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('c1fbc86a-242b-4e27-8a06-a781641be2a9', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-11-07 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('c2840783-61ef-4da7-b91d-91f327ed7dc7', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2021-11-08 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('c30e0fb8-96cd-443f-9eb3-958217861ccb', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-08-05 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('ca7fbcef-fca7-48ce-814b-3f423e975f45', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-12-11 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('cb16d44f-d249-4c5a-ac6b-5b9d741c6f9e', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-08-15 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('cbdfcffe-5e62-4c7a-acc0-046e63399a87', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-03-07 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('ce99c4d0-68fd-4236-b82b-2c3bc8fda20b', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-10-10 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('cf38fc53-b033-405b-8a5b-bf063577e03c', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-03-04 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('cfb52732-cc6d-4510-9857-49b0147956d6', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-09-26 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('d0c1d68b-6775-4d3c-96a2-b9ad787da341', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-12-30 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('d46c133b-016d-4a7c-ae1c-f9c3a9b4972b', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-08-28 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('d474712f-c3f4-414a-b3d8-9892193484a8', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-06-03 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('d624221e-63af-4c79-bd43-1224863e4110', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-04-17 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('d9ebf27f-2ca8-477c-9557-40f796ab01d6', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-02-28 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('da0fb64d-dca0-40d1-b5b9-3151b3a6adc2', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2021-06-21 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('dbd26444-4f70-4b35-b1cf-ecf0aa1b1408', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-10-14 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('dc5e5a93-9ed1-448a-a3ae-32a7aefe73fe', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-04-10 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('dc83ffa2-d748-4b1d-949c-11bcc2f01524', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-09-23 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('dd570ea7-de21-4493-a2d0-033117cb1fc2', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-03-14 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('dd7edd49-4e1c-42fe-9cf7-cecc0d49b610', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-04-18 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('de33bf04-3d0d-4fa0-9f8a-9a3a645465df', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-06-05 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('e09228ce-2cdb-47e5-a3bf-5133cc7e1c89', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-12-26 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('e171990c-bd4e-45b2-91ca-3b74951e035d', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2021-09-27 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('e2bacfdd-ae5f-47cb-b8b4-b26438cc76ce', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-12-23 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('e2f72e52-7a42-4b6a-8a93-145ee2777cb1', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-04-11 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('e4c82873-475a-492a-a2e8-4154f948dfbe', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-02-05 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('e5ad9047-ba40-4531-9f78-6709bf8dedf5', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-06-12 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('ed3bcf81-4666-4ffa-b22b-f175828a04fb', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2021-10-18 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('ed3d65b6-0cda-4d54-a108-911c0092b73a', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-07-29 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('ed5d50d9-0775-4361-8ee2-c6a3e333a900', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-08-07 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('edbb555b-2afd-44f5-92b3-019c3f353a9d', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-07-04 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('edede705-cdd8-48e0-a305-4748608bbd72', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-02-13 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('f5016295-cc3b-4bba-bef8-978b847210d3', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-05-20 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('f5d19219-3422-42e7-af70-b88d2af86fd0', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-11-28 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('f8c5e41b-ea64-4d9c-9fda-bb737d94445a', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2021-12-06 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('f93710b3-22e4-4d03-a9f7-6e0770d24d43', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-10-02 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('f95f2352-1800-40e6-9f8b-3cfa1c690626', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-04-15 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('f9a579fa-e0f8-4022-9754-613486c937c3', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2021-10-25 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('fabdf86a-6a90-48d1-bb02-541b64c75b48', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2024-12-16 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('fe4b02a5-0dd0-484f-b051-f913fa2cf287', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2023-08-21 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4'),
('ff663ccc-afae-47ce-a4a2-4e7805233807', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', '2022-01-03 00:00:00', '3c59c62d-9a0c-4459-86a6-d2e215811e1b', 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `formularconfigs`
--

DROP TABLE IF EXISTS `formularconfigs`;
CREATE TABLE `formularconfigs` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aco_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `formular_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_to` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_bcc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `use_conversion_tracking` tinyint(1) NOT NULL DEFAULT '1',
  `conversion_tracking_code` longtext COLLATE utf8mb4_unicode_ci,
  `after_send_redirect` longtext COLLATE utf8mb4_unicode_ci,
  `after_send_content` longtext COLLATE utf8mb4_unicode_ci,
  `after_send_headline` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=3276 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `formularconfigs`
--

INSERT INTO `formularconfigs` (`id`, `aco_id`, `email_id`, `formular_id`, `name`, `email_to`, `email_bcc`, `subject`, `use_conversion_tracking`, `conversion_tracking_code`, `after_send_redirect`, `after_send_content`, `after_send_headline`, `identifier`, `created`, `modified`) VALUES
('0ce43ee0-f3b0-11ea-9b77-0b089fb1f902', '54be0e1d-3fc0-4c82-b250-1f041a25c659', '51a62852-43a4-4bd7-bf77-17fc1a25c659', '52e5d3d9-a8a4-4717-a2f1-26041a25c659', 'Register', '', '', '', 1, '<script>\nalert(\'YWS\');\n</script>', 'http://sx3.local/anfahrt', '<p>Welcome to the club!</p><p data-f-id=\"pbf\" style=\"text-align: center; font-size: 14px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;\">Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', 'Yeah!', 'Ey', '2020-09-10 23:53:56', '2020-10-15 13:57:14'),
('0fb785b0-0ee4-11eb-be86-fbc707e94e47', 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', NULL, '437cc9d0-de84-4dfe-b7a9-1a841a25c659', 'Testformular', '', '', '', 0, 's', '', '', '', '', '2020-10-15 14:44:03', '2020-10-15 14:44:23'),
('20737110-df47-11ea-b910-6b5c851e3b36', '54be0e1d-3fc0-4c82-b250-1f041a25c659', NULL, '52dcb698-2d08-4802-acea-10a01a25c659', 'Login', '', '', '', 0, '0', '', '', 'Jetzt geht es los!', 'XXXXX', '2020-08-16 00:32:18', '2020-09-10 23:47:23'),
('5274ad2a-c378-4f12-94b7-10e81a25c659', '54be0e1d-3fc0-4c82-b250-1f041a25c659', '51a62852-43a4-4bd7-bf77-17fc1a25c659', '4e60bb70-7378-4a7c-a5f4-016f1a25c659', 'Kasse', '', '', 'Sie haben eine neue Bestellung erhalten', 0, '0', '', '', '', '', '2013-11-02 08:43:38', '2019-09-26 13:13:48'),
('52dcb796-88fc-494e-af1d-10a01a25c659', '54be0e1d-3fc0-4c82-b250-1f041a25c659', '51a62852-43a4-4bd7-bf77-17fc1a25c659', '437cc9d0-de84-4dfe-b7a9-1a841a25c659', 'Kontakt', '', '', '', 0, '-', '', '', '', 'asdasdas', '2014-01-20 06:43:50', '2020-10-15 13:40:00'),
('52dcfee7-7b40-4359-a2a1-10a01a25c659', '54be0e1d-3fc0-4c82-b250-1f041a25c659', '51a62852-43a4-4bd7-bf77-17fc1a25c659', '52dcfe90-7770-4d81-9c91-10a01a25c659', 'Passwort vergessen', '', '', '', 0, '0', '', '', '', 'pwlost', '2014-01-20 11:48:07', '2019-09-26 13:14:00'),
('52f8eabe-0680-4164-8467-21141a25c659', '54be0e1d-3fc0-4c82-b250-1f041a25c659', '51a62852-43a4-4bd7-bf77-17fc1a25c659', '4ebd11e1-842c-4eb7-9d4d-02041a25c659', 'Newsletter', '', '', '', 0, '0', '', '', '', '', '2014-02-10 16:05:34', '2019-09-26 13:14:07');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `formularconfigs_translations`
--

DROP TABLE IF EXISTS `formularconfigs_translations`;
CREATE TABLE `formularconfigs_translations` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `locale` char(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `after_send_headline` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `after_send_content` longtext COLLATE utf8mb4_unicode_ci,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `formulars`
--

DROP TABLE IF EXISTS `formulars`;
CREATE TABLE `formulars` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `component` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `horizontal` tinyint(1) DEFAULT '0',
  `hidelabels` tinyint(1) DEFAULT '0',
  `alignment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `btnname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `captcha` tinyint(1) DEFAULT '0',
  `send_email` tinyint(1) DEFAULT '0',
  `send_attachments` tinyint(1) DEFAULT '0',
  `save_attachments` tinyint(1) DEFAULT '0',
  `generate_attachments` tinyint(1) DEFAULT '0',
  `extended_data` longtext COLLATE utf8mb4_unicode_ci,
  `conversion_tracking` text COLLATE utf8mb4_unicode_ci,
  `use_conversion_tracking` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=16384 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `formulars`
--

INSERT INTO `formulars` (`id`, `name`, `component`, `horizontal`, `hidelabels`, `alignment`, `btnname`, `captcha`, `send_email`, `send_attachments`, `save_attachments`, `generate_attachments`, `extended_data`, `conversion_tracking`, `use_conversion_tracking`, `created`, `modified`) VALUES
('437cc9d0-de84-4dfe-b7a9-1a841a25c659', 'Testformular', 'formular', 1, 0, 'left', 'Jetzt kostenfrei testen!', 1, 1, 0, 0, 0, '{\"fields\":[{\"id\":\"a8a9e75f-2e32-4183-ad2e-894b8557648f\",\"label\":\"\\u00dcberschrift h3\",\"type\":\"h3\",\"colXs\":\"12\",\"colSm\":\"12\",\"colMd\":\"12\",\"colLg\":\"12\",\"placeholder\":\"\",\"inputName\":\"\",\"options\":\"\",\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"labelaligntop\":false,\"labelalignleft\":false,\"allowEmpty\":false,\"validationRule\":null,\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"infotext\":\"\",\"defaultValue\":\"\",\"showOnFieldComparator\":null,\"prepend\":\"\",\"append\":\"\",\"notBlank\":false,\"validation1Rule\":null,\"validation1Param1\":\"\",\"validation1Param2\":\"\",\"validation1Message\":\"\",\"validation2Rule\":null,\"validation2Param1\":\"\",\"validation2Param2\":\"\",\"validation2Message\":\"\",\"validation3Rule\":null,\"validation3Param1\":\"\",\"validation3Param2\":\"\",\"validation3Message\":\"\",\"tooltip\":\"\",\"_dragid\":\"4661ad51-dad0-11e9-a9d9-fd4ccdcc21be\"},{\"_dragid\":\"b689e2d0-d0d6-11ea-9c82-e1bc06e44659\",\"label\":\"Anrede\",\"type\":\"select\",\"inputName\":\"salutation\",\"id\":\"b68a09e0-d0d6-11ea-9c82-e1bc06e44659\",\"placeholder\":\"Anrede\",\"options\":\"salutation\",\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"showOnFieldComparator\":null,\"colXs\":null,\"colSm\":null,\"colMd\":null,\"colLg\":\"4\",\"infotext\":\"\",\"defaultValue\":\"\",\"prepend\":\"p\",\"append\":\"a\",\"notBlank\":true,\"tooltip\":\"\",\"hideLabel\":true,\"cssClass\":\"a b c\",\"validationRules\":[{\"validationMessage\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationRule\":null},{\"validationMessage\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationRule\":null},{\"validationMessage\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationRule\":null},{\"validationMessage\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationRule\":null}],\"validationMessage\":\"\"},{\"id\":\"c255a790-0ecd-11eb-ab49-11141145507e\",\"label\":\"Vorname\",\"type\":\"text\",\"inputName\":\"firstname\",\"validationRules\":[{\"validationMessage\":\"\",\"validationParam1\":\"1\",\"validationParam2\":\"2\",\"validationRule\":\"checked\"}],\"hideLabel\":true,\"placeholder\":\"\",\"defaultValue\":\"\",\"options\":\"\",\"tooltip\":\"\",\"infotext\":\"\",\"showOnFieldName\":\"\",\"showOnFieldComparator\":\"\",\"showOnFieldValue\":\"\",\"notBlank\":true,\"validationMessage\":\"Schrein mal was rein\",\"cssClass\":\"\",\"prepend\":\"p\",\"append\":\"a\",\"colXs\":\"\",\"colSm\":\"\",\"colMd\":\"\",\"colLg\":\"4\"},{\"label\":\"Nachname\",\"inputName\":\"person.lastname\",\"type\":\"text\",\"einspaltig\":false,\"allowEmpty\":false,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"71b70e9a-7233-4053-b4ce-a48960245ee0\",\"order\":2,\"colSm\":null,\"colMd\":\"4\",\"colLg\":\"4\",\"placeholder\":\"Nachname\",\"labelaligntop\":true,\"labelalignleft\":false,\"colXs\":null,\"validation1Rule\":\"alphaNumeric\",\"notBlank\":true,\"showOnFieldComparator\":null,\"prepend\":\"\",\"append\":\"\\u20ac\",\"validation1Param1\":\"\",\"validation1Param2\":\"\",\"validation1Message\":\"\",\"validation2Rule\":null,\"validation2Param1\":\"\",\"validation2Param2\":\"\",\"validation2Message\":\"\",\"validation3Rule\":null,\"validation3Param1\":\"\",\"validation3Param2\":\"\",\"validation3Message\":\"\",\"tooltip\":\"t\",\"hideLabel\":true,\"cssClass\":\"\",\"_dragid\":\"4661ad53-dad0-11e9-a9d9-fd4ccdcc21be\",\"defaultvalue\":\"Nachname\",\"validationRules\":[]},{\"id\":\"4359b530-42a8-4561-b791-2fa339b88155\",\"label\":\"--\",\"inputName\":\"Trennlinie\",\"type\":\"hr\",\"placeholder\":\"\",\"options\":\"\",\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"labelaligntop\":false,\"labelalignleft\":false,\"colXs\":\"12\",\"colSm\":\"12\",\"colMd\":\"12\",\"colLg\":\"12\",\"allowEmpty\":false,\"validationRule\":null,\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"infotext\":\"\",\"defaultValue\":\"\",\"showOnFieldComparator\":null,\"prepend\":\"\",\"append\":\"\",\"notBlank\":false,\"validation1Rule\":null,\"validation1Param1\":\"\",\"validation1Param2\":\"\",\"validation1Message\":\"\",\"validation2Rule\":null,\"validation2Param1\":\"\",\"validation2Param2\":\"\",\"validation2Message\":\"\",\"validation3Rule\":null,\"validation3Param1\":\"\",\"validation3Param2\":\"\",\"validation3Message\":\"\",\"tooltip\":\"\",\"_dragid\":\"4661ad54-dad0-11e9-a9d9-fd4ccdcc21be\"},{\"id\":\"c49b63bc-0a7c-49aa-a4af-6172eacb0db7\",\"label\":\"Eine einfache Checkbox\",\"type\":\"checkbox\",\"notBlank\":true,\"placeholder\":\"\",\"inputName\":\"MyCheckBox\",\"defaultValue\":\"\",\"options\":\"Checkbox\",\"showOnFieldName\":\"\",\"showOnFieldComparator\":null,\"showOnFieldValue\":\"\",\"labelaligntop\":false,\"labelalignleft\":false,\"prepend\":\"\",\"append\":\"\",\"colXs\":null,\"colSm\":null,\"colMd\":null,\"colLg\":null,\"validationMessage\":\"\",\"validation1Rule\":null,\"validation1Param1\":\"10\",\"validation1Param2\":\"\",\"validation1Message\":\"\",\"validation2Rule\":null,\"validation2Param1\":\"\",\"validation2Param2\":\"\",\"validation2Message\":\"\",\"validation3Rule\":null,\"validation3Param1\":\"\",\"validation3Param2\":\"\",\"validation3Message\":\"\",\"tooltip\":\"Mit Tooltip\",\"infotext\":\"& Infottext\",\"hideLabel\":false,\"cssClass\":\"\",\"_dragid\":\"4661ad55-dad0-11e9-a9d9-fd4ccdcc21be\",\"validationRules\":[]},{\"_dragid\":\"e294bb70-d31a-11ea-9c57-e7399ef41144\",\"label\":\"Eine Checkboxgruppe\",\"type\":\"checkbox\",\"inputName\":\"test\",\"id\":\"e294e280-d31a-11ea-9c57-e7399ef41144\",\"placeholder\":\"\",\"options\":\"1|2|3\",\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"showOnFieldComparator\":null,\"colXs\":null,\"colSm\":null,\"colMd\":null,\"colLg\":null,\"infotext\":\"\",\"defaultValue\":\"\",\"prepend\":\"\",\"append\":\"\",\"notBlank\":false,\"tooltip\":\"\",\"hideLabel\":false,\"cssClass\":\"\",\"validationRules\":[]},{\"label\":\"Strasse\",\"inputName\":\"street\",\"type\":\"text\",\"einspaltig\":false,\"allowEmpty\":true,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"f6885f1b-86af-4346-93eb-43b5643ec475\",\"order\":3,\"placeholder\":\"\",\"labelaligntop\":false,\"showOnFieldComparator\":null,\"labelalignleft\":false,\"prepend\":\"\",\"append\":\"\",\"colXs\":null,\"colSm\":null,\"colMd\":null,\"colLg\":null,\"notBlank\":false,\"validation1Rule\":null,\"validation1Param1\":\"\",\"validation1Param2\":\"\",\"validation1Message\":\"\",\"validation2Rule\":null,\"validation2Param1\":\"\",\"validation2Param2\":\"\",\"validation2Message\":\"\",\"validation3Rule\":null,\"validation3Param1\":\"\",\"validation3Param2\":\"\",\"validation3Message\":\"\",\"tooltip\":\"\",\"_dragid\":\"4661ad56-dad0-11e9-a9d9-fd4ccdcc21be\"},{\"label\":\"Hausnummer\",\"inputName\":\"housenumber\",\"type\":\"text\",\"einspaltig\":false,\"allowEmpty\":true,\"validationRule\":\"\",\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"2faeac4a-bd01-47ee-8597-9dc42c88b090\",\"order\":4,\"placeholder\":\"\",\"showOnFieldComparator\":null,\"labelaligntop\":false,\"labelalignleft\":false,\"prepend\":\"\",\"append\":\"\",\"colXs\":null,\"colSm\":null,\"colMd\":null,\"colLg\":null,\"notBlank\":false,\"validation1Rule\":null,\"validation1Param1\":\"\",\"validation1Param2\":\"\",\"validation1Message\":\"\",\"validation2Rule\":null,\"validation2Param1\":\"\",\"validation2Param2\":\"\",\"validation2Message\":\"\",\"validation3Rule\":null,\"validation3Param1\":\"\",\"validation3Param2\":\"\",\"validation3Message\":\"\",\"tooltip\":\"\",\"_dragid\":\"4661ad57-dad0-11e9-a9d9-fd4ccdcc21be\",\"hideLabel\":false,\"cssClass\":\"\",\"validationRules\":[]},{\"label\":\"Plz\",\"inputName\":\"plz\",\"type\":\"text\",\"einspaltig\":false,\"allowEmpty\":true,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"797dd937-020e-4c2f-b443-37dd0d73f778\",\"order\":7,\"colXs\":null,\"colMd\":null,\"colLg\":null,\"placeholder\":\"\",\"labelaligntop\":false,\"labelalignleft\":false,\"colSm\":null,\"showOnFieldComparator\":null,\"prepend\":\"\",\"append\":\"\",\"notBlank\":true,\"validation1Rule\":\"numeric\",\"validation1Param1\":\"\",\"validation1Param2\":\"\",\"validation1Message\":\"\",\"validation2Rule\":null,\"validation2Param1\":\"\",\"validation2Param2\":\"\",\"validation2Message\":\"\",\"validation3Rule\":null,\"validation3Param1\":\"\",\"validation3Param2\":\"\",\"validation3Message\":\"\",\"tooltip\":\"\",\"hideLabel\":false,\"cssClass\":\"\",\"_dragid\":\"4661ad58-dad0-11e9-a9d9-fd4ccdcc21be\"},{\"label\":\"Ort\",\"inputName\":\"city\",\"type\":\"text\",\"einspaltig\":false,\"allowEmpty\":true,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"2c8b2413-1c07-49bf-9d25-a5f63a957faa\",\"order\":6,\"colMd\":null,\"colLg\":null,\"placeholder\":\"\",\"labelaligntop\":false,\"labelalignleft\":false,\"colXs\":null,\"colSm\":null,\"showOnFieldComparator\":null,\"prepend\":\"\",\"append\":\"\",\"notBlank\":false,\"validation1Rule\":null,\"validation1Param1\":\"\",\"validation1Param2\":\"\",\"validation1Message\":\"\",\"validation2Rule\":null,\"validation2Param1\":\"\",\"validation2Param2\":\"\",\"validation2Message\":\"\",\"validation3Rule\":null,\"validation3Param1\":\"\",\"validation3Param2\":\"\",\"validation3Message\":\"\",\"tooltip\":\"\",\"_dragid\":\"4661ad59-dad0-11e9-a9d9-fd4ccdcc21be\",\"hideLabel\":false,\"cssClass\":\"\",\"validationRules\":[]},{\"label\":\"Land\",\"inputName\":\"country\",\"type\":\"select\",\"einspaltig\":true,\"allowEmpty\":true,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"country\",\"infotext\":\"Infotext\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"242ac2f3-c093-4661-ae0b-2bbd8a43e19e\",\"order\":5,\"colMd\":null,\"colLg\":null,\"placeholder\":\"\",\"labelaligntop\":false,\"labelalignleft\":false,\"colXs\":null,\"colSm\":null,\"showOnFieldComparator\":null,\"prepend\":\"\",\"append\":\"\",\"notBlank\":false,\"validation1Rule\":null,\"validation1Param1\":\"\",\"validation1Param2\":\"\",\"validation1Message\":\"\",\"validation2Rule\":null,\"validation2Param1\":\"\",\"validation2Param2\":\"\",\"validation2Message\":\"\",\"validation3Rule\":null,\"validation3Param1\":\"\",\"validation3Param2\":\"\",\"validation3Message\":\"\",\"tooltip\":\"\",\"_dragid\":\"4661ad5a-dad0-11e9-a9d9-fd4ccdcc21be\",\"hideLabel\":false,\"cssClass\":\"\",\"validationRules\":[]},{\"id\":\"c3e9037a-557c-4f6e-8a61-419f333df042\",\"label\":\"Datetime\",\"type\":\"datetime\",\"inputName\":\"datetime\",\"notBlank\":true,\"validation1Rule\":null,\"placeholder\":\"\",\"defaultValue\":\"\",\"options\":\"\",\"showOnFieldName\":\"\",\"showOnFieldComparator\":null,\"showOnFieldValue\":\"\",\"labelaligntop\":false,\"labelalignleft\":false,\"prepend\":\"\",\"append\":\"\",\"colXs\":null,\"colSm\":null,\"colMd\":null,\"colLg\":null,\"validationMessage\":\"\",\"validation1Param1\":\"\",\"validation1Param2\":\"\",\"validation1Message\":\"\",\"validation2Rule\":null,\"validation2Param1\":\"\",\"validation2Param2\":\"\",\"validation2Message\":\"\",\"validation3Rule\":null,\"validation3Param1\":\"\",\"validation3Param2\":\"\",\"validation3Message\":\"\",\"tooltip\":\"\",\"infotext\":\"\",\"_dragid\":\"4661ad5c-dad0-11e9-a9d9-fd4ccdcc21be\"},{\"label\":\"E-Mail\",\"inputName\":\"email\",\"type\":\"text\",\"einspaltig\":true,\"allowEmpty\":false,\"validationRule\":\"email\",\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"19535586-4fa0-4b62-8548-588e94eaa26b\",\"order\":8,\"placeholder\":\"\",\"showOnFieldComparator\":null,\"labelaligntop\":false,\"labelalignleft\":false,\"prepend\":\"\",\"append\":\"\",\"colXs\":null,\"colSm\":null,\"colMd\":null,\"colLg\":null,\"notBlank\":false,\"validation1Rule\":null,\"validation1Param1\":\"\",\"validation1Param2\":\"\",\"validation1Message\":\"\",\"validation2Rule\":null,\"validation2Param1\":\"\",\"validation2Param2\":\"\",\"validation2Message\":\"\",\"validation3Rule\":null,\"validation3Param1\":\"\",\"validation3Param2\":\"\",\"validation3Message\":\"\",\"tooltip\":\"\",\"_dragid\":\"4661ad5b-dad0-11e9-a9d9-fd4ccdcc21be\"},{\"label\":\"Telefon\",\"inputName\":\"telefon\",\"type\":\"text\",\"einspaltig\":false,\"allowEmpty\":false,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"9553ec53-e18c-4a6e-849c-305b14f61590\",\"order\":10,\"placeholder\":\"\",\"labelaligntop\":false,\"showOnFieldComparator\":null,\"labelalignleft\":false,\"prepend\":\"\",\"append\":\"\",\"colXs\":null,\"colSm\":null,\"colMd\":null,\"colLg\":null,\"notBlank\":false,\"validation1Rule\":null,\"validation1Param1\":\"\",\"validation1Param2\":\"\",\"validation1Message\":\"\",\"validation2Rule\":null,\"validation2Param1\":\"\",\"validation2Param2\":\"\",\"validation2Message\":\"\",\"validation3Rule\":null,\"validation3Param1\":\"\",\"validation3Param2\":\"\",\"validation3Message\":\"\",\"tooltip\":\"\",\"_dragid\":\"4661ad5d-dad0-11e9-a9d9-fd4ccdcc21be\"},{\"label\":\"Bitte rufen Sie mich zur\\u00fcck.\",\"inputName\":\"callback\",\"type\":\"checkbox\",\"einspaltig\":false,\"allowEmpty\":true,\"validationRule\":\"\",\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"407d1b16-87ef-4266-aca1-2b32104b4786\",\"order\":9,\"placeholder\":\"\",\"showOnFieldComparator\":null,\"labelaligntop\":false,\"labelalignleft\":false,\"prepend\":\"\",\"append\":\"\",\"colXs\":null,\"colSm\":null,\"colMd\":null,\"colLg\":null,\"notBlank\":true,\"validation1Rule\":null,\"validation1Param1\":\"\",\"validation1Param2\":\"\",\"validation1Message\":\"\",\"validation2Rule\":null,\"validation2Param1\":\"\",\"validation2Param2\":\"\",\"validation2Message\":\"\",\"validation3Rule\":null,\"validation3Param1\":\"\",\"validation3Param2\":\"\",\"validation3Message\":\"\",\"tooltip\":\"\",\"_dragid\":\"4661ad5e-dad0-11e9-a9d9-fd4ccdcc21be\"},{\"label\":\"Nachricht\",\"inputName\":\"message\",\"type\":\"textarea\",\"einspaltig\":true,\"allowEmpty\":true,\"validationRule\":\"\",\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"34cb53c6-0e1f-4639-a2c3-4da1805c178d\",\"order\":11,\"placeholder\":\"\",\"showOnFieldComparator\":null,\"labelaligntop\":false,\"labelalignleft\":false,\"prepend\":\"\",\"append\":\"\",\"colXs\":null,\"colSm\":null,\"colMd\":null,\"colLg\":null,\"notBlank\":false,\"validation1Rule\":null,\"validation1Param1\":\"\",\"validation1Param2\":\"\",\"validation1Message\":\"\",\"validation2Rule\":null,\"validation2Param1\":\"\",\"validation2Param2\":\"\",\"validation2Message\":\"\",\"validation3Rule\":null,\"validation3Param1\":\"\",\"validation3Param2\":\"\",\"validation3Message\":\"\",\"tooltip\":\"\",\"_dragid\":\"4661ad5f-dad0-11e9-a9d9-fd4ccdcc21be\"},{\"id\":\"9ec8ea29-7f22-487c-a473-e1c4fdac037f\",\"label\":\"datetest\",\"type\":\"date\",\"inputName\":\"datetest\",\"placeholder\":\"\",\"defaultValue\":\"\",\"options\":\"\",\"showOnFieldName\":\"\",\"showOnFieldComparator\":null,\"showOnFieldValue\":\"\",\"labelaligntop\":false,\"labelalignleft\":false,\"prepend\":\"\",\"append\":\"\",\"colXs\":null,\"colSm\":null,\"colMd\":null,\"colLg\":null,\"notBlank\":false,\"validationMessage\":\"\",\"validation1Rule\":null,\"validation1Param1\":\"\",\"validation1Param2\":\"\",\"validation1Message\":\"\",\"validation2Rule\":null,\"validation2Param1\":\"\",\"validation2Param2\":\"\",\"validation2Message\":\"\",\"validation3Rule\":null,\"validation3Param1\":\"\",\"validation3Param2\":\"\",\"validation3Message\":\"\",\"tooltip\":\"\",\"infotext\":\"\",\"_dragid\":\"4661ad60-dad0-11e9-a9d9-fd4ccdcc21be\"},{\"id\":\"ea1dcc0f-72f3-4110-acff-eb030c062f91\",\"label\":\"Timetest\",\"inputName\":\"timetest\",\"type\":\"time\",\"placeholder\":\"\",\"defaultValue\":\"\",\"options\":\"\",\"showOnFieldName\":\"\",\"showOnFieldComparator\":null,\"showOnFieldValue\":\"\",\"labelaligntop\":false,\"labelalignleft\":false,\"prepend\":\"\",\"append\":\"\",\"colXs\":null,\"colSm\":null,\"colMd\":null,\"colLg\":null,\"notBlank\":true,\"validationMessage\":\"\",\"validation1Rule\":null,\"validation1Param1\":\"\",\"validation1Param2\":\"\",\"validation1Message\":\"\",\"validation2Rule\":null,\"validation2Param1\":\"\",\"validation2Param2\":\"\",\"validation2Message\":\"\",\"validation3Rule\":null,\"validation3Param1\":\"\",\"validation3Param2\":\"\",\"validation3Message\":\"\",\"tooltip\":\"\",\"infotext\":\"\",\"_dragid\":\"4661ad61-dad0-11e9-a9d9-fd4ccdcc21be\"},{\"id\":\"acd3db70-26af-4f56-a564-233d8e800f3d\",\"label\":\"Multiselect test\",\"inputName\":\"multiselecttest\",\"type\":\"multiselect\",\"options\":\"country\",\"defaultValue\":\"\",\"placeholder\":\"\",\"showOnFieldName\":\"\",\"showOnFieldComparator\":null,\"showOnFieldValue\":\"\",\"labelaligntop\":false,\"labelalignleft\":false,\"prepend\":\"\",\"append\":\"\",\"colXs\":null,\"colSm\":null,\"colMd\":null,\"colLg\":null,\"notBlank\":false,\"validationMessage\":\"\",\"validation1Rule\":null,\"validation1Param1\":\"\",\"validation1Param2\":\"\",\"validation1Message\":\"\",\"validation2Rule\":null,\"validation2Param1\":\"\",\"validation2Param2\":\"\",\"validation2Message\":\"\",\"validation3Rule\":null,\"validation3Param1\":\"\",\"validation3Param2\":\"\",\"validation3Message\":\"\",\"tooltip\":\"\",\"infotext\":\"\",\"_dragid\":\"4661ad62-dad0-11e9-a9d9-fd4ccdcc21be\",\"hideLabel\":false,\"cssClass\":\"\",\"validationRules\":[]},{\"id\":\"a38b7809-d80d-4718-bb3e-2b5bd138954b\",\"label\":\"Radios\",\"options\":\"1|2|3\",\"inputName\":\"Radios\",\"type\":\"radio\",\"notBlank\":true,\"placeholder\":\"\",\"defaultValue\":\"\",\"showOnFieldName\":\"\",\"showOnFieldComparator\":null,\"showOnFieldValue\":\"\",\"labelaligntop\":false,\"labelalignleft\":false,\"prepend\":\"\",\"append\":\"\",\"colXs\":null,\"colSm\":null,\"colMd\":null,\"colLg\":null,\"validationMessage\":\"\",\"validation1Rule\":null,\"validation1Param1\":\"\",\"validation1Param2\":\"\",\"validation1Message\":\"\",\"validation2Rule\":null,\"validation2Param1\":\"\",\"validation2Param2\":\"\",\"validation2Message\":\"\",\"validation3Rule\":null,\"validation3Param1\":\"\",\"validation3Param2\":\"\",\"validation3Message\":\"\",\"tooltip\":\"asd\",\"infotext\":\"\",\"hideLabel\":false,\"_dragid\":\"4661ad63-dad0-11e9-a9d9-fd4ccdcc21be\"},{\"id\":\"685a2209-2f41-4e26-9616-f1936f8b8976\",\"label\":\"Checkboxen\",\"type\":\"checkbox\",\"options\":\"1|2|3\",\"placeholder\":\"Checkboxen\",\"inputName\":\"Checkboxen\",\"notBlank\":true,\"defaultValue\":\"\",\"showOnFieldName\":\"\",\"showOnFieldComparator\":null,\"showOnFieldValue\":\"\",\"labelaligntop\":false,\"labelalignleft\":false,\"prepend\":\"\",\"append\":\"\",\"colXs\":null,\"colSm\":null,\"colMd\":null,\"colLg\":null,\"validationMessage\":\"\",\"validation1Rule\":null,\"validation1Param1\":\"\",\"validation1Param2\":\"\",\"validation1Message\":\"\",\"validation2Rule\":null,\"validation2Param1\":\"\",\"validation2Param2\":\"\",\"validation2Message\":\"\",\"validation3Rule\":null,\"validation3Param1\":\"\",\"validation3Param2\":\"\",\"validation3Message\":\"\",\"tooltip\":\"\",\"infotext\":\"\",\"_dragid\":\"4661ad64-dad0-11e9-a9d9-fd4ccdcc21be\"}],\"colXs\":null,\"colSm\":null,\"colMd\":null,\"colLg\":\"6\"}', '', 0, '2014-05-21 17:44:16', '2020-10-15 14:13:21'),
('4e60bb70-7378-4a7c-a5f4-016f1a25c659', 'Kasse (Online-Shop)', 'checkout', 0, 0, 'left', 'Jetzt zahlungspflichtig bestellen', 0, 1, 0, 0, 0, '{\"formular\":{\"formtype\":1},\"fields\":[{\"label\":\"Ihr Warenkorb:\",\"inputName\":\"ueberschriftwarenkorb\",\"type\":\"h3\",\"einspaltig\":true,\"allowEmpty\":true,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":\"\",\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"10\",\"order\":0},{\"label\":\"Warenkorb\",\"inputName\":\"card\",\"type\":\"element\",\"einspaltig\":true,\"allowEmpty\":true,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":\"\",\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"11\",\"order\":1},{\"label\":\"Bitte w\\u00e4hlen Sie Ihr Gutschein-Layout\",\"inputName\":\"voucherlayout\",\"type\":\"element\",\"einspaltig\":true,\"allowEmpty\":false,\"validationRule\":null,\"defaultValue\":\"01\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":true,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"13\",\"order\":2},{\"label\":\"Ihre pers\\u00f6nliche Widmung\",\"inputName\":\"dedication\",\"type\":\"textarea\",\"einspaltig\":true,\"allowEmpty\":true,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":true,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"14\",\"order\":3},{\"label\":\"formgutscheinvorschau\",\"inputName\":\"voucherpreview\",\"type\":\"element\",\"einspaltig\":true,\"allowEmpty\":true,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":\"\",\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"17\",\"order\":4},{\"label\":\"--\",\"inputName\":\"\",\"type\":\"hr\",\"einspaltig\":true,\"allowEmpty\":true,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":\"\",\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"18\",\"order\":5},{\"label\":\"Rechnungsanschrift \\/ Lieferanschrift\",\"inputName\":\"\",\"type\":\"h3\",\"einspaltig\":true,\"allowEmpty\":true,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":\"\",\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"19\",\"order\":6},{\"label\":\"Firma\",\"inputName\":\"company\",\"type\":\"text\",\"einspaltig\":false,\"allowEmpty\":true,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":\"\",\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"23\",\"order\":7},{\"label\":\"Umbruch\",\"inputName\":\"\",\"type\":\"umbruch\",\"einspaltig\":false,\"allowEmpty\":true,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":false,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"ded1793a-484a-4a0e-b28d-d9b5a5d4949e\",\"order\":8},{\"label\":\"Anrede\",\"inputName\":\"salutation\",\"type\":\"select\",\"einspaltig\":false,\"allowEmpty\":false,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"salutation\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":\"\",\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"20\",\"order\":9},{\"label\":\"Umbruch\",\"inputName\":\"\",\"type\":\"umbruch\",\"einspaltig\":false,\"allowEmpty\":true,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":false,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"184cbc26-1936-48fb-95a5-fe439d7f1f9e\",\"order\":10},{\"label\":\"Vorname\",\"inputName\":\"firstname\",\"type\":\"text\",\"einspaltig\":false,\"allowEmpty\":false,\"validationRule\":\"\",\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":\"\",\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"21\",\"order\":11},{\"label\":\"Nachname\",\"inputName\":\"lastname\",\"type\":\"text\",\"einspaltig\":false,\"allowEmpty\":false,\"validationRule\":\"\",\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":false,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"22\",\"order\":12},{\"label\":\"Land\",\"inputName\":\"country\",\"type\":\"select\",\"einspaltig\":false,\"allowEmpty\":false,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"country\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":\"\",\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"24\",\"order\":13},{\"label\":\"Umbruch\",\"inputName\":\"\",\"type\":\"umbruch\",\"einspaltig\":false,\"allowEmpty\":true,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":\"\",\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"25\",\"order\":14},{\"label\":\"Plz\",\"inputName\":\"plz\",\"type\":\"selectsuggest\",\"einspaltig\":false,\"allowEmpty\":false,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":\"\",\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"26\",\"order\":15},{\"label\":\"Ort\",\"inputName\":\"city\",\"type\":\"selectsuggest\",\"einspaltig\":false,\"allowEmpty\":false,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":\"\",\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"27\",\"order\":16},{\"label\":\"Strasse\",\"inputName\":\"street\",\"type\":\"text\",\"einspaltig\":false,\"allowEmpty\":false,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":\"\",\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"28\",\"order\":17},{\"label\":\"Hausnummer\",\"inputName\":\"housenumber\",\"type\":\"text\",\"einspaltig\":false,\"allowEmpty\":false,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":\"\",\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"29\",\"order\":18},{\"label\":\"Umbruch\",\"inputName\":\"\",\"type\":\"umbruch\",\"einspaltig\":false,\"allowEmpty\":true,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":\"\",\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"30\",\"order\":19},{\"label\":\"E-Mail\",\"inputName\":\"email\",\"type\":\"text\",\"einspaltig\":false,\"allowEmpty\":false,\"validationRule\":\"email\",\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"An diese E-Mail-Adresse senden wir Ihnen Ihren Gutschein und Ihre Rechnung.\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":false,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"31\",\"order\":20},{\"label\":\"Telefonnummer\",\"inputName\":\"telefon\",\"type\":\"text\",\"einspaltig\":false,\"allowEmpty\":true,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":false,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"32\",\"order\":21},{\"label\":\"--\",\"inputName\":\"\",\"type\":\"hr\",\"einspaltig\":true,\"allowEmpty\":true,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":\"\",\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"33\",\"order\":22},{\"label\":\"Ja, ich m\\u00f6chte mir die Bestellung an eine abweichende Lieferanschrift (z.B. an meinen Arbeitsplatz) senden lassen.\",\"inputName\":\"liefer\",\"type\":\"checkbox\",\"einspaltig\":true,\"allowEmpty\":true,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":true,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"34\",\"order\":23},{\"label\":\"Firma\",\"inputName\":\"liefercompany\",\"type\":\"text\",\"einspaltig\":false,\"allowEmpty\":true,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":false,\"showOnFieldName\":\"liefer\",\"showOnFieldValue\":\"1\",\"id\":\"39\",\"order\":24},{\"label\":\"Umbruch\",\"inputName\":\"\",\"type\":\"umbruch\",\"einspaltig\":false,\"allowEmpty\":true,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":false,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"bc30c091-ef69-4bcd-8301-6e0af5518c8c\",\"order\":25},{\"label\":\"Anrede\",\"inputName\":\"liefersalutation\",\"type\":\"select\",\"einspaltig\":false,\"allowEmpty\":false,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"salutation\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":false,\"showOnFieldName\":\"liefer\",\"showOnFieldValue\":\"1\",\"id\":\"36\",\"order\":26},{\"label\":\"Umbruch\",\"inputName\":\"\",\"type\":\"umbruch\",\"einspaltig\":false,\"allowEmpty\":true,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":false,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"acaba7b9-6051-40ed-9c21-be3f6ae07e2f\",\"order\":27},{\"label\":\"Vorname\",\"inputName\":\"lieferfirstname\",\"type\":\"text\",\"einspaltig\":false,\"allowEmpty\":false,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":false,\"showOnFieldName\":\"liefer\",\"showOnFieldValue\":\"1\",\"id\":\"37\",\"order\":28},{\"label\":\"Nachname\",\"inputName\":\"lieferlastname\",\"type\":\"text\",\"einspaltig\":false,\"allowEmpty\":false,\"validationRule\":\" \",\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":false,\"showOnFieldName\":\"liefer\",\"showOnFieldValue\":\"1\",\"id\":\"38\",\"order\":29},{\"label\":\"Land\",\"inputName\":\"liefercountry\",\"type\":\"select\",\"einspaltig\":false,\"allowEmpty\":false,\"validationRule\":null,\"defaultValue\":\"de\",\"options\":\"country\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":false,\"showOnFieldName\":\"liefer\",\"showOnFieldValue\":\"1\",\"id\":\"40\",\"order\":30},{\"label\":\"Umbruch\",\"inputName\":\"\",\"type\":\"umbruch\",\"einspaltig\":false,\"allowEmpty\":true,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":false,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"d0825d2e-3570-42ff-a3f5-75424987e474\",\"order\":31},{\"label\":\"Plz\",\"inputName\":\"lieferplz\",\"type\":\"text\",\"einspaltig\":false,\"allowEmpty\":false,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":false,\"showOnFieldName\":\"liefer\",\"showOnFieldValue\":\"1\",\"id\":\"41\",\"order\":32},{\"label\":\"Ort\",\"inputName\":\"liefercity\",\"type\":\"text\",\"einspaltig\":false,\"allowEmpty\":false,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":false,\"showOnFieldName\":\"liefer\",\"showOnFieldValue\":\"1\",\"id\":\"42\",\"order\":33},{\"label\":\"Strasse\",\"inputName\":\"lieferstreet\",\"type\":\"text\",\"einspaltig\":false,\"allowEmpty\":false,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":false,\"showOnFieldName\":\"liefer\",\"showOnFieldValue\":\"1\",\"id\":\"43\",\"order\":34},{\"label\":\"Hausnummer\",\"inputName\":\"lieferhousenumber\",\"type\":\"text\",\"einspaltig\":false,\"allowEmpty\":false,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":false,\"showOnFieldName\":\"liefer\",\"showOnFieldValue\":\"1\",\"id\":\"44\",\"order\":35},{\"label\":\"--\",\"inputName\":\"\",\"type\":\"hr\",\"einspaltig\":true,\"allowEmpty\":false,\"validationRule\":\"\",\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":\"\",\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"6bb98a5d-091a-4c5f-a8a9-e230f7a70c04\",\"order\":36},{\"label\":\"Wie wollen Sie den Gutschein erhalten?\",\"inputName\":\"shippingoption\",\"type\":\"radio\",\"einspaltig\":true,\"allowEmpty\":false,\"validationRule\":\"\",\"defaultValue\":\"download\",\"options\":\"shippingoption\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":true,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"47\",\"order\":37},{\"label\":\"--\",\"inputName\":\"\",\"type\":\"hr\",\"einspaltig\":true,\"allowEmpty\":true,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":\"\",\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"48\",\"order\":38},{\"label\":\"Wie m\\u00f6chten Sie Ihren Gutschein bezahlen?\",\"inputName\":\"paymentmethod\",\"type\":\"element\",\"einspaltig\":true,\"allowEmpty\":false,\"validationRule\":\"\",\"defaultValue\":\"\",\"options\":\"paymentmethod\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":true,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"49\",\"order\":39},{\"label\":\"--\",\"inputName\":\"\",\"type\":\"hr\",\"einspaltig\":true,\"allowEmpty\":true,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":\"\",\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"51\",\"order\":40},{\"label\":\"Karteninhaber\",\"inputName\":\"holder\",\"type\":\"text\",\"einspaltig\":true,\"allowEmpty\":false,\"validationRule\":\"\",\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":false,\"showOnFieldName\":\"paymentmethod\",\"showOnFieldValue\":\"creditcard\",\"id\":\"52\",\"order\":41},{\"label\":\"Kreditkarte\",\"inputName\":\"cc\",\"type\":\"select\",\"einspaltig\":true,\"allowEmpty\":false,\"validationRule\":\"cc\",\"defaultValue\":\"visa\",\"options\":\"cc\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":false,\"showOnFieldName\":\"paymentmethod\",\"showOnFieldValue\":\"creditcard\",\"id\":\"53\",\"order\":42},{\"label\":\"Kartennummer\",\"inputName\":\"number\",\"type\":\"text\",\"einspaltig\":true,\"allowEmpty\":false,\"validationRule\":\"cc\",\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":false,\"showOnFieldName\":\"paymentmethod\",\"showOnFieldValue\":\"creditcard\",\"id\":\"54\",\"order\":43},{\"label\":\"G\\u00fcltig bis\",\"inputName\":\"validuntil\",\"type\":\"element\",\"einspaltig\":true,\"allowEmpty\":false,\"validationRule\":\"validuntil\",\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":false,\"showOnFieldName\":\"paymentmethod\",\"showOnFieldValue\":\"creditcard\",\"id\":\"55\",\"order\":44},{\"label\":\"Ich habe die <a class=\\\"confirm-link\\\">AGB<\\/a> gelesen und akzeptiere diese.\",\"inputName\":\"agb\",\"type\":\"confirm\",\"einspaltig\":true,\"allowEmpty\":false,\"validationRule\":\"\",\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":true,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"ddad907a-005c-4321-957f-cab0e9178ea3\",\"order\":45},{\"label\":\"Newsletter\",\"inputName\":\"newsletter\",\"type\":\"element\",\"einspaltig\":true,\"allowEmpty\":true,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":true,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"46\",\"order\":46},{\"label\":\"Zusammenfassung Ihrer Bestellung\",\"inputName\":\"\",\"type\":\"h3\",\"einspaltig\":true,\"allowEmpty\":false,\"validationRule\":\"\",\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":false,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"c1c52306-2da8-45c8-9f3a-31b6820b513a\",\"order\":47},{\"label\":\"Warenkorb\",\"inputName\":\"cardsummary\",\"type\":\"element\",\"einspaltig\":true,\"allowEmpty\":false,\"validationRule\":\"\",\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"labelaligntop\":\"\",\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"ba76aa64-9116-493d-a0ef-6c56f1235cc6\",\"order\":48}]}', '', 0, '2011-09-02 13:18:08', '2016-10-31 12:56:18'),
('4ebd11e1-842c-4eb7-9d4d-02041a25c659', 'Newsletter', 'formular', 0, 0, 'left', '', 0, 0, 0, 0, 0, '{\"fields\":[{\"label\":\"Anrede\",\"inputName\":\"salutation\",\"type\":\"select\",\"einspaltig\":false,\"allowEmpty\":false,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"salutation\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"1\",\"order\":0},{\"label\":\"Vorname\",\"inputName\":\"firstname\",\"type\":\"umbruch\",\"einspaltig\":false,\"allowEmpty\":false,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"3\",\"order\":1},{\"label\":\"Nachname\",\"inputName\":\"lastname\",\"type\":\"text\",\"einspaltig\":false,\"allowEmpty\":false,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"4\",\"order\":2},{\"label\":\"--\",\"inputName\":\"trenner\",\"type\":\"hr\",\"einspaltig\":true,\"allowEmpty\":true,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"5\",\"order\":3},{\"label\":\"E-Mail\",\"inputName\":\"email\",\"type\":\"text\",\"einspaltig\":false,\"allowEmpty\":false,\"validationRule\":\"email\",\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"6\",\"order\":4},{\"label\":\"--\",\"inputName\":\"trenner\",\"type\":\"hr\",\"einspaltig\":true,\"allowEmpty\":true,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"7\",\"order\":5},{\"label\":\"Newsletter\",\"inputName\":\"newsletter\",\"type\":\"element\",\"einspaltig\":false,\"allowEmpty\":false,\"validationRule\":\"checked\",\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"8\",\"order\":6},{\"label\":\"Haben Sie Interesse an bestimmten Themen?\",\"inputName\":\"interessen\",\"type\":\"checkbox\",\"einspaltig\":true,\"allowEmpty\":false,\"validationRule\":\"\",\"defaultValue\":\"\",\"options\":\"wellness|tagung\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"e2daa6a5-011b-46f2-bf43-2aaccd311930\",\"order\":7}]}', '', 0, '2011-11-11 13:15:29', '2019-09-18 15:42:10'),
('52dcb698-2d08-4802-acea-10a01a25c659', 'Login', 'LoginDefault', 0, 0, 'left', 'Anmelden', 0, 0, 0, 0, 0, '{\"fields\":[{\"label\":\"Benutzername \\/ E-Mail Adresse\",\"inputName\":\"login\",\"type\":\"text\",\"einspaltig\":true,\"allowEmpty\":false,\"validationRule\":\"\",\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"8b01bc54-e7be-439c-8110-3517ec142f3d\",\"order\":0,\"placeholder\":\"\",\"showOnFieldComparator\":null,\"colXs\":null,\"colSm\":null,\"colMd\":null,\"colLg\":null,\"prepend\":\"\",\"append\":\"\",\"notBlank\":true,\"tooltip\":\"\",\"hideLabel\":false,\"cssClass\":\"\",\"validationRules\":[]},{\"label\":\"Passwort\",\"inputName\":\"password\",\"type\":\"password\",\"einspaltig\":true,\"allowEmpty\":false,\"validationRule\":\"\",\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"effa4a82-c794-40ea-900c-5c2d36de7085\",\"order\":1,\"placeholder\":\"\",\"showOnFieldComparator\":null,\"colXs\":null,\"colSm\":null,\"colMd\":null,\"colLg\":null,\"prepend\":\"\",\"append\":\"\",\"notBlank\":true,\"tooltip\":\"\",\"hideLabel\":false,\"cssClass\":\"\",\"validationRules\":[]}],\"colXs\":null,\"colSm\":null,\"colMd\":null,\"colLg\":null}', '', 0, '2014-01-20 06:39:36', '2020-09-02 22:27:20'),
('52dcfe90-7770-4d81-9c91-10a01a25c659', 'Passwort vergessen', 'Pwlost', 0, 0, 'left', 'Anmelden', 0, 0, 0, 0, 0, '{\"fields\":[{\"label\":\"Benutzername \\/ E-Mail Adresse\",\"inputName\":\"login\",\"type\":\"text\",\"einspaltig\":true,\"allowEmpty\":false,\"validationRule\":\"\",\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"8b01bc54-e7be-439c-8110-3517ec142f3d\",\"order\":0}]}', '', 0, '2014-01-20 11:46:40', '2016-10-31 12:56:02');
INSERT INTO `formulars` (`id`, `name`, `component`, `horizontal`, `hidelabels`, `alignment`, `btnname`, `captcha`, `send_email`, `send_attachments`, `save_attachments`, `generate_attachments`, `extended_data`, `conversion_tracking`, `use_conversion_tracking`, `created`, `modified`) VALUES
('52e5d3d9-a8a4-4717-a2f1-26041a25c659', 'Register', 'RegisterDefault', 1, 0, 'left', 'Jetzt registrieren', 0, 0, 0, 0, 0, '{\"fields\":[{\"label\":\"Anrede\",\"inputName\":\"person.salutation\",\"type\":\"select\",\"einspaltig\":false,\"allowEmpty\":false,\"validationRule\":null,\"defaultValue\":\"\",\"options\":\"salutation\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"1c53b930-34c5-4529-84d9-b5efcfa63afd\",\"order\":0,\"placeholder\":\"\",\"labelaligntop\":false,\"showOnFieldComparator\":null,\"colXs\":null,\"colSm\":null,\"colMd\":null,\"colLg\":null,\"prepend\":\"\",\"append\":\"\",\"notBlank\":true,\"tooltip\":\"\",\"hideLabel\":false,\"cssClass\":\"\",\"validationRules\":[]},{\"label\":\"Vorname\",\"inputName\":\"person.firstname\",\"type\":\"text\",\"einspaltig\":false,\"allowEmpty\":false,\"validationRule\":\"\",\"defaultValue\":\"Zelestin\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"59fdc727-e2ac-4039-aaa2-3f1d141a6a0f\",\"order\":1,\"placeholder\":\"\",\"showOnFieldComparator\":null,\"colXs\":null,\"colSm\":null,\"colMd\":null,\"colLg\":null,\"prepend\":\"\",\"append\":\"\",\"notBlank\":true,\"tooltip\":\"\",\"hideLabel\":false,\"cssClass\":\"\",\"validationRules\":[]},{\"label\":\"Nachname\",\"inputName\":\"person.lastname\",\"type\":\"text\",\"einspaltig\":false,\"allowEmpty\":false,\"validationRule\":\"\",\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"1691ddab-c45b-4fc0-b2e7-c939faa7497b\",\"order\":2,\"placeholder\":\"\",\"showOnFieldComparator\":null,\"colXs\":null,\"colSm\":null,\"colMd\":null,\"colLg\":null,\"prepend\":\"\",\"append\":\"\",\"notBlank\":true,\"tooltip\":\"\",\"hideLabel\":false,\"cssClass\":\"\",\"validationRules\":[]},{\"label\":\"Passwort\",\"inputName\":\"newpassword\",\"type\":\"password\",\"einspaltig\":false,\"allowEmpty\":false,\"validationRule\":\"\",\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"6610fa37-40bc-4e26-af65-d6ade072fa85\",\"order\":3,\"placeholder\":\"\",\"showOnFieldComparator\":null,\"colXs\":null,\"colSm\":null,\"colMd\":null,\"colLg\":null,\"prepend\":\"\",\"append\":\"\",\"notBlank\":true,\"tooltip\":\"\",\"hideLabel\":false,\"cssClass\":\"\",\"validationRules\":[]},{\"label\":\"E-Mail\",\"inputName\":\"communication.email\",\"type\":\"text\",\"einspaltig\":false,\"allowEmpty\":false,\"validationRule\":\"email\",\"defaultValue\":\"\",\"options\":\"\",\"infotext\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationMessage\":\"\",\"dontclose\":false,\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"id\":\"e8bafa8c-3cb2-4204-8e3e-e3a04a852146\",\"order\":4,\"placeholder\":\"\",\"showOnFieldComparator\":null,\"colXs\":null,\"colSm\":null,\"colMd\":null,\"colLg\":null,\"prepend\":\"\",\"append\":\"\",\"notBlank\":true,\"tooltip\":\"\",\"hideLabel\":false,\"cssClass\":\"\",\"validationRules\":[{\"validationMessage\":\"\",\"validationParam1\":\"\",\"validationParam2\":\"\",\"validationRule\":\"email\"}]},{\"_dragid\":\"13cae7e0-f405-11ea-9db3-8d397a1e331d\",\"label\":\"Login\",\"type\":\"text\",\"inputName\":\"login\",\"id\":\"13cb0ef0-f405-11ea-9db3-8d397a1e331d\",\"placeholder\":\"\",\"options\":\"\",\"showOnFieldName\":\"\",\"showOnFieldValue\":\"\",\"showOnFieldComparator\":null,\"colXs\":null,\"colSm\":null,\"colMd\":null,\"colLg\":null,\"infotext\":\"\",\"defaultValue\":\"\",\"prepend\":\"\",\"append\":\"\",\"notBlank\":true,\"tooltip\":\"\",\"hideLabel\":false,\"cssClass\":\"\",\"validationRules\":[]}],\"colXs\":null,\"colSm\":null,\"colMd\":\"6\",\"colLg\":\"6\"}', '', 0, '2014-01-27 04:34:49', '2020-09-28 16:55:39');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `menus`
--

DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foreign_key` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aco_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `extended_data` longtext COLLATE utf8mb4_unicode_ci,
  `extended_data_cfg` longtext COLLATE utf8mb4_unicode_ci,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` mediumtext COLLATE utf8mb4_unicode_ci,
  `blank` tinyint(1) DEFAULT '0',
  `path` mediumtext COLLATE utf8mb4_unicode_ci,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `params` mediumtext COLLATE utf8mb4_unicode_ci,
  `teaser` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0',
  `begin_publishing` datetime DEFAULT NULL,
  `end_publishing` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=481 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `menus`
--

INSERT INTO `menus` (`id`, `alias_id`, `parent_id`, `foreign_key`, `aco_id`, `email_id`, `lft`, `rght`, `type`, `level`, `extended_data`, `extended_data_cfg`, `slug`, `title`, `url`, `blank`, `path`, `controller`, `action`, `model`, `params`, `teaser`, `active`, `begin_publishing`, `end_publishing`, `created`, `modified`) VALUES
('3c59c62d-9a0c-4459-86a6-d2e215811e1b', NULL, 'a837465c-ce7f-42a3-8af8-ef8db50fc1d4', 'dfbaf39a-9550-40ab-a073-ef2ac921c294', 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', NULL, 7, 8, 'event', 4, NULL, NULL, 'serienevent', 'Serienevent', NULL, 0, 'www.client.de/multipage/kalender/serienevent', 'articles', 'view_event', 'Article', NULL, 4, 1, NULL, NULL, '2021-06-27 00:14:53', '2021-06-27 00:27:42'),
('44475b77-1bc7-459b-ba07-9215c7513e50', NULL, '85bad654-97a7-41b9-adc5-330867b7208d', '79724c82-9e33-4bc5-913c-efe125d85664', 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', NULL, 10, 11, 'link', 3, NULL, NULL, 'testlink', 'Externer Link', 'https://www.google.de', 1, 'www.client.de/multipage/testlink', 'articles', 'view_link', 'Article', NULL, 8, 1, NULL, NULL, '2021-06-26 23:41:14', '2021-06-29 00:25:35'),
('4e69e939-11b4-45d8-a767-0ff41a25c659', NULL, NULL, NULL, 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', '51a62852-43a4-4bd7-bf77-17fc1a25c659', 1, 28, 'domain', 0, '{\"menu\":{\"pagetitle\":\"\",\"locale\":null,\"country\":null}}', '{\"menu\":{\"theme\":null,\"google_analytics_id\":\"G-55PEJTY9C1\"}}', 'www.client.de', 'www.client.de', NULL, 0, 'www.client.de', NULL, NULL, 'Menus', NULL, 1, 1, NULL, NULL, '2011-09-09 12:23:53', '2021-05-07 22:00:17'),
('4e69e939-182c-4aef-a5a7-0ff41a25c659', NULL, '4e69e939-11b4-45d8-a767-0ff41a25c659', NULL, 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', NULL, 4, 19, 'menu', 1, NULL, NULL, NULL, 'Hauptmenü', NULL, 0, 'www.client.de', NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, '2011-05-15 19:51:36', '2014-10-07 18:32:44'),
('77c396c8-9225-47e8-927d-59505fb073b9', NULL, 'c8f9d96d-f551-4e36-8857-2577067cdf4e', '9fdb7903-e50d-4f81-8cdc-a69d1b97c9ac', 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', NULL, 23, 24, 'page', 2, NULL, NULL, 'impressum', 'Impressum', NULL, 0, 'www.client.de/impressum', 'articles', 'view_page', 'Article', NULL, 2, 1, NULL, NULL, '2021-06-29 00:25:03', '2021-06-29 00:25:03'),
('85bad654-97a7-41b9-adc5-330867b7208d', NULL, '4e69e939-182c-4aef-a5a7-0ff41a25c659', '1c0e98ab-50b4-47e0-bfee-461e6ee51007', 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', NULL, 5, 18, 'page', 2, NULL, NULL, 'multipage', 'Multipage', NULL, 0, 'www.client.de/multipage', 'articles', 'view_page', 'Article', NULL, 2, 1, NULL, NULL, '2021-06-26 22:31:54', '2021-06-27 21:37:46'),
('9929e670-d39a-4673-9a44-d014631b09a0', NULL, 'c8f9d96d-f551-4e36-8857-2577067cdf4e', '2218f099-3715-4542-a87c-79c3a016350f', 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', NULL, 21, 22, 'page', 2, NULL, NULL, 'datenschutz', 'Datenschutz', NULL, 0, 'www.client.de/datenschutz', 'articles', 'view_page', 'Article', NULL, 2, 1, NULL, NULL, '2021-06-29 00:24:48', '2021-06-29 00:24:48'),
('a38f9a74-9f4b-4082-98d6-b6fab3cb88cf', '85bad654-97a7-41b9-adc5-330867b7208d', '85bad654-97a7-41b9-adc5-330867b7208d', '1c0e98ab-50b4-47e0-bfee-461e6ee51007', 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', NULL, 16, 17, 'alias', 3, NULL, NULL, 'multipage', 'Multipage', NULL, 0, 'www.client.de/multipage', NULL, NULL, 'Menu', NULL, 16, 1, NULL, NULL, '2021-06-29 00:38:16', '2021-06-29 00:38:16'),
('a837465c-ce7f-42a3-8af8-ef8db50fc1d4', NULL, '85bad654-97a7-41b9-adc5-330867b7208d', 'fd31e22d-3e99-444b-8d6d-7f3f56a3a727', 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', NULL, 6, 9, 'calendar', 3, NULL, NULL, 'kalender', 'Kalender', NULL, 0, 'www.client.de/multipage/kalender', 'articles', 'view_calendar', 'Article', NULL, 4, 1, NULL, NULL, '2021-06-26 23:57:43', '2021-06-27 21:35:49'),
('ab26125f-e7d1-4b9a-b982-b833628f3c7d', NULL, '4e69e939-11b4-45d8-a767-0ff41a25c659', '52e11a50-5bbe-11eb-9735-8ba84fe9803a', 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', NULL, 26, 27, 'frontpage', 1, NULL, NULL, 'startseite', 'Startseite', NULL, 0, 'www.client.de', 'articles', 'view_frontpage', 'Article', NULL, 2, 1, NULL, NULL, '2021-01-21 08:57:50', '2021-05-06 21:07:18'),
('b194ff66-617f-451d-92c1-e727900714eb', NULL, '4e69e939-11b4-45d8-a767-0ff41a25c659', NULL, NULL, NULL, 2, 3, 'menu', 1, NULL, NULL, 'topmenu', 'Topmenü', NULL, 0, 'www.client.de', NULL, NULL, 'Menus', NULL, 1, 1, NULL, NULL, '2021-01-13 12:15:05', '2021-01-13 12:15:05'),
('c8f9d96d-f551-4e36-8857-2577067cdf4e', NULL, '4e69e939-11b4-45d8-a767-0ff41a25c659', NULL, NULL, NULL, 20, 25, 'menu', 1, NULL, NULL, 'footermenu', 'Footermenü', NULL, 0, 'www.client.de', NULL, NULL, 'Menus', NULL, 1, 1, NULL, NULL, '2021-05-06 17:40:04', '2021-05-06 17:40:04'),
('ed4dd09a-fe8d-4039-9c3c-858bc3ed9c9c', NULL, '85bad654-97a7-41b9-adc5-330867b7208d', '1c0e98ab-50b4-47e0-bfee-461e6ee51007', 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', NULL, 12, 13, 'page', 3, NULL, NULL, 'multipage', 'Multipage', NULL, 0, 'www.client.de/multipage/multipage', 'articles', 'view_page', 'Article', NULL, 8, 1, NULL, NULL, '2021-06-27 21:22:33', '2021-06-27 21:37:46'),
('fb595b11-aaae-4c82-893f-616006461412', NULL, '85bad654-97a7-41b9-adc5-330867b7208d', '6298de48-5606-4fe8-9dba-ce0b7e9f3255', 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', NULL, 14, 15, 'page', 3, NULL, NULL, 'abschnitt-mit-galerie-downloads', 'Abschnitt mit Galerie & Downloads', NULL, 0, 'www.client.de/multipage', 'articles', 'view_page', 'Article', NULL, 1024, 1, NULL, NULL, '2021-06-27 00:39:02', '2021-06-27 00:52:39');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `menus_temporary`
--

DROP TABLE IF EXISTS `menus_temporary`;
CREATE TABLE `menus_temporary` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `locale` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foreign_key` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` mediumtext COLLATE utf8mb4_unicode_ci,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=468 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `menus_translations`
--

DROP TABLE IF EXISTS `menus_translations`;
CREATE TABLE `menus_translations` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `locale` char(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` mediumtext COLLATE utf8mb4_unicode_ci,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extended_data` longtext COLLATE utf8mb4_unicode_ci,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `newslettercampaigns`
--

DROP TABLE IF EXISTS `newslettercampaigns`;
CREATE TABLE `newslettercampaigns` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `newsletter_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `newsletterrecipient_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `send` tinyint(1) DEFAULT '0',
  `read` tinyint(1) DEFAULT '0',
  `clicked` tinyint(1) DEFAULT '0',
  `unsubscribed` tinyint(1) DEFAULT '0',
  `hardbounced` int(11) DEFAULT '0',
  `softbounced` int(11) DEFAULT '0',
  `senddate` datetime DEFAULT NULL,
  `readdate` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=MyISAM AVG_ROW_LENGTH=140 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `newsletterinterests`
--

DROP TABLE IF EXISTS `newsletterinterests`;
CREATE TABLE `newsletterinterests` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `selectable` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=8192 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `newsletterinterests_newsletterrecipients`
--

DROP TABLE IF EXISTS `newsletterinterests_newsletterrecipients`;
CREATE TABLE `newsletterinterests_newsletterrecipients` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `newsletterinterest_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `newsletterrecipient_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=8192 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `newsletterinterests_newsletters`
--

DROP TABLE IF EXISTS `newsletterinterests_newsletters`;
CREATE TABLE `newsletterinterests_newsletters` (
  `id` int(11) NOT NULL,
  `newsletterinterest_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `newsletter_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=4096 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `newsletterrecipients`
--

DROP TABLE IF EXISTS `newsletterrecipients`;
CREATE TABLE `newsletterrecipients` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `salutation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `locale` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approved` tinyint(1) DEFAULT '0',
  `approved_date` datetime DEFAULT NULL,
  `approved_mail` tinyint(1) DEFAULT '0',
  `approved_mail_date` datetime DEFAULT NULL,
  `unsubscribed` tinyint(1) DEFAULT '0',
  `unsubscribed_date` datetime DEFAULT NULL,
  `hardbounced` tinyint(1) DEFAULT '0',
  `bouncestatus` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `source` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=8192 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `newsletters`
--

DROP TABLE IF EXISTS `newsletters`;
CREATE TABLE `newsletters` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aco_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menu_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `locale` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `domain` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `list` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `testreceiveremail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_send` tinyint(1) DEFAULT '0',
  `send` tinyint(1) DEFAULT '0',
  `senddate` datetime DEFAULT NULL,
  `sendcount` int(11) DEFAULT '0',
  `delivered` int(11) DEFAULT '0',
  `readcount` int(11) DEFAULT '0',
  `softbounces` int(11) DEFAULT '0',
  `hardbounces` int(11) DEFAULT '0',
  `unsubscribes` int(11) DEFAULT '0',
  `clickers` int(11) DEFAULT '0',
  `campaigncreated` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=16384 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `orderpositions`
--

DROP TABLE IF EXISTS `orderpositions`;
CREATE TABLE `orderpositions` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `article_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `count` int(11) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `price_vat` double DEFAULT NULL,
  `price_netto` double DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `amount_vat` double DEFAULT NULL,
  `amount_netto` double DEFAULT NULL,
  `taxrate` double DEFAULT NULL,
  `currency` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` mediumtext COLLATE utf8mb4_unicode_ci,
  `extended_data` longtext COLLATE utf8mb4_unicode_ci,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rnr` int(11) NOT NULL,
  `aco_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menu_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shippingoption` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `totalamount` double DEFAULT NULL,
  `paymentmethod` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_log` text COLLATE utf8mb4_unicode_ci,
  `paymentprovider_trans_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_paid` tinyint(1) DEFAULT '0',
  `is_send` tinyint(1) DEFAULT '0',
  `is_canceled` tinyint(1) NOT NULL DEFAULT '0',
  `note` mediumtext COLLATE utf8mb4_unicode_ci,
  `process` tinyint(1) DEFAULT '0',
  `processed` tinyint(1) DEFAULT '0',
  `manual_processed` tinyint(1) DEFAULT '0',
  `shipped` tinyint(1) DEFAULT '0',
  `extended_data` longtext COLLATE utf8mb4_unicode_ci,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `persons`
--

DROP TABLE IF EXISTS `persons`;
CREATE TABLE `persons` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `salutation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `familystatus` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` longtext COLLATE utf8mb4_unicode_ci,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=4096 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `persons`
--

INSERT INTO `persons` (`id`, `foreign_key`, `model`, `type`, `salutation`, `title`, `firstname`, `lastname`, `birthday`, `familystatus`, `note`, `created`, `modified`) VALUES
('511273d3-7908-4c1f-9138-189c1a25c659', '11111111-1111-1111-1111-111111111111', 'Users', 'Persons', '', NULL, 'Visualisten', 'Systemadmin', NULL, NULL, '', '2021-01-01 00:00:00', '2021-06-29 00:29:21'),
('5311b9a7-6494-4e78-b3cd-19e41a25c659', 'cccccccc-cccc-cccc-cccc-cccccccccccc', 'Users', 'Persons', 'herr', NULL, 'Kunden', 'Administrator', NULL, NULL, '', '2021-01-01 00:00:00', '2021-06-29 00:30:33');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `phinxlog`
--

DROP TABLE IF EXISTS `phinxlog`;
CREATE TABLE `phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `breakpoint` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AVG_ROW_LENGTH=910 DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sale` tinyint(1) NOT NULL DEFAULT '0',
  `ean` text COLLATE utf8mb4_unicode_ci,
  `sku` text COLLATE utf8mb4_unicode_ci,
  `producer_sku` text COLLATE utf8mb4_unicode_ci,
  `price` float NOT NULL,
  `purchase_price` float DEFAULT NULL,
  `producer_price` float DEFAULT NULL,
  `taxrate` float NOT NULL,
  `depth` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `height` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `width` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `quantity_alert` int(11) NOT NULL,
  `shipping_rate` float DEFAULT NULL,
  `shipping_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extended_data` text COLLATE utf8mb4_unicode_ci,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `redirects`
--

DROP TABLE IF EXISTS `redirects`;
CREATE TABLE `redirects` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from_url` text COLLATE utf8mb4_unicode_ci,
  `target_url` text COLLATE utf8mb4_unicode_ci,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `generated` tinyint(1) NOT NULL DEFAULT '0',
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  `notify` tinyint(1) NOT NULL DEFAULT '0',
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=5461 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `resources`
--

DROP TABLE IF EXISTS `resources`;
CREATE TABLE `resources` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `configuration_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `modul` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_accesslevel` int(10) DEFAULT NULL,
  `can` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=352 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `resources`
--

INSERT INTO `resources` (`id`, `configuration_id`, `modul`, `name`, `type`, `description`, `controller`, `action`, `url`, `model`, `model_accesslevel`, `can`, `created`, `modified`) VALUES
('0d9b20a0-0f40-4589-b383-ef7f5bd8ed8d', 'c6c1f873-6005-42ca-bbef-01e05e1a1574', 'Article', 'Article | Article > Pagetypes > Link > Default ist sichtbar', 'can', 'Kann Article > Pagetypes > Link > Default im Backend sehen', NULL, NULL, NULL, NULL, NULL, 'resource.modules.article.pagetypes.link.default.show', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('1e3f9fc6-3982-429a-8bb1-c120fd9aaab4', NULL, 'Role', 'Role | Daten: (3) Abteilung-Datensätze und darunter', 'model', 'Role | Daten: (3) Abteilung-Datensätze und darunter - Level: 3', NULL, NULL, '', 'Role', 3, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('312b9280-aed5-443c-abb8-b357535c0ae8', NULL, 'Role', 'Role | Daten: (2) Standort-Datensätze und darunter', 'model', 'Role | Daten: (2) Standort-Datensätze und darunter - Level: 2', NULL, NULL, '', 'Role', 2, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('38229c08-c208-450e-ae27-c1588e9b9247', NULL, 'Element', 'Element | Daten: (2) Standort-Datensätze und darunter', 'model', 'Element | Daten: (2) Standort-Datensätze und darunter - Level: 2', NULL, NULL, '', 'Element', 2, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('46fb171e-e566-472b-8db7-fef78171c584', '97021a67-e2f9-49fd-9000-ab15286ab4a2', 'Pageconfiguration', 'Pageconfiguration | Modul ist sichtbar', 'can', 'Kann Pageconfiguration Modul im Backend sehen', NULL, NULL, '', NULL, NULL, 'resource.modules.pageconfiguration.show', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4b38c1fa-aee8-4abb-b211-0a381a25c659', NULL, 'Element', 'Element | Action: elements/*', 'action', 'Zugriff auf alle Actions des Controllers elements', 'elements', '*', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4b38c6fa-54c0-4fde-a867-0ac81a25c659', NULL, 'Article', 'Article | Action: articles/index', 'action', 'Zugriff auf articles/index', 'articles', 'index', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4b38c6fa-aa14-411d-a712-0ac81a25c659', NULL, 'Article', 'Article | Action: articles/load', 'action', 'Zugriff auf articles/load', 'articles', 'load', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4b38c6fa-aee8-4abb-b111-0ac81a25c659', NULL, 'Article', 'Article | Action: articles/*', 'action', 'Zugriff auf alle Actions des Controllers articles', 'articles', '*', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4b822900-b6c4-4efb-bc93-10f01a25c659', NULL, 'System', 'System | Action: system/*', 'action', 'Zugriff auf alle Actions des Controllers system', 'system', '*', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4b827f4a-e9e4-49c3-8907-03cc1a25c659', NULL, 'Attachment', 'Attachment | Action: attachments/index', 'action', 'Zugriff auf attachments/index', 'attachments', 'index', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4b839391-12dc-468c-b087-06f01a25c659', NULL, 'Attachment', 'Attachment | Action: attachments/delete', 'action', 'Zugriff auf attachments/delete', 'attachments', 'delete', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4b83c291-f534-4233-9500-06f01a25c659', NULL, 'Resource', 'Resource | Action: resources/*', 'action', 'Zugriff auf alle Actions des Controllers resources', 'resources', '*', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4b83c296-c468-47aa-980a-06f01a25c659', NULL, 'Role', 'Role | Action: roles/*', 'action', 'Zugriff auf alle Actions des Controllers roles', 'roles', '*', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4b83c3fd-cea8-4c6e-8146-06f01a25c659', NULL, 'Menu', 'Menu | Action: menus/*', 'action', 'Zugriff auf alle Actions des Controllers menus', 'menus', '*', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4b8a9d93-58e4-45fa-bdff-0fbc1a25c659', NULL, 'User', 'User | Action: users/*', 'action', 'Zugriff auf alle Actions des Controllers users', 'users', '*', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4b96198f-19d8-4714-900c-17dc1a25c659', 'abe893e2-f979-11df-a4c3-002421a2bcd6', 'Article', 'Article | Modul ist sichtbar', 'can', 'Kann Article Modul im Backend sehen', '', '', '', '', NULL, 'resource.modules.article.show', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4b96198f-5254-41a5-ab2b-17dc1a25c659', 'abe6cf26-f979-11df-a4c3-002421a2bcd6', 'Article', 'Article | Article > Pagetypes > Frontpage > Default ist sichtbar', 'can', 'Kann Article > Pagetypes > Frontpage > Default im Backend sehen', '', '', NULL, '', NULL, 'resource.modules.article.pagetypes.frontpage.default.show', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4b96198f-fc10-4197-82e9-17dc1a25c659', '50ae3cfa-f4d0-4d0e-9519-271c1a25c659', 'Article', 'Article | Article > Pagetypes > Page > Default ist sichtbar', 'can', 'Kann Article > Pagetypes > Page > Default im Backend sehen', '', '', NULL, '', NULL, 'resource.modules.article.pagetypes.page.default.show', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4b963db5-69a8-4d3d-89e3-17dc1a25c659', 'abe902b4-f979-11df-a4c3-002421a2bcd6', 'Attachment', 'Attachment | Modul ist sichtbar', 'can', 'Kann Attachment Modul im Backend sehen', '', '', NULL, '', NULL, 'resource.modules.attachment.show', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4b963e23-7314-4351-af8d-17dc1a25c659', 'abe9322a-f979-11df-a4c3-002421a2bcd6', 'Resource', 'Resource | Modul ist sichtbar', 'can', 'Kann Resource Modul im Backend sehen', '', '', NULL, '', NULL, 'resource.modules.resource.show', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4b963e23-951c-44ae-896c-17dc1a25c659', 'abe9357c-f979-11df-a4c3-002421a2bcd6', 'Role', 'Role | Modul ist sichtbar', 'can', 'Kann Role Modul im Backend sehen', '', '', NULL, '', NULL, 'resource.modules.role.show', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4b96406c-dc10-4136-b333-17dcc0a8b222', NULL, 'Attachment', 'Attachment | Action: attachments/*', 'action', 'Zugriff auf alle Actions des Controllers attachments', 'attachments', '*', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4b96534a-aef4-4aa0-8e2f-17dc1a25c659', 'abe93c52-f979-11df-a4c3-002421a2bcd6', 'User', 'User | Modul ist sichtbar', 'can', 'Kann User Modul im Backend sehen', '', '', NULL, '', NULL, 'resource.modules.user.show', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4ba3e3e4-3aa0-4788-ae67-0a321a25c659', '8b0e93c2-0582-4e6d-8e50-31eed44a7b4a', 'Newsletterinterest', 'Newsletterinterest | Modul ist sichtbar', 'can', 'Kann Newsletterinterest Modul im Backend sehen', '', '', NULL, '', NULL, 'resource.modules.newsletterinterest.show', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4ba3e3e4-3aa0-4788-ae67-0a3c1a25c659', 'abe92140-f979-11df-a4c3-002421a2bcd6', 'Newsletter', 'Newsletter | Modul ist sichtbar', 'can', 'Kann Newsletter Modul im Backend sehen', '', '', NULL, '', NULL, 'resource.modules.newsletter.show', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4ba3e42b-3598-4cff-a346-0a3c1a25c659', NULL, 'Newsletter', 'Newsletter | Action: newsletters/*', 'action', 'Zugriff auf alle Actions des Controllers newsletters', 'newsletters', '*', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4c1928d7-5878-446e-8ac4-03bf1a25c659', 'abe91a7e-f979-11df-a4c3-002421a2bcd6', 'Menu', 'Menübaum | Modul ist sichtbar', 'can', 'Kann Menübaum im Backend sehen', '', '', NULL, '', NULL, 'resource.modules.menu.domain.show', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4c1928d7-e238-4bcb-8557-03bf1a25c659', 'abe9170e-f979-11df-a4c3-002421a2bcd6', 'Menu', 'Menübaum | Menübaum > Interner Link ist sichtbar', 'can', 'Kann Menübaum > Interner Link im Backend sehen', '', '', NULL, '', NULL, 'resource.modules.menu.alias.show', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4c1928d7-e318-4ff7-be8f-03bf1a25c659', 'abe91dda-f979-11df-a4c3-002421a2bcd6', 'Menu', 'Menübaum | Menübaum > Neues Menü ist sichtbar', 'can', 'Kann Menübaum > Neues Menü im Backend sehen', '', '', NULL, '', NULL, 'resource.modules.menu.menu.show', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4c1928d8-1424-4616-ab6a-03bf1a25c659', 'abe92820-f979-11df-a4c3-002421a2bcd6', 'Newsletterrecipient', 'Newsletterrecipient | Modul ist sichtbar', 'can', 'Kann Newsletterrecipient Modul im Backend sehen', '', '', NULL, '', NULL, 'resource.modules.newsletterrecipient.show', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4c2089e8-1fa0-4fb7-8435-011e1a25c659', NULL, 'Newsletterrecipient', 'Newsletterrecipient | Action: newsletterrecipients/*', 'action', 'Zugriff auf alle Actions des Controllers newsletterrecipients', 'newsletterrecipients', '*', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4c5023cf-d038-44b7-b129-024b1a25c659', 'abe9104c-f979-11df-a4c3-002421a2bcd6', 'Crontask', 'Crontask | Modul ist sichtbar', 'can', 'Kann Crontask Modul im Backend sehen', '', '', NULL, '', NULL, 'resource.modules.crontask.show', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4c502b9f-6cdc-4786-8ebc-00be1a25c659', NULL, 'Crontask', 'Crontask | Action: crontasks/*', 'action', 'Zugriff auf alle Actions des Controllers crontasks', 'crontasks', '*', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4cce8606-9674-4d81-b9c0-0a781a25c659', NULL, 'Article', 'Article | Action: articles/save', 'action', 'Zugriff auf articles/save', 'articles', 'save', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4cd803e8-94f0-4825-9f07-11201a25c659', NULL, 'Menu', 'Menu | Action: menus/load', 'action', 'Zugriff auf menus/load', 'menus', 'load', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4cd8041e-8a34-4cb6-8c31-11201a25c659', NULL, 'Menu', 'Menu | Action: menus/delete', 'action', 'Zugriff auf menus/delete', 'menus', 'delete', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4ceba586-014c-4df7-a7c7-0e7c1a25c659', 'abe90cf0-f979-11df-a4c3-002421a2bcd6', 'Configuration', 'Configuration | Modul ist sichtbar', 'can', 'Kann Configuration Modul im Backend sehen', '', '', NULL, '', NULL, 'resource.modules.configuration.show', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4cebadec-6174-49f1-8660-19071a25c659', NULL, 'Configuration', 'Configuration | Action: configurations/*', 'action', 'Zugriff auf alle Actions des Controllers configurations', 'configurations', '*', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4cebae1d-f5b8-48ee-8bc7-16b91a25c659', NULL, 'Text', 'Text | Action: texts/*', 'action', 'Zugriff auf alle Actions des Controllers texts', 'texts', '*', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4d7e67ae-7bd8-4e85-a662-03f11a25c659', NULL, 'Formular', 'Formular | Action: formulars/*', 'action', 'Zugriff auf alle Actions des Controllers formulars', 'formulars', '*', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('4db83fcd-8cac-42a9-866c-03321a25c659', '4d7e62e9-08cc-4384-aef6-03f41a25c659', 'Formular', 'Formular | Modul ist sichtbar', 'can', 'Kann Formular Modul im Backend sehen', '', '', NULL, '', NULL, 'resource.modules.formular.show', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('51482173-6e2c-4841-a1ca-0bac1a25c659', NULL, 'Article', 'Article | Action: articles/delete', 'action', 'Zugriff auf articles/delete', 'articles', 'delete', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('514a9a5d-7c50-4917-874b-0bac1a25c659', NULL, 'Email', 'Email | Action: emails/*', 'action', 'Zugriff auf alle Actions des Controllers emails', 'emails', '*', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('51a600d4-6574-418d-91fe-17fc1a25c659', '51a5ee25-ee0c-4ee3-8b60-17fc1a25c659', 'Email', 'Email | Modul ist sichtbar', 'can', 'Kann Email Modul im Backend sehen', '', '', NULL, '', NULL, 'resource.modules.email.show', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('51a65617-dad8-45b3-92db-17fc1a25c659', NULL, 'Newsletterinterest', 'Newsletterinterest | Action: newsletterinterests/*', 'action', 'Zugriff auf alle Actions des Controllers newsletterinterests', 'newsletterinterests', '*', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('51aee413-2e8c-477c-95ac-17fc1a25c659', '51aee413-c4e8-40b3-8fd9-17fc1a25c659', 'Actionview', 'Actionview | Modul ist sichtbar', 'can', 'Kann Actionview Modul im Backend sehen', '', '', '', '', NULL, 'resource.modules.actionview.show', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('51b0ed87-6468-4a81-be84-17fc1a25c659', '51b0ed87-3b0c-4cd1-bc98-17fc1a25c659', 'Formularconfig', 'Formularconfig | Modul ist sichtbar', 'can', 'Kann Formularconfig Modul im Backend sehen', '', '', NULL, '', NULL, 'resource.modules.formularconfig.show', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('51b0ee03-2ec8-41ab-89a0-17fc1a25c659', NULL, 'Formularconfig', 'Formularconfig | Action: formularconfigs/*', 'action', 'Zugriff auf alle Actions des Controllers formularconfigs', 'formularconfigs', '*', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('5219d72b-5a70-4859-801b-19541a25c659', NULL, 'Domain', 'Domain | Action: domains/*', 'action', 'Zugriff auf alle Actions des Controllers domains', 'domains', '*', '', '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('5219d780-0d74-4d29-94c7-19541a25c659', '5219d780-dc74-4cc4-b88e-19541a25c659', 'Domain', 'Liste | Modul ist sichtbar', 'can', 'Kann Liste Modul im Backend sehen', '', '', NULL, '', NULL, 'resource.modules.domain.show', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('52383449-47ec-4f80-ab18-1d601a25c659', NULL, 'Category', 'Category | Action: categories/*', 'action', 'Zugriff auf alle Actions des Controllers categories', 'categories', '*', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('525c17ed-ec44-4565-bb4c-0df81a25c659', NULL, 'Redirect', 'Redirect | Action: redirects/*', 'action', 'Zugriff auf alle Actions des Controllers redirects', 'redirects', '*', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('525c1854-d25c-4df1-9fdc-0df81a25c659', '54c71ec3-d29c-430b-8f66-1f141a25c659', 'Redirect', 'Redirect | Modul ist sichtbar', 'can', 'Kann Redirect Modul im Backend sehen', '', '', NULL, '', NULL, 'resource.modules.redirect.show', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('5281278d-7b58-4c81-adec-13f41a25c659', '5281278d-a49c-46a4-9fbf-13f41a25c659', 'Order', 'Order | Modul ist sichtbar', 'can', 'Kann Order Modul im Backend sehen', '', '', NULL, '', NULL, 'resource.modules.order.show', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('52812833-8fbc-434c-93c6-13f41a25c659', NULL, 'Order', 'Order | Action: orders/*', 'action', 'Zugriff auf alle Actions des Controllers orders', 'orders', '*', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('5302e337-8138-4d7d-ad78-181c1a25c659', NULL, 'Order', 'Order | Action: orders/delete', 'action', 'Zugriff auf orders/delete', 'orders', 'delete', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('5302e36f-9dac-44c1-8ef9-181c1a25c659', NULL, 'Order', 'Order | Action: orders/load', 'action', 'Zugriff auf orders/load', 'orders', 'load', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('5302e381-82cc-4e4c-b2ce-181c1a25c659', NULL, 'Order', 'Order | Action: orders/save', 'action', 'Zugriff auf orders/save', 'orders', 'save', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('5302e3ac-8910-4437-b45d-181c1a25c659', NULL, 'Order', 'Order | Action: orders/index', 'action', 'Zugriff auf orders/index', 'orders', 'index', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('5311ca4b-019c-484e-87c0-19e41a25c659', NULL, 'Attachment', 'Attachment | Action: attachments/save', 'action', 'Zugriff auf attachments/save', 'attachments', 'save', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('5311ca4b-8e34-4dcb-b828-19e41a25c659', NULL, 'Actionview', 'Actionview | Action: actionviews/*', 'action', 'Zugriff auf alle Actions des Controllers actionviews', 'actionviews', '*', '', '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('5311ca4b-94c0-4db1-9cbe-19e41a25c659', NULL, 'Article', 'Article | Action: articles/copy', 'action', 'Zugriff auf articles/copy', 'articles', 'copy', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('5311ca4b-caa4-4029-adc9-19e41a25c659', NULL, 'Newsletterrecipient', 'Newsletterrecipient | Action: newsletterrecipients/index', 'action', 'Zugriff auf newsletterrecipients/index', 'newsletterrecipients', 'index', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('5311ca4b-d43c-4109-9eec-19e41a25c659', NULL, 'Newsletterrecipient', 'Newsletterrecipient | Action: newsletterrecipients/delete', 'action', 'Zugriff auf newsletterrecipients/delete', 'newsletterrecipients', 'delete', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('5311ca4c-0fd8-4c3c-a8ed-19e41a25c659', NULL, 'Text', 'Text | Action: texts/save', 'action', 'Zugriff auf texts/save', 'texts', 'save', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('5311ca4c-2348-4ff1-bff5-19e41a25c659', NULL, 'Menu', 'Menu | Action: menus/index', 'action', 'Zugriff auf menus/index', 'menus', 'index', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('5311ca4c-2d7c-4a28-927e-19e41a25c659', NULL, 'User', 'User | Action: users/delete', 'action', 'Zugriff auf users/delete', 'users', 'delete', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('5311ca4c-3f60-4a24-8faf-19e41a25c659', NULL, 'User', 'User | Action: users/load', 'action', 'Zugriff auf users/load', 'users', 'load', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('5311ca4c-4094-4436-a52f-19e41a25c659', NULL, 'User', 'User | Action: users/index', 'action', 'Zugriff auf users/index', 'users', 'index', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('5311ca4c-49e4-4be9-b39c-19e41a25c659', NULL, 'Newsletterinterest', 'Newsletterinterest | Action: newsletterinterests/delete', 'action', 'Zugriff auf newsletterinterests/delete', 'newsletterinterests', 'delete', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('5311ca4c-5354-4928-a892-19e41a25c659', NULL, 'Text', 'Text | Action: texts/index', 'action', 'Zugriff auf texts/index', 'texts', 'index', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('5311ca4c-634c-4e14-b340-19e41a25c659', NULL, 'Menu', 'Menu | Action: menus/save', 'action', 'Zugriff auf menus/save', 'menus', 'save', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('5311ca4c-895c-46c2-8f75-19e41a25c659', NULL, 'User', 'User | Action: users/save', 'action', 'Zugriff auf users/save', 'users', 'save', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('5311ca4c-dbbc-40a0-82ba-19e41a25c659', NULL, 'Newsletterinterest', 'Newsletterinterest | Action: newsletterinterests/index', 'action', 'Zugriff auf newsletterinterests/index', 'newsletterinterests', 'index', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('53424454-6ca0-40e0-b2b9-0b101a25c659', NULL, 'Order', 'Order | Action: orders/download', 'action', 'Zugriff auf orders/download', 'orders', 'download', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('53424468-6c10-42de-b5f1-0b101a25c659', NULL, 'Voucher', 'Voucher | Action: vouchers/download', 'action', 'Zugriff auf vouchers/download', 'vouchers', 'download', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('5405b6e9-a848-481e-b242-26241a25c659', NULL, 'Voucher', 'Voucher | Action: vouchers/load', 'action', 'Zugriff auf vouchers/load', 'vouchers', 'load', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('5405b6e9-fb68-4789-92b8-26241a25c659', NULL, 'Voucher', 'Voucher | Action: vouchers/save', 'action', 'Zugriff auf vouchers/save', 'vouchers', 'save', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('5405b6ea-2fd8-404e-988f-26241a25c659', NULL, 'Voucher', 'Voucher | Action: vouchers/index', 'action', 'Zugriff auf vouchers/index', 'vouchers', 'index', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('5405b6ea-4b34-4a9f-9c47-26241a25c659', NULL, 'Voucher', 'Voucher | Action: vouchers/*', 'action', 'Zugriff auf alle Actions des Controllers vouchers', 'vouchers', '*', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('5405b6ea-e7f4-4e4f-82f7-26241a25c659', NULL, 'Voucher', 'Voucher | Action: vouchers/delete', 'action', 'Zugriff auf vouchers/delete', 'vouchers', 'delete', NULL, '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54568059-67fb-4eb6-a029-95f7a4f34b0a', NULL, 'Pageconfiguration', 'Pageconfiguration | Action: pageconfigurations/*', 'action', 'Zugriff auf alle Actions des Controllers pageconfigurations', 'pageconfigurations', '*', '', NULL, NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('546e1148-aa50-4986-aeeb-18f81a25c659', '545c8af2-5620-4293-a44a-6a28ac10cd2f', 'Voucher', 'Voucher | Modul ist sichtbar', 'can', 'Kann Voucher Modul im Backend sehen', '', '', NULL, '', NULL, 'resource.modules.voucher.show', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54bca590-75e8-49ee-8759-1f041a25c659', NULL, 'Aco', 'Aco | Action: acos/*', 'action', 'Zugriff auf alle Actions des Controllers acos', 'acos', '*', '', '', NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54bca668-9818-42cd-8687-1f041a25c659', '54c71e56-5374-4e10-96d1-1f141a25c659', 'Aco', 'Aco | Modul ist sichtbar', 'can', 'Kann Aco Modul im Backend sehen', '', '', '', '', NULL, 'resource.modules.aco.show', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54c65c45-e05c-4dfc-a2be-1f141a25c659', NULL, 'Aco', 'Aco | Daten: (0) alle Datensätze', 'model', 'Aco | Daten: (0) alle Datensätze - Level: 0', '', '', '', 'Aco', 0, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54c65c52-b870-4492-a0c9-1f141a25c659', NULL, 'Aco', 'Aco | Daten: (1) Kunde-Datensätze und darunter', 'model', 'Aco | Daten: (1) Kunde-Datensätze und darunter - Level: 1', '', '', '', 'Aco', 1, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54c65c67-74c0-4e61-8933-1f141a25c659', NULL, 'Aco', 'Aco | Daten: (2) Standort-Datensätze und darunter', 'model', 'Aco | Daten: (2) Standort-Datensätze und darunter - Level: 2', '', '', '', 'Aco', 2, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54c65c6f-3598-4393-a159-1f141a25c659', NULL, 'Aco', 'Aco | Daten: (3) Abteilung-Datensätze und darunter', 'model', 'Aco | Daten: (3) Abteilung-Datensätze und darunter - Level: 3', '', '', '', 'Aco', 3, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd559e-1e70-476b-bc64-322c1a25c659', NULL, 'Element', 'Element | Daten: (0) alle Datensätze', 'model', 'Element | Daten: (0) alle Datensätze - Level: 0', '', '', NULL, 'Element', 0, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd559e-1e70-476b-bc64-3bbc1a25c659', NULL, 'Article', 'Article | Daten: (0) alle Datensätze', 'model', 'Article | Daten: (0) alle Datensätze - Level: 0', '', '', NULL, 'Article', 0, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd559f-1248-45fe-97e5-3bbc1a25c659', NULL, 'Email', 'Email | Daten: (0) alle Datensätze', 'model', 'Email | Daten: (0) alle Datensätze - Level: 0', '', '', NULL, 'Email', 0, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd559f-4118-41a0-8f73-3bbc1a25c659', NULL, 'Article', 'Article | Daten: (1) Kunde-Datensätze und darunter', 'model', 'Article | Daten: (1) Kunde-Datensätze und darunter - Level: 1', '', '', NULL, 'Article', 1, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd559f-50c8-4787-9f31-3bbc1a25c659', NULL, 'Attachment', 'Attachment | Daten: (1) Kunde-Datensätze und darunter', 'model', 'Attachment | Daten: (1) Kunde-Datensätze und darunter - Level: 1', '', '', NULL, 'Attachment', 1, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd559f-64c0-4297-aae6-3bbc1a25c659', NULL, 'Email', 'Email | Daten: (1) Kunde-Datensätze und darunter', 'model', 'Email | Daten: (1) Kunde-Datensätze und darunter - Level: 1', '', '', NULL, 'Email', 1, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd559f-6a00-49b4-a2c1-3bbc1a25c659', NULL, 'Attachment', 'Attachment | Daten: (3) Abteilung-Datensätze und darunter', 'model', 'Attachment | Daten: (3) Abteilung-Datensätze und darunter - Level: 3', '', '', NULL, 'Attachment', 3, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd559f-7390-4bcd-b293-3bbc1a25c659', NULL, 'Email', 'Email | Daten: (3) Abteilung-Datensätze und darunter', 'model', 'Email | Daten: (3) Abteilung-Datensätze und darunter - Level: 3', '', '', NULL, 'Email', 3, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd559f-76b4-477b-9da0-3bbc1a25c659', NULL, 'Email', 'Email | Daten: (2) Standort-Datensätze und darunter', 'model', 'Email | Daten: (2) Standort-Datensätze und darunter - Level: 2', '', '', NULL, 'Email', 2, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd559f-8298-4cab-b9bf-3bbc1a25c659', NULL, 'Article', 'Article | Daten: (2) Standort-Datensätze und darunter', 'model', 'Article | Daten: (2) Standort-Datensätze und darunter - Level: 2', '', '', NULL, 'Article', 2, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd559f-8494-4a19-86e4-3bbc1a25c659', NULL, 'Attachment', 'Attachment | Daten: (2) Standort-Datensätze und darunter', 'model', 'Attachment | Daten: (2) Standort-Datensätze und darunter - Level: 2', '', '', NULL, 'Attachment', 2, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd559f-b030-4128-a207-3bbc1a25c659', NULL, 'Attachment', 'Attachment | Daten: (0) alle Datensätze', 'model', 'Attachment | Daten: (0) alle Datensätze - Level: 0', '', '', NULL, 'Attachment', 0, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd559f-bc00-419c-bb9c-3bbc1a25c659', NULL, 'Article', 'Article | Daten: (3) Abteilung-Datensätze und darunter', 'model', 'Article | Daten: (3) Abteilung-Datensätze und darunter - Level: 3', '', '', NULL, 'Article', 3, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd55a0-14fc-4da1-be5f-3bbc1a25c659', NULL, 'Formular', 'Formular | Daten: (1) Kunde-Datensätze und darunter', 'model', 'Formular | Daten: (1) Kunde-Datensätze und darunter - Level: 1', '', '', NULL, 'Formular', 1, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd55a0-22a0-49e0-8ea3-3bbc1a25c659', NULL, 'Menu', 'Menu | Daten: (2) Standort-Datensätze und darunter', 'model', 'Menu | Daten: (2) Standort-Datensätze und darunter - Level: 2', '', '', NULL, 'Menu', 2, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd55a0-2714-42cf-9a81-3bbc1a25c659', NULL, 'Formularconfig', 'Formularconfig | Daten: (3) Abteilung-Datensätze und darunter', 'model', 'Formularconfig | Daten: (3) Abteilung-Datensätze und darunter - Level: 3', '', '', NULL, 'Formularconfig', 3, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd55a0-2778-4368-8a2b-3bbc1a25c659', NULL, 'Formularconfig', 'Formularconfig | Daten: (0) alle Datensätze', 'model', 'Formularconfig | Daten: (0) alle Datensätze - Level: 0', '', '', NULL, 'Formularconfig', 0, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd55a0-3450-4b7d-a9fe-3bbc1a25c659', NULL, 'Newsletter', 'Newsletter | Daten: (1) Kunde-Datensätze und darunter', 'model', 'Newsletter | Daten: (1) Kunde-Datensätze und darunter - Level: 1', '', '', NULL, 'Newsletter', 1, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd55a0-35a0-4c66-a960-3bbc1a25c659', NULL, 'Menu', 'Menu | Daten: (3) Abteilung-Datensätze und darunter', 'model', 'Menu | Daten: (3) Abteilung-Datensätze und darunter - Level: 3', '', '', NULL, 'Menu', 3, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd55a0-4f18-4967-9134-3bbc1a25c659', NULL, 'Formularconfig', 'Formularconfig | Daten: (2) Standort-Datensätze und darunter', 'model', 'Formularconfig | Daten: (2) Standort-Datensätze und darunter - Level: 2', '', '', NULL, 'Formularconfig', 2, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd55a0-5014-4611-99d1-3bbc1a25c659', NULL, 'Formular', 'Formular | Daten: (2) Standort-Datensätze und darunter', 'model', 'Formular | Daten: (2) Standort-Datensätze und darunter - Level: 2', '', '', NULL, 'Formular', 2, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd55a0-6734-450b-9a2a-3bbc1a25c659', NULL, 'Formularconfig', 'Formularconfig | Daten: (1) Kunde-Datensätze und darunter', 'model', 'Formularconfig | Daten: (1) Kunde-Datensätze und darunter - Level: 1', '', '', NULL, 'Formularconfig', 1, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd55a0-b8a8-4d51-b8ce-3bbc1a25c659', NULL, 'Formular', 'Formular | Daten: (0) alle Datensätze', 'model', 'Formular | Daten: (0) alle Datensätze - Level: 0', '', '', NULL, 'Formular', 0, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd55a0-c81c-4f4e-96fd-3bbc1a25c659', NULL, 'Formular', 'Formular | Daten: (3) Abteilung-Datensätze und darunter', 'model', 'Formular | Daten: (3) Abteilung-Datensätze und darunter - Level: 3', '', '', NULL, 'Formular', 3, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd55a0-ec00-4903-becf-3bbc1a25c659', NULL, 'Newsletter', 'Newsletter | Daten: (0) alle Datensätze', 'model', 'Newsletter | Daten: (0) alle Datensätze - Level: 0', '', '', NULL, 'Newsletter', 0, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd55a0-f2b8-4ec7-a957-3bbc1a25c659', NULL, 'Menu', 'Menu | Daten: (1) Kunde-Datensätze und darunter', 'model', 'Menu | Daten: (1) Kunde-Datensätze und darunter - Level: 1', '', '', NULL, 'Menu', 1, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd55a0-fccc-45cd-a716-3bbc1a25c659', NULL, 'Menu', 'Menu | Daten: (0) alle Datensätze', 'model', 'Menu | Daten: (0) alle Datensätze - Level: 0', '', '', NULL, 'Menu', 0, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd55a1-10d4-42c6-8c42-3bbc1a25c659', NULL, 'Newsletter', 'Newsletter | Daten: (2) Standort-Datensätze und darunter', 'model', 'Newsletter | Daten: (2) Standort-Datensätze und darunter - Level: 2', '', '', NULL, 'Newsletter', 2, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd55a1-1c64-417c-a69a-3bbc1a25c659', NULL, 'Order', 'Order | Daten: (0) alle Datensätze', 'model', 'Order | Daten: (0) alle Datensätze - Level: 0', '', '', NULL, 'Order', 0, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd55a1-459c-410c-8c48-3bbc1a25c659', NULL, 'Order', 'Order | Daten: (2) Standort-Datensätze und darunter', 'model', 'Order | Daten: (2) Standort-Datensätze und darunter - Level: 2', '', '', NULL, 'Order', 2, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd55a1-4c50-4a21-9d37-3bbc1a25c659', NULL, 'User', 'User | Daten: (3) Abteilung-Datensätze und darunter', 'model', 'User | Daten: (3) Abteilung-Datensätze und darunter - Level: 3', '', '', NULL, 'User', 3, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd55a1-6f0c-4f2a-8914-3bbc1a25c659', NULL, 'User', 'User | Daten: (2) Standort-Datensätze und darunter', 'model', 'User | Daten: (2) Standort-Datensätze und darunter - Level: 2', '', '', NULL, 'User', 2, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd55a1-8658-4e12-a81e-3bbc1a25c659', NULL, 'Order', 'Order | Daten: (1) Kunde-Datensätze und darunter', 'model', 'Order | Daten: (1) Kunde-Datensätze und darunter - Level: 1', '', '', NULL, 'Order', 1, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd55a1-b65c-4757-8b3d-3bbc1a25c659', NULL, 'User', 'User | Daten: (0) alle Datensätze', 'model', 'User | Daten: (0) alle Datensätze - Level: 0', '', '', NULL, 'User', 0, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd55a1-cbac-4f15-8aca-3bbc1a25c659', NULL, 'User', 'User | Daten: (1) Kunde-Datensätze und darunter', 'model', 'User | Daten: (1) Kunde-Datensätze und darunter - Level: 1', '', '', NULL, 'User', 1, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd55a1-ce94-4972-a64e-3bbc1a25c659', NULL, 'Order', 'Order | Daten: (3) Abteilung-Datensätze und darunter', 'model', 'Order | Daten: (3) Abteilung-Datensätze und darunter - Level: 3', '', '', NULL, 'Order', 3, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('54fd55a1-dd40-41f4-8457-3bbc1a25c659', NULL, 'Newsletter', 'Newsletter | Daten: (3) Abteilung-Datensätze und darunter', 'model', 'Newsletter | Daten: (3) Abteilung-Datensätze und darunter - Level: 3', '', '', NULL, 'Newsletter', 3, NULL, '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('6953fa91-391c-48a4-b307-197be9e75257', NULL, 'Newsletterinterest', 'Newsletterinterest | Action: newsletterinterests/save', 'action', 'Zugriff auf newsletterinterests/save', 'newsletterinterests', 'save', '', NULL, NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('6a13a583-7698-417a-af9b-b3a9e25173c2', NULL, 'Newsletterinterest', 'Newsletterinterest | Action: newsletterinterests/load', 'action', 'Zugriff auf newsletterinterests/load', 'newsletterinterests', 'load', '', NULL, NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('80588f00-d4e7-45ea-933b-a983c23a7e0f', NULL, 'Role', 'Role | Daten: (0) alle Datensätze', 'model', 'Role | Daten: (0) alle Datensätze - Level: 0', '', '', '', 'Role', 0, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('85796298-e870-4a0c-ad21-7b3e4aaa4fce', NULL, 'Role', 'Role | Daten: (1) Kunde-Datensätze und darunter', 'model', 'Role | Daten: (1) Kunde-Datensätze und darunter - Level: 1', NULL, NULL, '', 'Role', 1, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('9ed4e207-7500-4ff7-bb01-53a9b9dad6e3', NULL, 'Newsletterrecipient', 'Newsletterrecipient | Action: newsletterrecipients/load', 'action', 'Zugriff auf newsletterrecipients/load', 'newsletterrecipients', 'load', '', NULL, NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('a6d27835-5952-4074-89de-7f0bff0c1c97', 'be89bd79-99b0-40e6-95d8-3bda9c39691b', 'Article', 'Article | Article > Pagetypes > Direction > Default ist sichtbar', 'can', 'Kann Article > Pagetypes > Direction > Default im Backend sehen', NULL, NULL, NULL, NULL, NULL, 'resource.modules.article.pagetypes.direction.default.show', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('b8a36049-7c60-4ec8-b9a2-1ffc1125bf6e', NULL, 'Element', 'Element | Daten: (3) Abteilung-Datensätze und darunter', 'model', 'Element | Daten: (3) Abteilung-Datensätze und darunter - Level: 3', NULL, NULL, '', 'Element', 3, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('bcc4eee6-c352-4fc9-885f-89705aceb8a5', '6d75865e-e328-497f-bbe4-89094bd7e497', 'Article', 'Article | Article > Pagetypes > Faq > Default ist sichtbar', 'can', 'Kann Article > Pagetypes > Faq > Default im Backend sehen', NULL, NULL, NULL, NULL, NULL, 'resource.modules.article.pagetypes.faq.default.show', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('d4491990-a63a-4dc0-ba69-1d9c75a5fbe7', NULL, 'Element', 'Element | Daten: (1) Kunde-Datensätze und darunter', 'model', 'Element | Daten: (1) Kunde-Datensätze und darunter - Level: 1', NULL, NULL, '', 'Element', 1, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('e666d300-f390-11ea-a4d7-cbd1ba234cb9', NULL, 'Admintoolbar', 'Admintoolbar ist sichtbar', 'can', 'Kann die Admintoolbar im Frontend sehen', NULL, NULL, '', NULL, NULL, 'resource.modules.admintoolbar.show', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('e87db084-173e-4341-a6bd-0100b969df16', NULL, NULL, 'Url | Url: /anfahrt', 'url', 'Zugriff auf /anfahrt und darunter', NULL, NULL, '/anfahrt', NULL, NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('ef75455d-db95-4679-b798-cb0297a729d9', 'd2d5b44a-ba4c-4ac8-8352-9ee4a8ade369', 'Article', 'Article | Article > Pagetypes > Event > Default ist sichtbar', 'can', 'Kann Article > Pagetypes > Event > Default im Backend sehen', NULL, NULL, NULL, NULL, NULL, 'resource.modules.article.pagetypes.event.default.show', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('fd08c81d-711b-4210-890c-bd35c8444aae', NULL, 'Newsletterrecipient', 'Newsletterrecipient | Action: newsletterrecipients/save', 'action', 'Zugriff auf newsletterrecipients/save', 'newsletterrecipients', 'save', '', NULL, NULL, '', '2021-06-27 00:00:00', '2021-06-27 00:00:00'),
('fd340aff-af94-49e4-897c-1360f94220fe', '5df3cd58-f7d9-4449-8074-a5ada432091f', 'Article', 'Article | Article > Pagetypes > Calendar > Default ist sichtbar', 'can', 'Kann Article > Pagetypes > Calendar > Default im Backend sehen', NULL, NULL, NULL, NULL, NULL, 'resource.modules.article.pagetypes.calendar.default.show', '2021-06-27 00:00:00', '2021-06-27 00:00:00');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `resources_roles`
--

DROP TABLE IF EXISTS `resources_roles`;
CREATE TABLE `resources_roles` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `resource_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=279 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `resources_roles`
--

INSERT INTO `resources_roles` (`id`, `role_id`, `resource_id`) VALUES
('320f22a6-41f1-4266-a13a-865a1ef40331', '22222222-2222-2222-2222-222222222222', '0d9b20a0-0f40-4589-b383-ef7f5bd8ed8d'),
('3445ae78-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '46fb171e-e566-472b-8db7-fef78171c584'),
('34461696-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '4b38c1fa-aee8-4abb-b211-0a381a25c659'),
('3445121d-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '4b38c6fa-aee8-4abb-b111-0ac81a25c659'),
('344533dd-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '4b822900-b6c4-4efb-bc93-10f01a25c659'),
('34452fb6-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '4b83c291-f534-4233-9500-06f01a25c659'),
('344531af-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '4b83c296-c468-47aa-980a-06f01a25c659'),
('34452344-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '4b83c3fd-cea8-4c6e-8146-06f01a25c659'),
('34453824-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '4b8a9d93-58e4-45fa-bdff-0fbc1a25c659'),
('34454e45-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '4b96198f-19d8-4714-900c-17dc1a25c659'),
('344541a3-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '4b96198f-5254-41a5-ab2b-17dc1a25c659'),
('34454a01-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '4b96198f-fc10-4197-82e9-17dc1a25c659'),
('3445508c-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '4b963db5-69a8-4d3d-89e3-17dc1a25c659'),
('34456b21-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '4b963e23-7314-4351-af8d-17dc1a25c659'),
('34456d13-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '4b963e23-951c-44ae-896c-17dc1a25c659'),
('3445144a-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '4b96406c-dc10-4136-b333-17dcc0a8b222'),
('34457111-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '4b96534a-aef4-4aa0-8e2f-17dc1a25c659'),
('3445e343-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '4ba3e3e4-3aa0-4788-ae67-0a321a25c659'),
('3445e110-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '4ba3e3e4-3aa0-4788-ae67-0a3c1a25c659'),
('344529f3-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '4ba3e42b-3598-4cff-a346-0a3c1a25c659'),
('34455ffb-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '4c1928d7-5878-446e-8ac4-03bf1a25c659'),
('34455e00-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '4c1928d7-e238-4bcb-8557-03bf1a25c659'),
('344561e2-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '4c1928d7-e318-4ff7-be8f-03bf1a25c659'),
('344566fe-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '4c1928d8-1424-4616-ab6a-03bf1a25c659'),
('34452809-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '4c2089e8-1fa0-4fb7-8435-011e1a25c659'),
('344554a2-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '4c5023cf-d038-44b7-b129-024b1a25c659'),
('344519e5-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '4c502b9f-6cdc-4786-8ebc-00be1a25c659'),
('344552a6-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '4ceba586-014c-4df7-a7c7-0e7c1a25c659'),
('34451803-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '4cebadec-6174-49f1-8660-19071a25c659'),
('34453624-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '4cebae1d-f5b8-48ee-8bc7-16b91a25c659'),
('34452155-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '4d7e67ae-7bd8-4e85-a662-03f11a25c659'),
('34455a5e-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '4db83fcd-8cac-42a9-866c-03321a25c659'),
('34451db6-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '514a9a5d-7c50-4917-874b-0bac1a25c659'),
('34455890-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '51a600d4-6574-418d-91fe-17fc1a25c659'),
('34452566-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '51a65617-dad8-45b3-92db-17fc1a25c659'),
('34453c34-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '51aee413-2e8c-477c-95ac-17fc1a25c659'),
('34455c2d-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '51b0ed87-6468-4a81-be84-17fc1a25c659'),
('34451f85-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '51b0ee03-2ec8-41ab-89a0-17fc1a25c659'),
('34451bd5-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '5219d72b-5a70-4859-801b-19541a25c659'),
('344556ac-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '5219d780-0d74-4d29-94c7-19541a25c659'),
('3445164a-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '52383449-47ec-4f80-ab18-1d601a25c659'),
('34452de5-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '525c17ed-ec44-4565-bb4c-0df81a25c659'),
('34456931-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '525c1854-d25c-4df1-9fdc-0df81a25c659'),
('34452c09-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '52812833-8fbc-434c-93c6-13f41a25c659'),
('34451059-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '5311ca4b-8e34-4dcb-b828-19e41a25c659'),
('34454c41-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '5322f1e2-8848-4f2e-a35c-1aa01a25c659'),
('34453a70-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '53424468-6c10-42de-b5f1-0b101a25c659'),
('34457305-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '5405b6ea-4b34-4a9f-9c47-26241a25c659'),
('3445b085-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '54568059-67fb-4eb6-a029-95f7a4f34b0a'),
('34458d4b-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '54bca590-75e8-49ee-8759-1f041a25c659'),
('34458812-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '54bca668-9818-42cd-8687-1f041a25c659'),
('34458b70-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '54c65c45-e05c-4dfc-a2be-1f141a25c659'),
('34461290-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '54fd559e-1e70-476b-bc64-322c1a25c659'),
('344576d3-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '54fd559e-1e70-476b-bc64-3bbc1a25c659'),
('344578c4-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '54fd559f-1248-45fe-97e5-3bbc1a25c659'),
('34457aaa-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '54fd559f-b030-4128-a207-3bbc1a25c659'),
('34457c76-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '54fd55a0-2778-4368-8a2b-3bbc1a25c659'),
('34457e5b-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '54fd55a0-b8a8-4d51-b8ce-3bbc1a25c659'),
('3445805f-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '54fd55a0-ec00-4903-becf-3bbc1a25c659'),
('34458268-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '54fd55a0-fccc-45cd-a716-3bbc1a25c659'),
('3445844b-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '54fd55a1-1c64-417c-a69a-3bbc1a25c659'),
('3445864a-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '54fd55a1-b65c-4757-8b3d-3bbc1a25c659'),
('34459fe7-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', '80588f00-d4e7-45ea-933b-a983c23a7e0f'),
('34461ce9-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', 'a6d27835-5952-4074-89de-7f0bff0c1c97'),
('344474ac-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', 'bcc4eee6-c352-4fc9-885f-89705aceb8a5'),
('344601d1-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', 'e666d300-f390-11ea-a4d7-cbd1ba234cb9'),
('344620ec-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', 'ef75455d-db95-4679-b798-cb0297a729d9'),
('34460c2e-d6ca-11eb-94ee-00e04cb86ff4', '22222222-2222-2222-2222-222222222222', 'fd340aff-af94-49e4-897c-1360f94220fe'),
('3446042f-d6ca-11eb-94ee-00e04cb86ff4', '4c39fb9a-c150-4c3d-a6f0-052b1a25c659', '4b96406c-dc10-4136-b333-17dcc0a8b222'),
('40eadef6-0c41-48c5-b77f-7dca19d33ccd', '4cce5f01-6d50-4cb0-ade0-0a781a25c659', '0d9b20a0-0f40-4589-b383-ef7f5bd8ed8d'),
('34441a9d-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f01-6d50-4cb0-ade0-0a781a25c659', '312b9280-aed5-443c-abb8-b357535c0ae8'),
('3444ba94-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f01-6d50-4cb0-ade0-0a781a25c659', '4b38c6fa-aee8-4abb-b111-0ac81a25c659'),
('3444d139-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f01-6d50-4cb0-ade0-0a781a25c659', '4b822900-b6c4-4efb-bc93-10f01a25c659'),
('3444cf0c-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f01-6d50-4cb0-ade0-0a781a25c659', '4b83c296-c468-47aa-980a-06f01a25c659'),
('3444ccdb-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f01-6d50-4cb0-ade0-0a781a25c659', '4b83c3fd-cea8-4c6e-8146-06f01a25c659'),
('3444db21-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f01-6d50-4cb0-ade0-0a781a25c659', '4b8a9d93-58e4-45fa-bdff-0fbc1a25c659'),
('34450866-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f01-6d50-4cb0-ade0-0a781a25c659', '4b96198f-19d8-4714-900c-17dc1a25c659'),
('3444e676-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f01-6d50-4cb0-ade0-0a781a25c659', '4b96198f-5254-41a5-ab2b-17dc1a25c659'),
('34450067-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f01-6d50-4cb0-ade0-0a781a25c659', '4b96198f-fc10-4197-82e9-17dc1a25c659'),
('34450a89-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f01-6d50-4cb0-ade0-0a781a25c659', '4b963db5-69a8-4d3d-89e3-17dc1a25c659'),
('3444bc8f-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f01-6d50-4cb0-ade0-0a781a25c659', '4b96406c-dc10-4136-b333-17dcc0a8b222'),
('34450e5a-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f01-6d50-4cb0-ade0-0a781a25c659', '4b96534a-aef4-4aa0-8e2f-17dc1a25c659'),
('34450c82-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f01-6d50-4cb0-ade0-0a781a25c659', '4c1928d7-e238-4bcb-8557-03bf1a25c659'),
('3445e9cf-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f01-6d50-4cb0-ade0-0a781a25c659', '4d7e67ae-7bd8-4e85-a662-03f11a25c659'),
('3445ebcc-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f01-6d50-4cb0-ade0-0a781a25c659', '4db83fcd-8cac-42a9-866c-03321a25c659'),
('3444cac7-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f01-6d50-4cb0-ade0-0a781a25c659', '514a9a5d-7c50-4917-874b-0bac1a25c659'),
('3445edc5-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f01-6d50-4cb0-ade0-0a781a25c659', '51b0ed87-6468-4a81-be84-17fc1a25c659'),
('3445efb2-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f01-6d50-4cb0-ade0-0a781a25c659', '51b0ee03-2ec8-41ab-89a0-17fc1a25c659'),
('3445a1d8-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f01-6d50-4cb0-ade0-0a781a25c659', '5219d72b-5a70-4859-801b-19541a25c659'),
('3445064f-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f01-6d50-4cb0-ade0-0a781a25c659', '5322f1e2-8848-4f2e-a35c-1aa01a25c659'),
('3445dc8e-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f01-6d50-4cb0-ade0-0a781a25c659', '54c65c67-74c0-4e61-8933-1f141a25c659'),
('3445aa62-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f01-6d50-4cb0-ade0-0a781a25c659', '54fd559f-50c8-4787-9f31-3bbc1a25c659'),
('3445f1a8-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f01-6d50-4cb0-ade0-0a781a25c659', '54fd55a0-14fc-4da1-be5f-3bbc1a25c659'),
('3445f38f-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f01-6d50-4cb0-ade0-0a781a25c659', '54fd55a0-6734-450b-9a2a-3bbc1a25c659'),
('3445a5b2-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f01-6d50-4cb0-ade0-0a781a25c659', '54fd55a1-cbac-4f15-8aca-3bbc1a25c659'),
('34461f05-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f01-6d50-4cb0-ade0-0a781a25c659', 'a6d27835-5952-4074-89de-7f0bff0c1c97'),
('344614a7-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f01-6d50-4cb0-ade0-0a781a25c659', 'bcc4eee6-c352-4fc9-885f-89705aceb8a5'),
('3444791f-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f01-6d50-4cb0-ade0-0a781a25c659', 'ef75455d-db95-4679-b798-cb0297a729d9'),
('3446080e-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f01-6d50-4cb0-ade0-0a781a25c659', 'fd340aff-af94-49e4-897c-1360f94220fe'),
('2b49a1d5-c614-4c40-b2e5-482e6262e71e', '4cce5f29-45ec-47b6-8e5c-0a781a25c659', '0d9b20a0-0f40-4589-b383-ef7f5bd8ed8d'),
('34447fbb-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f29-45ec-47b6-8e5c-0a781a25c659', '4b38c6fa-aee8-4abb-b111-0ac81a25c659'),
('3444a06e-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f29-45ec-47b6-8e5c-0a781a25c659', '4b822900-b6c4-4efb-bc93-10f01a25c659'),
('34449ddf-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f29-45ec-47b6-8e5c-0a781a25c659', '4b83c296-c468-47aa-980a-06f01a25c659'),
('344499a3-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f29-45ec-47b6-8e5c-0a781a25c659', '4b83c3fd-cea8-4c6e-8146-06f01a25c659'),
('3444a279-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f29-45ec-47b6-8e5c-0a781a25c659', '4b8a9d93-58e4-45fa-bdff-0fbc1a25c659'),
('3444b319-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f29-45ec-47b6-8e5c-0a781a25c659', '4b96198f-19d8-4714-900c-17dc1a25c659'),
('3444a8c9-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f29-45ec-47b6-8e5c-0a781a25c659', '4b96198f-5254-41a5-ab2b-17dc1a25c659'),
('3444aed5-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f29-45ec-47b6-8e5c-0a781a25c659', '4b96198f-fc10-4197-82e9-17dc1a25c659'),
('3444b4f1-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f29-45ec-47b6-8e5c-0a781a25c659', '4b963db5-69a8-4d3d-89e3-17dc1a25c659'),
('3444833f-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f29-45ec-47b6-8e5c-0a781a25c659', '4b96406c-dc10-4136-b333-17dcc0a8b222'),
('3444b8a8-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f29-45ec-47b6-8e5c-0a781a25c659', '4b96534a-aef4-4aa0-8e2f-17dc1a25c659'),
('3444b6bd-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f29-45ec-47b6-8e5c-0a781a25c659', '4c1928d7-e238-4bcb-8557-03bf1a25c659'),
('34449681-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f29-45ec-47b6-8e5c-0a781a25c659', '514a9a5d-7c50-4917-874b-0bac1a25c659'),
('3445ffd8-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f29-45ec-47b6-8e5c-0a781a25c659', '51b0ed87-6468-4a81-be84-17fc1a25c659'),
('3445954e-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f29-45ec-47b6-8e5c-0a781a25c659', '51b0ee03-2ec8-41ab-89a0-17fc1a25c659'),
('3445a3c9-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f29-45ec-47b6-8e5c-0a781a25c659', '5219d72b-5a70-4859-801b-19541a25c659'),
('3444b0e7-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f29-45ec-47b6-8e5c-0a781a25c659', '5322f1e2-8848-4f2e-a35c-1aa01a25c659'),
('3445f7bd-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f29-45ec-47b6-8e5c-0a781a25c659', '54c65c52-b870-4492-a0c9-1f141a25c659'),
('0bac9697-2674-4068-ae0b-a66e314e8751', '4cce5f29-45ec-47b6-8e5c-0a781a25c659', '54fd559e-1e70-476b-bc64-3bbc1a25c659'),
('34459345-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f29-45ec-47b6-8e5c-0a781a25c659', '54fd559f-1248-45fe-97e5-3bbc1a25c659'),
('34459132-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f29-45ec-47b6-8e5c-0a781a25c659', '54fd559f-50c8-4787-9f31-3bbc1a25c659'),
('3445973a-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f29-45ec-47b6-8e5c-0a781a25c659', '54fd55a0-6734-450b-9a2a-3bbc1a25c659'),
('34459959-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f29-45ec-47b6-8e5c-0a781a25c659', '54fd55a0-fccc-45cd-a716-3bbc1a25c659'),
('34459dc6-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f29-45ec-47b6-8e5c-0a781a25c659', '54fd55a1-cbac-4f15-8aca-3bbc1a25c659'),
('3445a7bb-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f29-45ec-47b6-8e5c-0a781a25c659', '85796298-e870-4a0c-ad21-7b3e4aaa4fce'),
('34460a28-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f29-45ec-47b6-8e5c-0a781a25c659', 'a6d27835-5952-4074-89de-7f0bff0c1c97'),
('344618c1-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f29-45ec-47b6-8e5c-0a781a25c659', 'bcc4eee6-c352-4fc9-885f-89705aceb8a5'),
('34446efc-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f29-45ec-47b6-8e5c-0a781a25c659', 'ef75455d-db95-4679-b798-cb0297a729d9'),
('34447696-d6ca-11eb-94ee-00e04cb86ff4', '4cce5f29-45ec-47b6-8e5c-0a781a25c659', 'fd340aff-af94-49e4-897c-1360f94220fe'),
('344472cc-d6ca-11eb-94ee-00e04cb86ff4', '4dc80103-5f44-48b1-a09d-02e41a25c659', '4b96406c-dc10-4136-b333-17dcc0a8b222');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aco_accesslevel` int(11) DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `auth_config_identifier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=2730 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `roles`
--

INSERT INTO `roles` (`id`, `name`, `aco_accesslevel`, `description`, `auth_config_identifier`, `start_controller`, `created`, `modified`) VALUES
('22222222-2222-2222-2222-222222222222', 'Systemadministrator', 0, '', 'backend', 'Article', '2020-09-10 00:00:00', '2021-06-29 00:29:21'),
('4cce5f01-6d50-4cb0-ade0-0a781a25c659', 'WebAdmin (eine Domain)', 2, '', '0', 'Articles', '2020-09-10 00:00:00', '2021-06-29 00:03:36'),
('4cce5f29-45ec-47b6-8e5c-0a781a25c659', 'WebAdmin (alle Domains)', 1, '', '0', 'Articles', '2020-09-10 00:00:00', '2021-06-29 00:30:33'),
('ffffffff-ffff-ffff-ffff-ffffffffffff', 'Frontenduser', 100, 'Die Rolle eines User im Frontend\n', 'defaultfrontend', '', '2020-09-10 00:00:00', '2021-06-27 00:07:54');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `roles_users`
--

DROP TABLE IF EXISTS `roles_users`;
CREATE TABLE `roles_users` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_default` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=5461 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `roles_users`
--

INSERT INTO `roles_users` (`id`, `role_id`, `user_id`, `is_default`, `created`, `modified`) VALUES
('8486309', '22222222-2222-2222-2222-222222222222', '11111111-1111-1111-1111-111111111111', 0, '2019-05-03 13:36:00', '2021-06-29 00:29:21'),
('84863248', '4cce5f29-45ec-47b6-8e5c-0a781a25c659', 'cccccccc-cccc-cccc-cccc-cccccccccccc', 0, '2019-09-20 15:34:03', '2021-06-29 00:30:33');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `texts`
--

DROP TABLE IF EXISTS `texts`;
CREATE TABLE `texts` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `source` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci,
  `default_value` longtext COLLATE utf8mb4_unicode_ci,
  `is_html` tinyint(1) DEFAULT NULL,
  `in_use` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=266 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `texts`
--

INSERT INTO `texts` (`id`, `source`, `type`, `name`, `value`, `default_value`, `is_html`, `in_use`, `created`, `modified`) VALUES
('00712ed3-266d-435a-9045-9ef41e5a0000', 'domain', 'language', 'Javanisch', 'Javanisch', 'Javanisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('00d89ba4-cdbc-4f1b-b579-b9ba90a3c4f5', 'domain', 'country', 'Iran', 'Iran', 'Iran', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('012b681d-c3a5-4868-b181-bbc63ba14048', 'domain', 'country', 'Guyana', 'Guyana', 'Guyana', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('01522312-0ae2-45f6-8198-0158766492f4', 'domain', 'language', 'Korsisch', 'Korsisch', 'Korsisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('01665d7b-1dc8-4db8-8054-fada14f9bdf1', 'domain', 'country', 'Südafrika, Republik', 'Südafrika, Republik', 'Südafrika, Republik', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('016de572-3c80-4a6f-9a87-ea4d5b1059c7', 'domain', 'country', 'Mexiko', 'Mexiko', 'Mexiko', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('0213081c-07a1-4ca4-ac47-48b211c208b7', 'domain', 'language', 'Tamilisch', 'Tamilisch', 'Tamilisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('0255f6f5-e81f-499a-a089-c1a8ea3f349c', 'domain', 'language', 'Gujaratisch', 'Gujaratisch', 'Gujaratisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('03172146-7ee8-41f1-8adc-d67c3e491245', 'domain', 'language', 'Französisch', 'Französisch', 'Französisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('036903a5-b439-4a1d-a2a0-009e7633415c', 'locales', '__(Texte)', 'mehr erfahren', 'mehr erfahren', 'mehr erfahren', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('0380ba9f-8298-480b-8010-344a9ea4a09e', 'locales', '__(Texte)', 'MwSt.', 'MwSt.', 'MwSt.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('03b4540f-fae3-41fd-98e5-aa7c8b14c36e', 'domain', 'language', 'Kijarwanda', 'Kijarwanda', 'Kijarwanda', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('04c67c40-ec66-423c-9008-bbadb347767b', 'locales', 'cake', 'just now', 'jetzt', 'jetzt', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('04d09b14-3728-425e-b113-628908337e30', 'domain', 'formfield', 'Element (.ctp)', 'Element (.ctp)', 'Element (.ctp)', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('05188eac-6c0f-439b-ab54-0bd2f167a01d', 'locales', '__(Texte)', 'Bitte auswählen', 'Bitte auswählen', 'Bitte auswählen', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('0547f13a-8908-48f2-ad64-9b88de853281', 'domain', 'language', 'Persisch', 'Persisch', 'Persisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('0555318e-3c51-4369-b0fd-fabd3cbb0319', 'domain', 'language', 'Swasiländisch', 'Swasiländisch', 'Swasiländisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('05a25467-f995-4cca-9cd7-219b03f530cc', 'locales', '__(Texte)', 'Bitte geben Sie eine gültige Adresse ein', 'Bitte geben Sie eine gültige Adresse ein', 'Bitte geben Sie eine gültige Adresse ein', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('0620e152-6f78-4a7e-b9fd-d3de4639be60', 'domain', 'language', 'Polnisch', 'Polnisch', 'Polnisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('06e5b06b-d6c4-4aa0-a9d0-a7688b5762e3', 'locales', '__(Texte)', 'März', 'März', 'März', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('071957cb-420a-4497-831d-069c35219a5c', 'domain', 'country', 'Réunion', 'Réunion', 'Réunion', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('0744002f-a3d3-479e-81bc-d7f0a82f553c', 'domain', 'country', 'Kroatien', 'Kroatien', 'Kroatien', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('07a3f527-b59e-4af6-b716-dd012ecba8ba', 'domain', 'language', 'Arabisch', 'Arabisch', 'Arabisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('07e22e9a-5081-46db-822c-fb8a06873f5c', 'domain', 'deliverytime', '7 Tage', '7 Tage', '7 Tage', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('07f29972-83c6-4f20-afc8-4e5fe47b27c7', 'locales', 'system', 'Den gewählten Artikel gibt es nicht auf der gewählten Sprache.', 'Den gewählten Artikel gibt es nicht auf der gewählten Sprache.', 'Den gewählten Artikel gibt es nicht auf der gewählten Sprache.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('083227e8-13c4-415a-a82f-fd27fac92e8b', 'domain', 'country', 'Guatemala', 'Guatemala', 'Guatemala', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('087d7efd-441f-444a-9526-6d34634c962a', 'domain', 'country', 'Uruguay', 'Uruguay', 'Uruguay', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('08ac0114-e798-4dfa-b8ae-a93c7d0282ed', 'domain', 'country', 'Kamerun', 'Kamerun', 'Kamerun', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('08ba3864-89f6-42e9-9f93-1d8c58b4f6b7', 'domain', 'country', 'Aruba', 'Aruba', 'Aruba', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('08f79c93-9fc4-4eab-bd1d-4a74589b0776', 'domain', 'country', 'Spanien', 'Spanien', 'Spanien', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('09067b28-dd36-4a5d-bd7a-839f0f4c20a1', 'locales', '__(Texte)', 'Hey,\\nwäre das nicht was für Dich?\\n\\nViele Grüße!\\n\\n', 'Hey,\\nwäre das nicht was für Dich?\\n\\nViele Grüße!\\n\\n', 'Hey,\\nwäre das nicht was für Dich?\\n\\nViele Grüße!\\n\\n', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('0917bbcf-6317-41df-9f6f-552bd1010d47', 'domain', 'country', 'Schweiz', 'Schweiz', 'Schweiz', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('09336274-eaeb-4d5c-b231-cb4dea0767ba', 'domain', 'formfield', 'Select (Multiple)', 'Select (Multiple)', 'Select (Multiple)', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('0995398c-6785-4856-b0b1-e8583a1078b1', 'domain', 'country', 'Neutrale Zone', 'Neutrale Zone', 'Neutrale Zone', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('09a06d47-d034-49a3-a036-f4a9b5d89ec3', 'locales', 'cake', 'Missing CSRF token cookie', 'Missing CSRF token cookie', 'Missing CSRF token cookie', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('09c2dc87-c613-4995-9ed4-47711695410b', 'domain', 'country', 'Guadeloupe', 'Guadeloupe', 'Guadeloupe', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('09da0250-d2d6-41ed-b589-a011341a0965', 'domain', 'country', 'St. Pierre und Miquelon', 'St. Pierre und Miquelon', 'St. Pierre und Miquelon', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('09ff90c8-39c8-45f1-9600-bba5ca23b568', 'domain', 'interval', 'täglich um 07:00', 'täglich um 07:00', 'täglich um 07:00', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('0a16011f-1a30-4557-843c-22d7a75f861e', 'domain', 'country', 'Tadschikistan', 'Tadschikistan', 'Tadschikistan', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('0a233324-f4ff-4087-ad33-5b447d6355af', 'locales', 'cake', '{0} minute', '{0} minute', '{0} minute', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('0af30a04-b6f5-4580-bd8f-8e256416e05a', 'domain', 'berufserfahrung', '3 Jahre', '3 Jahre', '3 Jahre', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('0b5dfcbf-42de-41f7-870d-ff83943b8fd0', 'locales', 'system', 'Die Daten wurden gespeichert.', 'Die Daten wurden gespeichert.', 'Die Daten wurden gespeichert.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('0b61fdb8-8909-413d-96b2-eec1ff4741c3', 'domain', 'country', 'Lesotho', 'Lesotho', 'Lesotho', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('0c399513-2a61-4c2c-aced-5e238e64c459', 'domain', 'title', 'Prof.', 'Prof.', 'Prof.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('0c565576-f1df-444f-af2b-047ec2d9bbb9', 'domain', 'country', 'Gabun', 'Gabun', 'Gabun', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('0c6b3cc6-44f8-4857-9c9d-7a5da69c991c', 'locales', '__(Texte)', 'Einträge konnten nicht verschoben werden.', 'Einträge konnten nicht verschoben werden.', 'Einträge konnten nicht verschoben werden.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('0c769792-5f09-41df-8789-a068fd93bf84', 'locales', '__(Texte)', 'Maintenance', 'Maintenance', 'Maintenance', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('0ca22f08-6a0e-422a-b110-ce3ffe89a921', 'domain', 'country', 'Zentralafrikanische Republik', 'Zentralafrikanische Republik', 'Zentralafrikanische Republik', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('0cc90211-1ae5-4e5a-a788-e2bec1b3b344', 'domain', 'country', 'Libanon', 'Libanon', 'Libanon', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('0ccc1b6c-23b8-4ee0-83c1-168a6664be14', 'domain', 'interval', 'alle 12 Stunden', 'alle 12 Stunden', 'alle 12 Stunden', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('0d5b284d-bd31-40b4-94ff-9f48fd680a9c', 'domain', 'language', 'Haussa', 'Haussa', 'Haussa', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('0db68518-618f-4c67-8830-4cb55efd4a74', 'domain', 'country', 'Monaco', 'Monaco', 'Monaco', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('0e818f77-dece-4476-aafc-a816a3155073', 'domain', 'interval', 'täglich um 09:30', 'täglich um 09:30', 'täglich um 09:30', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('0f507036-073d-4008-889f-995c7d2af76f', 'domain', 'country', 'Irak', 'Irak', 'Irak', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('0f9de166-b758-42b0-820f-1ed80086e808', 'domain', 'language', 'Kannada', 'Kannada', 'Kannada', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('0fdf5d52-b61c-43e9-8c6b-17add15d2da1', 'domain', 'language', 'Afrikaans', 'Afrikaans', 'Afrikaans', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('10077ed6-d796-46d4-9c2d-5e87256c6a6d', 'locales', 'system', 'Alle Konfigurationen für neue Seitentyp wurde erfolgreich erstellt.', 'Alle Konfigurationen für neue Seitentyp wurde erfolgreich erstellt.', 'Alle Konfigurationen für neue Seitentyp wurde erfolgreich erstellt.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('10613aae-a1be-4bcb-981d-29495217bb08', 'locales', '__(Texte)', 'Berechnen', 'Berechnen', 'Berechnen', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('10b546b6-e03c-4a4d-a461-f0c098adbdde', 'locales', 'cake', 'about a week ago', 'about a week ago', 'about a week ago', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('1204989a-0c81-40d3-8ac9-18bea9cdc50e', 'locales', 'system', 'Die Unterordner konnten nicht gespeichert werden,', 'Die Unterordner konnten nicht gespeichert werden,', 'Die Unterordner konnten nicht gespeichert werden,', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('12b4fed7-808d-4b67-8dcd-96cb6aa55391', 'domain', 'country', 'St. Kitts und Nevis', 'St. Kitts und Nevis', 'St. Kitts und Nevis', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('1326d0c7-81d0-48bf-b1cd-c8ac17f9a002', 'domain', 'country', 'Armenien', 'Armenien', 'Armenien', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('134c2259-7d44-477d-8cd1-ea5ce59397f4', 'domain', 'language', 'Volapük', 'Volapük', 'Volapük', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('13c330fe-98b8-46b7-93e4-472a2de931e0', 'domain', 'language', 'Kirundisch', 'Kirundisch', 'Kirundisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('13e42c77-abda-4137-9a8a-307ce6d77e26', 'domain', 'country', 'Fidschi', 'Fidschi', 'Fidschi', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('1447a571-7d0e-4736-a4a4-4c4a5ee71445', 'locales', '__(Texte)', 'Letzte', 'Letzte', 'Letzte', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('146b0e59-79bd-4a7f-82d9-3aa90fb4a63a', 'locales', 'cake', 'in about an hour', 'in about an hour', 'in about an hour', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('147aa5b0-40f1-4741-a970-c23dd705bf80', 'domain', 'country', 'Mongolei', 'Mongolei', 'Mongolei', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('14993d10-b8bd-44e9-9bc4-f152ffb0c02e', 'domain', 'interval', 'alle halbe Stunde', 'alle halbe Stunde', 'alle halbe Stunde', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('14b7cae5-a192-4921-896b-60d96d3c3023', 'domain', 'country', 'Mauritius, Republik', 'Mauritius, Republik', 'Mauritius, Republik', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('14fc4432-9c28-477a-a151-912186c171c8', 'domain', 'language', 'Pundjabisch', 'Pundjabisch', 'Pundjabisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('15523cd9-441a-4fec-8751-c6e01f8fc004', 'domain', 'language', 'Litauisch', 'Litauisch', 'Litauisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('15630af1-a29e-4366-9784-5a07e4f522c0', 'domain', 'country', 'Dominica', 'Dominica', 'Dominica', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('15806270-d8a8-460e-a98a-fe376adcd8cf', 'locales', '__(Texte)', 'Ja', 'Ja', 'Ja', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('15c9c230-07f3-4aa4-8d67-0b91d85a9b6e', 'domain', 'interval', 'alle 8 Stunden', 'alle 8 Stunden', 'alle 8 Stunden', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('176935d7-ce1c-47c9-9102-a8e9f05fad76', 'locales', 'cake', '{0,number,#,###.##} TB', '{0,number,#,###.##} TB', '{0,number,#,###.##} TB', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('188f44b2-b2ec-4c1d-a99e-1c54c8f83c4a', 'domain', 'language', 'Tadschikisch', 'Tadschikisch', 'Tadschikisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('18bb75ca-cdd4-4c59-8e00-d9b333d1506a', 'domain', 'language', 'Serbisch', 'Serbisch', 'Serbisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('1925393b-381c-4f3c-8a4d-1e52d3668ef6', 'locales', '__(Texte)', 'Anfahrt', 'Anfahrt', 'Anfahrt', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('1a24cc9b-3eca-43ff-b5ff-7d427a9ef005', 'domain', 'language', 'Lateinisch', 'Lateinisch', 'Lateinisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('1a89c82b-1e75-43f0-a1fd-6e5958ae0486', 'locales', '__(Texte)', 'Auswählen', 'Auswählen', 'Auswählen', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('1aa08cf8-f53d-44f6-b921-045f0cd2a490', 'domain', 'country', 'Kuba', 'Kuba', 'Kuba', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('1aeaf1e5-6a25-4bb1-8e1d-5e5b4edb3d89', 'locales', '__(Texte)', 'Diese Datei wurde aus Sicherheitsgründen abgelehnt.', 'Diese Datei wurde aus Sicherheitsgründen abgelehnt.', 'Diese Datei wurde aus Sicherheitsgründen abgelehnt.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('1b49f13b-d558-48e4-a9ca-1004f80800bb', 'domain', 'language', 'Twi', 'Twi', 'Twi', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('1b69e2b0-c40f-4e31-a176-be808e85d2aa', 'domain', 'interval', 'täglich um 01:00', 'täglich um 01:00', 'täglich um 01:00', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('1b880425-54cc-4843-8863-cd9fea2d7351', 'domain', 'country', 'Syrien', 'Syrien', 'Syrien', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('1bb58a47-e9af-4fa5-b065-12af434302cd', 'domain', 'formfield', 'Radio', 'Radio', 'Radio', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('1c0192fb-160a-4530-8252-114d5a914f3b', 'locales', '__(Texte)', 'Error', 'Error', 'Error', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('1c1bcdbe-c8c1-42e0-9475-48d671d6a634', 'domain', 'language', 'Urdu', 'Urdu', 'Urdu', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('1c28f477-70b1-4075-b68a-ff0592c42fb6', 'domain', 'language', 'Suaheli', 'Suaheli', 'Suaheli', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('1d4af090-d2b8-4ea4-81c6-fd5ba2d7c487', 'domain', 'country', 'Norfolkinsel', 'Norfolkinsel', 'Norfolkinsel', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('1d678cde-aa71-435f-93f9-0eb6ee9f5961', 'domain', 'country', 'Irland, Republik', 'Irland, Republik', 'Irland, Republik', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('1d74c2bd-4aef-43fa-aa4c-6e57434c9eb9', 'domain', 'country', 'Belize', 'Belize', 'Belize', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('1d827fd9-e4c6-4b4f-8b57-c72b02c26f56', 'domain', 'country', 'Weißrussland', 'Weißrussland', 'Weißrussland', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('1d8dabc5-0377-4606-88cf-8b343ec0de74', 'domain', 'country', 'Palästinensische Autonomiegebiete', 'Palästinensische Autonomiegebiete', 'Palästinensische Autonomiegebiete', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('1e0f4667-b259-4b6e-ae5a-b008f72e38d1', 'domain', 'formfield', 'Textblock', 'Textblock', 'Textblock', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('1e36a7ec-7ed6-4904-8801-3a156a917826', 'locales', '__(Texte)', 'Hinweis', 'Hinweis', 'Hinweis', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('1e4c497b-5f43-4c23-94bc-928cbc5b7c69', 'domain', 'language', 'Japanisch', 'Japanisch', 'Japanisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('1e53bce5-c8eb-41ad-a9b4-a2f96c284adf', 'domain', 'country', 'Montenegro', 'Montenegro', 'Montenegro', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('1ef171f7-a2ab-4f67-a132-2d3904c91e8b', 'locales', '__(Texte)', 'Mit dem gewählten Zahlungsanbieter konnte keine Zahlung durchgeführt werden. Bitte versuchen Sie es erneut oder wählen Sie eine andere Zahlungsart.', 'Mit dem gewählten Zahlungsanbieter konnte keine Zahlung durchgeführt werden. Bitte versuchen Sie es erneut oder wählen Sie eine andere Zahlungsart.', 'Mit dem gewählten Zahlungsanbieter konnte keine Zahlung durchgeführt werden. Bitte versuchen Sie es erneut oder wählen Sie eine andere Zahlungsart.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('1f086f5e-1513-4cd5-b59d-a428bb63c0d8', 'domain', 'interval', 'täglich um 05:30', 'täglich um 05:30', 'täglich um 05:30', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('1fc01bd4-3e26-49d4-8308-c4875955e949', 'domain', 'country', 'Algerien', 'Algerien', 'Algerien', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('1fc66133-e3d6-4a8a-8a25-d616348dc0e1', 'locales', '__(Texte)', 'Diese E-Mail ist schon registriert. Haben Sie vielleicht nur Ihr Passwort vergessen?', 'Diese E-Mail ist schon registriert. Haben Sie vielleicht nur Ihr Passwort vergessen?', 'Diese E-Mail ist schon registriert. Haben Sie vielleicht nur Ihr Passwort vergessen?', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('200f0c18-1ffa-4901-8705-b1c779fcdd95', 'domain', 'language', 'Baskisch', 'Baskisch', 'Baskisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('201d3e21-bf3d-4e36-a631-10b3267ec063', 'domain', 'country', 'Neuseeland', 'Neuseeland', 'Neuseeland', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('2080b66f-54ce-4c5b-a598-8db62ce98f42', 'locales', '__(Texte)', 'Bitte geben Sie eine Domain ein', 'Bitte geben Sie eine Domain ein', 'Bitte geben Sie eine Domain ein', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('20af7835-f3fe-42a4-89bb-e2da56823f16', 'domain', 'country', 'Nauru', 'Nauru', 'Nauru', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('20b81da9-1f69-4d2d-b512-a722368e53cf', 'domain', 'interval', 'Jetzt einmal', 'Jetzt einmal', 'Jetzt einmal', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('20b8ecc9-17a6-4814-b5b8-ce7ad7e6ee32', 'domain', 'country', 'Rumänien', 'Rumänien', 'Rumänien', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('20d06a0a-8633-4c0d-b369-b3660b6b93fa', 'domain', 'country', 'Kaimaninseln', 'Kaimaninseln', 'Kaimaninseln', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('211a7460-790b-40f1-8806-3a967926dfc7', 'domain', 'country', 'Anguilla', 'Anguilla', 'Anguilla', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('21269fa9-3fc2-466e-99e6-b9480610c869', 'domain', 'country', 'Insel Man', 'Insel Man', 'Insel Man', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('219c9bea-2302-4459-8a1e-1c29942fb34d', 'domain', 'language', 'Kasachisch', 'Kasachisch', 'Kasachisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('21b2d6a2-54ac-46f2-b6d9-87d91d98fc3a', 'domain', 'country', 'Indonesien', 'Indonesien', 'Indonesien', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('21d7b2fb-866d-41b1-855a-0abe4003e375', 'locales', '__(Texte)', 'Thumbnails regenerated for ', 'Thumbnails regenerated for ', 'Thumbnails regenerated for ', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('21dcd0f1-f82e-4a88-9a95-a9893f0055b9', 'domain', 'interval', 'täglich um 03:00', 'täglich um 03:00', 'täglich um 03:00', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('22092093-e4e2-44bb-9bb1-b717826a6550', 'domain', 'language', 'Tibetanisch', 'Tibetanisch', 'Tibetanisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('22545865-59fd-46b9-8ce6-f40e1e2ee4ba', 'domain', 'formfield', 'Headline (h2)', 'Headline (h2)', 'Headline (h2)', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('22854ef6-cf9f-4a18-809e-70a4aa964dc7', 'domain', 'interval', 'täglich um 11:30', 'täglich um 11:30', 'täglich um 11:30', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('22f48b77-90ba-40d3-9b8f-de6ba40e329a', 'locales', 'cake', 'The provided value is invalid', 'Die Eingabe ist nicht gültig', 'Die Eingabe ist nicht gültig', 0, 1, '2020-09-10 00:00:00', '2020-10-15 23:50:42'),
('234b1271-86f6-4132-86ce-5b43a87bcd5e', 'domain', 'country', 'Griechenland', 'Griechenland', 'Griechenland', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('238ddaf7-7073-4738-8de1-bfa38ec47f9b', 'domain', 'country', 'Tunesien', 'Tunesien', 'Tunesien', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('23c9f7d0-0ef7-4f37-b116-08b542f519f2', 'domain', 'language', 'Baschkirisch', 'Baschkirisch', 'Baschkirisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('23e2530f-f733-4595-a0ec-618de781960f', 'domain', 'country', 'China, Volksrepublik', 'China, Volksrepublik', 'China, Volksrepublik', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('244971f1-ec27-4444-8170-2e7c7f5a0804', 'locales', 'system', 'Die Daten wurden erfolgreich kopiert.', 'Die Daten wurden erfolgreich kopiert.', 'Die Daten wurden erfolgreich kopiert.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('2508da67-1617-44ab-be2a-ab37b3b73f2b', 'domain', 'country', 'Guinea, Republik', 'Guinea, Republik', 'Guinea, Republik', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('2584976b-c5b6-4833-b58d-dfe7379f8e1a', 'domain', 'country', 'Sambia, Republik', 'Sambia, Republik', 'Sambia, Republik', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('260a837f-9224-4ab2-89e9-b46561f9cb6c', 'domain', 'country', 'Niederlande', 'Niederlande', 'Niederlande', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('265fdeb4-afb5-4c57-a27c-e40c599a25b7', 'domain', 'formfield', 'Datum und Zeit', 'Datum und Zeit', 'Datum und Zeit', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('26a02d80-1476-48d0-8692-f1d820fa50f8', 'locales', '__(Texte)', 'Februar', 'Februar', 'Februar', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('26bd2dfd-3407-49fb-8fe9-93da22805617', 'locales', '__(Texte)', 'Suchen', 'Suchen', 'Suchen', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('26be2b98-849e-480a-892e-7bcf4523fdda', 'domain', 'formfield', 'Input', 'Input', 'Input', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('27ad8989-e5ee-4131-be6b-d0672a5ad8b8', 'domain', 'country', 'Jordanien', 'Jordanien', 'Jordanien', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('288f042e-442f-4fb8-8f00-a9abea192d70', 'locales', '__(Texte)', 'Ziel konnte nicht gefunden werden.', 'Ziel konnte nicht gefunden werden.', 'Ziel konnte nicht gefunden werden.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('28be1b51-0c3c-433a-a91c-7feee22c1d96', 'domain', 'language', 'Kambodschanisch', 'Kambodschanisch', 'Kambodschanisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('28dda513-3faa-4372-accb-1abde349cbf8', 'domain', 'country', 'Lettland', 'Lettland', 'Lettland', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('2993ea99-db35-4aed-b51b-7bc6a038f1ee', 'domain', 'interval', 'täglich um 23:30', 'täglich um 23:30', 'täglich um 23:30', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('299d44ec-88c3-4631-a05f-7386eed02cc3', 'locales', 'system', 'Alle aktuellen Crontasks wurden ausgeführt.', 'Alle aktuellen Crontasks wurden ausgeführt.', 'Alle aktuellen Crontasks wurden ausgeführt.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('29a35a0d-8d29-495c-ae31-def65992de94', 'domain', 'language', 'Russisch', 'Russisch', 'Russisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('29b9fbfa-54b5-4864-bce9-b20dcde60c52', 'domain', 'interval', 'täglich um 19:30', 'täglich um 19:30', 'täglich um 19:30', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('29c96767-4bde-497d-9972-e5bf9964b8ca', 'domain', 'country', 'Korea, Demokratische Volkrepublik', 'Korea, Demokratische Volkrepublik', 'Korea, Demokratische Volkrepublik', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('29cf7aef-117e-4805-8bc7-5d08f4a743e8', 'locales', 'system', 'Es sind Fehler aufgetreten.', 'Es sind Fehler aufgetreten.', 'Es sind Fehler aufgetreten.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('2a84d792-f582-44fa-98e4-18d956bfdc11', 'domain', 'interval', 'täglich um 02:00', 'täglich um 02:00', 'täglich um 02:00', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('2afef17b-f939-4631-99bb-3be6de15eee1', 'locales', '__(Texte)', 'Ungültig - Vorschau', 'Ungültig - Vorschau', 'Ungültig - Vorschau', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('2bcd9e33-aed7-46c6-9330-a44923f7b4da', 'domain', 'country', 'Suriname', 'Suriname', 'Suriname', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('2c107ebd-b7b3-41cb-b27b-6417c68da66a', 'locales', '__(Texte)', 'OK', 'OK', 'OK', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('2c350984-534c-4c6b-a0e6-f7116ab5e48f', 'locales', 'cake', 'about a month ago', 'about a month ago', 'about a month ago', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('2c50ce18-d7f8-4879-9ff3-b9b2152236b6', 'domain', 'country', 'Uganda, Republik', 'Uganda, Republik', 'Uganda, Republik', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('2c690f7f-9b8f-406d-818e-1a9f8aff1524', 'domain', 'language', 'Lettisch', 'Lettisch', 'Lettisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('2cb54386-9617-484a-bc08-56adc5b644b2', 'locales', '__(Texte)', 'zur Website', 'zur Website', 'zur Website', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('2d056785-7435-4a6d-8ae4-469b22d26968', 'domain', 'interval', 'täglich um 17:00', 'täglich um 17:00', 'täglich um 17:00', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('2d3a288c-b0ea-4e83-8beb-3555084f16fc', 'domain', 'country', 'Martinique', 'Martinique', 'Martinique', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('2d758f5e-0a1c-427e-8792-70a327bd3ede', 'domain', 'language', 'Maltesisch', 'Maltesisch', 'Maltesisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('2dc66424-ff29-4fad-89ab-703764c35b2d', 'locales', '__(Texte)', 'Es ist ein interner Fehler aufgetreten.', 'Es ist ein interner Fehler aufgetreten.', 'Es ist ein interner Fehler aufgetreten.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('2dca7d1a-47e6-484b-9d56-578016dede19', 'domain', 'berufserfahrung', '1 Jahr', '1 Jahr', '1 Jahr', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('2e2175ca-64f4-410e-9812-db94ce5d377e', 'domain', 'country', 'Tansania, Vereinigte Republik', 'Tansania, Vereinigte Republik', 'Tansania, Vereinigte Republik', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('2e7a42e4-cba3-44b8-80dc-9a59c3832c42', 'domain', 'language', 'Afar', 'Afar', 'Afar', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('2ebca30a-43de-43fe-b4d3-5a19790d4348', 'domain', 'country', 'Kolumbien', 'Kolumbien', 'Kolumbien', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('2f29877d-0d7c-48c2-b183-ebca733566fc', 'locales', '__(Texte)', 'Bitte geben Sie ein Passwort ein, das den festgelegten Regeln entspricht.', 'Bitte geben Sie ein Passwort ein, das den festgelegten Regeln entspricht.', 'Bitte geben Sie ein Passwort ein, das den festgelegten Regeln entspricht.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('2f658b71-6a8b-464b-8827-9a4952d04e09', 'locales', '__(Texte)', 'H:i', 'H:i', 'H:i', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('2ff0fece-6ef9-476c-b2e5-73f2db240f87', 'domain', 'country', 'São Tomé und Príncipe', 'São Tomé und Príncipe', 'São Tomé und Príncipe', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('30619b40-dd2e-411a-9d21-f42b315857c0', 'locales', 'cake', '{0,number,integer} Byte', '{0,number,integer} Byte', '{0,number,integer} Byte', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('306e121f-a543-47ab-8b0a-21607ecfc7b5', 'domain', 'country', 'Georgien', 'Georgien', 'Georgien', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('313267bf-16ff-4b79-a10e-a63f7f3ac5bc', 'domain', 'country', 'Turks- und Caicosinseln', 'Turks- und Caicosinseln', 'Turks- und Caicosinseln', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('314491b6-5051-473e-879b-36818fade151', 'locales', '__(Texte)', 'Seite {{page}} von {{pages}}', 'Seite {{page}} von {{pages}}', 'Seite {{page}} von {{pages}}', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('3175c45c-3b06-48e0-9f41-7a82ae31df85', 'domain', 'language', 'Armenisch', 'Armenisch', 'Armenisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('3190b5bc-3c8f-496a-96d3-c8d2a5ab20dd', 'locales', 'system', 'Bitte beachten Sie die Hinweise.', 'Bitte beachten Sie die Hinweise.', 'Bitte beachten Sie die Hinweise.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('31b21177-49c0-442a-9afe-9248bc358893', 'domain', 'country', 'Dschibuti', 'Dschibuti', 'Dschibuti', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('31e5f0d0-77d1-4aa2-87e4-7ed7044eae52', 'locales', 'cake', 'on %s', 'on %s', 'on %s', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('3227c288-fb85-47fc-bd2e-8c49335065c4', 'domain', 'interval', 'täglich um 08:30', 'täglich um 08:30', 'täglich um 08:30', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('3308d992-196a-48c7-9ffa-fd6be06ffa44', 'locales', '__(Texte)', 'Von', 'Von', 'Von', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('3456e83c-b04c-4232-96fd-2e38a326286a', 'domain', 'country', 'Somalia, Demokratische Republik', 'Somalia, Demokratische Republik', 'Somalia, Demokratische Republik', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('345f1379-5027-473a-a5e1-6621f2abd052', 'domain', 'country', 'Ungarn', 'Ungarn', 'Ungarn', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('34c49158-e337-4910-974e-934cef4f5775', 'domain', 'interval', 'täglich um 08:00', 'täglich um 08:00', 'täglich um 08:00', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('353cd318-5752-4019-b35d-531ec37d4411', 'domain', 'language', 'Holländisch', 'Holländisch', 'Holländisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('35a445f1-d9bb-4ad2-86c5-a69aa7cf7740', 'domain', 'country', 'Salomonen', 'Salomonen', 'Salomonen', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('360bef58-56c6-47a1-a121-02f47b155dd7', 'domain', 'formfield', 'Fileinput (Multiple)', 'Fileinput (Multiple)', 'Fileinput (Multiple)', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('361a559b-dd3f-4f3d-84f6-341d1ac14c40', 'domain', 'language', 'Katalanisch', 'Katalanisch', 'Katalanisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('36deada2-840e-4fe1-85a4-8a93490d3f3d', 'domain', 'language', 'Faröisch', 'Faröisch', 'Faröisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('36e1e886-5ca6-43bd-88be-2c7fe4a6d860', 'domain', 'language', 'Italienisch', 'Italienisch', 'Italienisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('370c7488-6610-496b-8d3c-36684662566b', 'domain', 'country', 'Simbabwe, Republik', 'Simbabwe, Republik', 'Simbabwe, Republik', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('381ae707-1502-42a8-9d1a-3b72004aa874', 'locales', 'system', 'Youtube Video nicht gefunden.', 'Youtube Video nicht gefunden.', 'Youtube Video nicht gefunden.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('38bb153e-b3e6-4b56-baaa-fb819f804c4e', 'locales', '__(Texte)', 'Alle Termine zu diesem Event', 'Alle Termine zu diesem Event', 'Alle Termine zu diesem Event', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('39d22aac-e390-41f9-b13b-4ef618dfbb51', 'locales', '__(Texte)', 'Performance', 'Performance', 'Performance', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('39eaecff-ddd2-4934-88ec-a8642982ad13', 'domain', 'language', 'Inuktitut', 'Inuktitut', 'Inuktitut', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('3a1d191d-9969-44d3-a02d-3e465b733ed9', 'locales', '__(Texte)', 'Alle anzeigen', 'Alle anzeigen', 'Alle anzeigen', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('3a394143-cdfa-46a9-a6e0-81b083d1b3ba', 'domain', 'country', 'Japan', 'Japan', 'Japan', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('3a4f2c98-54e8-4d1b-afb4-686ea628622e', 'domain', 'country', 'Venezuela', 'Venezuela', 'Venezuela', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('3a71020c-848a-4a44-9b4e-31e30a0d681f', 'domain', 'country', 'Burundi', 'Burundi', 'Burundi', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('3a95aee9-d8e3-48fe-b8dd-4e86414f5c9b', 'domain', 'interval', 'täglich um 06:30', 'täglich um 06:30', 'täglich um 06:30', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('3a9f8518-5851-41fe-86cb-ff13b3d841de', 'domain', 'country', 'Tristan da Cunha', 'Tristan da Cunha', 'Tristan da Cunha', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('3ab65a8c-06b7-4b34-9f13-b9b61d1d3a81', 'domain', 'country', 'Ukraine', 'Ukraine', 'Ukraine', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('3ac07299-829f-45b9-b589-16bb2b726295', 'domain', 'paymentmethod', 'PayPal', 'PayPal', 'PayPal', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('3b430e9f-f989-4cef-bb18-51dd46096bfe', 'domain', 'country', 'Finnland', 'Finnland', 'Finnland', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('3b8397ed-cc04-4fbd-963c-0278c5260b9e', 'domain', 'country', 'Heard und McDonaldinseln', 'Heard und McDonaldinseln', 'Heard und McDonaldinseln', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('3b8ca2a3-0fc1-45a3-adf7-7cf3700d635c', 'domain', 'formfield', 'Zahl', 'Zahl', 'Zahl', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('3caa8812-cc2d-4817-af4c-f0994c3cff12', 'locales', '__(Texte)', 'Dezember', 'Dezember', 'Dezember', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('3d6ecf42-239e-4e61-b7da-b4046d485bba', 'domain', 'language', 'Okzitanisch', 'Okzitanisch', 'Okzitanisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('3dad7190-fcd1-4d98-9228-ed0872d839cb', 'domain', 'interval', 'täglich um 19:00', 'täglich um 19:00', 'täglich um 19:00', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('3e0a6bf9-e4fc-4539-a01f-603b2d6121ad', 'domain', 'operator', 'Ist größer oder gleich (>=)', 'Ist größer oder gleich (>=)', 'Ist größer oder gleich (>=)', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('3e50666c-f1f7-4e7c-9fc5-6eac15a0a56c', 'domain', 'language', 'Uigur', 'Uigur', 'Uigur', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('3ecacb86-bc02-11df-a83e-002421a2bc07', 'text', 'text', 'newsletter.bestaetigung.email.content', '<p>Sehr geehrte Damen und Herren,</p><p>vielen Dank für Ihre Anmeldung für den :from_name Newsletter.</p><p>Bitte klicken Sie zur Bestätigung auf den folgenden Link: <a href=\"%3Abase_href/newsletterrecipients/approve/%3Anewsletterrecipient_id\"></a><br><a href=\"%3Abase_href/newsletterrecipients/approve/%3Anewsletterrecipient_id\">:base_href/newsletterrecipients/approve/:newsletterrecipient_id</a></p><p>Erst danach ist Ihre Anmeldung endgültig abgeschlossen!</p><p>Wir stellen so sicher, dass Ihre E-Mail-Adresse nicht von einer dritten Person auf unserer Website eingetragen wurde. Sollten Sie unseren Newsletter nicht bestellt haben, ignorieren Sie bitte diese E-Mail. Sie werden keine weiteren E-Mails von uns erhalten.</p><p>Übrigens enthält jeder :from_name Newsletter am Ende einen Link, über den Sie sich jederzeit wieder abmelden können.</p>', '<p>Sehr geehrte Damen und Herren,</p><p>vielen Dank für Ihre Anmeldung für den :from_name Newsletter.</p><p>Bitte klicken Sie zur Bestätigung auf den folgenden Link: <a href=\"%3Abase_href/newsletterrecipients/approve/%3Anewsletterrecipient_id\"></a><br><a href=\"%3Abase_href/newsletterrecipients/approve/%3Anewsletterrecipient_id\">:base_href/newsletterrecipients/approve/:newsletterrecipient_id</a></p><p>Erst danach ist Ihre Anmeldung endgültig abgeschlossen!</p><p>Wir stellen so sicher, dass Ihre E-Mail-Adresse nicht von einer dritten Person auf unserer Website eingetragen wurde. Sollten Sie unseren Newsletter nicht bestellt haben, ignorieren Sie bitte diese E-Mail. Sie werden keine weiteren E-Mails von uns erhalten.</p><p>Übrigens enthält jeder :from_name Newsletter am Ende einen Link, über den Sie sich jederzeit wieder abmelden können.</p>', 1, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('3ecacb86-bc02-11df-a83e-002421a2bc10', 'text', 'text', 'newsletterrecipients.approve.content', '<p>Ihre E-Mail Adresse ist nun bestätigt und Sie erhalten ab sofort unseren Newsletter.</p>', '<p>Ihre E-Mail Adresse ist nun bestätigt und Sie erhalten ab sofort unseren Newsletter.</p>', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('3ecacb86-bc02-11df-a83e-002421a2bcd9', 'text', 'text', 'newsletterrecipients.approve.title', 'Vielen Dank!', 'Vielen Dank!', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('3f5f7a71-aa39-49c5-bcd3-0df31bf4fbf0', 'domain', 'country', 'Niger', 'Niger', 'Niger', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('3fb5567b-14ef-457c-be2d-5c9909bd5949', 'domain', 'language', 'Bengalisch', 'Bengalisch', 'Bengalisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('3fc7fc3d-70df-4e64-924b-3ba72b713d92', 'domain', 'country', 'Luxemburg', 'Luxemburg', 'Luxemburg', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('4079ccac-c4bb-41f5-aa48-ccc97ca4b275', 'domain', 'language', 'Burmesisch', 'Burmesisch', 'Burmesisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('410277ed-d720-44f7-98c4-0611448d852f', 'domain', 'column', '9/12', '9/12', '9/12', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('411b17e4-a5c3-4d93-b080-234092ab841f', 'locales', '__(Texte)', 'Einzelpreis', 'Einzelpreis', 'Einzelpreis', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('4133f652-862f-445c-ae80-97db9a042fa7', 'domain', 'formfield', 'Text aus Texttabelle (Schlüssel)', 'Text aus Texttabelle (Schlüssel)', 'Text aus Texttabelle (Schlüssel)', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('4184dc07-b886-4cbe-a7de-53a58b0de3f9', 'domain', 'language', 'Koreanisch', 'Koreanisch', 'Koreanisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('432b8160-610a-42c6-accb-3139d7128e38', 'locales', 'cake', 'This field is required', 'This field is required', 'This field is required', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('44093982-87b1-4fa9-93a8-fa2363fca6d1', 'domain', 'country', 'Haiti', 'Haiti', 'Haiti', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('441d7f38-7647-4acf-8941-ddcb0dae2166', 'domain', 'column', '6/12', '6/12', '6/12', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('447b6830-5b65-4a59-80a7-9beaa10e53a9', 'locales', '__(Texte)', 'Dieser Dateityp ist nicht erlaubt.', 'Dieser Dateityp ist nicht erlaubt.', 'Dieser Dateityp ist nicht erlaubt.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('44cdfdd8-8f02-4cc9-a349-ee4848ed2758', 'domain', 'country', 'Honduras', 'Honduras', 'Honduras', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('4512eade-0891-4752-8374-85118a8d76fd', 'domain', 'interval', 'jede Stunde', 'jede Stunde', 'jede Stunde', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('4562866c-3de9-40cc-b6ae-d77bec6afac8', 'domain', 'country', 'Barbados', 'Barbados', 'Barbados', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('458b2436-4f17-4900-90af-313771aef6fe', 'domain', 'language', 'Kirgisisch', 'Kirgisisch', 'Kirgisisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('45eb9549-65aa-4687-95dd-9cc79900ccd9', 'locales', '__(Texte)', 'Erste', 'Erste', 'Erste', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('46293fa4-91fc-470a-b04d-db6f467030a9', 'domain', 'country', 'Französische Süd- und Antarktisgebiete', 'Französische Süd- und Antarktisgebiete', 'Französische Süd- und Antarktisgebiete', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('46e634ac-f923-4f32-8dbd-565c8febb6b8', 'domain', 'country', 'Bahrain', 'Bahrain', 'Bahrain', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('46e92f89-2bb4-47f5-84c3-49de690bd940', 'domain', 'formfield', 'Headline (h6)', 'Headline (h6)', 'Headline (h6)', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('4735022d-b15b-4461-8dc6-ce6bad453c77', 'domain', 'formfield', 'Umbruch', 'Umbruch', 'Umbruch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('48e2b2aa-6205-4cad-a039-ddcb1732a9c6', 'domain', 'language', 'Bulgarisch', 'Bulgarisch', 'Bulgarisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('49120d79-951a-4f0a-a225-e3b39c681519', 'locales', '__(Texte)', 'Abschicken', 'Abschicken', 'Abschicken', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('493fb52a-2a6f-4462-99cc-f47d3cc88222', 'domain', 'country', 'Jersey', 'Jersey', 'Jersey', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('49e619e0-ca72-48ad-9933-c4beb22a1161', 'domain', 'salutation', 'Frau', 'Frau', 'Frau', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('4a50a24a-6d55-40cc-9b68-ae4e51dff499', 'domain', 'interval', 'täglich um 11:00', 'täglich um 11:00', 'täglich um 11:00', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('4a691583-a3f3-4d8e-bcfb-01322ad499b9', 'domain', 'language', 'Griechisch', 'Griechisch', 'Griechisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('4ab2d641-c393-4dd3-96e0-39ec869100e0', 'domain', 'country', 'Kiribati', 'Kiribati', 'Kiribati', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('4ade7c0d-9372-46e3-83f4-55784b937e2b', 'locales', 'cake', 'An Internal Error Has Occurred.', 'Es ist ein interner Fehler aufgetreten.', 'Es ist ein interner Fehler aufgetreten.', 0, 1, '2020-09-10 00:00:00', '2020-10-15 23:48:50'),
('4b49f725-2db9-4a58-803b-49131ac36cea', 'domain', 'language', 'Tegulu', 'Tegulu', 'Tegulu', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('4b711dd4-0872-4acf-8488-d1da7efe7468', 'domain', 'country', 'Kokosinseln', 'Kokosinseln', 'Kokosinseln', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('4b81bed5-b074-4750-82a7-453a577b128b', 'locales', 'cake', 'The requested file contains `..` and will not be read.', 'The requested file contains `..` and will not be read.', 'The requested file contains `..` and will not be read.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('4bb228cb-f8d7-4318-b7f9-d920bc464b67', 'domain', 'country', 'Zypern, Republik', 'Zypern, Republik', 'Zypern, Republik', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('4bbfba8a-9c23-462b-8135-0aea314a28cd', 'domain', 'country', 'Bahamas', 'Bahamas', 'Bahamas', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('4c99d580-2f2f-4ec7-9334-352f34f7e4cc', 'domain', 'language', 'Friesisch', 'Friesisch', 'Friesisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('4cb6136c-62dd-4327-8491-4e5e30b3f3ed', 'domain', 'interval', 'täglich um 10:30', 'täglich um 10:30', 'täglich um 10:30', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('4cf2afa8-5ef4-49c1-9528-b1dd355e6a53', 'locales', 'cake', '{0,number,#,###.##} GB', '{0,number,#,###.##} GB', '{0,number,#,###.##} GB', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('4d2603de-f848-4208-94ae-204ae3fc57c4', 'domain', 'country', 'Französisch-Guayana', 'Französisch-Guayana', 'Französisch-Guayana', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('4d42ac7a-a9d0-4d10-9255-493f8d11bab9', 'domain', 'column', '12/12', '12/12', '12/12', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('4d5d553a-73c6-47f7-bca2-4903b348053c', 'domain', 'language', 'Norwegisch', 'Norwegisch', 'Norwegisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('4d8a0f7f-77e0-4ac2-9900-00c71a25c659', 'text', 'text', 'formular.base.send.content', '<p>Wir werden Ihre Anfrage schnellstmöglich beantworten!</p>\n', '<p>Wir werden Ihre Anfrage schnellstmöglich beantworten!</p>\n', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('4d8a0fc9-31dc-44e3-b404-01851a25c659', 'text', 'text', 'formular.base.send.headline', 'Vielen Dank!', 'Vielen Dank!', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('4db88595-1e12-47c6-95cb-c7c280271b6e', 'domain', 'country', 'Korea, Republik', 'Korea, Republik', 'Korea, Republik', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('4dcfd503-1535-4455-a119-4eaed09b1d95', 'domain', 'country', 'Tschad, Republik', 'Tschad, Republik', 'Tschad, Republik', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('4e7c2939-7ae1-426a-a99f-6629dc7c0f5a', 'domain', 'interval', 'täglich um 12:30', 'täglich um 12:30', 'täglich um 12:30', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('4e88e295-047d-433c-9ebc-e817ccb6d2ef', 'domain', 'country', 'Niederländische Antillen', 'Niederländische Antillen', 'Niederländische Antillen', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('4ed5093d-4c94-4013-b95d-1efa2efc16a2', 'text', 'text', 'formular.checkout.email.content', '<p>Sehr geehrte(r) :salutation,<br>vielen Dank für Ihre Bestellung!</p>', '<p>Sehr geehrte(r) :salutation,<br>vielen Dank für Ihre Bestellung!</p>', 1, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('4ed51270-c490-4e8a-ab36-45672efc16a2', 'text', 'text', 'formular.checkout.email.subject', 'Ihre Bestellung', 'Ihre Bestellung', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('4f26ba0d-f6ba-4655-8ae3-c4ae5f87c9a8', 'domain', 'country', 'Mazedonien', 'Mazedonien', 'Mazedonien', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('4f399a10-199b-479e-8d03-4578cf1d19c9', 'locales', '__(Texte)', 'Vom', 'Vom', 'Vom', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('5044d62b-3b6b-4bf9-8945-e5d5c214f03b', 'locales', '__(Texte)', 'Newsletter', 'Newsletter', 'Newsletter', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('51b0718a-d79c-41ff-92a3-17fc1a25c659', 'text', 'text', 'newsletterrecipients.unsubscribe.content', '<p>Wir haben Ihre E-Mail-Adresse wunschgemäß aus dem Verteiler genommen.</p>', '<p>Wir haben Ihre E-Mail-Adresse wunschgemäß aus dem Verteiler genommen.</p>', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('51b071f4-3cd8-4f89-9b30-17fc1a25c659', 'text', 'text', 'newsletterrecipients.unsubscribe.title', 'Newsletter abbestellen', 'Newsletter abbestellen', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('51b889e5-6b90-405c-8294-0d641a25c659', 'text', 'text', 'formfield.newsletter.text', 'Hiermit bestelle ich den Newsletter.<br/><small>(Ich habe die <a class=\"confirm-link\" href=\"#\" >Datenschutzerklärung</a> gelesen und akzeptiere diese)</small> ', 'Hiermit bestelle ich den Newsletter.<br/><small>(Ich habe die <a class=\"confirm-link\" href=\"#\" >Datenschutzerklärung</a> gelesen und akzeptiere diese)</small> ', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('51de5384-d9f8-488a-9a5d-a4ef416383c6', 'domain', 'operator', 'Ist gleich (==)', 'Ist gleich (==)', 'Ist gleich (==)', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('527bd4ca-1690-4723-999d-1e301a25c659', 'text', 'text', 'formular.checkout.cardempty.content', 'Der Warenkorb ist leer.', 'Der Warenkorb ist leer.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('527fbd7e-5e3c-4263-885d-13f41a25c659', 'text', 'text', 'formular.checkout.error.noorder.content', 'Es ist ein unerwarteter Fehler aufgetreten. Sollten Sie innerhalb der nächsten 60 Minuten keine Bestätigungsmail von uns erhalten, wenden Sie sich bitte direkt an uns. Sehen Sie in jedem Fall davon ab, die Zahlung erneut auszuführen um Doppelzahlungen zu vermeiden.\n\nWir bitten Sie die Unannehmlichkeiten zu entschuldigen!', 'Es ist ein unerwarteter Fehler aufgetreten. Sollten Sie innerhalb der nächsten 60 Minuten keine Bestätigungsmail von uns erhalten, wenden Sie sich bitte direkt an uns. Sehen Sie in jedem Fall davon ab, die Zahlung erneut auszuführen um Doppelzahlungen zu vermeiden.\n\nWir bitten Sie die Unannehmlichkeiten zu entschuldigen!', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('52aaeb09-4b3c-444b-a741-253e45bc8b55', 'domain', 'shippingoption', 'Als Download', 'Als Download', 'Als Download', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('52dd18b9-33b4-40d1-9d89-10a01a25c659', 'text', 'text', 'formular.pwlost.email.content', '<p>Sehr geehrte(r) :salutation,\n	<br>Sie haben über unsere Website ein neues Passwort angefordert. Aus Sicherheitsgründen klicken Sie bitte auf den folgenden Link. Erst dann können wir Ihnen per E-Mail Ihr neues Passwort zusenden.</p>\n\n<p><strong><a href=\":base_href:link\">Jetzt neues Passwort anfordern.</a></strong></p>\n', '<p>Sehr geehrte(r) :salutation,\n	<br>Sie haben über unsere Website ein neues Passwort angefordert. Aus Sicherheitsgründen klicken Sie bitte auf den folgenden Link. Erst dann können wir Ihnen per E-Mail Ihr neues Passwort zusenden.</p>\n\n<p><strong><a href=\":base_href:link\">Jetzt neues Passwort anfordern.</a></strong></p>\n', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('52e5bb9e-8c80-442d-8e10-26041a25c659', 'text', 'text', 'formular.pwlost.email.subject', 'Neues Passwort für :domain', 'Neues Passwort für :domain', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('52e5c48e-67ac-4c5b-8a8b-26041a25c659', 'text', 'text', 'user.sendnewpw.email.subject', 'Ihr neues Passwort für :domain', 'Ihr neues Passwort für :domain', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('52e5c67b-7314-47b9-b785-26041a25c659', 'text', 'text', 'user.sendnewpw.email.content', '<p>Sehr geehrte(r) :salutation,<br>Ihr neues Passwort lautet: :pw</p><p><a href=\"%3Abase_href%3Alink\">Hier</a> gelangen Sie zum <a href=\"%3Abase_href%3Alink\">Login</a>.</p>', '<p>Sehr geehrte(r) :salutation,<br>Ihr neues Passwort lautet: :pw</p><p><a href=\"%3Abase_href%3Alink\">Hier</a> gelangen Sie zum <a href=\"%3Abase_href%3Alink\">Login</a>.</p>', 1, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00');
INSERT INTO `texts` (`id`, `source`, `type`, `name`, `value`, `default_value`, `is_html`, `in_use`, `created`, `modified`) VALUES
('52e6184f-0274-4a32-8684-26041a25c659', 'text', 'text', 'formular.register.email.content', '<p>Sehr geehrte(r) :salutation,\n<br />\nvielen Dank für Ihre Anmeldung auf :domain. Um Ihren Zugang zu aktivieren, rufen Sie bitte den folgenden Link auf:</p>\n<p><strong><a href=\":base_href:link\">Jetzt Zugang aktivieren!</a></strong></p>\n\n ', '<p>Sehr geehrte(r) :salutation,\n<br />\nvielen Dank für Ihre Anmeldung auf :domain. Um Ihren Zugang zu aktivieren, rufen Sie bitte den folgenden Link auf:</p>\n<p><strong><a href=\":base_href:link\">Jetzt Zugang aktivieren!</a></strong></p>\n\n ', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('52e61893-f0b4-466a-8503-26041a25c659', 'text', 'text', 'formular.register.email.subject', 'Ihre Anmeldung auf :domain', 'Ihre Anmeldung auf :domain', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('52e7b543-cb6c-47e9-8f64-26041a25c659', 'text', 'text', 'formular.base.email.subject', 'E-Mail von :domain', 'E-Mail von :domain', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('52e7bbd3-0684-4f88-a61e-26041a25c659', 'text', 'text', 'email.grusszeile', '<p><br></p><p>Mit freundlichen Grüßen,</p><p>:from_name</p>', '<p><br></p><p>Mit freundlichen Grüßen,</p><p>:from_name</p>', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('52e8b845-2c7c-4d6a-b8d0-26041a25c659', 'text', 'text', 'formular.pwlost.content', '<p>\nBitte rufen Sie Ihre E-Mails ab und folgen den weiteren Hinweisen. \n</p>', '<p>\nBitte rufen Sie Ihre E-Mails ab und folgen den weiteren Hinweisen. \n</p>', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('52eb520b-66ac-4561-b09d-187080128e6c', 'locales', '__(Texte)', 'Benutzer mit angegeben Daten nicht vorhanden!', 'Benutzer mit angegeben Daten nicht vorhanden!', 'Benutzer mit angegeben Daten nicht vorhanden!', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('52f4a5e2-7c20-4659-a458-24d01a25c659', 'text', 'text', 'invoice.biller', '', '', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('52f4a700-277c-454a-bf44-24d01a25c659', 'text', 'text', 'invoice.thanks', 'Wir danken Ihnen für Ihren Einkauf.', 'Wir danken Ihnen für Ihren Einkauf.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('52f4a7f7-453c-4649-a9db-24d01a25c659', 'text', 'text', 'invoice.paymentinfo', 'Bitte überweisen Sie Ihren Rechnungsbetrag in Höhe von :total_amount_with_shipping_costs_formated  innerhalb von 7 Tagen mit dem Verwendungszweck \"Gutschein RNR :rnr\" auf das unten angegebene Konto.', 'Bitte überweisen Sie Ihren Rechnungsbetrag in Höhe von :total_amount_with_shipping_costs_formated  innerhalb von 7 Tagen mit dem Verwendungszweck \"Gutschein RNR :rnr\" auf das unten angegebene Konto.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('52f4a84d-8cf8-49ab-8527-24d01a25c659', 'text', 'text', 'invoice.paid', 'Den Rechnungsbetrag haben wir dankend via :paymentmethod erhalten.', 'Den Rechnungsbetrag haben wir dankend via :paymentmethod erhalten.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('530576ac-4a40-49ea-999e-a3fccf880337', 'domain', 'formfield', 'Fileinput (Single)', 'Fileinput (Single)', 'Fileinput (Single)', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('53720a77-3ba5-489e-a590-52824def3028', 'domain', 'country', 'Myanmar', 'Myanmar', 'Myanmar', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('53aabf3b-f442-47a0-9a8f-f513950f5b4c', 'domain', 'language', 'Rätoromanisch', 'Rätoromanisch', 'Rätoromanisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('53be9117-9104-4e22-a8e6-9321221f0ca7', 'domain', 'language', 'Kroatisch', 'Kroatisch', 'Kroatisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('544a2ebc-9d94-48ca-abf3-366e2efc16a2', 'text', 'text', 'formular.checkout.content', '<p>In wenigen Minuten erhalten Sie Ihren Gutschein per E-Mail.<p/><h3>Viel Freude beim Verschenken!</h3>', '<p>In wenigen Minuten erhalten Sie Ihren Gutschein per E-Mail.<p/><h3>Viel Freude beim Verschenken!</h3>', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('544a2ee9-7680-4854-82a4-366e2efc16a2', 'text', 'text', 'formular.checkout.title', 'Vielen Dank für Ihre Bestellung!', 'Vielen Dank für Ihre Bestellung!', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('5461d076-43b6-4b7e-8762-bf08fcd8a8f0', 'domain', 'country', 'Brasilien', 'Brasilien', 'Brasilien', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('5476dc1c-6749-4c53-a619-696baaa33a6d', 'domain', 'country', 'Palau', 'Palau', 'Palau', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('5478a3c8-7992-49e4-b4a4-129d44b4505f', 'domain', 'country', 'Australien', 'Australien', 'Australien', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('551e6e9b-89c8-4f83-9454-ac006c1f101b', 'domain', 'berufserfahrung', '6 Jahre', '6 Jahre', '6 Jahre', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('55376cf6-bc47-45d1-9491-a3cd3506bd7b', 'domain', 'country', 'Usbekistan', 'Usbekistan', 'Usbekistan', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('5558907c-100d-4f61-b158-0429f0cc9379', 'domain', 'language', 'Laotisch', 'Laotisch', 'Laotisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('55cde88e-cd8d-4247-b33c-710758fe59ce', 'domain', 'language', 'Somalisch', 'Somalisch', 'Somalisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('565f49ed-083a-4dff-80b2-7ac370e8e887', 'locales', '__(Texte)', 'Datum', 'Datum', 'Datum', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('56bc3007-efdc-4885-b92f-305819e73b10', 'domain', 'country', 'Marshallinseln', 'Marshallinseln', 'Marshallinseln', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('56bd5e07-845b-4ef2-b48c-6a22587932ed', 'domain', 'country', 'Nepal', 'Nepal', 'Nepal', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('56c7773c-b1ad-4efa-b64b-df52c5ed6975', 'domain', 'interval', 'täglich um 16:00', 'täglich um 16:00', 'täglich um 16:00', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('57957741-7462-4bcf-afe4-4f5f0b6d1eda', 'domain', 'interval', 'täglich um 02:30', 'täglich um 02:30', 'täglich um 02:30', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('57bce4f9-06a5-4ba6-8f94-2be22d16411d', 'locales', '__(Texte)', 'Notwendig', 'Notwendig', 'Notwendig', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('588f4e1d-7316-4d32-95dc-5958fe8ac7ba', 'locales', '__(Texte)', 'Wird dieser Newsletter nicht richtig angezeigt, klicken Sie bitte hier.', 'Wird dieser Newsletter nicht richtig angezeigt, klicken Sie bitte hier.', 'Wird dieser Newsletter nicht richtig angezeigt, klicken Sie bitte hier.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('593c0fab-5c17-4d7e-9369-6f9bb73c81b3', 'domain', 'country', 'Laos', 'Laos', 'Laos', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('5997cfcc-d442-4acc-831a-45ac6d9b9a0e', 'domain', 'country', 'St. Vincent und die Grenadinen (GB)', 'St. Vincent und die Grenadinen (GB)', 'St. Vincent und die Grenadinen (GB)', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('59a5cc53-a669-4661-b738-ec00226d142e', 'locales', 'cake', 'Cannot modify row: a constraint for the `{0}` association fails.', 'Cannot modify row: a constraint for the `{0}` association fails.', 'Cannot modify row: a constraint for the `{0}` association fails.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('59a66614-c31f-4790-aec6-3e217ab770a8', 'locales', 'cake', 'about a minute ago', 'about a minute ago', 'about a minute ago', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('59b1db03-7ccd-4390-940f-13cd8b548312', 'domain', 'country', 'Tonga', 'Tonga', 'Tonga', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('59f6852c-49a6-4182-8566-a5cfbe2f78a5', 'domain', 'country', 'Mayotte', 'Mayotte', 'Mayotte', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('5a21f089-d095-4b72-bd70-bc04dd3f6a50', 'locales', 'cake', '{0} from now', '{0} from now', '{0} from now', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('5a521851-0003-47a8-81f2-8a31ad9f7556', 'locales', '__(Texte)', 'Bitte treffen Sie eine Auswahl.', 'Bitte treffen Sie eine Auswahl.', 'Bitte treffen Sie eine Auswahl.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('5ab17512-cc95-43d6-8018-73d3ca682080', 'domain', 'country', 'Jemen', 'Jemen', 'Jemen', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('5ad2b647-c01f-4542-95b9-d414d64a9725', 'domain', 'formfield', 'Trennlinie', 'Trennlinie', 'Trennlinie', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('5b05f3f0-abb6-468b-9be0-a10afd851f67', 'locales', '__(Texte)', 'April', 'April', 'April', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('5bb8b3b3-529f-44f9-842c-3ea93fdd316c', 'locales', '__(Texte)', 'zurück', 'zurück', 'zurück', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('5bc231bc-af47-43a7-9c84-37c59934ced6', 'domain', 'language', 'Tigrinja', 'Tigrinja', 'Tigrinja', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('5bfe157a-d52a-484b-a32f-5c323409a1d7', 'locales', '__(Texte)', 'Erste Seite', 'Erste Seite', 'Erste Seite', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('5c147176-3bd1-4a89-ad9e-3d8194eeb788', 'domain', 'language', 'Interlingue', 'Interlingue', 'Interlingue', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('5c41a5d3-ba55-4e62-8ca9-f231147dab6d', 'domain', 'language', 'Aserbaidschanisch', 'Aserbaidschanisch', 'Aserbaidschanisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('5c4bd575-6a42-4348-b403-aa19040a6a71', 'locales', '__(Texte)', 'View', 'View', 'View', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('5cd95d03-b6c5-4bf5-a32c-75cea642305a', 'domain', 'language', 'Aymara', 'Aymara', 'Aymara', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('5d8d231a-e46b-4aac-a262-e2c441e570b7', 'domain', 'country', 'Panama', 'Panama', 'Panama', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('5f031e9d-06e9-4283-92ae-8fef97b73e2c', 'locales', '__(Texte)', 'Oktober', 'Oktober', 'Oktober', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('5f30a44c-bb12-4af1-8290-2bf5facde2f5', 'domain', 'language', 'Singhalesisch', 'Singhalesisch', 'Singhalesisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('5f61693e-0c4a-4139-a7c1-b9afd81b9e67', 'domain', 'country', 'Gambia', 'Gambia', 'Gambia', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('5f8845d3-bfd4-4390-b162-b3098d31f6fd', 'domain', 'country', 'Benin', 'Benin', 'Benin', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('60154aa6-ed1d-4b7a-9f1e-50b6d8e26a30', 'locales', 'cake', '{0,number,#,###.##} MB', '{0,number,#,###.##} MB', '{0,number,#,###.##} MB', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('60899b12-8d9f-4c7d-b285-b17b8331dd2b', 'domain', 'country', 'Mali, Republik', 'Mali, Republik', 'Mali, Republik', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('60aa7349-5429-4bba-9c9a-c2b884b74aaf', 'domain', 'familystatus', 'verheiratet', 'verheiratet', 'verheiratet', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('60b95ddb-bbad-40bb-9967-b50e5c149262', 'domain', 'country', 'Mosambik', 'Mosambik', 'Mosambik', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('617a8ae8-2012-4c9b-a452-9d03a1523159', 'domain', 'country', 'Italien', 'Italien', 'Italien', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('61ac5f2b-f08f-4967-9345-fc63f1c0327a', 'domain', 'formfield', 'Textarea', 'Textarea', 'Textarea', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('61ae7b54-36f6-4fa1-af92-91ebeccbdde9', 'domain', 'language', 'Sango', 'Sango', 'Sango', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('61e012f2-4deb-41fb-9b92-57b514cfc9d1', 'domain', 'berufserfahrung', '4 Jahre', '4 Jahre', '4 Jahre', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('6242b16e-ed10-4448-b0b7-f255a6be0284', 'domain', 'country', 'Mikronesien, Föderierte Staaten von', 'Mikronesien, Föderierte Staaten von', 'Mikronesien, Föderierte Staaten von', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('625be330-0482-4fde-b4ef-7635210a6280', 'domain', 'operator', 'Ist kleiner oder gleich (<=)', 'Ist kleiner oder gleich (<=)', 'Ist kleiner oder gleich (<=)', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('625de1b9-34f5-42b8-a0b9-deb30f707881', 'locales', '__(Texte)', 'Sie haben sich erfolgreich abgemeldet.', 'Sie haben sich erfolgreich abgemeldet.', 'Sie haben sich erfolgreich abgemeldet.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('63624bb7-b4e1-4ffe-8498-8b02ecf22ed1', 'locales', '__(Texte)', 'Downloads', 'Downloads', 'Downloads', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('639846f5-cf19-4ee6-98cd-717a5499a33d', 'domain', 'country', 'Kongo, Republik', 'Kongo, Republik', 'Kongo, Republik', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('64b3ebb1-dd38-4365-9a79-8d9731435d53', 'domain', 'country', 'Französisch-Polynesien', 'Französisch-Polynesien', 'Französisch-Polynesien', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('653fb134-ce18-4feb-a045-b38418a6054c', 'domain', 'language', 'Zhuang', 'Zhuang', 'Zhuang', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('660c18e5-cedd-11e2-a31e-c48508c1745f', 'text', 'text', 'newsletter.bestaetigung.email.subject', 'Bitte bestätigen Sie Ihre Anmeldung für den :from_name Newsletter', 'Bitte bestätigen Sie Ihre Anmeldung für den :from_name Newsletter', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('661ea579-73d2-4da0-aa74-d5f4a16ad380', 'domain', 'language', 'Belorussisch', 'Belorussisch', 'Belorussisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('661ff75d-1646-4af9-820d-cf5cfd969e0b', 'domain', 'interval', 'täglich um 17:30', 'täglich um 17:30', 'täglich um 17:30', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('66bed301-75e5-4aef-9729-7ffdb421d557', 'locales', '__(Texte)', 'formular.formular.email.subject', 'E-Mail von :domain 2', 'E-Mail von :domain 2', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('6788fc68-04c0-4439-b3f8-fa7c64dbd8d2', 'domain', 'formfield', 'Headline (h4)', 'Headline (h4)', 'Headline (h4)', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('67b5af56-cc17-43ca-9077-beb6c223341d', 'domain', 'berufserfahrung', '9 oder mehr Jahre', '9 oder mehr Jahre', '9 oder mehr Jahre', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('67def282-dc2b-413b-aeab-e01023860a7f', 'locales', 'cake', '{0} month', '{0} month', '{0} month', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('68152ed9-bdec-42da-b01b-500f41f9b70b', 'locales', '__(Texte)', 'Newsletter abbestellen', 'Newsletter abbestellen', 'Newsletter abbestellen', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('6861dea2-5cb6-40ca-86a4-364042b74dc8', 'domain', 'country', 'Diego Garcia', 'Diego Garcia', 'Diego Garcia', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('687350d7-5179-4d45-a6ef-dae171afbc1a', 'domain', 'country', 'Färöer', 'Färöer', 'Färöer', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('68a214f1-d2cd-4327-b7cf-c77300db6961', 'locales', '__(Texte)', 'Abbrechen', 'Abbrechen', 'Abbrechen', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('68f501e3-7349-4b19-96f9-6d89d287fd9f', 'domain', 'interval', 'täglich um 21:00', 'täglich um 21:00', 'täglich um 21:00', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('68ff59db-865c-4d87-a597-8c529e3a7ffa', 'domain', 'language', 'Mongolisch', 'Mongolisch', 'Mongolisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('69459495-0e78-4e9f-aca3-0adc698bdc30', 'domain', 'column', '2/12', '2/12', '2/12', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('6a54e2e5-132d-4415-9a89-93a0d1d654d2', 'domain', 'language', 'Deutsch', 'Deutsch', 'Deutsch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('6c271045-6823-48d6-8888-7908154f6c74', 'domain', 'pricesuffix', 'pro Person', 'pro Person', 'pro Person', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('6d233373-6dbd-4327-9a77-651c807eaf51', 'locales', 'cake', '{0,number,#,###.##} KB', '{0,number,#,###.##} KB', '{0,number,#,###.##} KB', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('6de6015c-86a4-4207-8ef6-96c98a1aa9ca', 'domain', 'language', 'Malaysisch', 'Malaysisch', 'Malaysisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('6e658a31-2ee7-4f1c-b09c-2030a4aadee7', 'domain', 'country', 'Philippinen', 'Philippinen', 'Philippinen', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('6e6acb34-3c07-4b48-8c76-cb2f31e5f1cb', 'domain', 'column', '10/12', '10/12', '10/12', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('6e841e92-f282-4562-81f8-3d5b6603af68', 'domain', 'country', 'Liberia, Republik', 'Liberia, Republik', 'Liberia, Republik', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('6ec109cf-aef7-4d88-b40c-9f3349334b20', 'domain', 'country', 'Oman', 'Oman', 'Oman', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('6f8e5082-3115-482f-a7f8-98cf980d8d14', 'domain', 'country', 'Bouvetinsel', 'Bouvetinsel', 'Bouvetinsel', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('6f928c1b-9564-4740-a680-01023b409473', 'domain', 'operator', 'Ist kleiner (<)', 'Ist kleiner (<)', 'Ist kleiner (<)', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('7067ccd3-df62-465d-a478-d3d6260e5525', 'locales', '__(Texte)', 'Back', 'Back', 'Back', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('70c71e8e-5fcd-475e-a865-e2bb5ecfa422', 'domain', 'cc', 'American Express', 'American Express', 'American Express', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('70f9c251-c36d-499f-9640-38e211510e92', 'domain', 'gender', 'männlich', 'männlich', 'männlich', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('71464001-535d-4930-8b4a-a71a91e44c0a', 'domain', 'shippingcost', '4.90', '4.90', '4.90', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('717b246e-11f9-43d7-aaf6-ade0ae7af8c0', 'locales', '__(Texte)', 'Failed to open input stream.', 'Failed to open input stream.', 'Failed to open input stream.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('71c54f12-9811-4feb-b780-6b5ae9747c71', 'domain', 'country', 'Frankreich', 'Frankreich', 'Frankreich', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('71ce6a92-71e4-47af-92b1-305f0abec1e9', 'domain', 'country', 'Katar', 'Katar', 'Katar', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('75763c7c-345d-4537-8808-317e221a7494', 'domain', 'language', 'Ukrainisch', 'Ukrainisch', 'Ukrainisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('75b18e74-71db-4067-ab4a-01bc8d1c9e67', 'domain', 'language', 'Vietnamesisch', 'Vietnamesisch', 'Vietnamesisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('75baf932-4a43-4c5b-999f-053640a9d7b1', 'locales', 'cake', '{0} after', '{0} after', '{0} after', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('765bb53f-6d1e-4c8e-8631-bbc09672531d', 'domain', 'language', 'Malajalam', 'Malajalam', 'Malajalam', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('771c4762-6caf-4afe-932e-a6f5df2fbaa3', 'domain', 'interval', 'täglich um 16:30', 'täglich um 16:30', 'täglich um 16:30', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('787cdf78-9bf7-46f4-bc8e-0077b6071266', 'locales', '__(Texte)', 'Der Gutschein enthält', 'Der Gutschein enthält', 'Der Gutschein enthält', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('78cbd177-b5a4-4a94-a0aa-e446dfc6e7da', 'domain', 'country', 'Äthiopien', 'Äthiopien', 'Äthiopien', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('7979d2d7-abd3-46ed-9359-0228b923a75d', 'locales', 'cake', 'This field cannot be left empty', 'This field cannot be left empty', 'This field cannot be left empty', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('7a76c8c1-8985-4f91-b72b-1c9aae8a0b90', 'domain', 'country', 'Vereinigtes Königreich von Großbritannien und Nordirland', 'Vereinigtes Königreich von Großbritannien und Nordirland', 'Vereinigtes Königreich von Großbritannien und Nordirland', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('7a852540-6090-4fc5-9715-4aa5e54f5a3a', 'locales', 'system', 'Youtube API nicht erreichbar oder Video nicht existent.', 'Youtube API nicht erreichbar oder Video nicht existent.', 'Youtube API nicht erreichbar oder Video nicht existent.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('7ad0b4ea-f6d0-4fc6-9410-e974a20eedeb', 'domain', 'country', 'Guinea-Bissau, Republik', 'Guinea-Bissau, Republik', 'Guinea-Bissau, Republik', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('7b38be79-5ca0-4c40-9e8f-6a541ed88f37', 'domain', 'language', 'Moldavisch', 'Moldavisch', 'Moldavisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('7be050c1-3262-445c-bac9-e548dec83c76', 'locales', 'system', 'Youtube ID konnte nicht ausgelesen werden.', 'Youtube ID konnte nicht ausgelesen werden.', 'Youtube ID konnte nicht ausgelesen werden.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('7bf0b3a9-28c0-43d4-8951-cd3a6ff2661b', 'domain', 'language', 'Kurdisch', 'Kurdisch', 'Kurdisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('7d52d582-ca8d-4d79-89c0-89c250f96502', 'domain', 'language', 'Estnisch', 'Estnisch', 'Estnisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('7e074815-73b2-4be5-9d4e-9ff09aa26e72', 'domain', 'country', 'Kasachstan', 'Kasachstan', 'Kasachstan', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('7f839f5e-24b5-4fe4-ad5c-e295933394f9', 'domain', 'country', 'Aland', 'Aland', 'Aland', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('7f9fa353-2c94-42e0-8382-6a18be4343ae', 'domain', 'country', 'Papua-Neuguinea', 'Papua-Neuguinea', 'Papua-Neuguinea', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('7fdc76ee-f9cb-42f0-9a19-b972bf1d4c71', 'domain', 'mwst', '7%', '7%', '7%', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('805314b4-ffb2-42c5-9620-8cd58b9008d7', 'domain', 'country', 'Burkina Faso', 'Burkina Faso', 'Burkina Faso', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('80cc0f12-cad8-4e6d-92a2-4f961f11b25d', 'locales', '__(Texte)', 'Gesamtpreis', 'Gesamtpreis', 'Gesamtpreis', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('80f73195-d7b9-442d-9089-3774669a7567', 'locales', '__(Texte)', 'weiter', 'weiter', 'weiter', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('811c80e7-5520-4b5e-9496-7e9c1c979a87', 'locales', '__(Texte)', 'Statistik', 'Statistik', 'Statistik', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('8165db9f-52f6-4325-9d27-35a2f59b1247', 'locales', '__(Texte)', 'd.m.Y', 'd.m.Y', 'd.m.Y', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('81680555-d552-4556-983c-15b62c0c2ca2', 'locales', '__(Texte)', 'Startadresse', 'Startadresse', 'Startadresse', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('818e4f60-c4e4-46fa-8f95-3b3ee4b66848', 'domain', 'language', 'Marathi', 'Marathi', 'Marathi', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('81912b30-a27b-4e62-b728-16629b9b6223', 'domain', 'country', 'Tokelau', 'Tokelau', 'Tokelau', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('81b5c3aa-d77a-4a6c-82ad-346a141a129f', 'domain', 'formfield', 'Headline (h3)', 'Headline (h3)', 'Headline (h3)', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('81ce7dc2-a862-4dd9-969b-111a15a4b8b7', 'domain', 'title', 'Dr.', 'Dr.', 'Dr.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('82f7ff06-ef94-4594-bffc-a3e2458f0b4c', 'domain', 'language', 'Hebräisch', 'Hebräisch', 'Hebräisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('83021593-a72a-47d3-8f7f-6239de13dd40', 'domain', 'interval', 'täglich um 22:00', 'täglich um 22:00', 'täglich um 22:00', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('830ffa47-38bc-4e93-a5b9-3165ed68f29b', 'locales', '__(Texte)', 'Bitte haben Sie einen Moment Geduld. Die Gutschein Vorschau steht gleich für Sie bereit.', 'Bitte haben Sie einen Moment Geduld. Die Gutschein Vorschau steht gleich für Sie bereit.', 'Bitte haben Sie einen Moment Geduld. Die Gutschein Vorschau steht gleich für Sie bereit.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('839c84dd-e1d6-4625-b5a2-738d464babf7', 'domain', 'language', 'Assamesisch', 'Assamesisch', 'Assamesisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('83bbd648-b12f-44c3-b2d4-42a62e021938', 'domain', 'country', 'Cookinseln', 'Cookinseln', 'Cookinseln', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('83c863df-74fa-4c44-b6d6-41468f489495', 'domain', 'language', 'Abchasisch', 'Abchasisch', 'Abchasisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('849ce63e-72d5-4369-90ef-83f1af18190c', 'domain', 'country', 'Grenada', 'Grenada', 'Grenada', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('850dccf3-f483-4495-ab6e-fe420a43951f', 'locales', '__(Texte)', 'Diese Website nutzt Cookies, um bestmögliche Funktionalität bieten zu können.', 'Diese Website nutzt Cookies, um bestmögliche Funktionalität bieten zu können.', 'Diese Website nutzt Cookies, um bestmögliche Funktionalität bieten zu können.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('851407b0-7e45-4ac5-ba1a-417a770f95e4', 'domain', 'country', 'Schweden', 'Schweden', 'Schweden', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('855441c3-0b10-4777-a59e-02d5df972bec', 'domain', 'language', 'Sezuan', 'Sezuan', 'Sezuan', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('85734c61-6733-443c-9a3c-84b3a1954056', 'domain', 'interval', 'täglich um 20:00', 'täglich um 20:00', 'täglich um 20:00', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('859186f8-3565-45e9-827f-e3dc09c103f7', 'locales', '__(Texte)', 'Rechnungs-Nr.', 'Rechnungs-Nr.', 'Rechnungs-Nr.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('85d28695-7c95-4e1c-af9e-c011d0701a1d', 'domain', 'country', 'Dänemark', 'Dänemark', 'Dänemark', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('862320dc-0bf3-4f8c-8ff8-2a1a29c8d265', 'domain', 'country', 'Westsahara', 'Westsahara', 'Westsahara', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('8645ff4c-f68c-4ba2-985b-c43c991bc60f', 'domain', 'country', 'El Salvador', 'El Salvador', 'El Salvador', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('86a9f3dc-ce37-4727-821d-0f7e8260854e', 'domain', 'paymentmethod', 'Vorkasse', 'Vorkasse', 'Vorkasse', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('86cfd64f-55d8-44c9-9c7b-c938e288128d', 'locales', '__(Texte)', 'Es sind Fehler aufgetreten, bitte überprüfen Sie Ihre Eingaben.', 'Es sind Fehler aufgetreten, bitte überprüfen Sie Ihre Eingaben.', 'Es sind Fehler aufgetreten, bitte überprüfen Sie Ihre Eingaben.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('870d8439-a665-4646-a409-142885ee1c1e', 'locales', 'cake', 'in about a day', 'in about a day', 'in about a day', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('87857837-87c4-42eb-aebf-62621d2d7722', 'locales', '__(Texte)', 'Vorschau', 'Vorschau', 'Vorschau', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('878625ad-5170-43e7-bf61-1adf85bf02fd', 'locales', '__(Texte)', 'Gültig bis ', 'Gültig bis ', 'Gültig bis ', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('8ae804cb-b6b2-4ec9-9f21-ccc1c511389f', 'locales', 'cake', '{0} hour', '{0} hour', '{0} hour', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('8af69e2d-1de3-46b8-a355-d9a613bd9155', 'locales', '__(Texte)', 'search.noresults.headline', 'search.noresults.headline', 'search.noresults.headline', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('8b34a6f8-c3f8-4cca-9b65-c35c94a44a65', 'locales', 'cake', 'in about a year', 'in about a year', 'in about a year', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('8b3d4711-b4b0-4430-88bd-77b6b7641d9a', 'domain', 'formfield', 'Headline (h5)', 'Headline (h5)', 'Headline (h5)', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('8bb65e0b-901c-4edc-bbca-7f90f64512f2', 'domain', 'language', 'Amharisch', 'Amharisch', 'Amharisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('8bd61acc-25aa-4d88-866d-3f9321c6a939', 'domain', 'country', 'Sierra Leone, Republik', 'Sierra Leone, Republik', 'Sierra Leone, Republik', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('8bf7edf7-58de-4443-aab7-681e84534736', 'domain', 'country', 'Deutschland', 'Deutschland', 'Deutschland', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('8caf11e2-46e9-4235-b99a-c6717ab3af0c', 'locales', 'cake', 'about a year ago', 'about a year ago', 'about a year ago', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('8cc8cec2-c201-4ebf-92e7-421b1ccd9535', 'locales', 'system', 'Der Test-Newsletter wurde versandt.', 'Der Test-Newsletter wurde versandt.', 'Der Test-Newsletter wurde versandt.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('8d8bf15a-e225-4137-a66f-75cb3fa31d67', 'locales', '__(Texte)', 'Sehr geehrte/r', 'Sehr geehrte/r', 'Sehr geehrte/r', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('8dd1f94d-21d9-4dd4-a017-0393e387aa3e', 'locales', '__(Texte)', 'Was suchen Sie?', 'Was suchen Sie?', 'Was suchen Sie?', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('8dd711ad-3104-4602-8a26-8395c8dab148', 'domain', 'country', 'Kuwait', 'Kuwait', 'Kuwait', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('8e32aead-3550-4a15-95c6-cb53db625969', 'locales', '__(Texte)', 'Letzte Seite', 'Letzte Seite', 'Letzte Seite', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('8e67d3b6-e45e-4682-ab4a-46f193cc3bf3', 'locales', '__(Texte)', 'Zzgl. MwSt.', 'Zzgl. MwSt.', 'Zzgl. MwSt.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('8ee92a3f-ebff-42ee-9af1-ba86dd06c533', 'locales', 'system', 'Vimeo API nicht erreichbar oder Video nicht existent.', 'Vimeo API nicht erreichbar oder Video nicht existent.', 'Vimeo API nicht erreichbar oder Video nicht existent.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('8f681986-8877-45b0-be65-ebc21f411a1b', 'locales', '__(Texte)', 'Google Analytics wurde deaktiviert.', 'Google Analytics wurde deaktiviert.', 'Google Analytics wurde deaktiviert.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('8f847aba-a6f0-4af0-9934-5c5ba802e620', 'domain', 'country', 'Vanuatu', 'Vanuatu', 'Vanuatu', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('8fc296ea-1a1a-4137-a81c-8712259861f8', 'locales', 'system', 'Die Daten konnten nicht gelöscht werden.', 'Die Daten konnten nicht gelöscht werden.', 'Die Daten konnten nicht gelöscht werden.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('8fc6d563-65ef-40c9-bc5c-459d7cf27091', 'domain', 'language', 'Joruba', 'Joruba', 'Joruba', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('90201b92-c7e4-4787-beec-3ae265b44416', 'domain', 'country', 'Kongo, Demokratische Republik', 'Kongo, Demokratische Republik', 'Kongo, Demokratische Republik', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('90832e36-211c-4336-b38e-a2b758691fb6', 'domain', 'country', 'Türkei', 'Türkei', 'Türkei', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('90fe8eb3-ce89-4039-9b35-8cd086236a7c', 'domain', 'country', 'Slowenien', 'Slowenien', 'Slowenien', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('912c1075-528d-4ff0-87b6-c9606229423c', 'domain', 'country', 'Malaysia', 'Malaysia', 'Malaysia', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('9148229e-864d-4b64-a8cb-21d0fc4a81b9', 'domain', 'interval', 'täglich um 05:00', 'täglich um 05:00', 'täglich um 05:00', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('91577f36-ab96-4b00-b927-eb0e0d45732f', 'locales', '__(Texte)', 'Bis', 'Bis', 'Bis', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('91b19f0b-0a66-475f-8721-96a9f4a1b742', 'domain', 'language', 'Biharisch', 'Biharisch', 'Biharisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('92110f5c-4b77-4504-a8e8-2f6543dac682', 'domain', 'country', 'Norwegen', 'Norwegen', 'Norwegen', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('924f64e6-1b66-4ec0-bfa7-45cfacd70079', 'domain', 'country', 'Nördliche Marianen', 'Nördliche Marianen', 'Nördliche Marianen', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('929c5f2a-fc1a-4a08-83ab-4287087df660', 'domain', 'country', 'Seychellen, Republik der', 'Seychellen, Republik der', 'Seychellen, Republik der', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('92ae07fb-ba91-4fca-adac-d17537569163', 'domain', 'language', 'Kaschmirisch', 'Kaschmirisch', 'Kaschmirisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('92bb14b4-c9c1-415a-aaef-67448d06dfaa', 'domain', 'country', 'Falklandinseln', 'Falklandinseln', 'Falklandinseln', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('938676d4-cd9d-4e41-a80f-973a06ab9820', 'domain', 'country', 'Amerikanisch-Samoa', 'Amerikanisch-Samoa', 'Amerikanisch-Samoa', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('93fb1eca-da01-4982-8067-aa161b56ef54', 'domain', 'paymentmethod', 'Kreditkarte', 'Kreditkarte', 'Kreditkarte', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('94a50449-4500-4fe0-8b6e-f5a8586ec3dd', 'domain', 'mwst', '19%', '19%', '19%', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('950a51e6-2e88-4704-827e-b3d2df8113c1', 'domain', 'country', 'Angola', 'Angola', 'Angola', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('95161072-949e-4648-937b-1d748073e189', 'locales', 'system', 'Der Newsletter konnte nicht versandt werden.', 'Der Newsletter konnte nicht versandt werden.', 'Der Newsletter konnte nicht versandt werden.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('9522edd4-0d8d-4f82-ac01-1415110d632c', 'locales', '__(Texte)', 'Mehr Informationen dazu', 'Mehr Informationen dazu', 'Mehr Informationen dazu', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('955dbce2-f8aa-44eb-ba27-90faf91678cb', 'domain', 'country', 'Togo, Republik', 'Togo, Republik', 'Togo, Republik', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('96106b7c-1d39-40f8-b20c-9c6815503df3', 'domain', 'language', 'Wolof', 'Wolof', 'Wolof', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('9649b07f-80a1-46d6-b674-10f1e54f84f1', 'domain', 'language', 'Hindi', 'Hindi', 'Hindi', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('971cfaf0-9e85-4f29-9f3d-905fe20d9599', 'domain', 'country', 'Ghana, Republik', 'Ghana, Republik', 'Ghana, Republik', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('97a64801-2c6b-4ba3-8bde-b5b933e0f30c', 'domain', 'country', 'Bhutan', 'Bhutan', 'Bhutan', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('97d3b399-f3f8-421d-832a-dc8cc2638b93', 'locales', '__(Texte)', 'Die angeforderte Adresse {0} wurde nicht gefunden.', 'Die angeforderte Adresse {0} wurde nicht gefunden.', 'Die angeforderte Adresse {0} wurde nicht gefunden.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('98d0d6fe-dee9-4837-87e0-541991564cef', 'domain', 'column', '8/12', '8/12', '8/12', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('998a0238-6f27-4e75-b8f0-cafcd87e890d', 'domain', 'country', 'Costa Rica', 'Costa Rica', 'Costa Rica', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('99925fab-abce-4c39-9b4b-3d809c120fd9', 'domain', 'country', 'Vereinigte Staaten von Amerika', 'Vereinigte Staaten von Amerika', 'Vereinigte Staaten von Amerika', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('99c08b00-f91e-4ca5-a98e-e256454141f9', 'locales', 'cake', 'in about a minute', 'in about a minute', 'in about a minute', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('9aa5e6ce-7877-4006-b234-1f8134277bbd', 'locales', '__(Texte)', 'Du bist hier', 'Du bist hier', 'Du bist hier', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('9af5c371-f746-4d74-9e32-b42b802d7a94', 'domain', 'language', 'Isländisch', 'Isländisch', 'Isländisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('9b032836-5578-4272-8470-dbf4054d622f', 'domain', 'interval', 'täglich um 18:30', 'täglich um 18:30', 'täglich um 18:30', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('9b21a0ee-b51b-4f27-8ad0-da8529efd4b1', 'domain', 'language', 'Kalaallisut (Grönländisch)', 'Kalaallisut (Grönländisch)', 'Kalaallisut (Grönländisch)', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('9b433b41-9589-470a-b1f3-d220494d78dd', 'domain', 'country', 'Serbien und Montenegro', 'Serbien und Montenegro', 'Serbien und Montenegro', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('9b6223c5-630a-4ce9-aa33-7563778ecbe4', 'locales', 'system', 'Vimeo ID konnte nicht ausgelesen werden.', 'Vimeo ID konnte nicht ausgelesen werden.', 'Vimeo ID konnte nicht ausgelesen werden.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('9b765341-b28a-42c0-ae10-336e6a059ce2', 'locales', 'cake', 'in about a week', 'in about a week', 'in about a week', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('9b784685-76fe-4e38-bf9e-ef5b81bd57a4', 'locales', '__(Texte)', 'November', 'November', 'November', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('9b7d042e-f7f5-477e-aed1-5c8686af786e', 'domain', 'language', 'Orija', 'Orija', 'Orija', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('9c50dc57-e4f3-43bc-9999-66d2194e4903', 'domain', 'language', 'Tongaisch', 'Tongaisch', 'Tongaisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('9cb161c5-cf0c-4865-ad2b-476f977da819', 'domain', 'country', 'Union der Sozialistischen Sowjetrepubliken', 'Union der Sozialistischen Sowjetrepubliken', 'Union der Sozialistischen Sowjetrepubliken', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('9d47c946-769d-48ec-b030-b3a2db209740', 'domain', 'country', 'Guam', 'Guam', 'Guam', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('9d678e8c-6e91-4f11-a492-d044776d156b', 'locales', 'cake', '{0} day', '{0} day', '{0} day', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('9e0293c1-cea9-4275-bd83-23ecdf1ea5ee', 'domain', 'language', 'Dänisch', 'Dänisch', 'Dänisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('9e3a0cde-b033-4639-b72c-4785a02f55e2', 'locales', '__(Texte)', 'Termine einschränken', 'Termine einschränken', 'Termine einschränken', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('9e86fa7e-9ef9-4f48-b570-76a4fa3ecfcd', 'domain', 'column', '4/12', '4/12', '4/12', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('9e91e1a0-24a5-4730-983d-bb57cd46873c', 'domain', 'formfield', 'Zeit', 'Zeit', 'Zeit', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('9f19c082-e797-4a1e-954f-eec85095630a', 'locales', '__(Texte)', 'Warenkorb', 'Warenkorb', 'Warenkorb', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('9f3dbe27-2500-4d78-8713-14daf90bdbc7', 'domain', 'country', 'Mauretanien', 'Mauretanien', 'Mauretanien', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('9f6ffd68-cc24-4c70-bc5c-a0ebde20c0d2', 'domain', 'language', 'Guarani', 'Guarani', 'Guarani', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('9fc892c9-56d0-43c2-83af-e53c33a1518a', 'domain', 'country', 'Montserrat', 'Montserrat', 'Montserrat', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('a005ec02-1f29-4b82-aa9c-f24de80cf272', 'domain', 'language', 'Tschechisch', 'Tschechisch', 'Tschechisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('a013e202-1f86-4cb7-9dbb-bb93a56b2304', 'domain', 'interval', 'täglich um 23:00', 'täglich um 23:00', 'täglich um 23:00', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('a08ad0c8-6902-466c-a461-89633c8716b0', 'domain', 'language', 'Esperanto', 'Esperanto', 'Esperanto', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('a144e04c-6fea-484e-a3a5-bde64b997c8f', 'locales', '__(Texte)', 'Versandkosten', 'Versandkosten', 'Versandkosten', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('a1620305-1a22-4c3b-a354-636b88c0720a', 'domain', 'interval', 'täglich um 14:30', 'täglich um 14:30', 'täglich um 14:30', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('a163f2d8-e569-4585-a221-879a8922a813', 'domain', 'country', 'Vatikanstadt', 'Vatikanstadt', 'Vatikanstadt', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('a223e6cb-af16-4c7a-ba5a-f1b226f4292d', 'domain', 'interval', 'täglich um 13:30', 'täglich um 13:30', 'täglich um 13:30', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('a25ff3f4-145c-4391-83d2-061a4da19442', 'domain', 'country', 'Äquatorialguinea, Republik', 'Äquatorialguinea, Republik', 'Äquatorialguinea, Republik', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('a279c88c-fb3a-4a96-b188-18111444aef1', 'domain', 'country', 'Brunei', 'Brunei', 'Brunei', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('a28a315f-50b2-4213-ae3d-b7a4c8f88421', 'domain', 'country', 'Jamaika', 'Jamaika', 'Jamaika', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('a29ebd32-57e3-4b13-841c-19118b219282', 'domain', 'country', 'Tschechische Republik', 'Tschechische Republik', 'Tschechische Republik', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('a2b8c1cc-77fd-4d7b-aa2f-0ec3b846bcde', 'domain', 'country', 'Madagaskar, Republik', 'Madagaskar, Republik', 'Madagaskar, Republik', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('a2e7035a-7cee-4299-8a74-d724304aa4ed', 'locales', 'cake', '{0} year', '{0} year', '{0} year', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('a43b17c8-4357-4199-be2c-e1d46b7e5a62', 'domain', 'country', 'Botswana', 'Botswana', 'Botswana', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('a44f9049-bc69-4700-9d7c-9895dbe7228b', 'domain', 'language', 'Paschtu', 'Paschtu', 'Paschtu', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('a479bb67-435e-47c9-9be6-a2e89605d373', 'domain', 'interval', 'täglich um 07:30', 'täglich um 07:30', 'täglich um 07:30', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('a5469dff-40d8-496b-8f86-7015e36ad9cb', 'domain', 'language', 'Chinesisch', 'Chinesisch', 'Chinesisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('a5ed980f-d2a5-4fea-ba0b-0d8e3e6a980b', 'domain', 'mapgroup', 'Gestaltung', 'Gestaltung', 'Gestaltung', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('a627dfb0-7002-49fa-8522-1314baab9b61', 'locales', '__(Texte)', 'Gesamtbetrag', 'Gesamtbetrag', 'Gesamtbetrag', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('a6a77f30-bb3a-4d8a-aecd-523bd6faac99', 'locales', 'cake', 'You are not authorized to access that location.', 'You are not authorized to access that location.', 'You are not authorized to access that location.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('a765961f-32b1-423d-bbe9-a5fc77c1f38b', 'domain', 'country', 'Sri Lanka', 'Sri Lanka', 'Sri Lanka', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('a7c2dc31-c055-44ee-8b90-6451b5bf7d83', 'domain', 'country', 'Indien', 'Indien', 'Indien', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('a80bc29b-a764-4c88-9634-4a519e266c3d', 'domain', 'column', '11/12', '11/12', '11/12', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('a82b7896-3e4f-4479-bdec-f987924b1060', 'domain', 'country', 'Tuvalu', 'Tuvalu', 'Tuvalu', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('a9419e2d-1ea2-4bb5-993c-ebce0ce4722d', 'domain', 'country', 'Britisches Territorium im Indischen Ozean', 'Britisches Territorium im Indischen Ozean', 'Britisches Territorium im Indischen Ozean', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('a95e88e4-d6cd-40e9-8224-5afde64d52a2', 'locales', '__(Texte)', 'Vorheriger Artikel', 'Vorheriger Artikel', 'Vorheriger Artikel', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('a962c90d-2852-4f1e-b6cb-75a11a9920f6', 'domain', 'language', 'Schwedisch', 'Schwedisch', 'Schwedisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('a99e2559-0f62-44ab-9ab2-2b023f684928', 'domain', 'eventlabel', 'Standard', 'Standard', 'Standard', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('aa2104a2-cdd2-4b25-9226-6b979279573d', 'domain', 'interval', 'alle 6 Stunden', 'alle 6 Stunden', 'alle 6 Stunden', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('aa4d8d3c-eb9d-40cb-94b5-7f8266e46551', 'domain', 'country', 'Sudan', 'Sudan', 'Sudan', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('aa7ef6f9-b365-4624-9e64-03c9021c2e07', 'domain', 'country', 'Ägypten', 'Ägypten', 'Ägypten', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('ab37dee8-5843-4bc1-a93f-7e23a752454a', 'locales', '__(Texte)', 'Bitte markieren Sie die Checkbox.', 'Bitte markieren Sie die Checkbox.', 'Bitte markieren Sie die Checkbox.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('ab5bdec4-6a5d-44ae-880a-95379b9ec81f', 'locales', '__(Texte)', 'Akzeptieren', 'Akzeptieren', 'Akzeptieren', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('ab77fac3-b9ab-4f81-ae02-fbcf672d5294', 'domain', 'language', 'Sanskrit', 'Sanskrit', 'Sanskrit', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('ab9c74f9-66e5-4fed-bb3c-146d69c583d6', 'locales', '__(Texte)', 'search.noresults.content', 'search.noresults.content', 'search.noresults.content', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('abb849a2-c5d9-42bb-973c-daaf13c4c893', 'domain', 'country', 'Macao', 'Macao', 'Macao', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('abc379d7-d201-442e-bcb6-821632c636ab', 'domain', 'interval', 'täglich um 15:30', 'täglich um 15:30', 'täglich um 15:30', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('ac1ea1dd-9f73-465a-8b45-1db0aacd76d3', 'domain', 'country', 'Saudi-Arabien, Königreich', 'Saudi-Arabien, Königreich', 'Saudi-Arabien, Königreich', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('ac4e6a40-6d8a-48e7-b6b1-c8b707dc7f0d', 'domain', 'language', 'Slowakisch', 'Slowakisch', 'Slowakisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('ad25c17a-23cd-4953-a5e2-157c7cdb25d9', 'locales', '__(Texte)', 'Die Datei konnte nicht verschoben werden.', 'Die Datei konnte nicht verschoben werden.', 'Die Datei konnte nicht verschoben werden.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('ad35df35-cddc-401f-9ec8-49415ae5f84a', 'domain', 'language', 'Samoanisch', 'Samoanisch', 'Samoanisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('ade8a6ac-08db-48c7-9930-6689e108a386', 'locales', '__(Texte)', 'Sie haben kein Zugriff auf {0}.', 'Sie haben kein Zugriff auf {0}.', 'Sie haben kein Zugriff auf {0}.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('ae12c9ab-8bd9-4ba0-bd20-0b524faf50e7', 'domain', 'interval', 'täglich um 21:30', 'täglich um 21:30', 'täglich um 21:30', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('ae19b80c-0dca-40eb-a25d-f69338a48dd8', 'locales', '__(Texte)', 'formular.', 'formular.', 'formular.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('ae4c6b6c-6837-4a5e-a991-d5625185aeff', 'domain', 'mapgroup', 'Entwickler', 'Entwickler', 'Entwickler', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('ae93ec3f-278f-4f77-824b-5a5c08a67ed8', 'domain', 'country', 'Singapur', 'Singapur', 'Singapur', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('aec1cbd8-50a5-4425-9621-0602f91c5f67', 'domain', 'interval', 'täglich um 18:00', 'täglich um 18:00', 'täglich um 18:00', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('aed98c48-28d3-4035-b6ed-44500f99db56', 'domain', 'country', 'Kanarische Inseln', 'Kanarische Inseln', 'Kanarische Inseln', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('af4e2b25-e20d-4bc5-a909-3b9f07f6fbe3', 'domain', 'language', 'Usbekisch', 'Usbekisch', 'Usbekisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('af993406-25ae-49b0-891d-68dc3c2e75af', 'locales', 'system', 'Bitte geben Sie einen Namen ein.', 'Bitte geben Sie einen Namen ein.', 'Bitte geben Sie einen Namen ein.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('afa5af3b-c65e-47cb-85e0-372a0f57cadf', 'locales', 'cake', '{0} second', '{0} second', '{0} second', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('afdc211a-19b2-4946-a25a-cf79b57aa850', 'domain', 'country', 'Chile', 'Chile', 'Chile', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('b0188bbe-f6b3-47cf-975d-7ed38269d8f9', 'domain', 'country', 'Pakistan', 'Pakistan', 'Pakistan', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('b0ed8a60-4b09-4c73-8dd6-c23552d4d108', 'domain', 'country', 'Kambodscha', 'Kambodscha', 'Kambodscha', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('b1a2b9a9-da4f-4fd7-9fd7-6ef66fca8f21', 'domain', 'language', 'Turkmenisch', 'Turkmenisch', 'Turkmenisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('b205df49-c04c-4c22-bae5-5531958a220e', 'locales', '__(Texte)', 'Zwischensumme (netto)', 'Zwischensumme (netto)', 'Zwischensumme (netto)', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('b2154d31-4ef0-43af-9d13-48a8a708a732', 'domain', 'country', 'Paraguay', 'Paraguay', 'Paraguay', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('b2ca5071-6f49-44f1-b1b8-416c2d3f61c0', 'locales', 'system', 'Der Hauptordner kann nicht gespeichert werden,', 'Der Hauptordner kann nicht gespeichert werden,', 'Der Hauptordner kann nicht gespeichert werden,', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('b2eae6c7-1fff-4925-b03f-9be28b67dc5b', 'locales', 'system', 'Die Daten konnten nicht gespeichert werden, da die Validerung fehlgeschlagen ist', 'Die Daten konnten nicht gespeichert werden, da die Validerung fehlgeschlagen ist', 'Die Daten konnten nicht gespeichert werden, da die Validerung fehlgeschlagen ist', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('b4882f04-1ed7-4a53-9ecd-2b149251147e', 'domain', 'country', 'Litauen', 'Litauen', 'Litauen', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('b4899075-d2e4-4254-8a1d-d038d87f37ed', 'domain', 'language', 'Indonesisch', 'Indonesisch', 'Indonesisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('b4e112e1-6aa9-4e2b-9c77-db9654986e03', 'domain', 'interval', 'täglich um 22:30', 'täglich um 22:30', 'täglich um 22:30', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('b505f965-7aa9-4f1f-b2ca-beed216047b9', 'domain', 'formfield', 'Confirm', 'Confirm', 'Confirm', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('b52b24d1-d86c-4743-b121-06016b629be3', 'locales', 'cake', 'Submit', 'Absenden', 'Absenden', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('b54d1c5c-740b-4eaf-aeb6-772ea5efa8f6', 'domain', 'language', 'Nauruisch', 'Nauruisch', 'Nauruisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('b566c1c0-20cd-467a-9450-a7c60e129b4e', 'domain', 'interval', 'täglich um 15:00', 'täglich um 15:00', 'täglich um 15:00', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00');
INSERT INTO `texts` (`id`, `source`, `type`, `name`, `value`, `default_value`, `is_html`, `in_use`, `created`, `modified`) VALUES
('b6070b13-235b-402a-896f-60bd3189f401', 'domain', 'interval', 'täglich um 03:30', 'täglich um 03:30', 'täglich um 03:30', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('b6162e32-b784-493b-a3b5-902c48e1734b', 'domain', 'country', 'Malawi, Republik', 'Malawi, Republik', 'Malawi, Republik', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('b61f7452-a49a-4ce8-a3cc-75f91af8ca4f', 'domain', 'country', 'Britische Jungferninseln', 'Britische Jungferninseln', 'Britische Jungferninseln', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('b63dd050-cb0b-4073-837f-8170c13a415d', 'locales', 'cake', '%s ago', '%s ago', '%s ago', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('b66cee2c-05bf-463e-a0f9-daca3bcdc600', 'domain', 'cc', 'Mastercard', 'Mastercard', 'Mastercard', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('b68a0f62-aa45-41a4-adec-78b788cf1526', 'domain', 'interval', 'alle 3 Stunden', 'alle 3 Stunden', 'alle 3 Stunden', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('b71d06e8-5b1d-4f6a-8a46-ce96225b15e3', 'domain', 'country', 'Hongkong', 'Hongkong', 'Hongkong', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('b7a1218b-4600-4f08-9105-e29a2d997125', 'domain', 'language', 'Malagasisch', 'Malagasisch', 'Malagasisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('b7ef6037-b96c-41a0-b281-3726801cc440', 'domain', 'interval', 'täglich um 10:00', 'täglich um 10:00', 'täglich um 10:00', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('b810b63d-bbbd-4918-9a94-64e1f9f0091e', 'locales', '__(Texte)', 'Bitte wählen Sie eine Datei aus.', 'Bitte wählen Sie eine Datei aus.', 'Bitte wählen Sie eine Datei aus.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('b8858aad-66c9-437b-a6ea-0b69afd98cb5', 'domain', 'column', '3/12', '3/12', '3/12', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('b9b6c74b-e7f2-422c-879f-0405182ad8cd', 'domain', 'country', 'Kenia', 'Kenia', 'Kenia', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('b9d4588a-c69a-401e-8422-c2ef37cb3458', 'domain', 'country', 'Kirgisistan', 'Kirgisistan', 'Kirgisistan', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('b9eec27a-8882-4fbc-bfc2-e11113cccf43', 'locales', '__(Texte)', 'Mai', 'Mai', 'Mai', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('ba681e64-cc6d-4bd5-a46b-5f4c73d9396f', 'locales', 'cake', '{0} week', '{0} week', '{0} week', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('bb29e4ca-e551-445a-ba3e-5222af675668', 'domain', 'language', 'Zulu', 'Zulu', 'Zulu', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('bb403535-5d15-417a-ac02-f8b2d29d29cf', 'locales', 'cake', 'in about a month', 'in about a month', 'in about a month', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('bb6d401a-d977-4d7c-91ad-9402f4c2fcb3', 'locales', '__(Texte)', 'Juni', 'Juni', 'Juni', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('bbd23234-2438-4c5f-ad60-eb475e5dfc82', 'domain', 'country', 'Israel', 'Israel', 'Israel', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('bcebdede-8b45-4a07-ba94-df927fecd056', 'domain', 'interval', 'täglich um 14:00', 'täglich um 14:00', 'täglich um 14:00', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('bcece62c-d928-4aab-b2fe-96efed8f8815', 'locales', '__(Texte)', 'Klicken Sie hier um eine Vorschau Ihres Gutscheines zu erstellen.', 'Klicken Sie hier um eine Vorschau Ihres Gutscheines zu erstellen.', 'Klicken Sie hier um eine Vorschau Ihres Gutscheines zu erstellen.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('bdbce321-4d54-41ff-87b4-5c5274878d30', 'domain', 'language', 'Interlingua', 'Interlingua', 'Interlingua', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('be08ca44-418d-4412-8a94-d54ec88c4186', 'domain', 'country', 'Bangladesch', 'Bangladesch', 'Bangladesch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('beb0d870-7ef0-49e6-9501-6c71ddc95198', 'locales', 'cake', 'New {0}', 'New {0}', 'New {0}', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c06dbac4-8c36-48f1-9651-098b59c57457', 'domain', 'formfield', 'Select', 'Select', 'Select', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c07275e5-b6ef-4dea-a932-86e4a06c64b8', 'locales', '__(Texte)', 'datenschutzerklaerung.text', 'datenschutzerklaerung.text', 'datenschutzerklaerung.text', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c0d27fcd-0f4b-4154-a05d-3664ba8b51df', 'locales', '__(Texte)', 'Rechnung', 'Rechnung', 'Rechnung', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c0d6e73b-af06-45fa-9fb3-718cd7906a2c', 'domain', 'priceprefix', 'ab', 'ab', 'ab', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c0e0760f-87af-4e91-a002-a3cac5480e81', 'domain', 'paymentmethod', 'Sofortüberweisung', 'Sofortüberweisung', 'Sofortüberweisung', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c15bd098-cba8-4bfe-813c-117eb406983f', 'domain', 'language', 'Xhosa', 'Xhosa', 'Xhosa', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c1de266e-f3d3-4faf-bcb4-352d583b3810', 'locales', '__(Texte)', 'Zur Übersicht', 'Zur Übersicht', 'Zur Übersicht', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c1f29edd-0601-4960-b2bf-ab1a42a36196', 'domain', 'language', 'Tsongaisch', 'Tsongaisch', 'Tsongaisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c2a0fd0a-d8a1-423c-b7a9-90628cb10afc', 'locales', '__(Texte)', 'Wir bitten Sie die mit einem * gekennzeichneten Felder auszufüllen. Vielen Dank!', 'Wir bitten Sie die mit einem * gekennzeichneten Felder auszufüllen. Vielen Dank!', 'Wir bitten Sie die mit einem * gekennzeichneten Felder auszufüllen. Vielen Dank!', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c2d32f64-bbf3-4b58-9ec0-b36763aa574c', 'domain', 'language', 'Spanisch', 'Spanisch', 'Spanisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c2e0e55a-378e-4a4f-ad8f-62202e2dbc9f', 'domain', 'language', 'Schottisches Gälisch', 'Schottisches Gälisch', 'Schottisches Gälisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c368ad3e-a6e7-4cf7-a90b-4e70e1b30681', 'domain', 'country', 'Bolivien', 'Bolivien', 'Bolivien', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c39d6da0-2d56-4bc4-a090-028bc2a1d208', 'domain', 'language', 'Thai', 'Thai', 'Thai', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c42c34bd-6624-41f3-a28b-89234094950a', 'locales', '__(Texte)', 'Processing ', 'Processing ', 'Processing ', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c4375ac4-8f06-484b-abc9-73b33b1eaa4f', 'locales', 'cake', 'The requested file was not found', 'The requested file was not found', 'The requested file was not found', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c45f1653-562e-404a-8879-a6d78f44cdda', 'domain', 'country', 'Libyen', 'Libyen', 'Libyen', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c475fa26-1ea7-40ed-ba2e-057ff481bf2b', 'domain', 'country', 'Ascension', 'Ascension', 'Ascension', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c48eea8a-ec6d-4547-9c4d-bafc8912e258', 'locales', '__(Texte)', 'Zeichen', 'Zeichen', 'Zeichen', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c49b72b4-d33e-4c22-a1f2-4dcbdfdfb506', 'domain', 'country', 'Gibraltar', 'Gibraltar', 'Gibraltar', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c4b1d5e4-4f8d-4fc8-93a5-c4e7ab2f283f', 'domain', 'country', 'Liechtenstein, Fürstentum', 'Liechtenstein, Fürstentum', 'Liechtenstein, Fürstentum', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c4e21de4-21aa-473c-b9f1-a3337df87259', 'locales', '__(Texte)', 'Datei', 'Datei', 'Datei', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c4fe7bdd-e9b3-4688-9012-dad13146df3f', 'locales', '__(Texte)', 'Sehr geehrte Damen und Herren', 'Sehr geehrte Damen und Herren', 'Sehr geehrte Damen und Herren', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c576a280-9471-47b3-ad38-958964d43b31', 'domain', 'country', 'Polen', 'Polen', 'Polen', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c5a1f31d-07f2-4396-9bbd-7ca774b5fb01', 'domain', 'mwst', '0%', '0%', '0%', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c5d9bc4e-69c3-4333-9ac8-51ce52562d36', 'locales', '__(Texte)', 'Loading', 'Loading', 'Loading', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c6139f66-4432-48ce-9fb6-2c1b1539a6d9', 'domain', 'operator', 'Ist größer (>)', 'Ist größer (>)', 'Ist größer (>)', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c66fc7ba-0c52-4611-b6c1-ac8d0fd03364', 'locales', 'system', 'Bitte wählen Sie einen übergeordneten Eintrag aus.', 'Bitte wählen Sie einen übergeordneten Eintrag aus.', 'Bitte wählen Sie einen übergeordneten Eintrag aus.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c68cdbc9-d20f-45d5-9b9a-d6d31c3f913b', 'domain', 'language', 'Mazedonisch', 'Mazedonisch', 'Mazedonisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c75c331c-14b5-448d-b0e0-cada2b5719e4', 'locales', 'cake', 'Edit {0}', 'Edit {0}', 'Edit {0}', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c7c4b00f-4913-45c7-89c3-0b9980deea1b', 'locales', '__(Texte)', 'Fehler', 'Fehler', 'Fehler', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c8126492-c959-433a-9d1b-27691d479b7d', 'domain', 'country', 'Andorra', 'Andorra', 'Andorra', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c83cd1c8-dc14-4a0d-98f6-41e275fe4bc3', 'domain', 'interval', 'täglich um 06:00', 'täglich um 06:00', 'täglich um 06:00', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c849b4bb-6149-4cda-8e67-ee78c5b4872d', 'domain', 'country', 'Island', 'Island', 'Island', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c84caec8-ef70-4b4e-a929-df95fca4718a', 'locales', '__(Texte)', 'Irgendwas ist schief gelaufen.', 'Irgendwas ist schief gelaufen.', 'Irgendwas ist schief gelaufen.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c8573381-c086-4536-af10-490b7d0a2c4a', 'locales', 'cake', '{0} ago', '{0} ago', '{0} ago', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c8806dbd-7c46-4bed-8f65-d547576dd712', 'domain', 'country', 'Weihnachtsinsel', 'Weihnachtsinsel', 'Weihnachtsinsel', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c881842c-7a2d-481c-b723-a0fa897cafbc', 'domain', 'country', 'Malediven', 'Malediven', 'Malediven', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c8923dd8-9541-4eef-b58a-a693ff937ac5', 'domain', 'country', 'Ecuador', 'Ecuador', 'Ecuador', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c8bad49c-3caa-43e4-a523-caf1359c8ec3', 'domain', 'formfield', 'Password', 'Password', 'Password', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c8f3b0cd-5997-425e-8ee7-d9f907ff53d9', 'locales', '__(Texte)', 'Bitte geben Sie Menünamen ein', 'Bitte geben Sie Menünamen ein', 'Bitte geben Sie Menünamen ein', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c952c1ae-c5b6-44e3-8287-eef4d07b7422', 'locales', '__(Texte)', 'Bitte füllen Sie das Feld aus.', 'Bitte füllen Sie das Feld aus.', 'Bitte füllen Sie das Feld aus.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('c9e6193d-80f1-4d0a-8244-7fbcdc8d7ef3', 'domain', 'country', 'Antarktis', 'Antarktis', 'Antarktis', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('ca0eae40-ea56-4f0a-9006-83649b5f0860', 'domain', 'country', 'Nigeria', 'Nigeria', 'Nigeria', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('ca104277-6715-400c-ad61-6f3f992b9410', 'locales', '__(Texte)', 'September', 'September', 'September', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('ca5ab149-f0ad-424e-bd26-8d224945ba43', 'locales', '__(Texte)', 'Quelle konnte nicht gefunden werden.', 'Quelle konnte nicht gefunden werden.', 'Quelle konnte nicht gefunden werden.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('cb20daad-3628-48ad-98bd-a6c77650b13b', 'domain', 'language', 'Lingala', 'Lingala', 'Lingala', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('cb31da85-bbc1-4a8e-af6c-bad24359078a', 'domain', 'country', 'Cote d\'Ivoire', 'Cote d\'Ivoire', 'Cote d\'Ivoire', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('cb604295-6689-4130-b3bf-c8bec6276b12', 'domain', 'country', 'Timor-Leste, Demokratische Republik', 'Timor-Leste, Demokratische Republik', 'Timor-Leste, Demokratische Republik', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('cb9328ae-ebd9-4622-87e4-0c39a11fb9d3', 'domain', 'language', 'Portugiesisch', 'Portugiesisch', 'Portugiesisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('cba1aada-f02e-4ca8-b58d-821f7cf5dd27', 'domain', 'country', 'San Marino', 'San Marino', 'San Marino', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('cbde43b8-8c2e-4dd1-b829-4df55539d769', 'domain', 'country', 'Belgien', 'Belgien', 'Belgien', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('cbde9087-e089-4811-a6c3-404ee0a8383d', 'domain', 'country', 'Österreich', 'Österreich', 'Österreich', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('cbfead22-ef81-44d6-a8ae-136fe4df1ca4', 'domain', 'language', 'Bretonisch', 'Bretonisch', 'Bretonisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('cc0639fb-3126-40c7-ada9-d0caa564b83e', 'domain', 'country', 'Swasiland', 'Swasiland', 'Swasiland', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('cc6f2820-9ad5-4a3d-bcc5-1c0088fa1eac', 'domain', 'berufserfahrung', '2 Jahre', '2 Jahre', '2 Jahre', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('cc91c24f-3bf3-4b03-b4a0-906e0b30093f', 'domain', 'country', 'Portugal', 'Portugal', 'Portugal', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('cddddf67-5acd-49c5-9f17-35dc4d9d71a0', 'domain', 'interval', 'täglich um 13:00', 'täglich um 13:00', 'täglich um 13:00', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('ce1fb594-4067-4318-9a64-75772258fa39', 'domain', 'interval', 'täglich um 20:30', 'täglich um 20:30', 'täglich um 20:30', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('ceb6e248-882d-486a-ba52-6c188e6e7c30', 'domain', 'gender', 'weiblich', 'weiblich', 'weiblich', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('ced721bf-f1af-44e2-a4ee-c41d895e8a2c', 'domain', 'country', 'Samoa', 'Samoa', 'Samoa', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('cf830bb1-6267-43cb-a3df-dd0f58e0471b', 'domain', 'country', 'Kanada', 'Kanada', 'Kanada', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('cff04fa3-1b92-4ecc-a47f-ff245b9c5c11', 'domain', 'cc', 'Visa', 'Visa', 'Visa', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('d017c9c0-e1a4-4c74-a1af-21ccfaa4b029', 'locales', 'system', 'Die Daten wurden erfolgreich gelöscht.', 'Die Daten wurden erfolgreich gelöscht.', 'Die Daten wurden erfolgreich gelöscht.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('d02e265b-fed5-4385-b0b4-38ef99e596be', 'locales', 'cake', 'about a second ago', 'about a second ago', 'about a second ago', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('d0fc281c-624c-446b-8d92-2409bebfe4d9', 'domain', 'berufserfahrung', 'keine', 'keine', 'keine', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('d1710b7a-3c52-498c-bfda-4806ac49638b', 'domain', 'interval', 'täglich um 09:00', 'täglich um 09:00', 'täglich um 09:00', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('d1a7fda1-2bef-49e0-b45c-5a1cf63b037f', 'domain', 'country', 'Nicaragua', 'Nicaragua', 'Nicaragua', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('d1afcf26-0555-44e4-9833-dfbaec7d30b6', 'domain', 'country', 'Senegal', 'Senegal', 'Senegal', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('d1dbc10a-e412-460f-a08d-3b152bb60a7f', 'domain', 'interval', 'täglich um 00:00', 'täglich um 00:00', 'täglich um 00:00', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('d1eeb17b-5ff5-40b4-ac89-18993e324372', 'domain', 'country', 'Südgeorgien und die Südlichen Sandwichinseln', 'Südgeorgien und die Südlichen Sandwichinseln', 'Südgeorgien und die Südlichen Sandwichinseln', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('d1f4efc1-0387-4c9d-ba27-a655188ab288', 'domain', 'language', 'Tatarisch', 'Tatarisch', 'Tatarisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('d2f1a40c-e818-4751-8d68-3bd66be9a06f', 'locales', '__(Texte)', 'Bitte wählen Sie eine Stammzugehörigkeit aus.', 'Bitte wählen Sie eine Stammzugehörigkeit aus.', 'Bitte wählen Sie eine Stammzugehörigkeit aus.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('d35c9d22-d6ca-472d-afae-7234e0004ada', 'domain', 'shippingoption', 'Per Post', 'Per Post', 'Per Post', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('d36d6b4c-343f-4006-8ffb-0a681b8b85cd', 'domain', 'country', 'Namibia, Republik', 'Namibia, Republik', 'Namibia, Republik', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('d387658a-9a51-4878-82bb-23e5f8114a99', 'domain', 'interval', 'täglich um 01:30', 'täglich um 01:30', 'täglich um 01:30', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('d3e30e47-db8c-4cfd-a1ad-07928dcb4832', 'domain', 'country', 'Neukaledonien', 'Neukaledonien', 'Neukaledonien', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('d4206f75-281a-44ea-9f64-73d63bb67b8e', 'domain', 'language', 'Albanisch', 'Albanisch', 'Albanisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('d46f7d5b-9a10-4ecd-9c4e-e172c1229b8d', 'locales', 'cake', '{0} before', '{0} before', '{0} before', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('d496732b-b066-4de5-ba25-356141881d57', 'locales', 'system', 'Bitte wählen Sie einen Order aus.', 'Bitte wählen Sie einen Order aus.', 'Bitte wählen Sie einen Order aus.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('d49fdec4-466c-4b1b-b3ae-d1f486a74fe4', 'domain', 'country', 'Estland', 'Estland', 'Estland', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('d4cd729a-0bdb-4b9f-93a8-c7ee5483e87a', 'domain', 'interval', 'Immer', 'Immer', 'Immer', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('d4e50522-8933-498a-a36b-0f0442972e37', 'domain', 'country', 'Vereinigte Arabische Emirate', 'Vereinigte Arabische Emirate', 'Vereinigte Arabische Emirate', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('d55b076c-18ba-4278-b2e0-e47e25a3b6d6', 'domain', 'country', 'Bosnien und Herzegowina', 'Bosnien und Herzegowina', 'Bosnien und Herzegowina', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('d6421f69-a71f-4a73-a606-d1847d33785f', 'domain', 'country', 'Argentinien', 'Argentinien', 'Argentinien', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('d64a53ec-9a26-4efb-af58-d836133c8d7d', 'domain', 'country', 'Die Kronkolonie St. Helena und Nebengebiete', 'Die Kronkolonie St. Helena und Nebengebiete', 'Die Kronkolonie St. Helena und Nebengebiete', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('d67d0c56-769d-47f5-b3da-973fd76ac3de', 'locales', '__(Texte)', 'Januar', 'Januar', 'Januar', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('d6d06824-4452-4600-9ffe-152fa9542099', 'locales', 'cake', 'about a day ago', 'about a day ago', 'about a day ago', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('d8100f25-51d1-4b47-bb93-c5bbc9e6df38', 'domain', 'country', 'Wallis und Futuna', 'Wallis und Futuna', 'Wallis und Futuna', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('d8686ea7-2852-4285-bc13-0a8e8e5a6757', 'domain', 'language', 'Schonisch', 'Schonisch', 'Schonisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('d91700bd-2a0d-4316-b31d-45485aec38e1', 'domain', 'language', 'Tagalog', 'Tagalog', 'Tagalog', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('d92b3bd9-4424-42fc-bcec-0cecb6cd0952', 'domain', 'country', 'Eritrea', 'Eritrea', 'Eritrea', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('d9882153-e276-4104-9304-d3a3e938c314', 'domain', 'berufserfahrung', '8 Jahre', '8 Jahre', '8 Jahre', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('d9d1fc6c-e426-425d-a292-23555f5cc1ad', 'domain', 'country', 'Grönland', 'Grönland', 'Grönland', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('da0e2a32-9d55-458e-849b-8f8fca576b12', 'domain', 'country', 'Turkmenistan', 'Turkmenistan', 'Turkmenistan', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('da1a4f14-3851-4b50-991e-3056a111170f', 'domain', 'country', 'Vietnam', 'Vietnam', 'Vietnam', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('db1b318b-c799-494b-b6ba-4b337e652731', 'domain', 'language', 'Sesothisch', 'Sesothisch', 'Sesothisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('db70d3cc-495b-424c-8176-39d9832d65fb', 'domain', 'language', 'Oromo', 'Oromo', 'Oromo', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('dbe35b41-f74b-4664-b224-3bb38d917ac9', 'domain', 'country', 'Ruanda, Republik', 'Ruanda, Republik', 'Ruanda, Republik', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('dbf95a43-ecc2-428b-a172-367dd663a1e0', 'domain', 'interval', 'alle 4 Stunden', 'alle 4 Stunden', 'alle 4 Stunden', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('dc321c02-f750-440f-b3a5-2d580be93500', 'locales', '__(Texte)', '??', '??', '??', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('dd2b7804-ad18-4cf3-b5c3-91373afb5943', 'domain', 'language', 'Walisisch', 'Walisisch', 'Walisisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('dd49de9d-803a-46c4-89db-44d47cd40a9f', 'locales', 'cake', 'This value is already in use', 'Der Wert wird bereits verwendet', 'Der Wert wird bereits verwendet', 0, 1, '2020-09-10 00:00:00', '2020-10-15 23:48:18'),
('de7797a4-754c-4606-a724-0b460bc13b10', 'domain', 'country', 'Niue', 'Niue', 'Niue', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('de7b67ea-9aa7-4bf0-82fa-f8b4099c97e6', 'domain', 'interval', 'alle 2 Stunden', 'alle 2 Stunden', 'alle 2 Stunden', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('dee1fd1c-132c-47db-b9ce-66f8069d60b5', 'domain', 'language', 'Maorisch', 'Maorisch', 'Maorisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('e0957cb1-9517-4c62-b333-f6572001f77a', 'locales', '__(Texte)', 'Insgesamt {0} Einträgen importiert.', 'Insgesamt {0} Einträgen importiert.', 'Insgesamt {0} Einträgen importiert.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('e0be6003-7a13-4970-9121-1f24b14825e1', 'domain', 'formfield', 'Verstecktes Feld', 'Verstecktes Feld', 'Verstecktes Feld', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('e1adcf1f-f298-4468-844b-9562986566b4', 'domain', 'language', 'Jiddish', 'Jiddish', 'Jiddish', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('e2ebe132-f8a9-407b-8c1f-58715b7785a1', 'locales', '__(Texte)', 'Megapixel', 'Megapixel', 'Megapixel', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('e2eee42d-dcc4-406c-83b9-9b52beb5defa', 'locales', 'system', 'Vimeo Video nicht gefunden.', 'Vimeo Video nicht gefunden.', 'Vimeo Video nicht gefunden.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('e3322f52-4a28-4591-a877-485ab9c8d509', 'domain', 'country', 'Russische Föderation', 'Russische Föderation', 'Russische Föderation', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('e37ca514-114c-48b5-b8fc-720746fb8cde', 'locales', 'cake', 'in about a second', 'in about a second', 'in about a second', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('e3bc6b3d-e944-475f-83d2-7939be35e966', 'domain', 'country', 'Bermuda', 'Bermuda', 'Bermuda', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('e4170b52-ee8e-44af-aca4-10953d8ace6e', 'locales', '__(Texte)', 'Warnung', 'Warnung', 'Warnung', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('e430298e-1c8c-443f-8be0-75972842d57a', 'locales', 'system', 'Die Texte wurden neu generiert.', 'Die Texte wurden neu generiert.', 'Die Texte wurden neu generiert.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('e50413f3-08ff-4274-9b8f-679d152915e2', 'domain', 'language', 'Nepalisch', 'Nepalisch', 'Nepalisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('e53691d4-8472-4644-8332-3dc95e6bf9b9', 'domain', 'country', 'Aserbaidschan', 'Aserbaidschan', 'Aserbaidschan', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('e5f976c5-c535-4d4d-995a-43460cab2d92', 'domain', 'language', 'Sudanesisch', 'Sudanesisch', 'Sudanesisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('e60f2e50-4f83-488f-a485-938e6a0006e7', 'locales', '__(Texte)', 'Juli', 'Juli', 'Juli', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('e6746d6f-d5dd-4a52-b458-5a416f29cfa7', 'domain', 'language', 'Englisch', 'Englisch', 'Englisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('e6ebcf10-a0d1-4f86-8438-8bcfc77e510b', 'locales', 'cake', 'and', 'und', 'und', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('e76c5eeb-5f49-4447-9565-5e83270ffb65', 'domain', 'language', 'Fiji', 'Fiji', 'Fiji', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('e7ca061a-c542-4b16-8e70-e192091529d9', 'domain', 'mapgroup', 'Allgemein', 'Allgemein', 'Allgemein', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('e7cd10f2-68af-4f2d-83bc-a921ff046c00', 'domain', 'country', 'Puerto Rico', 'Puerto Rico', 'Puerto Rico', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('e7f0365f-5e8a-4a52-a103-c28448986d94', 'domain', 'country', 'Svalbard und Jan Mayen', 'Svalbard und Jan Mayen', 'Svalbard und Jan Mayen', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('e8890b3f-a34f-4e5b-a794-48766f5c069b', 'domain', 'language', 'Georgisch', 'Georgisch', 'Georgisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('e8c371c2-2753-4ecd-b729-a4f77adcb7ad', 'locales', '__(Texte)', 'Nächster Artikel', 'Nächster Artikel', 'Nächster Artikel', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('e8ee7c54-91b4-44b5-be6b-1f696b5be899', 'domain', 'language', 'Zinti', 'Zinti', 'Zinti', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('e9185007-d293-464e-9522-2788fe0a292b', 'locales', '__(Texte)', 'Seite', 'Seite', 'Seite', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('e9cbdfcb-6477-403f-a728-d087ae7fc4f2', 'locales', '__(Texte)', 'Nein', 'Nein', 'Nein', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('e9db9c97-30ef-4a27-922b-73e4a4d97a09', 'locales', 'system', 'Daten kopiert.', 'Daten kopiert.', 'Daten kopiert.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('ea4046b0-8b25-429f-aad5-23103cee2bd1', 'domain', 'country', 'Moldawien', 'Moldawien', 'Moldawien', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('eaacf9c9-d482-4927-8c08-53c91deae90f', 'domain', 'interval', 'täglich um 00:30', 'täglich um 00:30', 'täglich um 00:30', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('eae32074-7b04-4891-a922-a3dae16c858e', 'domain', 'country', 'Slowakei', 'Slowakei', 'Slowakei', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('eaf9cccb-9863-4415-9168-604ba670fe67', 'domain', 'language', 'Bislamisch', 'Bislamisch', 'Bislamisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('eb0da347-4537-4580-bfac-2e637ad630f1', 'domain', 'language', 'Dzongkha, Bhutani', 'Dzongkha, Bhutani', 'Dzongkha, Bhutani', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('ebedcab3-2c9f-4bf5-815e-202a1db4e9f7', 'domain', 'salutation', 'Herr', 'Herr', 'Herr', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('ec38e38c-21e6-4c50-8f5f-069dae8ecf39', 'locales', '__(Texte)', 'Menge', 'Menge', 'Menge', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('ecd06274-c2f4-4d7b-8bb5-d61ea20e67db', 'domain', 'language', 'Ungarisch', 'Ungarisch', 'Ungarisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('ed77fb91-ae3f-482a-9b76-7a7992064c1e', 'domain', 'berufserfahrung', '5 Jahre', '5 Jahre', '5 Jahre', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('edea3bf1-79d6-43e0-a826-7432b886ae42', 'domain', 'country', 'Bulgarien', 'Bulgarien', 'Bulgarien', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('ee4ae269-5620-44f1-8e9f-2e9398af045a', 'locales', '__(Texte)', 'Bitte laden Sie eine Datei hoch.', 'Bitte laden Sie eine Datei hoch.', 'Bitte laden Sie eine Datei hoch.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('ee91a60b-a79f-48a3-84bd-f0f9fdb244cd', 'domain', 'country', 'Marokko', 'Marokko', 'Marokko', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('ef3845a3-5c0d-445f-a88d-de31d0321ae1', 'domain', 'interval', 'täglich um 12:00', 'täglich um 12:00', 'täglich um 12:00', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('ef5ea881-3a56-4afd-941b-b9e92d5846a3', 'domain', 'country', 'Afghanistan', 'Afghanistan', 'Afghanistan', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('ef6b83f1-d8de-4b1c-b157-df7f26b3daa4', 'domain', 'language', 'Irisch', 'Irisch', 'Irisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('efde3c7d-73df-483b-9424-2d52868025f4', 'domain', 'country', 'Pitcairninseln', 'Pitcairninseln', 'Pitcairninseln', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('f070a17e-398a-4784-aa67-d7358e66e0ec', 'domain', 'language', 'Türkisch', 'Türkisch', 'Türkisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('f0c027b9-1c28-4c96-b103-ed23cdf26d55', 'domain', 'country', 'Trinidad und Tobago', 'Trinidad und Tobago', 'Trinidad und Tobago', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('f0c80f1f-e9a6-419a-997e-eb2885789002', 'locales', '__(Texte)', '(netto)', '(netto)', '(netto)', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('f12f4fa1-a969-41ca-a3ff-657a8e4ae2cc', 'domain', 'country', 'Peru', 'Peru', 'Peru', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('f20cad8f-e401-46d1-9a5b-fcdd70d5300d', 'locales', 'cake', 'today', 'heute', 'heute', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('f22f5d91-e254-4cfa-9290-d5df074b4fa4', 'domain', 'country', 'Thailand', 'Thailand', 'Thailand', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('f316c813-9660-45f3-a3c2-c5e10f8fee8d', 'locales', '__(Texte)', 'Jetzt Gutschein Vorschau als PDF herunterladen.', 'Jetzt Gutschein Vorschau als PDF herunterladen.', 'Jetzt Gutschein Vorschau als PDF herunterladen.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('f3321af3-a010-4a6c-b9dd-442e12fdb1c6', 'domain', 'pricesuffix', 'pro Nacht', 'pro Nacht', 'pro Nacht', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('f39ed41f-227c-4a24-b1e9-4ba98c4110e6', 'domain', 'column', '5/12', '5/12', '5/12', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('f3c21ed2-3242-4de3-9ee4-5e38be6914db', 'locales', '__(Texte)', 'Es ist ein Fehler aufgetreten. Bitte versuchen Sie es noch einaml.', 'Es ist ein Fehler aufgetreten. Bitte versuchen Sie es noch einaml.', 'Es ist ein Fehler aufgetreten. Bitte versuchen Sie es noch einaml.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('f3e81b34-3ce7-4c2f-a4cb-8d1715c750c3', 'domain', 'country', 'St. Lucia', 'St. Lucia', 'St. Lucia', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('f3ec578b-4f75-4bd6-9698-e80060974420', 'domain', 'country', 'Komoren', 'Komoren', 'Komoren', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('f41c7f71-1d72-41e6-8ec2-a2990cc36d4b', 'locales', '__(Texte)', 'Webseite besuchen', 'Webseite besuchen', 'Webseite besuchen', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('f4bfac31-40ba-4d0e-a2dc-e64f351427e0', 'domain', 'country', 'Guernsey, Vogtei', 'Guernsey, Vogtei', 'Guernsey, Vogtei', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('f4fe9196-800d-40f6-b199-7b40a259977c', 'locales', 'cake', 'The count does not match {0}{1}', 'The count does not match {0}{1}', 'The count does not match {0}{1}', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('f4fff643-fb6d-47c2-aad3-566be9f5f8a1', 'domain', 'formfield', 'Headline (h1)', 'Headline (h1)', 'Headline (h1)', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('f513479e-75b3-4901-b30e-71074890b1df', 'locales', 'cake', 'about an hour ago', 'about an hour ago', 'about an hour ago', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('f513546a-0a1d-474d-8b5e-2d957d4b4754', 'domain', 'country', 'Kap Verde, Republik', 'Kap Verde, Republik', 'Kap Verde, Republik', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('f5e788e1-044d-46c3-8b75-3be185a474c9', 'locales', 'cake', 'Missing CSRF token body', 'Missing CSRF token body', 'Missing CSRF token body', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('f753e2a8-a780-48e2-9ad7-a59c3269389b', 'domain', 'country', 'Albanien', 'Albanien', 'Albanien', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('f86ed2d3-e182-4e47-94ac-4ef64a3f3181', 'domain', 'familystatus', 'ledig', 'ledig', 'ledig', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('f8703659-a8cc-4d15-afb1-726cc229ef53', 'domain', 'language', 'Galizisch', 'Galizisch', 'Galizisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('f8939fc8-971f-4d1c-84f5-869773ec873a', 'domain', 'country', 'Antigua und Barbuda', 'Antigua und Barbuda', 'Antigua und Barbuda', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('f8b93b50-0ddc-428b-b3b1-be5bafba3e29', 'domain', 'country', 'Dominikanische Republik', 'Dominikanische Republik', 'Dominikanische Republik', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('f9433db4-c496-405d-90bd-114443c838ef', 'domain', 'language', 'Rumänisch', 'Rumänisch', 'Rumänisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('f98d06b5-8bb0-4b80-89e9-b403360cede6', 'locales', '__(Texte)', 'Diese Datei konnte nicht hochgeladen werden.', 'Diese Datei konnte nicht hochgeladen werden.', 'Diese Datei konnte nicht hochgeladen werden.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('f9d14f59-91a5-4cdb-a296-a413c1d4ed07', 'domain', 'formfield', 'Container Start', 'Container Start', 'Container Start', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('f9f82ab3-de3d-4d38-8975-c0ed5519d01c', 'locales', 'system', 'Die Daten konnten nicht kopiert werden.', 'Die Daten konnten nicht kopiert werden.', 'Die Daten konnten nicht kopiert werden.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('f9f83b47-9d08-4797-8f9e-445b86b77f74', 'locales', '__(Texte)', 'Bei der Eingabe Ihres Logins (Benutzername / E-Mail-Adresse) oder Ihres Kennworts ist ein Fehler aufgetreten. Bitte beachten Sie, dass Sie sich ausschließlich mit Ihrer bestätigten E-Mail-Adresse einloggen können.', 'Bei der Eingabe Ihres Logins (Benutzername / E-Mail-Adresse) oder Ihres Kennworts ist ein Fehler aufgetreten. Bitte beachten Sie, dass Sie sich ausschließlich mit Ihrer bestätigten E-Mail-Adresse einloggen können.', 'Bei der Eingabe Ihres Logins (Benutzername / E-Mail-Adresse) oder Ihres Kennworts ist ein Fehler aufgetreten. Bitte beachten Sie, dass Sie sich ausschließlich mit Ihrer bestätigten E-Mail-Adresse einloggen können.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('fa4483b8-619f-408d-93d6-153a923b4f8b', 'domain', 'country', 'Malta', 'Malta', 'Malta', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('fa684c57-7b78-453d-a3cf-11d216b9ea85', 'domain', 'language', 'Inupiak', 'Inupiak', 'Inupiak', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('fabcefc1-9b8a-49f3-92be-590c41f43489', 'domain', 'formfield', 'Select (Suggest)', 'Select (Suggest)', 'Select (Suggest)', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('fb0982be-b5b3-4328-a129-0abf9f7b695f', 'domain', 'country', 'Amerikanische Jungferninseln', 'Amerikanische Jungferninseln', 'Amerikanische Jungferninseln', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('fb2c02f7-d73e-4b80-a32f-4cc64a66d2c1', 'domain', 'language', 'Finnisch', 'Finnisch', 'Finnisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('fc331e46-a647-45a3-ba12-085c0489605c', 'domain', 'column', '7/12', '7/12', '7/12', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('fc33608b-d7ed-4375-acf3-fe74ba819aab', 'domain', 'interval', 'täglich um 04:00', 'täglich um 04:00', 'täglich um 04:00', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('fc744acc-dc00-41dc-9980-f3be04aa85f1', 'locales', '__(Texte)', 'August', 'August', 'August', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('fca066f4-6afa-42cd-b848-8b89ebd9975b', 'locales', '__(Texte)', 'Failed to move uploaded file.', 'Failed to move uploaded file.', 'Failed to move uploaded file.', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('fe2b3d39-2a3b-4bff-8915-32d772accf3f', 'locales', 'cake', 'This value does not exist', 'Dieser Wert existiert nicht', 'Dieser Wert existiert nicht', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('fe32ffae-6cb1-46a7-a743-8ff6c07c42b3', 'domain', 'language', 'Slowenisch', 'Slowenisch', 'Slowenisch', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('fe6bf37c-73b8-433b-98b6-46e4b237fad3', 'locales', 'cake', 'Not Found', 'Nicht gefunden', 'Nicht gefunden', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('fef7a49a-8097-4a9e-8d8b-7cf7de5834c8', 'domain', 'column', '1/12', '1/12', '1/12', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('ff5d9b05-e0c7-4611-8723-ef1393416ad1', 'domain', 'language', 'Quechua', 'Quechua', 'Quechua', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('ff69b731-9755-47b9-8ee7-2dfd6e8d1f84', 'domain', 'interval', 'täglich um 04:30', 'täglich um 04:30', 'täglich um 04:30', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('ff736144-d966-4202-8bb8-c286c50273c2', 'domain', 'country', 'Taiwan', 'Taiwan', 'Taiwan', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
('ffc94c36-d2e9-436c-aee9-ed9cf817b3ac', 'domain', 'newsletterlist', 'Wellness', 'Wellness', 'Wellness', 0, 1, '2020-09-10 00:00:00', '2020-09-10 00:00:00');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `texts_translations`
--

DROP TABLE IF EXISTS `texts_translations`;
CREATE TABLE `texts_translations` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `locale` char(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci,
  `default_value` longtext COLLATE utf8mb4_unicode_ci,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=215 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aco_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `auth_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `approved` tinyint(1) DEFAULT '0',
  `approved_date` datetime DEFAULT NULL,
  `confirmed` tinyint(1) DEFAULT '0',
  `last_login` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=8192 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `users`
--

INSERT INTO `users` (`id`, `aco_id`, `auth_id`, `type`, `login`, `password`, `active`, `approved`, `approved_date`, `confirmed`, `last_login`, `created`, `modified`) VALUES
('11111111-1111-1111-1111-111111111111', 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', NULL, 'Users', 'admin', '$2y$10$ispt96sj.eBQEK4/1qKWJ.KOC1Y3qu4KQgXKVaVmRkCOycIZ1a0C6', 1, 0, NULL, 1, '2021-06-29 00:34:14', '2014-01-01 00:00:00', '2021-06-29 00:34:14'),
('cccccccc-cccc-cccc-cccc-cccccccccccc', 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa', NULL, 'Users', 'client', '$2y$10$A/BvJ1Lqd4uY.M891QUQxeLOg1Tbi7pN3yo6CCIlQ1SsDaGbt1IsO', 1, 0, NULL, 1, '2021-06-29 00:30:48', '2014-01-01 00:00:00', '2021-06-29 00:30:48');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `vouchers`
--

DROP TABLE IF EXISTS `vouchers`;
CREATE TABLE `vouchers` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `voucherlayout` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dedication` text COLLATE utf8mb4_unicode_ci,
  `redeemed` tinyint(1) DEFAULT '0',
  `remainder` double DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `acos`
--
ALTER TABLE `acos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lft` (`lft`),
  ADD KEY `rght` (`rght`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indizes für die Tabelle `acos_users`
--
ALTER TABLE `acos_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique` (`user_id`,`aco_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indizes für die Tabelle `actionviews`
--
ALTER TABLE `actionviews`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique` (`root_id`,`menu_id`,`controller_action`(100)) USING BTREE,
  ADD KEY `created` (`created`),
  ADD KEY `menu_id` (`menu_id`),
  ADD KEY `modified` (`modified`),
  ADD KEY `controller_action` (`controller_action`(191)) USING BTREE;

--
-- Indizes für die Tabelle `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique` (`foreign_key`,`type`(100),`model`(100)) USING BTREE,
  ADD KEY `model` (`model`(191)),
  ADD KEY `type` (`type`(191));

--
-- Indizes für die Tabelle `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created` (`created`),
  ADD KEY `identifier` (`identifier`(191)),
  ADD KEY `modified` (`modified`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `type` (`type`(191)),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `aco_id` (`aco_id`),
  ADD KEY `title` (`title`(191));

--
-- Indizes für die Tabelle `articles_elements`
--
ALTER TABLE `articles_elements`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `article_id` (`element_id`,`article_id`) USING BTREE,
  ADD KEY `element_id` (`element_id`) USING BTREE;

--
-- Indizes für die Tabelle `articles_translations`
--
ALTER TABLE `articles_translations`
  ADD PRIMARY KEY (`id`,`locale`) USING BTREE,
  ADD KEY `id` (`id`);

--
-- Indizes für die Tabelle `attached`
--
ALTER TABLE `attached`
  ADD PRIMARY KEY (`id`),
  ADD KEY `attachment_id` (`attachment_id`);

--
-- Indizes für die Tabelle `attached_translations`
--
ALTER TABLE `attached_translations`
  ADD PRIMARY KEY (`id`,`locale`),
  ADD KEY `id` (`id`);

--
-- Indizes für die Tabelle `attachments`
--
ALTER TABLE `attachments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `foreign_key` (`foreign_key`),
  ADD KEY `is_leaf` (`is_leaf`),
  ADD KEY `name` (`name`(191)),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `type` (`type`(191)),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `aco_id` (`aco_id`);

--
-- Indizes für die Tabelle `attachments_translations`
--
ALTER TABLE `attachments_translations`
  ADD PRIMARY KEY (`locale`,`id`),
  ADD KEY `id` (`id`);

--
-- Indizes für die Tabelle `bankaccounts`
--
ALTER TABLE `bankaccounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `foreign_key` (`foreign_key`),
  ADD KEY `model` (`model`(191)),
  ADD KEY `type` (`type`(191));

--
-- Indizes für die Tabelle `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `categorized`
--
ALTER TABLE `categorized`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique` (`foreign_key`,`type`(100),`model`(100)) USING BTREE,
  ADD KEY `category_id` (`category_id`);

--
-- Indizes für die Tabelle `communications`
--
ALTER TABLE `communications`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique` (`foreign_key`,`type`(100),`model`(100)) USING BTREE,
  ADD KEY `model` (`model`(191)),
  ADD KEY `type` (`type`(191));

--
-- Indizes für die Tabelle `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique` (`foreign_key`,`model`(100),`type`(100)) USING BTREE,
  ADD KEY `foreign_key` (`foreign_key`),
  ADD KEY `model` (`model`(191)),
  ADD KEY `type` (`type`(191));

--
-- Indizes für die Tabelle `configurations`
--
ALTER TABLE `configurations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`(191));

--
-- Indizes für die Tabelle `crontasks`
--
ALTER TABLE `crontasks`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `domains`
--
ALTER TABLE `domains`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`key`,`type`,`info`(60)),
  ADD KEY `type` (`type`),
  ADD KEY `value` (`value`(191));

--
-- Indizes für die Tabelle `elements`
--
ALTER TABLE `elements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `aco_id` (`aco_id`),
  ADD KEY `creator_id` (`creator_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `type` (`type`(191));

--
-- Indizes für die Tabelle `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`),
  ADD KEY `article_id` (`article_id`),
  ADD KEY `menu_id` (`menu_id`),
  ADD KEY `menu_parent_id` (`menu_parent_id`);

--
-- Indizes für die Tabelle `formularconfigs`
--
ALTER TABLE `formularconfigs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email_id` (`email_id`),
  ADD KEY `office_id` (`aco_id`);

--
-- Indizes für die Tabelle `formularconfigs_translations`
--
ALTER TABLE `formularconfigs_translations`
  ADD PRIMARY KEY (`id`,`locale`),
  ADD UNIQUE KEY `unique` (`locale`,`id`),
  ADD KEY `id` (`id`);

--
-- Indizes für die Tabelle `formulars`
--
ALTER TABLE `formulars`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `begin_publishing` (`begin_publishing`),
  ADD KEY `end_publishing` (`end_publishing`),
  ADD KEY `foreign_key` (`foreign_key`),
  ADD KEY `freigegeben` (`active`),
  ADD KEY `level` (`level`),
  ADD KEY `lft` (`lft`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `path` (`path`(191)),
  ADD KEY `rght` (`rght`),
  ADD KEY `teaser` (`teaser`);

--
-- Indizes für die Tabelle `menus_temporary`
--
ALTER TABLE `menus_temporary`
  ADD KEY `foreign_key` (`foreign_key`),
  ADD KEY `id` (`id`),
  ADD KEY `locale` (`locale`),
  ADD KEY `path` (`path`(191));

--
-- Indizes für die Tabelle `menus_translations`
--
ALTER TABLE `menus_translations`
  ADD PRIMARY KEY (`locale`,`id`),
  ADD KEY `id` (`id`);

--
-- Indizes für die Tabelle `newslettercampaigns`
--
ALTER TABLE `newslettercampaigns`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `newsletter_id` (`newsletter_id`,`newsletterrecipient_id`),
  ADD KEY `created` (`created`),
  ADD KEY `modified` (`modified`),
  ADD KEY `send` (`send`);

--
-- Indizes für die Tabelle `newsletterinterests`
--
ALTER TABLE `newsletterinterests`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `newsletterinterests` ADD FULLTEXT KEY `value` (`name`);

--
-- Indizes für die Tabelle `newsletterinterests_newsletterrecipients`
--
ALTER TABLE `newsletterinterests_newsletterrecipients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `newsletterrecipient_id` (`newsletterrecipient_id`),
  ADD KEY `unique` (`newsletterinterest_id`,`newsletterrecipient_id`);

--
-- Indizes für die Tabelle `newsletterinterests_newsletters`
--
ALTER TABLE `newsletterinterests_newsletters`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique` (`newsletterinterest_id`,`newsletter_id`),
  ADD KEY `newsletter_id` (`newsletter_id`);

--
-- Indizes für die Tabelle `newsletterrecipients`
--
ALTER TABLE `newsletterrecipients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `approved` (`approved`),
  ADD KEY `created` (`created`),
  ADD KEY `modified` (`modified`),
  ADD KEY `email` (`email`(191));

--
-- Indizes für die Tabelle `newsletters`
--
ALTER TABLE `newsletters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created` (`created`),
  ADD KEY `modified` (`modified`),
  ADD KEY `send` (`send`),
  ADD KEY `to_send` (`to_send`);

--
-- Indizes für die Tabelle `orderpositions`
--
ALTER TABLE `orderpositions`
  ADD KEY `id` (`id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indizes für die Tabelle `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`rnr`),
  ADD KEY `created` (`created`),
  ADD KEY `id` (`id`),
  ADD KEY `menu_id` (`menu_id`),
  ADD KEY `modified` (`modified`),
  ADD KEY `office_id` (`aco_id`);

--
-- Indizes für die Tabelle `persons`
--
ALTER TABLE `persons`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique` (`foreign_key`,`type`(100),`model`(100)) USING BTREE,
  ADD KEY `foreign_key` (`foreign_key`),
  ADD KEY `model` (`model`(191)),
  ADD KEY `type` (`type`(191));

--
-- Indizes für die Tabelle `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indizes für die Tabelle `redirects`
--
ALTER TABLE `redirects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created` (`created`),
  ADD KEY `from_url` (`from_url`(191)),
  ADD KEY `modified` (`modified`);

--
-- Indizes für die Tabelle `resources`
--
ALTER TABLE `resources`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`(191)) USING BTREE,
  ADD KEY `type` (`type`(191)) USING BTREE;

--
-- Indizes für die Tabelle `resources_roles`
--
ALTER TABLE `resources_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique` (`role_id`,`resource_id`),
  ADD KEY `resource_id` (`resource_id`);

--
-- Indizes für die Tabelle `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`(180)) USING BTREE,
  ADD KEY `created` (`created`),
  ADD KEY `modified` (`modified`);

--
-- Indizes für die Tabelle `roles_users`
--
ALTER TABLE `roles_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique` (`role_id`,`user_id`),
  ADD KEY `role_id` (`role_id`);

--
-- Indizes für die Tabelle `texts`
--
ALTER TABLE `texts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created` (`created`),
  ADD KEY `modified` (`modified`),
  ADD KEY `name` (`name`(191)),
  ADD KEY `source` (`source`(191));

--
-- Indizes für die Tabelle `texts_translations`
--
ALTER TABLE `texts_translations`
  ADD PRIMARY KEY (`locale`,`id`),
  ADD KEY `id` (`id`);

--
-- Indizes für die Tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created` (`created`),
  ADD KEY `modified` (`modified`),
  ADD KEY `type` (`type`(191));

--
-- Indizes für die Tabelle `vouchers`
--
ALTER TABLE `vouchers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `order_id` (`order_id`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `created` (`created`),
  ADD KEY `modified` (`modified`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `categorized`
--
ALTER TABLE `categorized`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `newsletterinterests_newsletters`
--
ALTER TABLE `newsletterinterests_newsletters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `orders`
--
ALTER TABLE `orders`
  MODIFY `rnr` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
