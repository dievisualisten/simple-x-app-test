<script data-cookieconsent="ignore" src="/dist/bundle/js/jquery.min.js" ></script>
<script data-cookieconsent="ignore" src="/dist/bundle/js/pico-js.js" ></script>
<script data-cookieconsent="ignore" src="/dist/bundle/js/uikit-core.min.js"></script>
<?php
$theme = menu()->getDomainRecord('theme', 'default');

$this->Asset->script([
    'dist/bundle/js/moment.min.js',
    'dist/bundle/js/gmaps.min.js',
    'dist/bundle/js/moment/de.js',
    'dist/bundle/js/parallax.min.js',
    'dist/bundle/js/flickity.pkgd.min.js',
    'dist/bundle/js/lightgallery-all.min.js',
    'dist/bundle/js/lazysizes.min.js',
    'dist/bundle/js/ls.parent-fit.min.js',
    'dist/bundle/js/ls.rias.min.js',
    'dist/bundle/js/ls.optimumx.min.js',
    'dist/bundle/js/chosen.jquery.min.js',
    'dist/bundle/js/jquery.bootstrap-touchspin.js',
    'dist/bundle/js/bootstrap-datetimepicker.min.js',
    "dist/theme/{$theme}/script.js",
], false, ['defer' => false]);

$scriptAttrs = [
  'defer'         => true,
  'cookieconsent' => 'ignore',
];

echo $this->Asset->renderScripts($scriptAttrs,
  "dist/cache/theme/{$theme}/script.js");

?>

<?= $this->element('tracking/analytics'); ?>
<?= $this->element('conversion-tracking'); ?>
<?= $this->cell('Tracking::formConversionTracking'); ?>
<?= $this->fetch('scriptTop') ?>
<?= $this->fetch('scriptBottom') ?>
<script defer src="/src/static/js/quickfix.js"></script>

