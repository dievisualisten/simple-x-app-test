<!DOCTYPE html>
<html lang="<?= language()->getLanguage() ?>">
<head>
    <?=$this->Html->charset(); ?>
    <title><?= __('Error'); ?> - <?php echo config('Sx.app.cmsname'); ?></title>
    <?= $this->element('html/head'); ?>
</head>
<body class="layout-error">

<?= $this->Flash->render() ?>
<?= $this->fetch('content'); ?>

</body>
</html>
