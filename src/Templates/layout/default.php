<?php

/**
 * @version 4.0.0
 */

/**
 * @var $seperator
 */
$seperator = '-';

/**
 * @var string $theme
 */
$theme = menu()->getDomainRecord('theme', 'default');

?>
<!DOCTYPE html>
<html lang="<?= language()->getLanguage() ?>">
<head>
    <?= $this->element('html/cookie'); ?>
    <?= $this->element('meta'); ?>
    <?= $this->element('html/head'); ?>
</head>

<body class="<?= "theme{$seperator}{$theme} layout{$seperator}{$data->layout}" ?> <?= isset($data->type) ? "view{$seperator}{$data->type} view{$seperator}{$data->type}{$seperator}{$data->layout}" : '' ?>">

<?= $this->element('header'); ?>
<?= $this->fetch('content'); ?>
<?= $this->element('footer'); ?>

<?= $this->element('system/debugbar'); ?>
<?= $this->element('system/systembar'); ?>

<?= $this->element('html/end'); ?>

</body>
</html>
