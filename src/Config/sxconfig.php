<?php
return array (
  'Sx' => 
  array (
    'app' => 
    array (
      'aco' => 
      array (
        'levelnames' => 
        array (
          0 => 'System',
          1 => 'Kunde',
          2 => 'Standort',
          3 => 'Abteilung',
        ),
      ),
      'adminmode' => '0',
      'articles' => 
      array (
        'article' => 
        array (
          'extracontain' => 'Attachments',
        ),
        'children' => 
        array (
          'extracontain' => '',
        ),
        'defaulttype' => 'page',
        'limit' => '10',
      ),
      'baseprotocol' => 'http://',
      'calendar' => 
      array (
        'range' => 
        array (
          'default' => '180',
        ),
      ),
      'cmsname' => 'simple X CMS ™ - 4.0',
      'email' => 
      array (
        'systemadmin' => 'systemadmin@simple-x.de',
      ),
      'fetchextras' => 
      array (
        'canonicalurl' => '1',
        'locallinks' => '1',
        'neighbors' => '0',
      ),
      'hassearch' => '0',
      'language' => 
      array (
        'accept' => 'de',
        'defaultlanguage' => 'de',
        'defaultredirectlanguage' => 'de',
        'multilingual' => '0',
      ),
      'maintenance' => '0',
      'shop' => 
      array (
        'checkouturl' => 'checkout',
        'defaulttaxrate' => '19',
        'sendinvoice' => '1',
        'sendorderemailcopytoseller' => '0',
        'sendvoucher' => '1',
        'shippingcost' => 
        array (
          'calcmethods' => 'shipping',
          'costs' => 
          array (
            'download' => '0',
            'perpost' => '5',
          ),
          'freeover' => '1000000',
          'max' => '1000000',
        ),
        'vouchershop' => '1',
      ),
    ),
    'auth' => 
    array (
      'backend' => 
      array (
        'after_login_redirect_url' => '/system',
        'after_logout_redirect_url' => '/login',
        'credentials' => 
        array (
          'active' => '1',
          'username' => 'login',
        ),
      ),
      'defaultfrontend' => 
      array (
        'after_login_redirect_url' => '/',
        'after_logout_redirect_url' => '/',
        'credentials' => 
        array (
          'active' => '1',
          'approved' => '1',
          'username' => 
          array (
            0 => 'login',
            1 => 'communications.email',
          ),
        ),
      ),
    ),
    'client' => 
    array (
      'name' => 'Kundenname',
    ),
    'email' => 
    array (
      'bcc' => '',
    ),
    'identifier' => 
    array (
      'logindefault' => 
      array (
        'authconfig' => 'auth.defaultfrontend',
      ),
      'register' => 
      array (
        'type' => 'frontenduser',
      ),
      'registerdefault' => 
      array (
        'authconfig' => 'auth.defaultfrontend',
      ),
    ),
    'login' => 
    array (
      'frontenduser' => 
      array (
        'identifier' => 'register',
        'loginurl' => '/anmelden',
        'logouturl' => '/logout',
        'pwlosturl' => '/pwlost',
        'role' => 'ffffffff-ffff-ffff-ffff-ffffffffffff',
      ),
    ),
    'resource' => 
    array (
      'modules' => 
      array (
        'aco' => 
        array (
          'show' => '1',
        ),
        'actionview' => 
        array (
          'show' => '0',
        ),
        'article' => 
        array (
          'content2' => 
          array (
            'name' => '2. Inhalt',
          ),
          'content3' => 
          array (
            'name' => '3. Inhalt',
          ),
          'pagetypes' => 
          array (
            'calendar' => 
            array (
              'default' => 
              array (
                'content' => 
                array (
                  'show' => '1',
                ),
                'content2' => 
                array (
                  'show' => '0',
                ),
                'content3' => 
                array (
                  'show' => '0',
                ),
                'has_teasertext' => 
                array (
                  'show' => '1',
                ),
                'has_teasertext2' => 
                array (
                  'show' => '0',
                ),
                'link' => 
                array (
                  'show' => '0',
                ),
                'moretext' => 
                array (
                  'show' => '1',
                ),
                'name' => 'Standard',
                'panels' => 
                array (
                  'domain' => 
                  array (
                    'show' => '0',
                  ),
                  'event' => 
                  array (
                    'show' => '0',
                  ),
                  'formularconfig' => 
                  array (
                    'show' => '0',
                  ),
                  'map' => 
                  array (
                    'show' => '0',
                  ),
                  'media' => 
                  array (
                    'audio' => 
                    array (
                      'show' => '0',
                    ),
                    'backgroundimg' => 
                    array (
                      'show' => '0',
                    ),
                    'doc' => 
                    array (
                      'show' => '0',
                    ),
                    'download' => 
                    array (
                      'show' => '1',
                    ),
                    'footerimg' => 
                    array (
                      'show' => '0',
                    ),
                    'galerie' => 
                    array (
                      'show' => '0',
                    ),
                    'img' => 
                    array (
                      'show' => '0',
                    ),
                    'img2' => 
                    array (
                      'show' => '0',
                    ),
                    'img3' => 
                    array (
                      'show' => '0',
                    ),
                    'show' => '1',
                    'teaser' => 
                    array (
                      'show' => '1',
                    ),
                    'titleimg' => 
                    array (
                      'show' => '1',
                    ),
                    'video' => 
                    array (
                      'show' => '0',
                    ),
                  ),
                  'menu' => 
                  array (
                    'show' => '1',
                    'teaser1' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser1024' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser128' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser16' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser2' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser2048' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser256' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser32' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser4' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser4096' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser512' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser64' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser8' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser8192' => 
                    array (
                      'show' => '0',
                    ),
                  ),
                  'options' => 
                  array (
                    'show' => '1',
                  ),
                  'rights' => 
                  array (
                    'show' => '1',
                  ),
                  'seo' => 
                  array (
                    'show' => '1',
                  ),
                ),
                'price' => 
                array (
                  'show' => '1',
                ),
                'show' => '1',
                'subline' => 
                array (
                  'show' => '0',
                ),
                'teaser' => 
                array (
                  'show' => '1',
                ),
                'topline' => 
                array (
                  'show' => '0',
                ),
              ),
              'element' => '0',
              'icon' => 'fa fa-calendar',
              'name' => 'Kalender',
            ),
            'direction' => 
            array (
              'default' => 
              array (
                'content' => 
                array (
                  'show' => '1',
                ),
                'content2' => 
                array (
                  'show' => '0',
                ),
                'content3' => 
                array (
                  'show' => '0',
                ),
                'has_teasertext' => 
                array (
                  'show' => '1',
                ),
                'has_teasertext2' => 
                array (
                  'show' => '0',
                ),
                'link' => 
                array (
                  'show' => '0',
                ),
                'moretext' => 
                array (
                  'show' => '1',
                ),
                'name' => 'Standard',
                'panels' => 
                array (
                  'domain' => 
                  array (
                    'show' => '0',
                  ),
                  'event' => 
                  array (
                    'show' => '0',
                  ),
                  'formularconfig' => 
                  array (
                    'show' => '0',
                  ),
                  'map' => 
                  array (
                    'show' => '1',
                  ),
                  'media' => 
                  array (
                    'audio' => 
                    array (
                      'show' => '0',
                    ),
                    'backgroundimg' => 
                    array (
                      'show' => '0',
                    ),
                    'doc' => 
                    array (
                      'show' => '0',
                    ),
                    'download' => 
                    array (
                      'show' => '1',
                    ),
                    'footerimg' => 
                    array (
                      'show' => '0',
                    ),
                    'galerie' => 
                    array (
                      'show' => '0',
                    ),
                    'img' => 
                    array (
                      'show' => '0',
                    ),
                    'img2' => 
                    array (
                      'show' => '0',
                    ),
                    'img3' => 
                    array (
                      'show' => '0',
                    ),
                    'show' => '1',
                    'teaser' => 
                    array (
                      'show' => '1',
                    ),
                    'titleimg' => 
                    array (
                      'show' => '1',
                    ),
                    'video' => 
                    array (
                      'show' => '0',
                    ),
                  ),
                  'menu' => 
                  array (
                    'show' => '1',
                    'teaser1' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser1024' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser128' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser16' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser2' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser2048' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser256' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser32' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser4' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser4096' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser512' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser64' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser8' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser8192' => 
                    array (
                      'show' => '0',
                    ),
                  ),
                  'options' => 
                  array (
                    'show' => '1',
                  ),
                  'rights' => 
                  array (
                    'show' => '1',
                  ),
                  'seo' => 
                  array (
                    'show' => '1',
                  ),
                ),
                'price' => 
                array (
                  'show' => '1',
                ),
                'show' => '1',
                'subline' => 
                array (
                  'show' => '0',
                ),
                'teaser' => 
                array (
                  'show' => '1',
                ),
                'topline' => 
                array (
                  'show' => '0',
                ),
              ),
              'element' => '0',
              'icon' => 'fa fa-map-marker',
              'name' => 'Anfahrt',
            ),
            'event' => 
            array (
              'default' => 
              array (
                'content' => 
                array (
                  'show' => '1',
                ),
                'content2' => 
                array (
                  'show' => '0',
                ),
                'content3' => 
                array (
                  'show' => '0',
                ),
                'has_teasertext' => 
                array (
                  'show' => '1',
                ),
                'has_teasertext2' => 
                array (
                  'show' => '0',
                ),
                'link' => 
                array (
                  'show' => '0',
                ),
                'moretext' => 
                array (
                  'show' => '1',
                ),
                'name' => 'Standard',
                'panels' => 
                array (
                  'domain' => 
                  array (
                    'show' => '0',
                  ),
                  'event' => 
                  array (
                    'show' => '1',
                  ),
                  'formularconfig' => 
                  array (
                    'show' => '0',
                  ),
                  'map' => 
                  array (
                    'show' => '0',
                  ),
                  'media' => 
                  array (
                    'audio' => 
                    array (
                      'show' => '0',
                    ),
                    'backgroundimg' => 
                    array (
                      'show' => '0',
                    ),
                    'doc' => 
                    array (
                      'show' => '0',
                    ),
                    'download' => 
                    array (
                      'show' => '1',
                    ),
                    'footerimg' => 
                    array (
                      'show' => '0',
                    ),
                    'galerie' => 
                    array (
                      'show' => '0',
                    ),
                    'img' => 
                    array (
                      'show' => '0',
                    ),
                    'img2' => 
                    array (
                      'show' => '0',
                    ),
                    'img3' => 
                    array (
                      'show' => '0',
                    ),
                    'show' => '1',
                    'teaser' => 
                    array (
                      'show' => '1',
                    ),
                    'titleimg' => 
                    array (
                      'show' => '1',
                    ),
                    'video' => 
                    array (
                      'show' => '0',
                    ),
                  ),
                  'menu' => 
                  array (
                    'show' => '1',
                    'teaser1' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser1024' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser128' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser16' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser2' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser2048' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser256' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser32' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser4' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser4096' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser512' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser64' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser8' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser8192' => 
                    array (
                      'show' => '0',
                    ),
                  ),
                  'options' => 
                  array (
                    'show' => '1',
                  ),
                  'rights' => 
                  array (
                    'show' => '1',
                  ),
                  'seo' => 
                  array (
                    'show' => '1',
                  ),
                ),
                'price' => 
                array (
                  'show' => '1',
                ),
                'show' => '1',
                'subline' => 
                array (
                  'show' => '0',
                ),
                'teaser' => 
                array (
                  'show' => '1',
                ),
                'topline' => 
                array (
                  'show' => '0',
                ),
              ),
              'element' => '0',
              'icon' => 'fa fa-clock',
              'name' => 'Event',
            ),
            'faq' => 
            array (
              'default' => 
              array (
                'content' => 
                array (
                  'show' => '1',
                ),
                'content2' => 
                array (
                  'show' => '0',
                ),
                'content3' => 
                array (
                  'show' => '0',
                ),
                'has_teasertext' => 
                array (
                  'show' => '1',
                ),
                'has_teasertext2' => 
                array (
                  'show' => '0',
                ),
                'link' => 
                array (
                  'show' => '0',
                ),
                'moretext' => 
                array (
                  'show' => '1',
                ),
                'name' => 'Standard',
                'panels' => 
                array (
                  'domain' => 
                  array (
                    'show' => '0',
                  ),
                  'event' => 
                  array (
                    'show' => '0',
                  ),
                  'formularconfig' => 
                  array (
                    'show' => '0',
                  ),
                  'map' => 
                  array (
                    'show' => '0',
                  ),
                  'media' => 
                  array (
                    'audio' => 
                    array (
                      'show' => '0',
                    ),
                    'backgroundimg' => 
                    array (
                      'show' => '0',
                    ),
                    'doc' => 
                    array (
                      'show' => '0',
                    ),
                    'download' => 
                    array (
                      'show' => '1',
                    ),
                    'footerimg' => 
                    array (
                      'show' => '0',
                    ),
                    'galerie' => 
                    array (
                      'show' => '0',
                    ),
                    'img' => 
                    array (
                      'show' => '0',
                    ),
                    'img2' => 
                    array (
                      'show' => '0',
                    ),
                    'img3' => 
                    array (
                      'show' => '0',
                    ),
                    'show' => '1',
                    'teaser' => 
                    array (
                      'show' => '1',
                    ),
                    'titleimg' => 
                    array (
                      'show' => '1',
                    ),
                    'video' => 
                    array (
                      'show' => '0',
                    ),
                  ),
                  'menu' => 
                  array (
                    'show' => '1',
                    'teaser1' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser1024' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser128' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser16' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser2' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser2048' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser256' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser32' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser4' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser4096' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser512' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser64' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser8' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser8192' => 
                    array (
                      'show' => '0',
                    ),
                  ),
                  'options' => 
                  array (
                    'show' => '1',
                  ),
                  'rights' => 
                  array (
                    'show' => '1',
                  ),
                  'seo' => 
                  array (
                    'show' => '1',
                  ),
                ),
                'price' => 
                array (
                  'show' => '1',
                ),
                'show' => '1',
                'subline' => 
                array (
                  'show' => '0',
                ),
                'teaser' => 
                array (
                  'show' => '1',
                ),
                'topline' => 
                array (
                  'show' => '0',
                ),
              ),
              'element' => '1',
              'icon' => 'fa fa-question',
              'name' => 'Faq',
            ),
            'frontpage' => 
            array (
              'default' => 
              array (
                'content' => 
                array (
                  'show' => '1',
                ),
                'content2' => 
                array (
                  'show' => '0',
                ),
                'content3' => 
                array (
                  'show' => '0',
                ),
                'has_teasertext' => 
                array (
                  'show' => '0',
                ),
                'has_teasertext2' => 
                array (
                  'show' => '0',
                ),
                'link' => 
                array (
                  'show' => '0',
                ),
                'moretext' => 
                array (
                  'show' => '0',
                ),
                'name' => 'Standard',
                'panels' => 
                array (
                  'domain' => 
                  array (
                    'show' => '1',
                  ),
                  'event' => 
                  array (
                    'show' => '0',
                  ),
                  'formularconfig' => 
                  array (
                    'show' => '0',
                  ),
                  'map' => 
                  array (
                    'show' => '0',
                  ),
                  'media' => 
                  array (
                    'audio' => 
                    array (
                      'show' => '0',
                    ),
                    'backgroundimg' => 
                    array (
                      'show' => '0',
                    ),
                    'doc' => 
                    array (
                      'show' => '0',
                    ),
                    'download' => 
                    array (
                      'show' => '1',
                    ),
                    'footerimg' => 
                    array (
                      'show' => '0',
                    ),
                    'galerie' => 
                    array (
                      'show' => '0',
                    ),
                    'img' => 
                    array (
                      'show' => '0',
                    ),
                    'img2' => 
                    array (
                      'show' => '0',
                    ),
                    'img3' => 
                    array (
                      'show' => '0',
                    ),
                    'show' => '1',
                    'teaser' => 
                    array (
                      'show' => '1',
                    ),
                    'titleimg' => 
                    array (
                      'show' => '1',
                    ),
                    'video' => 
                    array (
                      'show' => '0',
                    ),
                  ),
                  'menu' => 
                  array (
                    'show' => '0',
                    'teaser1' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser1024' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser128' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser16' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser2' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser2048' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser256' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser32' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser4' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser4096' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser512' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser64' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser8' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser8192' => 
                    array (
                      'show' => '0',
                    ),
                  ),
                  'options' => 
                  array (
                    'show' => '1',
                  ),
                  'rights' => 
                  array (
                    'show' => '1',
                  ),
                  'seo' => 
                  array (
                    'show' => '1',
                  ),
                ),
                'price' => 
                array (
                  'show' => '0',
                ),
                'show' => '1',
                'subline' => 
                array (
                  'show' => '1',
                ),
                'teaser' => 
                array (
                  'show' => '0',
                ),
                'topline' => 
                array (
                  'show' => '1',
                ),
              ),
              'element' => '0',
              'icon' => 'fa fa-star',
              'name' => 'Startseite',
            ),
            'link' => 
            array (
              'default' => 
              array (
                'content' => 
                array (
                  'show' => '1',
                ),
                'content2' => 
                array (
                  'show' => '0',
                ),
                'content3' => 
                array (
                  'show' => '0',
                ),
                'has_teasertext' => 
                array (
                  'show' => '1',
                ),
                'has_teasertext2' => 
                array (
                  'show' => '0',
                ),
                'link' => 
                array (
                  'show' => '1',
                ),
                'moretext' => 
                array (
                  'show' => '1',
                ),
                'name' => 'Standard',
                'panels' => 
                array (
                  'domain' => 
                  array (
                    'show' => '0',
                  ),
                  'event' => 
                  array (
                    'show' => '0',
                  ),
                  'formularconfig' => 
                  array (
                    'show' => '0',
                  ),
                  'map' => 
                  array (
                    'show' => '0',
                  ),
                  'media' => 
                  array (
                    'audio' => 
                    array (
                      'show' => '0',
                    ),
                    'backgroundimg' => 
                    array (
                      'show' => '0',
                    ),
                    'doc' => 
                    array (
                      'show' => '0',
                    ),
                    'download' => 
                    array (
                      'show' => '0',
                    ),
                    'footerimg' => 
                    array (
                      'show' => '0',
                    ),
                    'galerie' => 
                    array (
                      'show' => '0',
                    ),
                    'img' => 
                    array (
                      'show' => '0',
                    ),
                    'img2' => 
                    array (
                      'show' => '0',
                    ),
                    'img3' => 
                    array (
                      'show' => '0',
                    ),
                    'show' => '1',
                    'teaser' => 
                    array (
                      'show' => '1',
                    ),
                    'titleimg' => 
                    array (
                      'show' => '0',
                    ),
                    'video' => 
                    array (
                      'show' => '0',
                    ),
                  ),
                  'menu' => 
                  array (
                    'show' => '1',
                    'teaser1' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser1024' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser128' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser16' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser2' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser2048' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser256' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser32' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser4' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser4096' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser512' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser64' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser8' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser8192' => 
                    array (
                      'show' => '0',
                    ),
                  ),
                  'options' => 
                  array (
                    'show' => '1',
                  ),
                  'rights' => 
                  array (
                    'show' => '1',
                  ),
                  'seo' => 
                  array (
                    'show' => '0',
                  ),
                ),
                'price' => 
                array (
                  'show' => '1',
                ),
                'show' => '1',
                'subline' => 
                array (
                  'show' => '0',
                ),
                'teaser' => 
                array (
                  'show' => '1',
                ),
                'topline' => 
                array (
                  'show' => '0',
                ),
              ),
              'element' => '0',
              'icon' => 'fa fa-link',
              'name' => 'Link',
            ),
            'page' => 
            array (
              'default' => 
              array (
                'content' => 
                array (
                  'show' => '1',
                ),
                'content2' => 
                array (
                  'show' => '0',
                ),
                'content3' => 
                array (
                  'show' => '0',
                ),
                'has_teasertext' => 
                array (
                  'show' => '1',
                ),
                'has_teasertext2' => 
                array (
                  'show' => '0',
                ),
                'link' => 
                array (
                  'show' => '0',
                ),
                'moretext' => 
                array (
                  'show' => '1',
                ),
                'name' => 'Standard',
                'panels' => 
                array (
                  'domain' => 
                  array (
                    'show' => '0',
                  ),
                  'event' => 
                  array (
                    'show' => '0',
                  ),
                  'formularconfig' => 
                  array (
                    'show' => '0',
                  ),
                  'map' => 
                  array (
                    'show' => '0',
                  ),
                  'media' => 
                  array (
                    'audio' => 
                    array (
                      'show' => '0',
                    ),
                    'backgroundimg' => 
                    array (
                      'show' => '0',
                    ),
                    'doc' => 
                    array (
                      'show' => '0',
                    ),
                    'download' => 
                    array (
                      'show' => '1',
                    ),
                    'footerimg' => 
                    array (
                      'show' => '0',
                    ),
                    'galerie' => 
                    array (
                      'show' => '1',
                    ),
                    'img' => 
                    array (
                      'show' => '0',
                    ),
                    'img2' => 
                    array (
                      'show' => '0',
                    ),
                    'img3' => 
                    array (
                      'show' => '0',
                    ),
                    'show' => '1',
                    'teaser' => 
                    array (
                      'show' => '1',
                    ),
                    'titleimg' => 
                    array (
                      'show' => '1',
                    ),
                    'video' => 
                    array (
                      'show' => '0',
                    ),
                  ),
                  'menu' => 
                  array (
                    'show' => '1',
                    'teaser1' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser1024' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser128' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser16' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser2' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser2048' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser256' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser32' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser4' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser4096' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser512' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser64' => 
                    array (
                      'show' => '0',
                    ),
                    'teaser8' => 
                    array (
                      'show' => '1',
                    ),
                    'teaser8192' => 
                    array (
                      'show' => '0',
                    ),
                  ),
                  'options' => 
                  array (
                    'show' => '1',
                  ),
                  'rights' => 
                  array (
                    'show' => '1',
                  ),
                  'seo' => 
                  array (
                    'show' => '1',
                  ),
                ),
                'price' => 
                array (
                  'show' => '1',
                ),
                'show' => '1',
                'subline' => 
                array (
                  'show' => '0',
                ),
                'teaser' => 
                array (
                  'show' => '1',
                ),
                'topline' => 
                array (
                  'show' => '0',
                ),
              ),
              'element' => '0',
              'icon' => 'fa fa-file',
              'name' => 'Seite',
            ),
          ),
          'panels' => 
          array (
            'media' => 
            array (
              'tabs' => 
              array (
                'audio' => 
                array (
                  'name' => 'Audio',
                  'order' => '900',
                ),
                'backgroundimg' => 
                array (
                  'name' => 'Hintergrundbilder',
                  'order' => '100',
                ),
                'doc' => 
                array (
                  'name' => 'Dokumente',
                  'order' => '700',
                ),
                'download' => 
                array (
                  'name' => 'Downloads',
                  'order' => '600',
                ),
                'footerimg' => 
                array (
                  'name' => 'Abschlussbild',
                  'order' => '1100',
                ),
                'galerie' => 
                array (
                  'name' => 'Galeriebilder',
                  'order' => '1000',
                ),
                'img' => 
                array (
                  'name' => 'Bilder',
                  'order' => '300',
                ),
                'img2' => 
                array (
                  'name' => 'Bilder (2)',
                  'order' => '400',
                ),
                'img3' => 
                array (
                  'name' => 'Bilder (3)',
                  'order' => '500',
                ),
                'teaser' => 
                array (
                  'name' => 'Teaserbild',
                  'order' => '1200',
                ),
                'titleimg' => 
                array (
                  'name' => 'Titelbilder',
                  'order' => '200',
                ),
                'video' => 
                array (
                  'name' => 'Videos',
                  'order' => '800',
                ),
              ),
            ),
          ),
          'show' => '1',
          'subline' => 
          array (
            'name' => 'Subline',
          ),
          'topline' => 
          array (
            'name' => 'Topline',
          ),
        ),
        'attachment' => 
        array (
          'show' => '1',
        ),
        'configuration' => 
        array (
          'show' => '1',
        ),
        'crontask' => 
        array (
          'show' => '1',
        ),
        'domain' => 
        array (
          'show' => '1',
        ),
        'email' => 
        array (
          'show' => '1',
        ),
        'formular' => 
        array (
          'show' => '1',
        ),
        'formularconfig' => 
        array (
          'show' => '1',
        ),
        'menu' => 
        array (
          'alias' => 
          array (
            'show' => '1',
          ),
          'domain' => 
          array (
            'show' => '1',
          ),
          'menu' => 
          array (
            'show' => '1',
          ),
          'teaser' => 
          array (
            'teaser1' => 
            array (
              'name' => 'Struktur',
              'order' => '1400',
              'show' => '1',
              'tooltip' => '1',
              'value' => '1',
            ),
            'teaser1024' => 
            array (
              'name' => 'Abschnitt',
              'order' => '1100',
              'show' => '1',
              'tooltip' => '1024',
              'value' => '1024',
            ),
            'teaser128' => 
            array (
              'name' => 'Frei ohne Abschnitt 1',
              'order' => '800',
              'show' => '0',
              'tooltip' => '128',
              'value' => '128',
            ),
            'teaser16' => 
            array (
              'name' => 'CTA',
              'order' => '500',
              'show' => '1',
              'tooltip' => '16',
              'value' => '16',
            ),
            'teaser2' => 
            array (
              'name' => 'Menü',
              'order' => '200',
              'show' => '1',
              'tooltip' => 'Menüpunkt / 2',
              'value' => '2',
            ),
            'teaser2048' => 
            array (
              'name' => 'Abschnitt 2',
              'order' => '1200',
              'show' => '0',
              'tooltip' => '2048',
              'value' => '2048',
            ),
            'teaser256' => 
            array (
              'name' => 'Frei ohne Abschnitt 3',
              'order' => '900',
              'show' => '0',
              'tooltip' => '256',
              'value' => '256',
            ),
            'teaser32' => 
            array (
              'name' => 'Liste',
              'order' => '600',
              'show' => '1',
              'tooltip' => 'Listeneintrag / 32',
              'value' => '32',
            ),
            'teaser4' => 
            array (
              'name' => 'Teaser',
              'order' => '300',
              'show' => '1',
              'tooltip' => '4',
              'value' => '4',
            ),
            'teaser4096' => 
            array (
              'name' => 'Akkordeon',
              'order' => '1300',
              'show' => '0',
              'tooltip' => 'Eintrag im Akkordeon / 4096',
              'value' => '4096',
            ),
            'teaser512' => 
            array (
              'name' => 'Frei ohne Abschnitt 4',
              'order' => '1000',
              'show' => '0',
              'tooltip' => '512',
              'value' => '512',
            ),
            'teaser64' => 
            array (
              'name' => 'Frei ohne Abschnitt 2',
              'order' => '700',
              'show' => '0',
              'tooltip' => '64',
              'value' => '64',
            ),
            'teaser8' => 
            array (
              'name' => 'Slider',
              'order' => '400',
              'show' => '1',
              'tooltip' => '8',
              'value' => '8',
            ),
          ),
        ),
        'newsletter' => 
        array (
          'show' => '1',
        ),
        'newsletterinterest' => 
        array (
          'show' => '1',
        ),
        'newsletterrecipient' => 
        array (
          'show' => '1',
        ),
        'order' => 
        array (
          'show' => '1',
        ),
        'pageconfiguration' => 
        array (
          'show' => '1',
        ),
        'redirect' => 
        array (
          'show' => '1',
        ),
        'resource' => 
        array (
          'show' => '1',
        ),
        'role' => 
        array (
          'show' => '1',
        ),
        'text' => 
        array (
          'show' => '1',
        ),
        'user' => 
        array (
          'show' => '1',
        ),
        'voucher' => 
        array (
          'show' => '1',
        ),
      ),
    ),
  ),
);