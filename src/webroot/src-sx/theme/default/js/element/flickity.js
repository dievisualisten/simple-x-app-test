import { Any, Dom, Element, Obj } from "@kizmann/pico-js";

/**
 * @see https://flickity.metafizzy.co/options.html
 */

window.defaultFlickity = {
    // autoPlay: 500,
    wrapAround: true,
    groupCells: true,
    pageDots: false,
    prevNextButtons: false,
    hideOnEmpty: null
};

Element.alias('flickity', function (el, options) {

    options = Obj.assign(window.defaultFlickity, options);

    $(el).on('initialized.flickity', () => {
        if ( $(el).data('flickity').slides.length <= 1 ) {
            $(el).parent().find(options.hideOnEmpty).hide();
        }
    });

    $(el).flickity(options).trigger('initialized.flickity');
});

Dom.ready(() => {
    Element.observe('flickity');
});

Element.alias('flickity-prev', function (el, options) {

    options = Obj.assign({
        el: null, hideOnEmpty: false
    }, options);

    if ( $(el).closest('.flickity-frame').find(options.el).length === 0 ) {
        return console.error('Element must be defined for flickity prev.');
    }

    if ( Any.isString(options.hideOnEmpty) ) {
        $(options.hideOnEmpty).hide();
    }

    if ( $(el).closest('.flickity-frame').find(options.el).data('flickity').slides.length <= 1 ) {
        $(el).attr('disabled', true);
    }

    $(el).click(() => $(el).closest('.flickity-frame').find(options.el).flickity('previous'));
});

Dom.ready(() => {
    Element.observe('flickity-prev');
});

Element.alias('flickity-next', function (el, options) {

    options = Obj.assign({
        el: null, hideOnEmpty: false
    }, options);

    if ( $(el).closest('.flickity-frame').find(options.el).length === 0 ) {
        return console.error('Element must be defined for flickity next.');
    }

    if ( Any.isString(options.hideOnEmpty) ) {
        $(options.hideOnEmpty).hide();
    }

    if ( $(el).closest('.flickity-frame').find(options.el).data('flickity').slides.length <= 1 ) {
        $(el).attr('disabled', true);
    }

    $(el).click(() => $(el).closest('.flickity-frame').find(options.el).flickity('next'));
});

Dom.ready(() => {
    Element.observe('flickity-next');
});


