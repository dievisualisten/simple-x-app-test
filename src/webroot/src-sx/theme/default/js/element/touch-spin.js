import { Dom, Element, Obj } from "@kizmann/pico-js";

/**
 * @see https://www.virtuosoft.eu/code/bootstrap-touchspin/
 */

Element.alias('touch-spin', function (el, options) {

    options = Obj.assign({
        verticalbuttons: false,
        buttondown_class: 'button button--primary button--square',
        buttonup_class: 'button button--primary button--square',
    }, options);

    $(el).TouchSpin(options);
});

Dom.ready(() => {
    Element.observe('touch-spin');
});
