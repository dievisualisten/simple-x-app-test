import { Element, Dom, Obj } from "@kizmann/pico-js";

/**
 * @see https://harvesthq.github.io/chosen/options.html
 */

Element.alias('chosen', function (el, options) {

    if ( $(window).width() <= 768 && !$(el).attr('multiple') ) {
        return;
    }

    options = Obj.assign({
        width: 'auto',
        disable_search_threshold: 10,
        allow_single_deselect: true,
    }, options);

    $(el).chosen(options);
});

Dom.ready(() => {
    Element.observe('chosen');
});
