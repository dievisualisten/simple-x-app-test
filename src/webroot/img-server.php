<?php
/* CONFIG ----------------------------------------------------------------------------------------------------------- */
require_once(dirname(__DIR__) . '/Bootstrap/constants.php');
error_reporting(0);
$maxRes = 1920;
$cachePath = "dist/cache/img"; // where to store the generated re-sized images. Specify from your document root!
$useRemoteCompressor = true;
$quality = 75; //the quality of any generated JPGs on a scale of 0 to 100
$remoteQuality = 75;
$browserCache = 60 * 60 * 24 * 180; // How long the BROWSER cache should last (seconds, minutes, hours, days. 7days by default)
$provider = 'resmush'; // resmush or kraken;
/* END CONFIG ----------------------------------------------------------------------------------------------------------

/* get all of the required data from the HTTP request */
$documentRoot = $_SERVER['DOCUMENT_ROOT'];
$requestedUri = parse_url(urldecode($_SERVER['REQUEST_URI']), PHP_URL_PATH);

$re = '/w-(\d+|width)\//';

preg_match($re, $requestedUri, $matches);

$requestedUri = preg_replace($re, '', $requestedUri);

$requestedFile = basename($requestedUri);
$sourceFile = $documentRoot . $requestedUri;
$resolution = 0;

// does the $cache_path directory exist already?
if ( ! is_dir($documentRoot . "/" . $cachePath) ) { // no
    if ( ! mkdir($documentRoot . "/" . $cachePath, 0755, true) ) { // so make it
        if ( ! is_dir($documentRoot . "/" . $cachePath) ) { // check again to protect against race conditions
            // uh-oh, failed to make that directory
            sendErrorImage("Failed to create cache directory at: " . $documentRoot . "/" . $cachePath);
        }
    }
}

/* helper function: Send headers and returns an image. */
function sendImage($filename, $browser_cache)
{
    $extension = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
    if ( in_array($extension, ['png', 'gif', 'jpeg']) ) {
        header("Content-Type: image/" . $extension);
    } else {
        header("Content-Type: image/jpeg");
    }
    //header("Cache-Control: private, max-age=" . $browser_cache);
    header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $browser_cache) . ' GMT');

    $lastModifiedTime = filemtime($filename);

    header("Last-Modified: " . gmdate("D, d M Y H:i:s", $lastModifiedTime) . " GMT");
    header('Etag: ' . md5_file($filename));
    header('Content-Length: ' . filesize($filename));
    readfile($filename);
    exit();
}

/* helper function: Create and send an image with an error message. */
function sendErrorImage($message)
{
    /* get all of the required data from the HTTP request */
    $documentRoot = $_SERVER['DOCUMENT_ROOT'];
    $requestedUri = parse_url(urldecode($_SERVER['REQUEST_URI']), PHP_URL_PATH);
    $requestedFile = basename($requestedUri);
    $sourceFile = $documentRoot . $requestedUri;


    $im = ImageCreateTrueColor(800, 300);
    $text_color = ImageColorAllocate($im, 233, 14, 91);
    $message_color = ImageColorAllocate($im, 91, 112, 233);

    ImageString($im, 5, 5, 5, "Adaptive Images encountered a problem:", $text_color);
    ImageString($im, 3, 5, 25, $message, $message_color);

    ImageString($im, 5, 5, 85, "Potentially useful information:", $text_color);
    ImageString($im, 3, 5, 105, "DOCUMENT ROOT IS: $documentRoot", $text_color);
    ImageString($im, 3, 5, 125, "REQUESTED URI WAS: $requestedUri", $text_color);
    ImageString($im, 3, 5, 145, "REQUESTED FILE WAS: $requestedFile", $text_color);
    ImageString($im, 3, 5, 165, "SOURCE FILE IS: $sourceFile", $text_color);


    header("Cache-Control: no-store");
    header('Expires: ' . gmdate('D, d M Y H:i:s', time() - 1000) . ' GMT');
    header('Content-Type: image/jpeg');
    ImageJpeg($im);
    ImageDestroy($im);
    exit();
}

/* generates the given cache file for the given source file with the given resolution */
function generateImage($sourceFile, $cacheFile, $resolution, $cacheFileUrl)
{

    global $quality, $useRemoteCompressor, $remoteQuality, $provider;

    $cacheDir = dirname($cacheFile);


    // does the directory exist already?
    if ( ! is_dir($cacheDir) ) {
        if ( ! mkdir($cacheDir, 0755, true) ) {
            // check again if it really doesn't exist to protect against race conditions
            if ( ! is_dir($cacheDir) ) {
                // uh-oh, failed to make that directory
                sendErrorImage("Failed to create cache directory: $cacheDir");
            }
        }
    }

    if ( ! is_writable($cacheDir) ) {
        sendErrorImage("The cache directory is not writable: $cacheDir");
    }

    $extension = strtolower(pathinfo($sourceFile, PATHINFO_EXTENSION));

    // Check the image dimensions
    $dimensions = GetImageSize($sourceFile);
    $width = $dimensions[0];
    $height = $dimensions[1];

    // We need to resize the source image to the width of the resolution breakpoint we're working with

    //die Angeforderte Bildbreite ist größer als das originalbild
    if ( $resolution >= $width || $resolution == 0 ) {
        $newWidth = $width;
        $newHeight = $height;
    } else {
        $ratio = $height / $width;
        $newWidth = $resolution;
        $newHeight = ceil($newWidth * $ratio);
    }

    if ( ! extension_loaded('imagick') ) {

        $dst = ImageCreateTrueColor($newWidth, $newHeight); // re-sized image

        switch ( $extension ) {
            case 'png':
                $src = @ImageCreateFromPng($sourceFile); // original image
                break;
            case 'gif':
                $src = @ImageCreateFromGif($sourceFile); // original image
                break;
            default:
                $src = @ImageCreateFromJpeg($sourceFile); // original image
                ImageInterlace($dst, true); // Enable interlancing (progressive JPG, smaller size file)
                break;
        }

        if ( $extension == 'png' ) {
            imagealphablending($dst, false);
            imagesavealpha($dst, true);
            $transparent = imagecolorallocatealpha($dst, 255, 255, 255, 127);
            imagefilledrectangle($dst, 0, 0, $newWidth, $newHeight, $transparent);
        }

        ImageCopyResampled($dst, $src, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height); // do the resize in memory
        ImageDestroy($src);


        // save the new file in the appropriate path
        switch ( $extension ) {
            case 'png':
                $gotSaved = ImagePng($dst, $cacheFile);
                break;
            case 'gif':
                $gotSaved = ImageGif($dst, $cacheFile);
                break;
            default:
                $gotSaved = ImageJpeg($dst, $cacheFile, $quality);
                break;
        }

    }


    if ( extension_loaded('imagick') ) {
        $imagick = new Imagick($sourceFile);
        $imagick->scaleImage($newWidth, $newHeight);
        $imagick->setImageCompressionQuality($quality);
        $gotSaved = $imagick->writeImage($cacheFile);
    }


    if ( $gotSaved && $useRemoteCompressor ) {


        $remoteFile = '';

        if ( $provider === 'resmush' ) {
            $o = json_decode(file_get_contents('http://api.resmush.it/ws.php?img=' . $cacheFileUrl . '&qlty=' . $remoteQuality));

            if ( ! isset($o->error) ) {
                $remoteFile = $o->dest;
                //file_put_contents('../logs/img-server.log', 'Resmush - ' . $o->dest . "\n", FILE_APPEND);
            } else {
                file_put_contents('../logs/img-server.log', 'Resmush Remote compressor error - ' . $o->error . "\n", FILE_APPEND);
            }

        } else {
            if ( $provider === 'kraken' ) {
                require_once("../vendor/kraken-io/kraken-php/lib/Kraken.php");

                $kraken = new Kraken(KRAKEN_IO_KEY, KRAKEN_IO_SECRET);

                $params = [
                  "url"     => $cacheFileUrl,
                  "wait"    => true,
                  "lossy"   => true,
                  "quality" => $remoteQuality,
                ];

                $data = $kraken->url($params);

                if ( $data["success"] ) {
                    $remoteFile = $data["kraked_url"];
                    //file_put_contents('../logs/img-server.log', 'Kraken - ' . $data["kraked_url"] . "\n", FILE_APPEND);
                } else {
                    file_put_contents('../logs/img-server.log', 'Kraken Remote compressor error - ' . $data["message"] . "\n", FILE_APPEND);
                }
            }
        }

        if ( ! empty($remoteFile) ) {
            copy($remoteFile, $cacheFile);
        } else {
            //Fehler, Bild noch mal, jetzt aber komprimiert speichern
            if ( extension_loaded('imagick') ) {

                $imagick = new Imagick($sourceFile);
                $imagick->scaleImage($newWidth, $newHeight);
                $imagick->setImageCompressionQuality($quality);
                $imagick->setInterlaceScheme(Imagick::INTERLACE_PLANE);
                $gotSaved = $imagick->writeImage($cacheFile);

                file_put_contents('../logs/img-server.log', date('Y-m-d H:i:s') . ' Info: - Saved with imagick' . "\n", FILE_APPEND);

            } else {

                // nur für JPEG
                switch ( $extension ) {
                    case 'jpg':
                        $gotSaved = ImageJpeg($dst, $cacheFile, $quality);
                        break;
                }

                file_put_contents('../logs/img-server.log', date('Y-m-d H:i:s') . ' Info: Saved with GD' . "\n", FILE_APPEND);
            }
        }
    }

    if ( ! $gotSaved && ! file_exists($cacheFile) ) {
        sendErrorImage("Failed to create image: $cacheFile");
    }

    return $cacheFile;
}


// check if the file exists at all
if ( ! file_exists($sourceFile) ) {
    header("Status: 404 Not Found");
    exit();
}

//resolution SX
if ( isset($matches[0]) ) {
    $resolution = (int) str_replace('/', '', str_replace('w-', '', $matches[0]));
}

$resolution = ceil($resolution);

if ( $resolution > $maxRes ) {
    $resolution = $maxRes;
}

/* if the requested URL starts with a slash, remove the slash */
if ( substr($requestedUri, 0, 1) == "/" ) {
    $requestedUri = substr($requestedUri, 1);
}

/* whew might the cache file be? */
$cache_file = $documentRoot . "/" . $cachePath . "/" . $resolution . "/" . $requestedUri;


/* Use the resolution value as a path variable and check to see if an image of the same name exists at that path */
if ( file_exists($cache_file) ) { // it exists cached at that size
    sendImage($cache_file, $browserCache);
}

if ( isset($_SERVER['HTTPS']) ) {
    $url = 'https://';
} else {
    $url = 'http://';
}

$cache_file_url = $url . $_SERVER['SERVER_NAME'] . "/" . $cachePath . "/" . $resolution . "/" . $requestedUri;


/* It exists as a source file, and it doesn't exist cached - lets make one: */
$file = generateImage($sourceFile, $cache_file, $resolution, $cache_file_url);

sendImage($cache_file, $browserCache);
exit;
