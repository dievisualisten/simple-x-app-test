const fs = require('fs');
const path = require('path');
const glob = require('glob');

const sourceDir = 'src/theme/default/scss';
const targetDir = 'src/theme/default/sass';

let targets = [];

glob(path.resolve(__dirname, sourceDir + '/**/*.scss'), {}, (err, files) => {

    let sourceReplace = path.resolve(__dirname, sourceDir);
    let targetReplace = path.resolve(__dirname, targetDir);

    files.forEach((file) => {

        let fileDisplay  = file.replace(path.resolve(__dirname), '');

        let fileBuffer = fs.readFileSync(file);

        file = file.replace(sourceReplace, targetReplace);

        if ( fileBuffer.toString().match(/\$.*?:\s*\(\s*\n/g) ) {

            console.log("\x1b[36m", 'Skip file: ' + fileDisplay);

            fs.writeFileSync(file, fileBuffer.toString());

            return targets.push(file);
        }

        let fileText = `${fileBuffer.toString().trim()}\n\n`;

        console.log("\x1b[32m", 'Reading file: ' + fileDisplay);

        file = file.replace(/\.scss/, '.sass');

        targets.push(file);

        if ( ! fs.existsSync(path.dirname(file)) ) {
            fs.mkdirSync(path.dirname(file), { recursive: true });
        }

        fileText = fileText.replace(/([^\n]+)\t*{(\s*\/\/[^\n]+)?\t*(\n|$)/g, "$1\n");
        fileText = fileText.replace(/([^\n]+)\t*;(\s*\/\/[^\n]+)?\t*(\n|$)/g, "$1$2\n");
        fileText = fileText.replace(/[\s\n]*}\t*(\/\/[^\t\n]+)?(\n|$)/g, "\n");

        fs.writeFileSync(file, fileText);
    });

    if ( files.length - targets.length > 0 ) {
        console.log("\x1b[31m", `Errors encountered ${files.length - targets.length} 🥺`);
    } else {
        console.log("\x1b[36m", `Total converted ${targets.length} 😍`);
    }
});