import { Element } from "@kizmann/pico-js";

Element.setPrefix('sx');

window.SX_BREAKPOINTS = {
    xs: 576,
    sm: 768,
    md: 992,
    lg: 1200,
    xl: 1440
};

require('@src-sx/library/lazysizes.js');
require('@src-sx/theme/default/js/element/form-visibility.js');
require('@src-sx/theme/default/js/element/touch-spin.js');
require('@src-sx/theme/default/js/element/datepicker.js');
require('@src-sx/theme/default/js/element/chosen.js');
require('@src-sx/theme/default/js/element/flickity.js');
require('@src-sx/theme/default/js/element/light-gallery.js');
require('@src-sx/theme/default/js/element/cookie.js');
require('@src-sx/theme/default/js/element/ratio.js');
require('@src-sx/theme/default/js/element/vimeo.js');
require('@src-sx/theme/default/js/element/youtube.js');
require('@src-sx/theme/default/js/element/video.js');

require('./project');
