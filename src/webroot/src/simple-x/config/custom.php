<script>

    /**
     * Customfields for article
     */

    pi.Dom.ready(function () {

        sx.Form.add('sx-article-custom', [
            {
                // <NTabsItem:0>
                'NTabsItem:0': {
                    vIf: function () {
                        return pi.Obj.get(this.pagetype,
                            'price.show', '0') === '1';
                    },
                    $props: {
                        name: 'custom',
                        sort: 30,
                        label: pi.Locale.trans('Zusatzfelder'),
                        icon: 'fa fa-globe'
                    },
                    content: {
                        'NFormItem:1': {
                            $props: {
                                label: pi.Locale.trans('Erstes FAQ Item öffnen')
                            },
                            content: {
                                'NSwitch:0': {
                                    model: {
                                        path: 'extended_data_cfg.layout.openFirstFaqItem',
                                        fallback: false
                                    },
                                    content: [
                                        pi.Locale.trans('Erstes FAQ Item öffnen')
                                    ]
                                }
                            }
                        },
                        'NFormItem:2': {
                            $props: {
                                label: pi.Locale.trans('Text linksbündig darstellen')
                            },
                            content: {
                                'NSwitch:0': {
                                    model: {
                                        path: 'extended_data_cfg.layout.textleft',
                                        fallback: false
                                    },
                                    content: [
                                        pi.Locale.trans('Text linksbündig darstellen')
                                    ]
                                }
                            }
                        },
                        'NFormItem:3': {
                            $props: {
                                label: pi.Locale.trans('Element')
                            },
                            content: {
                                'NInput:0': {
                                    model: {
                                        path: 'extended_data_cfg.custom.element'
                                    }
                                }
                            }
                        }
                    }
                },
                // </ NTabsItem:0>
            }
        ]);
    });
</script>
