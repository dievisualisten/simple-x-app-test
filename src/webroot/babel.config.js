module.exports = function (api) {

    api.cache(false);

    const presets = [
        [
            '@babel/preset-env',
            {
                modules: false,
            }
        ],
    ];

    const plugins= [
        "@babel/plugin-proposal-class-properties",
        "@babel/plugin-proposal-object-rest-spread",
        "@babel/plugin-proposal-export-default-from"
    ];

    return { presets,  plugins };
};
