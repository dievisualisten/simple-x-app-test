<?php

namespace App\Controller\Component;

/**
 * This component provides compatibility between the dataTables jQuery plugin and CakePHP 2
 *
 * @author chris
 * @package DataTableComponent
 * @link http://www.datatables.net/release-datatables/examples/server_side/server_side.html parts of code borrowed from dataTables example
 * @since version 1.2.1
 */
class DataTableComponent extends Component
{

    private $model;
    private $controller;
    public $conditionsByValidate = 0;
    public $emptyElements = 0;
    public $fields = [];
    public $mDataProp = false;
    public $hasToBeFiltered = false;
    public $minLengthOfFilter = 2;
    public $showDeleted = false;

    public function __construct()
    {

    }

    public function initialize(Controller $controller)
    {
        $this->controller = $controller;
        $modelName = $this->controller->modelClass;
        $this->model = $this->controller->{$modelName};
    }

    /**
     * returns dataTables compatible array - just json_encode the resulting aray
     *
     * @param object $controller optional
     * @param object $model optional
     * @return array
     */
    public function getResponse()
    {

        $this->controller->paginate['deleted'] = $this->showDeleted;

        $conditions = $rowConditions = isset($this->controller->paginate['conditions']) ? $this->controller->paginate['conditions'] : null;
        $contain = isset($this->controller->paginate['contain']) ? $this->controller->paginate['contain'] : false;

        $isFiltered = false;
        $isReallyFiltered = false;

        if (isset($this->controller->request->query)) {
            $httpGet = $this->controller->request->query;
        }

        // check for ORDER BY in GET request
        if (isset($httpGet) && isset($httpGet['order'])) {
            $orderBy = $this->getOrderByStatements();
            if (!empty($orderBy)) {
                $this->controller->paginate = array_merge($this->controller->paginate, ['order' => $orderBy]);
            }
        }

        // check for WHERE statement in GET request
        if (isset($httpGet) && !empty($httpGet['search'])) {

            $conditions = $this->getWhereConditions();

            if (!empty($conditions)) {
                if ($this->getLengthOfWhereConditions() >= $this->minLengthOfFilter) {
                    $isReallyFiltered = true;
                }
            }

            $this->controller->paginate = array_merge_recursive($this->controller->paginate,
                ['conditions' => ['AND' => $conditions]]);
            $isFiltered = true;
        }

        // @todo avoid multiple queries for finding count, maybe look into "SQL CALC FOUND ROWS"
        // get full count
        $this->model->recursive = -1;

        $total = $filteredTotal = 0;
        $data = [];

        if (!$this->hasToBeFiltered || ($this->hasToBeFiltered && $isReallyFiltered)) {

            $total = $this->model->find('count',
                ['deleted' => $this->showDeleted, 'contain' => $contain, 'conditions' => $rowConditions]);

            $parameters = $this->controller->paginate;

            if ($isFiltered) {
                $filteredTotal = $this->model->find('count', [
                    'deleted' => $this->showDeleted,
                    'contain' => $contain,
                    'conditions' => $parameters['conditions'],
                ]);
            }

            // set sql limits
            if (isset($this->controller->request->query['start']) && $this->controller->request->query['length'] != '-1') {
                $start = $this->controller->request->query['start'];
                $length = $this->controller->request->query['length'];
                $parameters['limit'] = $length;
                $parameters['offset'] = $start;
            }

            // execute sql select
            $data = $this->model->find('all', $parameters);
        }

        // dataTables compatible array
        $response = [
            'draw' => isset($this->controller->request->query['draw']) ? intval($this->controller->request->query['draw']) : 1,
            'recordsTotal' => $total,
            'recordsFiltered' => $isFiltered === true ? $filteredTotal : $total,
            'data' => [],
        ];

        // return data
        if (!$data) {
            return $response;
        } else {
            foreach ($data as $i) {
                if ($this->mDataProp == true) {

                }

                $response['data'][] = $i;

            }
        }

        return $response;
    }

    /**
     * returns sql order by string after converting dataTables GET request into Cake style order by
     *
     * @param void
     * @return string
     */
    private function getOrderByStatements()
    {

        $orderBy = '';

        $orders = $this->controller->request->query['order'];
        $columns = $this->controller->request->query['columns'];

        foreach ($orders as $order) {
            $orderBy = $columns[$order['column']]['data'] . ' ' . $order['dir'] . ', ';
        }

        //letzes Komma weg
        if (!empty($orderBy)) {
            return substr($orderBy, 0, -2);
        }

        return $orderBy;
    }

    /**
     * returns sql conditions array after converting dataTables GET request into Cake style conditions
     * will only search on fields with bSearchable set to true (which is the default value for bSearchable)
     *
     * @param void
     * @return array
     */
    private function getWhereConditions()
    {
        $search = $this->controller->request->query['search']['value'];
        $columns = $this->controller->request->query['columns'];

        $conditions = [];

        if (!empty($search)) {
            //OR
            //  $conditions['OR'][] = $column['data'] . ' LIKE "%' . $search . '%"';
        }

        foreach ($columns as $column) {

            //OMG!
            if ($column['searchable'] == 'true' && !empty($column['data'])) {
                if ($column['search']['value']) {

                    $filter = [];
                    parse_str($column['search']['value'], $filter);

                    $type = empty($filter['type']) ? 'string' : $filter['type'];

                    unset($filter['type']);

                    switch ($type) {
                        case 'string' :
                            foreach ($filter as $f => $v) {
                                if ($v != "") {
                                    switch ($f) {
                                        case 'start' :
                                            $conditions[] = $column['data'] . " LIKE '" . $v . "%'";
                                            Break;
                                        case 'like' :
                                            $conditions[] = $column['data'] . " LIKE '%" . $v . "%'";
                                            Break;
                                        case 'eq' :
                                            $conditions[] = $column['data'] . " = '" . ($v) . "'";
                                            Break;
                                    }
                                }
                            }
                            Break;

                        case 'list' :

                            foreach ($filter as $f => $v) {
                                if ($v != "") {
                                    switch ($f) {
                                        case 'eq' :
                                            $conditions[] = $column['data'] . " = " . ($v);
                                            Break;
                                    }
                                }
                            }

                            Break;
                        case 'boolean' :

                            foreach ($filter as $f => $v) {
                                if ($v != "") {
                                    switch ($f) {
                                        case 'eq' :
                                            $conditions[] = $column['data'] . " = " . ($v);
                                            Break;
                                    }
                                }
                            }
                        case 'numeric' :
                            foreach ($filter as $f => $v) {
                                if ($v != "") {
                                    switch ($f) {
                                        case 'ne' :
                                            $conditions[] = $column['data'] . " != " . $v;
                                            Break;
                                        case 'eq' :
                                            $conditions[] = $column['data'] . " = " . $v;
                                            Break;
                                        case 'lt' :
                                            $conditions[] = $column['data'] . " < " . $v;
                                            Break;
                                        case 'gt' :
                                            $conditions[] = $column['data'] . " > " . $v;
                                            Break;
                                    }
                                }
                            }
                            Break;
                        case 'date' :
                            foreach ($filter as $f => $v) {
                                if ($v != "") {
                                    switch ($f) {
                                        case 'ne' :
                                            $conditions[] = "DATE(" . $column['data'] . ") != '" . date('Y-m-d',
                                                    strtotime($v)) . "'";
                                            Break;
                                        case 'eq' :
                                            $conditions[] = "DATE(" . $column['data'] . ") = '" . date('Y-m-d',
                                                    strtotime($v)) . "'";
                                            Break;
                                        case 'lt' :
                                            $conditions[] = "DATE(" . $column['data'] . ") < '" . date('Y-m-d',
                                                    strtotime($v)) . "'";
                                            Break;
                                        case 'gt' :
                                            $conditions[] = "DATE(" . $column['data'] . ") > '" . date('Y-m-d',
                                                    strtotime($v)) . "'";
                                            Break;
                                    }
                                }
                            }
                            Break;
                    }

                    //$conditions[] = $column['data'] . ' LIKE "%' . $column['search']['value'] . '%"';
                }
            }
        }


        return $conditions;
    }

    private function getLengthOfWhereConditions()
    {
        $search = $this->controller->request->query['search']['value'];
        $columns = $this->controller->request->query['columns'];
        $string = "";
        foreach ($columns as $column) {
            //OMG!
            if ($column['searchable'] == 'true' && !empty($column['data'])) {
                if (!empty($search)) {
                    $string = $string . $search;
                }

                if ($column['search']['value']) {
                    $string = $string . $column['search']['value'];
                }
            }
        }

        $string = str_replace("%", "", $string);

        return strlen($string);
    }

    /**
     * looks through the models validate array to determine to create conditions based on datatype, returns condition array.
     * to enable this set $this->DataTable->conditionsByValidate = 1.
     *
     * @param string $field
     * @return array
     */
    private function conditionByDataType($field)
    {
        foreach ($this->model->validate[$field] as $rule => $j) {
            switch ($rule) {
                case 'boolean':
                case 'numeric':
                case 'naturalNumber':
                    $condition = [$field => $this->controller->request->query['search']['value']];
                    break;
            }
        }

        return $condition;
    }

}
