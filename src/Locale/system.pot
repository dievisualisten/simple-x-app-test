# LANGUAGE translation of CakePHP Application
# Copyright YEAR NAME <EMAIL@ADDRESS>
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2021-04-14 10:11+0200\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: Controller/SxAppController.php:115
#: Model/Table/SxPageconfigurationsTable.php:78
#: Model/Table/SxPageconfigurationsTable.php:96
#: Model/Table/SxPageconfigurationsTable.php:140
msgid "Es sind Fehler aufgetreten."
msgstr ""

#: Controller/SxAppController.php:121
msgid "Die Daten wurden gespeichert."
msgstr ""

#: Controller/SxAppController.php:127
#: Controller/SxAppController.php:133
msgid "Bitte beachten Sie die Hinweise."
msgstr ""

#: Controller/SxArticlesController.php:101
#: Controller/SxCrudController.php:273
msgid "Die Daten wurden erfolgreich gelöscht."
msgstr ""

#: Controller/SxArticlesController.php:104
#: Controller/SxCrudController.php:276
msgid "Die Daten konnten nicht gelöscht werden."
msgstr ""

#: Controller/SxAttachmentsController.php:182
msgid "Bitte wählen Sie einen Order aus."
msgstr ""

#: Controller/SxCrontasksController.php:20
msgid "Alle aktuellen Crontasks wurden ausgeführt."
msgstr ""

#: Controller/SxCrudController.php:221
#: Controller/SxMenusController.php:81
#: Controller/SxPageconfigurationsController.php:62
msgid "Die Daten konnten nicht gespeichert werden, da die Validerung fehlgeschlagen ist"
msgstr ""

#: Controller/SxCrudController.php:350
msgid "Die Daten wurden erfolgreich kopiert."
msgstr ""

#: Controller/SxCrudController.php:353
msgid "Die Daten konnten nicht kopiert werden."
msgstr ""

#: Controller/SxNewslettersController.php:106
msgid "Den gewählten Artikel gibt es nicht auf der gewählten Sprache."
msgstr ""

#: Controller/SxNewslettersController.php:127
msgid "Der Test-Newsletter wurde versandt."
msgstr ""

#: Controller/SxNewslettersController.php:129
msgid "Der Newsletter konnte nicht versandt werden."
msgstr ""

#: Controller/SxPageconfigurationsController.php:86
msgid "Alle Konfigurationen für Seitentyp wurden erstellt."
msgstr ""

#: Controller/SxTextsController.php:79
msgid "Die Texte wurden neu generiert."
msgstr ""

#: Model/Table/SxAcosTable.php:82
msgid "Bitte geben Sie einen Namen ein."
msgstr ""

#: Model/Table/SxAcosTable.php:85
msgid "Bitte wählen Sie einen übergeordneten Eintrag aus."
msgstr ""

#: Model/Table/SxAttachmentsTable.php:572
msgid "Der Hauptordner kann nicht gespeichert werden,"
msgstr ""

#: Model/Table/SxAttachmentsTable.php:603
msgid "Die Unterordner konnten nicht gespeichert werden,"
msgstr ""

#: Templates/element/system/systembar.php:38
msgid "Dashboard"
msgstr ""

#: Templates/element/system/systembar.php:45
msgid "Article"
msgstr ""

#: Templates/element/system/systembar.php:52
msgid "Formular"
msgstr ""

#: Templates/element/system/systembar.php:61
msgid "Medien"
msgstr ""

#: Templates/element/system/systembar.php:68
msgid "Konfiguration"
msgstr ""

#: Utility/SxVimeoUtility.php:34
msgid "Vimeo ID konnte nicht ausgelesen werden."
msgstr ""

#: Utility/SxVimeoUtility.php:48
msgid "Vimeo API nicht erreichbar oder Video nicht existent."
msgstr ""

#: Utility/SxVimeoUtility.php:60
msgid "Vimeo Video nicht gefunden."
msgstr ""

#: Utility/SxYoutubeUtility.php:46
msgid "Youtube ID konnte nicht ausgelesen werden."
msgstr ""

#: Utility/SxYoutubeUtility.php:60
msgid "Youtube API nicht erreichbar oder Video nicht existent."
msgstr ""

#: Utility/SxYoutubeUtility.php:72
msgid "Youtube Video nicht gefunden."
msgstr ""

